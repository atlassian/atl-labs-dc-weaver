# atl-labs-dc-weaver

Atlassian Labs Data Center Weaver is a tool which enables the creation and management of Jira and Confluence Data Center Cloudformation stacks of Atlassian products by users without physical access to the underlying AWS services.

We (the IT Platform Engineering team at Atlassian) built this tool internally to unlock product teams to allow them to manage their own instances of Confluence, Jira, and Crowd without having access to AWS itself (which is managed by our team).

This README outlines how to deploy Weaver to AWS and how to run locally for testing and development. For an overview of Weaver itself, including a list of actions possible through Weaver, see [the public announcement](https://community.atlassian.com/t5/Data-Center-articles/Introducing-Atlassian-CloudFormation-Forge/ba-p/881551).


**NOTE**:

Weaver is an unsupported application developed by Atlassian for creation and management of CloudFormation stacks for our products.  Atlassian takes no responsibility for your use of Weaver. The apps are often developed in conjunction with Atlassian hack-a-thons and ShipIt days and are provided to you completely “as-is”, with all bugs and errors, and are not supported by Atlassian. Unless licensed otherwise by Atlassian, Weaver is subject to Section 14 of the Cloud Terms of Service – https://www.atlassian.com/legal/cloud-terms-of-service – or Section 8 of the Software License Agreement – https://www.atlassian.com/legal/software-license-agreement, as applicable, and are "No-Charge Products" as defined therein.

## Deploying to AWS

We've provided a CloudFormation template for deploying Weaver to your AWS account. To deploy Weaver using the template, you'll either need to create a dedicated IAM role for Weaver, or just make sure the user deploying the template has an appropriate level of permissions (e.g., the "AdministratorAccess" managed policy). The template will spin up an instance of Weaver via an EC2 node in a VPC of your choosing, configured via the parameters below.

### Allowing Weaver access to your AWS resources

#### IAM Policy

The CloudFormation template for Weaver includes an IAM profile and role that will allow Weaver access to the AWS actions it needs to function. This includes everything from creating, editing, and deleting CloudFormation stacks to controlling auto-scaling for deployed EC2 nodes.

#### Status information from application nodes

Once Weaver is deployed, you'll need to modify the Security Group Ingress rules for any stack that you want Weaver to be able to access for things like service status checks. To do this, configure a new Security Group Ingress rule that allows traffic on whichever port you have configured for Tomcat (typically 8080) and restrict it to the IP address assigned to your Weaver node.

### CloudFormation Parameters

A quick overview of the parameters for Weaver's CloudFormation template. We provide sensible defaults where appropriate.

* **Analytics**
  A boolean value that determines whether or not you allow analytics on usage data to be sent back to Atlassian via Google Analytics.

* **CidrBlock**
  The CIDR IP range that is permitted to access Weaver.

* **FlaskSecretKey**
  The secret key passed to Flask to enable sessions ([more info][1])

* **HostedZone**
  The domain name of the Route53 Hosted Zone in which to create CNAME records

* **InternetAccessible**
  Whether or not the Elastic Load Balancer associated with Weaver is configured to be publicly-accessible

* **KeyPairName**
  The name of an existing EC2 KeyPair to enable SSH access to the instance

* **NodeInstanceType**
  The instance type for Weaver's EC2 node(s)

* **NodeVolumeSize**
  The size of the EBS volume for Weaver's EC2 node(s)

* **Nodes**
  The number of Weaver nodes to deploy. Note that currently, Weaver does not support auto-scaling or running on multiple nodes simultaneously; this is primarily for facilitating easy node replacement (spin down to 0 and then back up to 1).

* **Regions**
  A list of regions that Weaver should allow access to, defined in a comma-delimited list in the format `aws_region: region_name`, where aws_region is an AWS region (e.g. `us-east-1`) and region_name is a display value (e.g. `US East 1` or `Staging`, etc.). The first listed region will be the default.

* **SamlMetadataUrl**
  The metadata URL for your SAML auth provider. If you don't provide a metadata URL, SAML will not be enabled.

* **Subnet**
  The subnet in your VPC that Weaver will be deployed to.

* **VPC**
  The VPC that Weaver will be deployed to.
