#!/usr/bin/env python3

from __future__ import annotations

import logging
import re

from os import getenv

import boto3
import botocore

from flask import request
from flask.views import MethodView
from packaging import version

from weaver.utils import chunk, create_toast_alert

## AWS resources that are not stack or cfn/k8s specific
#

log = logging.getLogger('app_log')


class GetVpcs(MethodView):
    def get(self, region):
        ec2 = boto3.client('ec2', region_name=region)
        try:
            vpc_list = ec2.describe_vpcs()
        except botocore.exceptions.ClientError:
            log.exception('Error occurred getting VPCs')
            return
        vpcs = []
        for vpc in vpc_list['Vpcs']:
            vpc_name = next((tag['Value'] for tag in vpc.get('Tags', []) if tag['Key'] == 'Name'), None)
            label = f'{vpc_name} ({vpc["VpcId"]})' if vpc_name else vpc["VpcId"]
            vpcs.append({'label': label, 'value': vpc['VpcId']})
        vpcs = sorted(vpcs, key=lambda vpc: vpc['label'].casefold())
        return vpcs


class GetSubnetsForVpc(MethodView):
    def get(self, region, vpc):
        ec2 = boto3.client('ec2', region_name=region)
        try:
            subnets_list = ec2.describe_subnets(Filters=[{'Name': 'vpc-id', 'Values': [vpc]}])
        except botocore.exceptions.ClientError:
            log.exception('Error occurred getting subnets')
            return
        subnets = []
        for subnet in subnets_list['Subnets']:
            subnet_name = next((tag['Value'] for tag in subnet.get('Tags', []) if tag['Key'] == 'Name'), None)
            label = f'{subnet_name} ({subnet["AvailabilityZone"]}) ({subnet["SubnetId"]})' if subnet_name else f'{subnet["AvailabilityZone"]} ({subnet["SubnetId"]})'
            subnets.append({'label': label, 'value': subnet['SubnetId']})
        subnets = sorted(subnets, key=lambda subnet: subnet['label'].casefold())
        return subnets


class GetAllSubnetsForRegion(MethodView):
    def get(self, region):
        ec2 = boto3.client('ec2', region_name=region)
        try:
            subnets = ec2.describe_subnets()
        except botocore.exceptions.ClientError:
            log.exception('Error occurred getting subnets')
            return
        subnet_ids = []
        for subnet in subnets['Subnets']:
            subnet_ids.append(subnet['SubnetId'])
        return subnet_ids


class GetEc2InstanceTypes(MethodView):
    def get(self, region):
        ec2_instance_types = []
        client = boto3.client('ec2', region_name=region)
        paginator = client.get_paginator('describe_instance_types')
        response_iterator = paginator.paginate(
            Filters=[
                # filter out arm64 for now until we have better support for mixed-architecture clusters
                # (right now we use same LatestAmiId SSM param value for all instances (Cluster, Bot, NFS, and Synchrony)), but AMIs are architecture-specific
                {'Name': 'processor-info.supported-architecture', 'Values': ['i386', 'x86_64']},
                # filter out older paravirtual instances (though some support both hvm and paravirtual, and those will still be available)
                {'Name': 'supported-virtualization-type', 'Values': ['hvm']},
            ]
        )
        for batch in response_iterator:
            for instance in batch['InstanceTypes']:
                # filter out nano and micro instances (this isn't easily filterable above)
                if instance['InstanceType'].split('.')[-1] not in {'nano', 'micro'}:
                    ec2_instance_types.append(parse_instance_data(instance))
        ec2_instance_types = sorted(ec2_instance_types, key=get_instance_sorting_keys)
        return [
            {
                'label': f"{i['name']} ({i['count_vcpus']}x {i['cpu_speed']}GHz {i['arch']} vCPUs, {i['ram']}GB RAM, {i['root_device']}, {i['network']} networking, {i['hypervisor']} hypervisor)",
                'value': i['name'],
            }
            for i in ec2_instance_types
        ]


class GetRdsInstanceTypes(MethodView):
    def get(self, region, engine, engine_version):
        rds_instance_classes = []
        ec2_instance_types = []
        rds_client = boto3.client('rds', region_name=region)
        ec2_client = boto3.client('ec2', region_name=region)

        supplied_db_engine_version = version.Version(engine_version)

        # if we ever add support for other engines, we'll need to extend this block
        # for postgres, 9.6 is deprecated, so we can safely assume the engine version format of MAJOR.MINOR
        if engine == 'postgres' and len(supplied_db_engine_version.release) > 1:
            # if the user supplied a minor version, we'll just use the supplied version directly
            target_minor_engine_version = engine_version
        else:
            # otherwise, find the "default" (i.e., most recent) minor version for the major version specified. sometimes the available instance types can differ between minor
            # versions (typically the addition of new instance types, not the deprecation of existing instance types), but if you're creating a new stack you'll be using the
            # default minor version anyway, and if you're updating an existing stack to change the instance type, RDS is going to do an upgrade to the latest default minor
            # version anyways. this is only necessary because the `describe_orderable_db_instance_options` method used below requires full versions and doesn't accept
            # wildcards, and requests without scoping to an engine version take too long (2+ mins)
            try:
                target_minor_engine_version = rds_client.describe_db_engine_versions(Engine=engine, EngineVersion=engine_version, DefaultOnly=True)['DBEngineVersions'][0][
                    'EngineVersion'
                ]
            except botocore.exceptions.ClientError as e:
                log.exception(
                    f"Error occurred retrieving latest minor version for {engine} DB engine version {engine_version}: {e.response['Error']['Code']} - {e.response['Error']['Message']}"
                )
                return []

        # get a unique list (set) of all DB instance classes which can be created with the supplied version
        rds_paginator = rds_client.get_paginator('describe_orderable_db_instance_options')
        rds_response_iterator = rds_paginator.paginate(Engine=engine, EngineVersion=target_minor_engine_version)
        try:
            for db_batch in rds_response_iterator:
                for orderable_option in db_batch['OrderableDBInstanceOptions']:
                    rds_instance_classes.append(orderable_option['DBInstanceClass'])
        except botocore.exceptions.ClientError as e:
            log.exception(
                f"Error occurred retrieving instance classes for {engine} DB engine version {engine_version}: {e.response['Error']['Code']} - {e.response['Error']['Message']}"
            )
            return []
        else:
            rds_instance_classes = list(set(rds_instance_classes))

        # don't proceed to lookup the EC2 instance data if, for whatever reason, there are no rds_instance_classes available
        # (if the InstanceTypes filter used below receives an empty array, it'll just return _all_ EC2 instance types)
        if not rds_instance_classes:
            log.info(f'No available RDS instance classes for {engine} version {target_minor_engine_version}')
            return []

        # get the EC2 instance info for the available RDS instance classes
        ec2_paginator = ec2_client.get_paginator('describe_instance_types')
        # drop the "db." prefix and exclude the x2g instance classes – they have no EC2 equivalent (they are RDS-only) and so our lookups for EC2 instance data below will fail
        ec2_instance_types_to_query = [instance.removeprefix("db.") for instance in rds_instance_classes if ".x2g." not in instance]

        # we can only query 100 instance types at a time; there might be more RDS instance types than that
        for instance_types in chunk(ec2_instance_types_to_query, 100):
            ec2_response_iterator = ec2_paginator.paginate(InstanceTypes=instance_types)
            for instance_batch in ec2_response_iterator:
                for instance in instance_batch['InstanceTypes']:
                    ec2_instance_types.append(parse_instance_data(instance))
        ec2_instance_types = sorted(ec2_instance_types, key=get_instance_sorting_keys)
        return [
            {
                'label': f"db.{i['name']} ({i['count_vcpus']}x {i['cpu_speed']}GHz {i['arch']} vCPUs, {i['ram']}GB RAM, {i['root_device']}, {i['network']} networking, {i['hypervisor']} hypervisor",
                'value': f"db.{i['name']}",
            }
            for i in ec2_instance_types
        ]


def parse_instance_data(instance):
    cpu_speed_with_decimal = str(instance['ProcessorInfo'].get('SustainedClockSpeedInGhz', 'Unknown '))
    ram_with_decimal = str(round((instance['MemoryInfo']['SizeInMiB'] / 1024), 2))
    return {
        'name': instance['InstanceType'],
        'count_vcpus': instance['VCpuInfo']['DefaultVCpus'],
        'cpu_speed': cpu_speed_with_decimal.rstrip('0').rstrip('.') if '.' in cpu_speed_with_decimal else cpu_speed_with_decimal,
        'arch': ' or '.join(instance['ProcessorInfo']['SupportedArchitectures']),
        'ram': ram_with_decimal.rstrip('0').rstrip('.') if '.' in ram_with_decimal else ram_with_decimal,
        'root_device': (
            'EBS-only'
            if instance['InstanceStorageSupported'] is False
            else f"{instance['InstanceStorageInfo']['Disks'][0]['Count']}x {instance['InstanceStorageInfo']['Disks'][0]['SizeInGB']}GB {'NVMe ' if instance['InstanceStorageInfo']['NvmeSupport'] in ['supported', 'required'] else ''}{instance['InstanceStorageInfo']['Disks'][0]['Type'].upper()}"
        ),
        'network': instance['NetworkInfo']['NetworkPerformance'][0].lower() + instance['NetworkInfo']['NetworkPerformance'][1:],
        'hypervisor': f"{instance['Hypervisor'].capitalize()}" if instance['BareMetal'] is False else 'Bare-metal',
    }


def get_instance_sorting_keys(instance_type):
    """
    Allows for "proper" sorting (via `sorted` method) of a list of instance types in dict format with a "name" key; returns a tuple of:
     * the instance generation, converted to a list of strings and ints
     * the instance size referenced against a static mapping of sizes (instance_size_order)
     * the instance size converted to a list of strings and ints
    """
    instance_generation = instance_type['name'].removeprefix("db.").split('.')[0]  # "c5"
    instance_size = instance_type['name'].split('.', 1)[1]  # "9xlarge"
    instance_size_order = {'nano': 0, 'micro': 1, 'small': 2, 'medium': 3, 'large': 4, 'xlarge': 5, 'metal': 100}
    alphanum_list = lambda text: [int(chunk) if chunk.isdigit() else chunk.lower() for chunk in re.split('([0-9]+)', text)]  # noqa E731
    return alphanum_list(instance_generation), instance_size_order.get(instance_size, 99), alphanum_list(instance_size)


class GetDbVersionCompatibilityForSnapshot(MethodView):
    def get(self):
        """
        Returns a pre-formatted toast alert object (i.e., for passing directly to AJS.flag) with compatibility information for a given snapshot and RDS major or minor version.
        Required request args:
          * region
          * dbEngine
          * dbEngineVersion
          * snapshotEngineVersion
        """
        rds_client = boto3.client('rds', region_name=request.args['region'])
        db_engine_major_version: str

        # determine/set db_engine_major_version to "9.6" or "10", "11", etc.
        supplied_db_engine_version: version.Version = version.parse(request.args['dbEngineVersion'])
        try:
            if request.args['dbEngine'] == 'postgres' and supplied_db_engine_version.major > 9:
                db_engine_major_version = str(supplied_db_engine_version.major)
            else:
                db_engine_major_version = f'{supplied_db_engine_version.major}.{supplied_db_engine_version.minor}'
        except AttributeError:
            return create_toast_alert(
                type='warning',
                title='Invalid DB Engine Major Version',
                body=(
                    'Unable to parse DB engine version for evaluation of snapshot engine version compatibilty (is "DB Engine Major Version"'
                    f' set to a valid RDS engine version for {request.args["dbEngine"]}?).'
                ),
            )

        # get RDS info for the major version; return a warning if RDS balks at the version supplied
        try:
            db_engine_default_minor_version_data = rds_client.describe_db_engine_versions(Engine=request.args['dbEngine'], EngineVersion=db_engine_major_version, DefaultOnly=True)[
                'DBEngineVersions'
            ][0]
        except (botocore.exceptions.ClientError, botocore.parsers.ResponseParserError) as e:
            if (hasattr(e, 'response') and e.response['Error']['Code'] == 'InvalidParameterCombination') or 'InvalidParameterCombination' in e.args[0]:
                return create_toast_alert(
                    type='warning',
                    title='Invalid DB Engine Major Version',
                    body=(
                        f'Unable to query AWS for engine version "{supplied_db_engine_version}" for evaluation of snapshot engine version compatibilty'
                        f' (is "DB Engine Major Version" set to a valid RDS engine version for {request.args["dbEngine"]}?).'
                    ),
                )
            else:
                raise e

        # get version info for the snapshot
        snapshot_db_engine_version_data = rds_client.describe_db_engine_versions(Engine=request.args['dbEngine'], EngineVersion=request.args['snapshotEngineVersion'])[
            'DBEngineVersions'
        ][0]
        valid_upgrade_targets: list[str] = [target['EngineVersion'] for target in snapshot_db_engine_version_data['ValidUpgradeTarget']]

        # some setup for version comparisons
        parsed_snapshot_engine_version: version.Version = version.parse(request.args['snapshotEngineVersion'])
        parsed_db_engine_major_version: version.Version = version.parse(db_engine_major_version)
        parsed_db_engine_default_minor_version: version.Version = version.parse(db_engine_default_minor_version_data['EngineVersion'])

        # if the user supplied a full version number (e.g., 9.6.22, 11.5, etc.), determine if that exact version is an upgradeable target for the snapshot
        # comparisions for non-exact version numbers (where the user supplied just the major version) are below/ouside this conditional block
        if len(supplied_db_engine_version.release) != len(parsed_db_engine_major_version.release):
            if supplied_db_engine_version == parsed_snapshot_engine_version:
                return create_toast_alert(
                    type='success',
                    title='Compatible snapshot version',
                    body=f'DB Engine Major Version and DB snapshot engine version are compatible (exact match: {parsed_snapshot_engine_version}).',
                    close='auto',
                )
            elif any(target for target in valid_upgrade_targets if version.parse(target) == supplied_db_engine_version):
                return create_toast_alert(
                    type='info',
                    title='Database will be upgraded',
                    body=f'Snapshot on version {parsed_snapshot_engine_version} can and will be upgraded to {supplied_db_engine_version} by Cloudformation/RDS on restore.',
                )
            else:
                action = 'downgraded' if parsed_snapshot_engine_version > supplied_db_engine_version else 'upgraded'
                return create_toast_alert(
                    type='error',
                    title=f'Database cannot be {action}',
                    body=[
                        f'Snapshot on version {parsed_snapshot_engine_version} cannot be {action} to {supplied_db_engine_version}.',
                        f'Valid targets: {", ".join(valid_upgrade_targets)}',
                    ],
                )

        # if the snapshot version and default minor version are the same, we're good to go
        if parsed_snapshot_engine_version == parsed_db_engine_default_minor_version:
            return create_toast_alert(
                type='success',
                title='Compatible snapshot version',
                body=f'DB Engine Major Version and DB snapshot engine version are compatible (exact match: {parsed_db_engine_default_minor_version}).',
                close='auto',
            )

        # we can't use a snapshot with a newer RDS version than whatever minor version RDS will use by default
        if parsed_snapshot_engine_version > parsed_db_engine_default_minor_version:
            return create_toast_alert(
                type='error',
                title='Database cannot be downgraded',
                body=(
                    f'The current default minor version for {request.args["dbEngine"]} {db_engine_major_version} is {parsed_db_engine_default_minor_version};'
                    f' the selected recovery snapshost is from a newer version ({parsed_snapshot_engine_version}). You may need to specify the full/minor version'
                    ' for the "DB Engine Major Version" field.'
                ),
                close='manual',
            )

        # we can maybe use a snapshot with an older RDS version if the default minor version of the major version requested by the user is listed as one of the upgrade targets
        if parsed_snapshot_engine_version < parsed_db_engine_default_minor_version:
            if any(target for target in valid_upgrade_targets if version.parse(target) == parsed_db_engine_default_minor_version):
                return create_toast_alert(
                    type='info',
                    title='Database will be upgraded',
                    body=(
                        f'The current default minor version for {request.args["dbEngine"]} {db_engine_major_version} is {parsed_db_engine_default_minor_version};'
                        f' the selected recovery snapshost is from an older version ({parsed_snapshot_engine_version}) and will be upgraded'
                        ' by Cloudformation/RDS on restore.'
                    ),
                    close='manual',
                )
            else:
                return create_toast_alert(
                    type='error',
                    title='Database cannot be upgraded',
                    body=[
                        (
                            f'No valid upgrade target from {parsed_snapshot_engine_version} could be found for DB Engine Major Version {db_engine_major_version};'
                            f' the current default minor version for {request.args["dbEngine"]} {db_engine_major_version} is {parsed_db_engine_default_minor_version}.'
                        ),
                        f'Valid targets: {", ".join(valid_upgrade_targets)}',
                    ],
                    close='manual',
                )


class GetKmsKeys(MethodView):
    def get(self, region):
        keys = []
        account_id = boto3.client('sts').get_caller_identity().get('Account')
        client = boto3.client('kms', region)
        paginator = client.get_paginator('list_aliases')
        response_iterator = paginator.paginate()
        for kms_keys_aliases in response_iterator:
            for key in kms_keys_aliases['Aliases']:
                if key.get('TargetKeyId') and not key['AliasName'].startswith('alias/aws'):
                    label = f'{key["AliasName"].replace("alias/", "")} ({key["TargetKeyId"]})'
                    keys.append({'label': label, 'value': f'arn:aws:kms:{region}:{account_id}:key/{key["TargetKeyId"]}'})
        return keys


class GetSslCerts(MethodView):
    def get(self, region):
        ssl_certs = []
        client = boto3.client('acm', region)
        paginator = client.get_paginator('list_certificates')
        response_iterator = paginator.paginate()
        for ssl_certs_aliases in response_iterator:
            for cert in ssl_certs_aliases['CertificateSummaryList']:
                cert_id = cert['CertificateArn'].split('/')[-1]
                in_use_str = 'in use' if cert['InUse'] else 'not used'
                ssl_certs.append({'label': f"{cert['DomainName']} ({cert_id} — {cert['Status'].lower()}; {in_use_str})", 'value': cert['CertificateArn']})
        ssl_certs = sorted(ssl_certs, key=lambda x: x['label'])
        return ssl_certs


def get_available_regions() -> list[str]:
    available_regions: list[str] = []
    try:
        ec2 = boto3.client("ec2", region_name=getenv("REGION", "us-east-1"))  # fallback to us-east-1 since we're just retrieving the list of regions
        available_regions = [region["RegionName"] for region in ec2.describe_regions()["Regions"]]
    except Exception:
        log.exception("Unable to load regions from AWS")
    return available_regions
