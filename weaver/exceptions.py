## Custom Exceptions
#


class MissingConfiguration(Exception):
    pass


class SsmExecutionError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.return_code = kwargs.get("return_code")


class StackActionInProgressException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.action_name = kwargs.get('action_name')


class StackDoesNotExistException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.stack_name = kwargs.get('stack_name')


class StackChangeSetNotFoundException(Exception):
    pass


class StackDeregistrationDelaysNotFullValueException(Exception):
    pass


class StackEnvironmentInaccessible(Exception):
    pass


class StackMissingTag(Exception):
    pass


class StackParameterNotFound(Exception):
    pass


class StackUsingUnsupportedMethodException(Exception):
    pass


class StackParamaterNotFound(Exception):
    pass
