#!/usr/bin/env python3

from __future__ import annotations

import itertools
import json

from enum import Enum
from typing import TypedDict
from uuid import uuid4

import boto3
import botocore
import requests

from flask import current_app, request
from inflection import underscore
from marshmallow import Schema, fields, pre_dump, post_load, validate, validates, validates_schema, ValidationError
from pathlib import Path
from sqlalchemy.sql import func
from requests.auth import HTTPBasicAuth
from ruamel import yaml
from urllib.parse import urlparse

from weaver.extensions import db, ma
import weaver.enums as weaver_enums

DOWNLOAD_ATLASSIAN_SECRET_NAME = "download_atlassian"  # nosec B105


class ProductEnum(Enum):
    bitbucket = "Bitbucket"
    bitbucket_mesh = "Bitbucket Mesh"
    bitbucket_mirror = "Bitbucket Mirror"
    confluence = "Confluence"
    crowd = "Crowd"
    jira = "Jira"


#
# Custom Types
#
class CompatibilityDict(TypedDict, total=False):
    incompatible_with: list[str]
    requires: list[str]


#
# Database Models
#


class StackAction(db.Model):  # type: ignore[name-defined]
    id = db.Column(db.String(), primary_key=True, default=lambda: str(uuid4()))
    started_at = db.Column(db.DateTime(), default=func.now())
    updated_at = db.Column(db.DateTime(), default=func.now(), onupdate=func.now())
    stack_name = db.Column(db.String())
    region = db.Column(db.String())
    action = db.Column(db.String())
    user = db.Column(db.String())
    status = db.Column(db.Enum("IN_PROGRESS", "COMPLETE", name="stack_action_statuses"))
    result = db.Column(db.Enum("SUCCESS", "FAILURE", name="stack_action_results"))
    log_file = db.Column(db.String())
    change_log_file = db.Column(db.String())


#
# Custom Marshmallow Fields
#


# Takes a list of comma separated strings and breaks it up into a list of strings
# This can handle query parameters with multiple values as a comma delimited string and/or separate key-value pairs
# e.g.     query_param=a,b
#       or query_param=a&query_param=b
#       or even query_param=a,b&query_param=c,d,e
# Adapted from ref: https://stackoverflow.com/a/63350552
class DelimitedListField(fields.List):
    def __init__(self, cls_or_instance, **kwargs):
        super().__init__(cls_or_instance, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        delimited_list = []
        for list_elem in value:
            if type(list_elem) is not str:
                raise ValidationError(f"{attr} is not a delimited list, it has non-string value {list_elem}")
            delimited_list.extend(list_elem.split(','))
        return super()._deserialize(delimited_list, attr, data, **kwargs)


#
# Helper Functions
#


def get_incompatible_field_error_msg(incompatible_field: str, field_name: str, field_value: str) -> str:
    return f"Unsupported request: providing '{incompatible_field}' is not compatible with {field_name} '{field_value}'"


def get_missing_required_field_error_msg(required_field: str, field_name: str, field_value: str) -> str:
    return f"Unsupported request: '{required_field}' is required for {field_name} '{field_value}'"


# A helper for Marshmallow schema validation. For a given a field with name field_name, you can list out any
# incompatibilties or dependencies on other fields in the compatibility_dict and this will
# raise a ValidationError with a standardised message if any are violated.
# If the value of the field we are checking is not part of the compatibility_dict, then
# assume there are no incompatibilities or dependencies to check.
# If the target field is an Enum, set print_enums_as_value to True / False to match if the field is parsed by enum value or name
def raise_compatibility_errors(data, field_name: str, compatibility_dict: CompatibilityDict, print_enums_as_value: bool = False):
    field_value = data.get(field_name)
    if field_value not in compatibility_dict:
        # Assume there are no incompatibilities or dependencies to check for this field value.
        return
    # Figure out format of the field value we want displayed in the error message (should be what the user inputs to improve user-friendliness)
    formatted_field_value = field_value
    if isinstance(field_value, Enum):
        formatted_field_value = field_value.value if print_enums_as_value else field_value.name
    incompatible_fields = compatibility_dict.get(field_value).get('incompatible_with', [])
    if incompatible_fields:
        for incompatible_field in incompatible_fields:
            if data.get(incompatible_field):
                raise ValidationError(get_incompatible_field_error_msg(incompatible_field, field_name, formatted_field_value), field_name=field_name)
    required_fields = compatibility_dict.get(field_value).get('requires', [])
    if required_fields:
        for required_field in required_fields:
            if not data.get(required_field):
                raise ValidationError(get_missing_required_field_error_msg(required_field, field_name, formatted_field_value), field_name=field_name)


#
# Marshmallow Schemas
#

# Nested schemas


class StackParamsSchema(Schema):
    ParameterKey = fields.String(required=True, data_key="parameter_key")
    ParameterValue = fields.String(required=True, data_key="parameter_value")


class TemplateSchema(Schema):
    repo = fields.String(required=True)
    name = fields.String(required=True)

    @validates_schema
    def check_template_exists(self, data, **kwargs):
        if "atlassian-aws-deployment" in data["repo"]:
            template_folder = Path("atlassian-aws-deployment/templates")
        else:
            template_folder = Path() / "custom-templates" / data["repo"]
        try:
            next(template_folder.glob(f"**/{data['name']}"))
        except StopIteration:
            raise ValidationError("Template not found")


class UserAuthSchema(Schema):
    personal_access_token = fields.String(required=True)


# Request/Response schemas


class StackActionsResponseSchema(ma.SQLAlchemySchema):  # type: ignore[name-defined]
    class Meta:
        model = StackAction
        ordered = True

    id = ma.auto_field()  # type: ignore
    action = ma.auto_field()  # type: ignore
    user = ma.auto_field()  # type: ignore
    started_at = ma.auto_field()  # type: ignore
    status = ma.auto_field()  # type: ignore
    result = ma.auto_field()  # type: ignore


class StackActionResponseSchema(StackActionsResponseSchema):
    log_lines = fields.List(fields.String())


class StackBaseRequestBodySchema(Schema):
    template = fields.Nested(TemplateSchema, required=True)
    params = fields.Nested(StackParamsSchema, many=True, load_default=[])
    tags = fields.Mapping(keys=fields.String(required=True), values=fields.String(required=True), load_default={})  # ensure empty list on every reqeust if no tags are provided

    @validates_schema
    def check_no_unsupported_params(self, data, **kwargs):
        if not data["template"]:
            # the schema will take care of whether the template should be required or not; this validator is used for
            # PUT on stack and changeset though, and changesets don't require the template – so only run this validator
            # if there's actually a template here
            return

        def general_constructor(loader, tag_suffix, node):
            return node.value

        template_folder = data["template"]["repo"]
        template_name = data["template"]["name"]
        if 'atlassian-aws-deployment' in data["template"]["repo"]:
            template_folder = Path('atlassian-aws-deployment/templates')
        else:
            template_folder = Path() / 'custom-templates' / template_folder
        template = list(template_folder.glob(f"**/{template_name}"))[0]
        yaml.add_multi_constructor('!', general_constructor, Loader=yaml.SafeLoader)
        template_param_keys = yaml.YAML(typ="safe", pure=True).load(open(template, 'r'))['Parameters']
        unsupported_params = [param for param in data["params"] if param["ParameterKey"] not in template_param_keys]
        if any(unsupported_params):
            raise ValidationError(f"Unsupported parameters for template {template_name}: {', '.join([p['ParameterKey'] for p in unsupported_params])}", field_name="params")


class StackChangeSetChangesSchema(Schema):
    class Meta:
        ordered = True

    Action = fields.String()
    LogicalResourceId = fields.String()
    PhysicalResourceId = fields.String()
    ResourceType = fields.String()
    Replacement = fields.String()
    Scope = fields.List(fields.String())

    def on_bind_field(self, field_name, field_obj):
        field_obj.data_key = underscore(field_name)


class StackChangeSetSchema(Schema):
    class Meta:
        ordered = True

    ChangeSetName = fields.String()
    Description = fields.String()
    ExecutionStatus = fields.String()
    Status = fields.String()
    StatusReason = fields.String()
    CreationTime = fields.DateTime()
    Changes = fields.Nested(StackChangeSetChangesSchema, many=True)

    @pre_dump
    def unnest_changes(self, data, **kwargs):
        if "ChangeSetName" not in data and "Id" in data:
            data["ChangeSetName"] = data.pop("Id").split("/")[1]
        if "Changes" in data:
            changes = data.pop("Changes")
            data["Changes"] = [c["ResourceChange"] for c in changes]
        return data

    def on_bind_field(self, field_name, field_obj):
        field_obj.data_key = underscore(field_name)


class StackChangesetPutRequestBodySchema(StackBaseRequestBodySchema):
    template = fields.Nested(TemplateSchema, load_default=None, required=False)


class StackPutRequestBodySchema(StackBaseRequestBodySchema):
    product = fields.Enum(ProductEnum, required=True)
    cloned_from = fields.String(load_default=None)


class StackDiagnosticsGetRequestQuerySchema(Schema):
    diagnostic_types = DelimitedListField(fields.Enum(weaver_enums.DiagnosticType, by_value=True), required=True)
    node_ip = fields.String(load_default=None)

    @validates("diagnostic_types")
    def no_duplicates(self, data, **kwargs):
        if len(set(data)) != len(data):
            raise ValidationError("Cannot contain duplicates")

    @validates_schema
    def index_health_requires_pat(self, data, **kwargs):
        if weaver_enums.DiagnosticType.INDEX_HEALTH in data.get("diagnostic_types") and not request.headers.get("stack-personal-access-token"):
            raise ValidationError(f"{weaver_enums.DiagnosticType.INDEX_HEALTH.value} in diagnostic_types requires a stack-personal-access-token in the HTTP header")


class StackDiagnosticsPostRequestBodySchema(Schema):
    diagnostic_type = fields.Enum(weaver_enums.PostMethodDiagnosticType, by_value=True, required=True)
    node_ip = fields.String(load_default=None)
    personal_access_token = fields.String(load_default=None)

    @validates_schema
    def cant_select_node_ip_for_support_zip(self, data, **kwargs):
        if data.get("diagnostic_type") == weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP and data.get("node_ip"):
            raise ValidationError(f"Cannot select node_ip for {weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP.value} diagnostic type")

    @validates_schema
    def support_zip_requires_pat(self, data, **kwargs):
        if data.get("diagnostic_type") == weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP and not data.get("personal_access_token"):
            raise ValidationError(f"{weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP.value} diagnostic type requires a personal_access_token")


class StackUpgradeRequestBodySchema(Schema):
    new_version = fields.String(required=True)
    alternate_download_url = fields.String(load_default='')
    zdu = fields.Boolean(load_default=False)
    zdu_type = fields.String(load_default=None)
    auth = fields.Nested(UserAuthSchema, load_default=None)
    drain_type = fields.Enum(weaver_enums.DrainType, load_default='DEFAULT')
    startup_primary = fields.Boolean(load_default=True)
    upgrade_type = fields.Enum(weaver_enums.UpgradeType, load_default='DEFAULT')

    @validates('alternate_download_url')
    def validate_alternate_download_url(self, data, **kwargs):
        if data:
            allowed_domains = current_app.config["ALTERNATE_DOWNLOAD_URL_ALLOWED_DOMAINS"]
            url_components = urlparse(data)
            # if there's no netloc or the scheme isn't http[s], this is probably not a valid URL
            if not url_components.netloc or url_components.scheme not in {'http', 'https'}:
                raise ValidationError(f"Unable to parse provided value as a URL: {data}")
            if url_components.netloc not in allowed_domains:
                raise ValidationError(f"The domain of the provided value ({data}) is not in the list of allowed domains: {', '.join(allowed_domains)}")
            r = requests.head(data, allow_redirects=True, timeout=5)
            # handle the explict case where the response was 401 with a retry using credentials stored in Secrets Manager
            # catch and re-raise a couple possible/known failure modes around retrieval of the secret with helpful error messaging
            if r.status_code == 401:
                secretsmanager = boto3.client("secretsmanager", request.args.get("region"))
                try:
                    download_atlassian = json.loads(secretsmanager.get_secret_value(SecretId=DOWNLOAD_ATLASSIAN_SECRET_NAME)["SecretString"])
                except botocore.exceptions.ClientError as e:
                    raise Exception(
                        f"Product download secret '{DOWNLOAD_ATLASSIAN_SECRET_NAME}' could not be retrieved: {e.response['Error']['Message'] if hasattr(e, 'response') else e}"
                    ) from e
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f"Product download secret '{DOWNLOAD_ATLASSIAN_SECRET_NAME}' could not be parsed as JSON") from e
                r = requests.head(data, allow_redirects=True, auth=HTTPBasicAuth(**download_atlassian), timeout=5)
            if not r.ok:
                raise ValidationError(f"Invalid URL ({data})? Response from server: {r.status_code} {r.reason}")

    @validates_schema
    def validate_zdu_params(self, data, **kwargs):
        errors = {}
        valid_zdu_type_values = ["inplace", "rebuild"]
        if data.get("zdu"):
            # auth and zdu_type are only required if zdu is true
            if not data.get("auth"):
                errors["auth"] = ["Missing data for required field."]
            zdu_type = data.get("zdu_type")
            if not zdu_type:
                errors["zdu_type"] = ["Missing data for required field."]
            if zdu_type not in valid_zdu_type_values:
                errors["zdu_type"] = [f"Invalid value; expected one of: {', '.join(valid_zdu_type_values)}"]
        if errors:
            raise ValidationError(errors)


class StackRestartRequestBodySchema(Schema):
    restart_type = fields.Enum(weaver_enums.RestartType, by_value=True, required=True)
    node_ip = fields.IPv4(load_default="")
    asg_name = fields.String(load_default=None, validate=validate.Length(min=1))
    drain_type = fields.Enum(weaver_enums.DrainType, by_value=True, load_default=None)
    thread_dumps = fields.Boolean(load_default=False)
    heap_dumps = fields.Boolean(load_default=False)

    # We make node_ip an IPv4 field to take advantage of marshmallow's validation logic
    # but convert it to a string since most Weaver code assumes IPs are string types.
    @post_load
    def convert_node_ip_to_str(self, data, **kwargs):
        data['node_ip'] = str(data.get('node_ip'))
        return data

    @validates_schema
    def validate_restart_type_field_compatibilities(self, data, **kwargs):
        compatibility_dict = {
            weaver_enums.RestartType.FULL: {'incompatible_with': ['asg_name', 'drain_type', 'node_ip'], 'requires': []},
            weaver_enums.RestartType.ROLLING: {'incompatible_with': ['node_ip'], 'requires': ['drain_type']},
            weaver_enums.RestartType.SINGLE: {'incompatible_with': ['asg_name'], 'requires': ['drain_type', 'node_ip']},
        }
        raise_compatibility_errors(data, 'restart_type', compatibility_dict, print_enums_as_value=True)


class StackRebuildRequestBodySchema(Schema):
    asg_name = fields.String(load_default="")
    drain_type = fields.Enum(weaver_enums.DrainType, by_value=True, load_default=weaver_enums.DrainType.DEFAULT)


class StackReinitRequestBodySchema(Schema):
    drain_type = fields.Enum(weaver_enums.DrainType, by_value=True, load_default=weaver_enums.DrainType.DEFAULT)
