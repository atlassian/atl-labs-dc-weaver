#!/usr/bin/env python3

from __future__ import annotations

from typing import TYPE_CHECKING

from flask import Blueprint, jsonify
from flask.views import MethodView

from weaver.weaver import *
from weaver.token_auth import *
from weaver.aws_resources.aws_resources import *

if TYPE_CHECKING:
    from typing import Type

api_blueprint = Blueprint('api', __name__)


def add_api_route(method: Type[MethodView], url: str) -> None:
    """
    Wrapper for Blueprint.add_url_rule that emulates the core behavior of flask-restful's API.add_resource
    """
    api_blueprint.add_url_rule(url, view_func=method.as_view(method.__name__.lower()))


# generic API error handlers
@api_blueprint.app_errorhandler(400)
def handle_400_error(err):
    if hasattr(err, 'data'):
        messages = err.data.get("messages", ["Bad Request"])
    else:
        messages = err.description
    return jsonify({"error_msg": messages}), err.code


@api_blueprint.app_errorhandler(401)
@api_blueprint.app_errorhandler(403)
def handle_401_403_error(err):
    return jsonify({"error_msg": err.description}), err.code


@api_blueprint.app_errorhandler(404)
def handle_404_error(err):
    if hasattr(err, 'data'):
        messages = err.data.get("messages", ["Resource Not Found"])
    else:
        messages = err.description
    return jsonify({"error_msg": messages}), err.code


@api_blueprint.app_errorhandler(409)
def handle_409_error(err):
    if hasattr(err, 'data'):
        messages = err.data.get("messages", ["Action Already In Progress"])
    else:
        messages = err.description
    return jsonify({"error_msg": messages}), err.code


@api_blueprint.app_errorhandler(500)
def handle_500_error(err):
    if hasattr(err, 'data'):
        messages = err.data.get("messages", ["Internal Server Error"])
    elif hasattr(err.description, 'args'):
        messages = f"Unhandled Exception: {err.description.args[0]}"
    else:
        messages = err.description
    return jsonify({"error_msg": messages}), err.code


@api_blueprint.app_errorhandler(Exception)
def handle_exceptions(err):
    log.exception(err)
    module = type(err).__module__
    if module is None or module == type(str).__module__:
        exception_class = type(err).__name__
    else:
        exception_class = module + '.' + err.__class__.__name__
    return jsonify({'error_msg': f'An unhandled exception occurred in Weaver (see Weaver logs for stack trace): {exception_class}: {err}'}), 500


# # We are deprecating the previous API in favour of a redesigned API
# Since we have no consumers of the old API we are labeling the new API `/api/v1`
# Endpoints that start with `/api/v1` will be the public API contract going forward

# Stacks
add_api_route(StackListMethods, '/api/v1/stack')
add_api_route(StackMethods, '/api/v1/stack/<stack_name>')
add_api_route(StackActionMethods, '/api/v1/stack/<stack_name>/action')
add_api_route(StackChangeSetMethods, '/api/v1/stack/<stack_name>/changeset')
add_api_route(StackDiagnosticsMethods, '/api/v1/stack/<stack_name>/diagnostics')
add_api_route(StackUpgradeMethods, '/api/v1/stack/<stack_name>/upgrade')
add_api_route(StackRestartMethods, '/api/v1/stack/<stack_name>/restart')
add_api_route(StackRebuildMethods, '/api/v1/stack/<stack_name>/rebuild')
add_api_route(StackReinitMethods, '/api/v1/stack/<stack_name>/reinit')

# API Tokens
add_api_route(RefreshTokenMethods, '/api/v1/token/refresh')
add_api_route(AccessTokenMethods, '/api/v1/token/access')

# Utilities
add_api_route(UtilCfnTemplateMethods, '/api/v1/util/cfn_templates')

# Actions -- deprecated
add_api_route(DoEc2RollingReboot, '/doec2rollingreboot/<region>/<stack_name>/<asg_name>/<drain_type>')
add_api_route(DoRunSql, '/dorunsql/<region>/<stack_name>')
add_api_route(DoToggleNode, '/dotogglenode/<region>/<stack_name>/<node_ip>/<drain_type>/<threads>/<heaps>')

# Stack info -- deprecated
add_api_route(ClearStackActionInProgress, '/clearActionInProgress/<region>/<stack_name>')
add_api_route(GetLogs, '/getLogs')
add_api_route(GetStackActionInProgress, '/getActionInProgress/<region>/<stack_name>')
add_api_route(GetSql, '/getsql/<region>/<stack_name>')
add_api_route(GetSysLogs, '/getSysLogs')
add_api_route(GetTemplateParams, '/getTemplateParams/<repo_name>/<template_name>')
add_api_route(GetTemplateParamsForStack, '/getStackParams/<action>/<region>/<stack_name>/<template_name>')


# Helpers -- deprecated
add_api_route(GetAllSubnetsForRegion, '/getAllSubnetsForRegion/<region>')
add_api_route(GetDbVersionCompatibilityForSnapshot, '/getDbVersionCompatibilityForSnapshot')
add_api_route(GetEbsSnapshots, '/getEbsSnapshots/<region>/<stack_name>')
add_api_route(GetEc2InstanceTypes, '/getEc2InstanceTypes/<region>')
add_api_route(GetGitRepos, '/getGitRepos')
add_api_route(GetKmsKeys, '/getKmsKeys/<region>')
add_api_route(GetLockedStacks, '/getLockedStacks')
add_api_route(GetRdsInstanceTypes, '/getRdsInstanceTypes/<region>/<engine>/<engine_version>')
add_api_route(GetRdsSnapshots, '/getRdsSnapshots/<region>/<stack_name>')
add_api_route(GetSslCerts, '/getSslCerts/<region>')
add_api_route(SetStackLocking, '/setStackLocking/<lock>')
add_api_route(GetSubnetsForVpc, '/getSubnetsForVpc/<region>/<vpc>')
add_api_route(GetVpcs, '/getVpcs/<region>')

# Git -- deprecated
add_api_route(DoGitPull, '/doGitPull/<template_repo>/<stack_name>')
add_api_route(GetGitBranch, '/getGitBranch/<template_repo>')
add_api_route(GetGitCommitDifference, '/getGitCommitDifference/<template_repo>')
add_api_route(GetGitRevision, '/getGitRevision/<template_repo>')

# Restart Weaver -- deprecated
add_api_route(DoWeaverRestart, '/doWeaverRestart/<stack_name>')

# Status endpoint
add_api_route(WeaverStatus, '/status')
