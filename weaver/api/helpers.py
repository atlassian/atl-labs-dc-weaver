#!/usr/bin/env python3

from __future__ import annotations

import logging

from functools import wraps
from os import getenv
from typing import Literal

from flask import abort, current_app, request, session, flash
from flask_jwt_extended import get_jwt, verify_jwt_in_request
from webargs import ValidationError
from webargs.flaskparser import FlaskParser

from weaver.aws_resources import get_available_regions
from weaver.saml_auth import saml_auth
from weaver.sns import sns

log = logging.getLogger('app_log')


#
# Wrappers/Decorators
#
def auth_required(_func=None, *, auth_type: Literal["any", "saml", "token"] = "any"):
    """
    Decorator for requiring/checking SAML auth and user permissions
    Optional parameter `auth_type` allows restricting which authentication methods are allowed for a given route
    """

    def wrapper(func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            # dispatch request immediately if SAML is not configured
            if getenv('NO_SAML') or current_app.config.get('NO_SAML'):
                return func(*args, **kwargs)
            request_region = kwargs.get("region")
            request_stack = kwargs.get("stack_name")
            weaver_method_name = str(request.endpoint).split('.')[1]
            error_msg_parts = [
                f"to perform {request.method}",
                f"on {weaver_method_name}",
                f"for {request_stack}" if request_stack else "",
                f"in region {request_region}" if request_region else "",
            ]
            # check for an access token; this will automatically throw 401 for invalid or expired tokens
            # optional=True allows requests without a token to flow through to SAML session validation instead for frontend requests
            if auth_type in ("any", "token") and verify_jwt_in_request(optional=True):
                if check_token_permissions(request_region, request_stack):
                    return func(*args, **kwargs)
                else:
                    jwt = get_jwt()
                    log.error(f"Access token {jwt['jti']} for user {jwt['sub']} is not authorised {' '.join(error_msg_parts)}")
                    sns.send_sns_msg(request_stack, f"API request from user {jwt['sub']} attempted {' '.join(error_msg_parts)} but does not have permission")
                    abort(403, 'Forbidden')
            if auth_type in ("any", "saml"):
                # check if session has expired
                if 'saml' not in session:
                    abort(401, 'Session expired')
                # Start with permissions.yaml
                if saml_auth.check_user_permissions(weaver_method_name, request_region, request_stack):
                    return func(*args, **kwargs)
                # if permissions haven't been granted by permissions.yaml, fall back to permissions.json if it exists
                if saml_auth.check_user_permissions_legacy(weaver_method_name, request_region, request_stack):
                    return func(*args, **kwargs)
                username = session['saml']['subject']
                log.error(f"User {username} is not authorised {' '.join(error_msg_parts)}")
                sns.send_sns_msg(request_stack, f"User {username} attempted {' '.join(error_msg_parts)} but does not have permission")
                flash(f"You do not have permission to perform {' '.join(error_msg_parts)}", 'error')
                abort(403, 'Forbidden')
            else:
                abort(500, f"Cannot dispatch request; unexpected auth_type for route: {auth_type}")

        return decorated_function

    # this "magic" allows the decorator to be called with or without arguments
    if _func is None:
        return wrapper
    else:
        return wrapper(_func)


def check_token_permissions(request_region: str | None, request_stack: str | None) -> bool:
    token = get_jwt()
    # whittle down the valid routes to the current route
    valid_routes = [layer for layer in token["authorization_details"] if request.url_rule and request.url_rule.rule in layer["routes"] or layer["routes"] == ["*"]]
    # whittle down the valid routes to the current method
    valid_routes = [layer for layer in valid_routes if request.method.lower() in layer["methods"] or layer["methods"] == ["*"]]
    # whittle down the valid routes to the current region, if the request has one
    valid_routes = [layer for layer in valid_routes if request_region in layer["regions"] or layer["regions"] == ["*"]]
    # whittle down the valid routes to the current stack, if the request has one
    valid_routes = [layer for layer in valid_routes if request_stack in layer["stacks"] or layer["stacks"] == ["*"]]
    # if any valid routes are remaining, the user has permission to make this request
    return True if valid_routes else False


#
# Webargs
#


class CustomArgParser(FlaskParser):
    DEFAULT_VALIDATION_STATUS = 400  # default is 422; I'd prefer plain 400 for bad requests due to invalid request args


_parser = CustomArgParser()
use_args = _parser.use_args
use_kwargs = _parser.use_kwargs


def is_valid_aws_region(region_name):
    """
    Validator for region query param; compares user-provided value to AWS regions
    """
    available_regions = current_app.config.get("AVAILABLE_REGIONS")
    if not available_regions:
        # if for some reason we were unable to retrieve the list of regions at startup, try to retrieve it now
        current_app.config["AVAILABLE_REGIONS"] = available_regions = get_available_regions()
        if not available_regions:
            # super-unlikely we reach this point, but if we can't retrieve the list of regions, something's busted upstream (likely AWS); return 500 now
            abort(500, "Unable to retrieve valid regions from AWS; check logs for details")
    if region_name not in available_regions:
        log.error(f"Region for request ({region_name}) not found in list of valid regions: {', '.join(available_regions)}")
        raise ValidationError("Region not a valid AWS region")


def is_valid_weaver_region(region_name):
    """
    Validator for region query param; compares user-provided value to weaver's configured regions
    """
    weaver_regions = [region_tuple[0] for region_tuple in current_app.config["REGIONS"]]
    if region_name not in weaver_regions:
        raise ValidationError(f"Region not configured for use with this Weaver deployment (expected one of: {', '.join(weaver_regions)})")
