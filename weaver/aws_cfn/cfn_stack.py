#!/usr/bin/env python3

from __future__ import annotations

import concurrent.futures
import itertools
import json
import logging
import os
import time
import re
from contextlib import suppress
from datetime import datetime, timedelta
from logging import ERROR, INFO, WARN
from pathlib import Path
from typing import Any, Dict, List, Literal, Set, Union, TYPE_CHECKING
from defusedxml import ElementTree

from pytz import UTC

import boto3
import botocore
import grpc
import requests
import tenacity
from botocore.exceptions import ClientError
from flask import Blueprint, current_app, g, session
from grpc_health.v1 import health_pb2 as pb2
from grpc_health.v1 import health_pb2_grpc as pb2_grpc
from humanfriendly import format_timespan
from ipaddress import ip_address
from requests_toolbelt.sessions import BaseUrlSession
from retry import retry
from ruamel.yaml import YAML

import weaver.utils as utils
import weaver.enums as weaver_enums
import weaver.exceptions as weaver_exceptions
from weaver.custom_types import GetTemplateParamsResponse, ParameterTypeDefWithoutKey
from weaver.extensions import cache, executor
from weaver.models import ProductEnum
from weaver.Stack import Stack
from weaver.weaver_logging.weaver_logging import log_to_changelog, log_to_ui

if TYPE_CHECKING:
    from mypy_boto3_autoscaling.type_defs import AutoScalingGroupTypeDef
    from mypy_boto3_cloudformation.paginator import DescribeChangeSetPaginator, ListChangeSetsPaginator
    from mypy_boto3_cloudformation.type_defs import (
        CreateChangeSetOutputTypeDef,
        ChangeSetSummaryTypeDef,
        DescribeChangeSetOutputTypeDef,
        ParameterTypeDef,
        StackResourceSummaryTypeDef,
    )
    from mypy_boto3_ec2.service_resource import Instance
    from mypy_boto3_ec2.type_defs import FilterTypeDef
    from mypy_boto3_elbv2.type_defs import TargetGroupAttributeTypeDef
    from mypy_boto3_elbv2.literals import TargetHealthStateEnumType
    from mypy_boto3_s3.type_defs import ObjectIdentifierTypeDef


## Python helpers
#
def version_tuple(version):
    return tuple(int(i) for i in re.sub('[-A-Za-z]+', '.', version).split('.'))


## Tenacity helpers
#
def is_false_p(value) -> bool:
    return value is False


def return_last_value(retry_state):
    return retry_state.outcome.result()


## EC2 resource class extension methods
#
# These helper methods are attached to every new EC2 "resource" class/object created by Boto via the 'creating-resource-class.ec2.Instance'
# event; (see function `__add_ec2_resource_attributes` below). Since these functions are attached to the EC2 "resource" created by Boto,
# `self` in this context is the EC2 resource object (e.g., `self.id` is the EC2 instance ID).


# This gives EC2 resources the ability to query the stack's autoscaling
# groups and returns the ASG that the node belongs to.
def _get_auto_scaling_group(self, force=False) -> Union[AutoScalingGroupTypeDef, None]:
    asgs = self.stack.get_stack_asgs(force=force)
    asg_for_node = None
    for asg in asgs:
        if self.id in [instance['InstanceId'] for instance in asg['Instances']]:
            asg_for_node = asg
    return asg_for_node


# This gives EC2 resources the ability to query the instance's tags and return
# the _node_'s app_type (i.e., not the _cluster_ app_type)
def _get_app_type(self):
    product_tag = next((tag['Value'].lower() for tag in self.tags if tag['Key'] == 'product'), None)
    synchrony_service_tag_exists = any(tag for tag in self.tags if tag['Key'] == 'synchrony_service')
    return 'synchrony' if synchrony_service_tag_exists else product_tag


# returns a tuple with the days, hours, and minutes since the instance was launched
def _get_uptime(self):
    delta = datetime.now(UTC) - self.launch_time
    return delta.days, delta.seconds // 3600, (delta.seconds // 60) % 60


# A backup node is defined as one that has a node_type tag set to a value containing the string 'backup'
def _is_backup_node(self) -> bool:
    node_type_tag = next((tag['Value'].lower() for tag in self.tags if tag['Key'] == 'node_type'), None)
    if not node_type_tag:
        return False
    return 'backup' in node_type_tag


# The CFN parameter key for load balancer drain timeout
DRAIN_TIMEOUT_PARAM_KEY = {
    'ALB': 'WebTargetGroupDrainingTimeout',
    'NLB': 'SSHTargetGroupDrainingTimeout',
}


ZDU_MINIMUM_BITBUCKET_VERSION = version_tuple('7.9')
ZDU_MINIMUM_CONFLUENCE_VERSION = version_tuple('7.8')
ZDU_MINIMUM_JIRACORE_VERSION = version_tuple('7.3')
ZDU_MINIMUM_SERVICEDESK_VERSION = version_tuple('3.6')

READY_TO_SERVE_STATUSES = ('FIRST_RUN', 'OK', 'RUNNING', 'SERVING', 'SYNCHRONIZED', 'UNIMPLEMENTED')

aws_cfn_stack_blueprint = Blueprint('aws_cfn_stack', __name__)

log = logging.getLogger('app_log')


class CfnStack(Stack):
    """An object describing an instance of an aws cloudformation stack:

    Attributes:
        stackstate: A dict of dicts containing all state information.
        stack_name: The name of the stack we are keeping state for
    """

    def __init__(self, stack_name, region):
        self.cfn_init_cmd = None
        self.fast_deregistration_delay = 10
        self.load_balancers = {}
        self.load_balancer_tg_arns = {}
        self.primary_stack_asgs = []
        self.stack_asgs = []
        self.stack_id: str = ''
        self.stack_node_target_group_arns: list[str] = []

        Stack.__init__(self, stack_name, region)

        # Create a Boto session which can be used by any callers/accessors of this stack object during this thread (optionally, of course).
        self.boto_session = boto3.session.Session(region_name=region)
        # Register an event handler for any new EC2 "resource" classes created using the stack's Boto session for this thread;
        # see https://boto3.amazonaws.com/v1/documentation/api/latest/guide/events.html#creating-resource-class for details.
        self.boto_session.events.register('creating-resource-class.ec2.Instance', self.__add_ec2_resource_attributes)

    from ._cfn_stack_upgrade import (  # type: ignore[misc]
        filter_out_ready_to_serve_nodes,
        upgrade_in_place_stack_preparation_steps,
        select_not_ready_node,
        upgrade_in_place,
        upgrade_in_place_cleanup,
        upgrade_in_place_reinit_nodes,
        upgrade_in_place_reinit_all_nodes,
        upgrade_in_place_reinit_one_then_remaining_nodes,
        update_version_and_product_download_url,
    )

    # Handle events for newly-created EC2 resource classes; allows for attaching new attributes and methods to the class that will be used
    # to create new EC2 resource objects
    def __add_ec2_resource_attributes(self, class_attributes, **kwargs):
        # Attach a reference to this stack as a class attribute
        class_attributes['stack'] = self
        # Attach a method for retrieving details about the autoscaling group the instance belongs to
        class_attributes['get_auto_scaling_group'] = _get_auto_scaling_group
        # Attach a method for retrieving the instance's application type (e.g., could be "synchrony" even if cluster app_type is "confluence")
        class_attributes['get_app_type'] = _get_app_type
        # Attach a method for calculating the "uptime" since the instance was launched
        class_attributes['get_uptime'] = _get_uptime
        # Attach a method for checking if the node is a "backup" node
        class_attributes['is_backup_node'] = _is_backup_node

    ##
    # Stack - micro function methods
    #

    def check_exists(self):
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            cfn.describe_stacks(StackName=self.stack_name)
        except botocore.exceptions.ClientError as e:
            if 'does not exist' in e.response['Error']['Message']:
                raise weaver_exceptions.StackDoesNotExistException(f'Stack {self.stack_name} does not exist', stack_name=self.stack_name)
            else:
                raise
        return True

    @tenacity.retry(
        wait=tenacity.wait_exponential(),
        stop=tenacity.stop_after_attempt(5),
        retry=tenacity.retry_if_exception_type(botocore.exceptions.ClientError),
        # before=tenacity.after_log(log, DEBUG), # todo re-enable once https://github.com/jd/tenacity/issues/321 is released (it is not currently on 8.0.1)
    )
    def get_service_url(self):
        if self.service_url:
            return self.service_url
        else:
            try:
                cfn = boto3.client('cloudformation', region_name=self.region)
                stack_details = cfn.describe_stacks(StackName=self.stack_name)
                self.service_url = f"{[p['OutputValue'] for p in stack_details['Stacks'][0]['Outputs'] if p['OutputKey'] == 'ServiceURL'][0]}/"
                return self.service_url
            except botocore.exceptions.ClientError as e:
                # log and allow tenacity to retry
                log_to_ui(self, ERROR, f'ClientError received during get_service_url. Request will be retried a maximum of 5 times. Exception is: {e}', write_to_changelog=False)
                raise
            except Exception:
                log.exception('Exception occurred during get_service_url')
                return False

    def get_param_without_key(self, param: ParameterTypeDef) -> ParameterTypeDefWithoutKey:
        param_without_key: ParameterTypeDefWithoutKey = {}
        possible_inclusions: list[Literal['ParameterValue', 'UsePreviousValue', 'ResolvedValue']] = ['ParameterValue', 'UsePreviousValue', 'ResolvedValue']
        for possible_inclusion in possible_inclusions:
            if possible_inclusion in param:
                param_without_key[possible_inclusion] = param[possible_inclusion]
        # For password params: Get rid of the masked **** ParameterValue and replace with UsePreviousValue=True
        if self.check_cfn_param_key_is_password(param['ParameterKey']):
            try:
                del param_without_key['ParameterValue']
            except KeyError:
                return param_without_key
            param_without_key['UsePreviousValue'] = True
        return param_without_key

    def get_params(self, exclude_passwords: bool = False) -> List[ParameterTypeDef]:
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            stack_details = cfn.describe_stacks(StackName=self.stack_name)
            params: List[ParameterTypeDef] = stack_details['Stacks'][0]['Parameters']
        except botocore.exceptions.ClientError:
            log.exception('Error getting stack parameters')
            return []
        if exclude_passwords:
            return list(filter(lambda param: not self.check_cfn_param_key_is_password(param['ParameterKey']), params))
        return params

    def get_param_value(self, param_to_get: str, params: List[ParameterTypeDef] = []) -> str:
        if not params:
            params = self.get_params()
        param_value: str = ''
        legacy_params: List[str] = []
        if param_to_get == 'ProductVersion':
            legacy_params = ['confluenceversion', 'jiraversion', 'crowdversion']
        elif param_to_get == 'ClusterNodeCount':
            legacy_params = ['clusternodemax']
        elif param_to_get == 'SynchronyClusterNodeCount':
            legacy_params = ['synchronyclusternodemax']
        try:
            for param in params:
                if param['ParameterKey'].lower() == param_to_get.lower() or param['ParameterKey'].lower() in legacy_params:
                    param_value = param['ParameterValue'] if param['ParameterValue'] else ''
        except TypeError as e:
            log.exception(f'Error retrieving parameter value; no params available: {e}')
        return param_value

    def search_param_keys(self, regex_to_search: str, params: list[ParameterTypeDef] = []) -> List[ParameterTypeDef]:
        if not params:
            params = self.get_params()
        matched_params: List[ParameterTypeDef] = []
        for param in params:
            if re.fullmatch(regex_to_search, param['ParameterKey']):
                matched_params.append(param.copy())
        return matched_params

    def check_cfn_param_key_is_password(self, val: Any) -> bool:
        return bool(re.fullmatch(r'^.*(P|p)assword$', str(val)))

    def get_cfn_param_deregistration_delay(self, load_balancer: weaver_enums.LoadBalancer) -> int:
        if load_balancer.value not in DRAIN_TIMEOUT_PARAM_KEY:
            raise ValueError(f'Invalid load balancer specified: {load_balancer}')
        param_key: str = DRAIN_TIMEOUT_PARAM_KEY[load_balancer.value]
        cfn_set_delay: str = self.get_param_value(param_key)
        if not cfn_set_delay:
            raise weaver_exceptions.StackParameterNotFound(f"Could not find stack parameter '{param_key}'")
        return int(cfn_set_delay)

    # Check if a node belongs to the stack
    def has_node(self, node_ip: str, force: bool = False) -> bool:
        stack_nodes: List[Instance] = self.get_stacknodes(force=force)
        for node in stack_nodes:
            if node.private_ip_address == node_ip:
                return True
        return False

    # Check if an autoscaling group belongs to the stack
    def has_asg(self, asg_name: str, force: bool = False) -> bool:
        asgs: List[AutoScalingGroupTypeDef] = self.get_stack_asgs(force)
        for asg in asgs:
            if asg["AutoScalingGroupName"] == asg_name:
                return True
        return False

    # Looks for a CFN resource with logical ID 'ALB' or 'LoadBalancerV2', if it exists return True.
    def has_alb(self) -> bool:
        if 'ALB' in self.load_balancers:
            return self.load_balancers['ALB']
        if self.get_stack_resource_by_logical_id('ALB') or self.get_stack_resource_by_logical_id('LoadBalancerV2'):
            self.load_balancers['ALB'] = True
            return True
        self.load_balancers['ALB'] = False
        return False

    # Looks for a CFN resource with logical ID 'NLB', if it exists return True
    def has_nlb(self) -> bool:
        if 'NLB' in self.load_balancers:
            return self.load_balancers['NLB']
        if self.get_stack_resource_by_logical_id('NLB'):
            self.load_balancers['NLB'] = True
            return True
        self.load_balancers['NLB'] = False
        return False

    # For ALB try find stack resource 'ALB'
    def get_load_balancer_tg_arn(self, load_balancer: weaver_enums.LoadBalancer) -> str:
        tg_name_prefix: str
        tg_name_keyword: str
        if load_balancer in self.load_balancer_tg_arns:
            return self.load_balancer_tg_arns[load_balancer]
        if load_balancer == weaver_enums.LoadBalancer.ALB:
            tg_name_prefix = 'ALB'
            tg_name_keyword = 'Main'
        elif load_balancer == weaver_enums.LoadBalancer.NLB:
            tg_name_prefix = 'Nlb'
            tg_name_keyword = 'Ssh'
        else:
            raise ValueError(f"Load balancer {load_balancer.value} is currently unsupported by the method 'get_load_balancer_tg_arn'")
        tg_resource: StackResourceSummaryTypeDef | None = self.get_stack_resource_by_logical_id(f'{tg_name_prefix}{tg_name_keyword}TargetGroup')
        if not tg_resource:
            tg_resource = self.get_stack_resource_by_logical_id(f'{tg_name_keyword}TargetGroup')
        if not tg_resource:
            log_to_ui(
                self,
                ERROR,
                f"Could not find {load_balancer.value} target group ARN from CloudFormation resource '{tg_name_prefix}{tg_name_keyword}TargetGroup'' or '{tg_name_keyword}TargetGroup'.",
                write_to_changelog=False,
            )
            raise weaver_exceptions.StackParameterNotFound
        self.load_balancer_tg_arns[load_balancer] = tg_resource['PhysicalResourceId']
        return self.load_balancer_tg_arns[load_balancer]

    def update_paramlist(self, params_list, param_key, param_value):
        key_found = False
        for param in params_list:
            for key, value in param.items():
                if value == param_key:
                    param['ParameterValue'] = param_value
                    key_found = True
                if self.check_cfn_param_key_is_password(value):
                    try:
                        del param['ParameterValue']
                    except KeyError:
                        pass
                    param['UsePreviousValue'] = True
        if not key_found:
            params_list.append({'ParameterKey': param_key, 'ParameterValue': param_value})
        return params_list

    # A more efficient alternative to update_paramlist when updating parameters in bulk.
    # Takes in a parameter list in standard AWS CFN API format, transforms it into a more efficient-to-update-in-bulk format,
    # updates it, transforms it back into the standard AWS CFN API format.
    # Ignores any supplied parameters to update that don't appear in the list.
    def update_paramlist_bulk_ignore_non_existing(self, paramlist: List[ParameterTypeDef], params_to_update: dict[str, str]) -> List[ParameterTypeDef]:
        paramlist_dict: dict[str, ParameterTypeDefWithoutKey] = {param['ParameterKey']: self.get_param_without_key(param) for param in paramlist}
        for param_key, new_value in params_to_update.items():
            if param_key in paramlist_dict:
                paramlist_dict[param_key]['ParameterValue'] = new_value
        updated_paramlist: List[ParameterTypeDef] = [{'ParameterKey': param_key} | param_other_info for param_key, param_other_info in paramlist_dict.items()]  # type: ignore[misc]
        return updated_paramlist

    def get_spindown_to_zero_appnodes_paramlist(self) -> List[ParameterTypeDef]:
        spindown_params = self.get_params()
        # Look for and update any node count parameters (grouped by AZ)
        az_node_count_params = self.search_param_keys(r'^Az\dNodeCount$', spindown_params)
        az_node_count_params_keys: List[str] = [param['ParameterKey'] for param in az_node_count_params]
        # Look for and update any node count parameters (not grouped by AZ)
        spindown_params = self.update_paramlist_bulk_ignore_non_existing(
            spindown_params,
            {
                param_key: '0'
                for param_key in az_node_count_params_keys
                + [
                    'ClusterNodeMax',
                    'ClusterNodeMin',
                    'ClusterNodeCount',
                    'BackupNodeCount',
                    'SynchronyClusterNodeMax',
                    'SynchronyClusterNodeMin',
                    'SynchronyClusterNodeCount',
                    'ClusterBotNodeCount',
                ]
            },
        )
        return spindown_params

    def get_spinup_to_one_appnode_paramlist(self) -> List[ParameterTypeDef]:
        spinup_params = self.get_params()
        update_paramlist_dict: dict[str, str] = {}
        if self.preupgrade_az_node_counts:
            # Just choose the first parameter entry in the list which is non-zero
            az_node_count_param_to_spinup: str = next(az_node_count_param_key for az_node_count_param_key, count in self.preupgrade_az_node_counts.items() if count > 0)
            update_paramlist_dict.update({az_node_count_param_to_spinup: '1'})
        elif self.preupgrade_app_node_count > 0:
            update_paramlist_dict.update({param_key: '1' for param_key in ['ClusterNodeMax', 'ClusterNodeMin', 'ClusterNodeCount']})
        else:
            # Only spinup backup node first if it's the only one in the stack.
            update_paramlist_dict.update({'BackupNodeCount': '1'})
        if self.preupgrade_synchrony_node_count and self.preupgrade_synchrony_node_count > 0:
            update_paramlist_dict.update({param_key: '1' for param_key in ['SynchronyClusterNodeMax', 'SynchronyClusterNodeMin', 'SynchronyClusterNodeCount']})
        spinup_params = self.update_paramlist_bulk_ignore_non_existing(spinup_params, update_paramlist_dict)
        return spinup_params

    def get_spinup_to_full_node_count_paramlist(self) -> List[ParameterTypeDef]:
        spinup_params: List[ParameterTypeDef] = self.get_params()
        update_paramlist_dict: dict[str, str] = {}
        # Look for any node count parameters to update from preupgrade node count attributes
        if self.preupgrade_az_node_counts:
            update_paramlist_dict.update({param_key: str(count) for param_key, count in self.preupgrade_az_node_counts.items()})
        if self.preupgrade_backup_node_count:
            update_paramlist_dict.update({'BackupNodeCount': str(self.preupgrade_backup_node_count)})
        if self.preupgrade_app_node_count:
            update_paramlist_dict.update({param_key: str(self.preupgrade_app_node_count) for param_key in ['ClusterNodeMax', 'ClusterNodeMin', 'ClusterNodeCount']})
        if self.preupgrade_synchrony_node_count:
            update_paramlist_dict.update(
                {param_key: str(self.preupgrade_synchrony_node_count) for param_key in ['SynchronyClusterNodeMax', 'SynchronyClusterNodeMin', 'SynchronyClusterNodeCount']}
            )
        if self.preupgrade_bot_node_count:
            update_paramlist_dict.update({'ClusterBotNodeCount': str(self.preupgrade_bot_node_count)})
        # Update the param list and return
        spinup_params = self.update_paramlist_bulk_ignore_non_existing(spinup_params, update_paramlist_dict)
        return spinup_params

    def upload_file_to_s3(self, file, s3_filename):
        s3 = boto3.resource('s3', region_name=self.region)
        try:
            s3.meta.client.upload_file(str(file), self.get_s3_bucket_name(), s3_filename)
        except botocore.exceptions.ClientError:
            log.exception('boto ClientError')
            return False
        return True

    def download_files_from_s3(self, bucket, destination):
        return self.run_command(self.get_stacknodes(), f'aws s3 sync s3://{self.get_s3_bucket_name()}/{bucket} {destination}')

    def ssm_send_command(self, node_id: str, cmd: str, cmd_desc: str = '') -> str:
        """
        Sends a shell command to an EC2 instance via SSM; does not wait for a result.

        Args:
            node_id: The EC2 instance ID the command should be run on
            cmd: The command to run on the EC2 instance
            cmd_desc: An optional description of the command to be used in logs (useful for obscuring commmands which might contain secrets/tokens/etc.)

        Returns
            The SSM command ID (str), or False (bool) if SSM indicates there was some error in receiving the command
        """
        logs_bucket = f"{self.get_s3_bucket_name()}/logs"
        ssm = boto3.client('ssm', region_name=self.region)
        ssm_command = ssm.send_command(
            InstanceIds=[node_id],
            DocumentName='AWS-RunShellScript',
            Parameters={'commands': [cmd], 'executionTimeout': ["900"]},
            OutputS3BucketName=logs_bucket,
            OutputS3KeyPrefix='run-command-logs',
        )
        log_to_ui(
            self, INFO, f'for command{f" with description: {cmd_desc}" if cmd_desc else f": {cmd}"}, command_id is {ssm_command["Command"]["CommandId"]}', write_to_changelog=False
        )
        if ssm_command['ResponseMetadata']['HTTPStatusCode'] == requests.codes.ok:
            return ssm_command['Command']['CommandId']
        else:
            msg = f'Sending command {cmd} to SSM failed on instance {node_id}'
            log_to_ui(self, ERROR, msg)
            raise weaver_exceptions.SsmExecutionError(msg, return_code=ssm_command['ResponseMetadata']['HTTPStatusCode'])

    def ssm_cmd_status_check(self, cmd_id: str) -> str:
        ssm = boto3.client('ssm', region_name=self.region)
        cmd_status: str = 'Unknown'
        try:
            list_command = ssm.list_commands(CommandId=cmd_id)
        except botocore.exceptions.ClientError:
            log.exception('boto ClientError')
            log_to_ui(self, ERROR, f'retrieving SSM command {cmd_id} status failed', write_to_changelog=False)
        try:
            cmd_status = list_command['Commands'][0]['Status']
            node_id = list_command['Commands'][0]['InstanceIds'][0]
        except IndexError:
            log_to_ui(self, WARN, f'details for SSM command {cmd_id} are not yet available (perhaps command is not yet listed by SSM?)', write_to_changelog=False)
        else:
            log_to_ui(self, INFO, f'status of SSM command {cmd_id} on node {node_id} is {cmd_status}', write_to_changelog=False)
        return cmd_status

    def ssm_send_and_wait_response(self, node: str, cmd: str) -> Union[bool, str]:
        cmd_id = self.ssm_send_command(node, cmd)
        if not cmd_id:
            log_to_ui(self, ERROR, f'Command {cmd} on node {node} failed to send', write_to_changelog=False)
            return False
        else:
            result = self.wait_for_cmd_result(cmd_id)
        return result

    def ssm_get_command_output(self, cmd_id, instance_id):
        ssm = boto3.client('ssm', region_name=self.region)
        try:
            cmd_invocation = ssm.get_command_invocation(CommandId=cmd_id, InstanceId=instance_id)
            return cmd_invocation['StandardOutputContent'].strip()
        except botocore.exceptions.ClientError:
            log.exception('boto ClientError')
            log_to_ui(self, ERROR, f'retrieving StandardOutputContent for ssm command {cmd_id} failed', write_to_changelog=False)
        return False

    def run_command(self, nodes, cmd, return_results=False):
        cmd_id_dict = {}
        results = []
        for node in nodes:
            log_to_ui(self, INFO, f'Running command {cmd} on {node.id} ({node.private_ip_address})', write_to_changelog=False)
            cmd_id_dict[self.ssm_send_command(node.id, cmd)] = node.private_ip_address
        for cmd_id in cmd_id_dict:
            result = self.wait_for_cmd_result(cmd_id)
            if result == 'Failed':
                log_to_ui(self, ERROR, f'Command result for {cmd_id}: {result}', write_to_changelog=True)
                if not return_results:
                    return False
            else:
                log_to_ui(self, INFO, f'Command result for {cmd_id}: {result}', write_to_changelog=True)
            results.append({'node_ip': cmd_id_dict[cmd_id], 'result': result})
        return results if return_results else True

    def wait_for_cmd_result(self, cmd_id: str) -> str:
        result = self.ssm_cmd_status_check(cmd_id)
        while result not in ('Success', 'Failed'):
            time.sleep(10)
            result = self.ssm_cmd_status_check(cmd_id)
        return result

    def ssm_wait_response(self, instances: list[Instance], command: str) -> list[dict]:
        ssm = boto3.client("ssm", region_name=self.region)
        ssm_command_params: dict[str, Any] = {
            "InstanceIds": [instance.instance_id for instance in instances],
            "DocumentName": "AWS-RunShellScript",
            "DocumentVersion": "$LATEST",
            "Parameters": {"commands": [command]},
        }
        log.debug(f"Sending command `{command}` to instance(s): {', '.join([instance.instance_id for instance in instances])}")
        try:
            command_details = ssm.send_command(**ssm_command_params)
        except ssm.exceptions.InvalidInstanceId as e:
            if "not in a valid state" in e.response["Error"]["Message"]:
                log.warning("SSM returned an InvalidInstanceId error because the instance wasn't ready to receive SSM commands")
                return [
                    {
                        "instance_id": instance.instance_id,
                        "status": "None",
                        "std_out": "",
                    }
                    for instance in instances
                ]
            else:
                raise
        command_id = command_details["Command"]["CommandId"]
        log.debug(f"SSM command ID for `{command}`: {command_id}")
        command_status: str = "Pending"
        while command_status in ("Pending", "InProgress"):
            time.sleep(3)
            commands = ssm.list_commands(CommandId=command_id)["Commands"]
            # ITPLT-3204: 'list_commands' sometimes returns an empty list of commands (likely due to being run too soon after sending the command)
            if len(commands) > 0:
                command_status = commands[0]["Status"]
                log.debug(f"Status for SSM command ID {command_id}: {command_status}")
        results = []
        for instance in instances:
            instance_results = ssm.get_command_invocation(CommandId=command_id, InstanceId=instance.instance_id)
            if instance_results["ResponseCode"] != 0:
                raise weaver_exceptions.SsmExecutionError(
                    f"SSM command `{command}` failed on instance {instance.instance_id}: {instance_results['StandardErrorContent']}",
                    return_code=instance_results["ResponseCode"],
                )
            log.debug(f"Result for SSM command ID {command_id} on instance {instance.instance_id}: {command_status}")
            results.append(
                {
                    "instance_id": instance.instance_id,
                    "status": instance_results["Status"],
                    "std_out": instance_results["StandardOutputContent"].strip(),
                }
            )
        return results

    def copy_support_zip_to_s3(self, zip_filenames: Dict[str, str] = {}):
        nodes = self.get_stacknodes()
        for node_ip, zip_filename in zip_filenames.items():
            node_to_action = next(filter(lambda node: node.private_ip_address == node_ip, nodes), None)
            self.run_command(
                [node_to_action],
                f"aws s3 cp --content-type application/x-gzip {zip_filename} s3://{self.get_s3_bucket_name()}/diagnostics/{self.stack_name}/{datetime.now().strftime('%Y-%m-%d')}/",
            )

    ##
    # Stack - helper methods
    #

    # Checks if the drain type is allowed for a given load balancer.
    def drain_type_allowed_for_lb(self, load_balancer: weaver_enums.LoadBalancer, drain_type: weaver_enums.DrainType) -> bool:
        if drain_type == weaver_enums.DrainType.FAST and self.get_param_value(DRAIN_TIMEOUT_PARAM_KEY[load_balancer.value]) == '':
            # Without a CFN parameter for the drain timeout, reliably restoring the stack to its full drain time is difficult.
            log_to_ui(self, ERROR, f'{load_balancer.value} has no CFN parameter for drain timeout, fast drain type not allowed', write_to_changelog=True)
            return False
        return True

    # Checks if the drain type is allowed for the stack.
    def drain_type_allowed(self, drain_type: weaver_enums.DrainType) -> bool:
        drain_type_allowed = True
        if self.has_alb() and not self.drain_type_allowed_for_lb(weaver_enums.LoadBalancer('ALB'), drain_type):
            drain_type_allowed = False
        if self.has_nlb() and not self.drain_type_allowed_for_lb(weaver_enums.LoadBalancer('NLB'), drain_type):
            drain_type_allowed = False
        return drain_type_allowed

    # So long as the stack is tagged with an environment that isn't prod, and has an 'NFSServer' stack resource, create a change set
    # with no parameter updates to check if the NFS server needs replacing.
    def check_nfs_server_replacement(self) -> bool:
        env = self.get_tag('environment', log_msgs=False)
        # Make sure we never execute a replacement on prod stacks, potentially causing unintended downtime.
        # Also if the environment is not tagged, don't proceed.
        if not env:
            log_to_ui(self, INFO, "Skipping NFS server replacement check - stack is not tagged with 'environment'", write_to_changelog=False)
            return False
        if env == 'prod':
            log_to_ui(self, INFO, "Skipping NFS server replacement check - stack is tagged as 'prod'", write_to_changelog=False)
            return False
        if not self.get_stack_resource_by_logical_id('NFSServer'):
            log_to_ui(self, INFO, "Skipping NFS server replacement check - stack does not have 'NFSServer' resource", write_to_changelog=False)
            return False
        log_to_ui(self, INFO, 'Checking if NFS server needs replacing', write_to_changelog=True)
        change_set: CreateChangeSetOutputTypeDef = self.create_change_set(template=None, params=self.get_params(exclude_passwords=True))
        change_set_name: str = change_set['Id'].split('/')[1]
        cfn = boto3.client('cloudformation', region_name=self.region)
        change_set_status = cfn.describe_change_set(ChangeSetName=change_set_name, StackName=self.stack_name)['Status']
        if change_set_status == 'FAILED':
            # Change set generated did not include any changes
            log_to_ui(self, INFO, 'NFS server does not need replacing', write_to_changelog=True)
            return False
        waiter = cfn.get_waiter('change_set_create_complete')
        waiter.wait(ChangeSetName=change_set_name, StackName=self.stack_name, WaiterConfig={'Delay': 5})
        changes = cfn.describe_change_set(ChangeSetName=change_set_name, StackName=self.stack_name)["Changes"]
        for change in changes:
            if change["ResourceChange"]["LogicalResourceId"] == "NFSServer" and change["ResourceChange"]["Replacement"] in {"Conditional", "True"}:
                return True
        log_to_ui(self, INFO, 'NFS server does not need replacing', write_to_changelog=True)
        return False

    # Stops the application, detaches EBS restore volume from application nodes, replaces the NFS server via change set execution.
    # Then optionally reattaches the volume to existing nodes and starts up the application again.
    # Finally waits for the stack update to complete.
    def replace_nfs_server(self, skip_remount: bool = False, skip_app_start: bool = False) -> bool:
        step_log_msg: str = 'Replacing NFS server'
        log_to_ui(self, INFO, f'{step_log_msg}', write_to_changelog=True)
        cfn = boto3.resource("cloudformation", region_name=self.region)
        ec2 = boto3.resource("ec2", region_name=self.region)
        ssm = boto3.client("ssm", region_name=self.region)

        cfn_stack = cfn.Stack(self.stack_name)
        ebs_restore_vol = ec2.Volume(cfn_stack.Resource("EBSRestoreVol").physical_resource_id)
        log_to_ui(self, INFO, f"Found EBS restore volume ID: {ebs_restore_vol.volume_id}", write_to_changelog=False)

        stack_nodes = self.get_stacknodes()
        old_nfs_server = ec2.Instance(cfn_stack.Resource("NFSServer").physical_resource_id)
        log_to_ui(self, INFO, f"Found old NFS server ID: {old_nfs_server.instance_id}", write_to_changelog=False)

        # stop Atlassian service on all app nodes
        log_to_ui(self, INFO, f"Stopping {self.app_type.title()} on all application nodes", write_to_changelog=False)
        self.shutdown_app(stack_nodes)

        # stop cfn-hup on all app nodes
        log_to_ui(self, INFO, "Stopping cfn-hup on all application nodes", write_to_changelog=False)
        self.ssm_wait_response(stack_nodes, "systemctl stop cfn-hup")

        # unmount /media/atl on all app nodes
        log_to_ui(self, INFO, "Un-mounting /media/atl on all application nodes", write_to_changelog=False)
        self.ssm_wait_response(stack_nodes, 'umount /media/atl')

        # stop nfs-server on NFS server node
        log_to_ui(self, INFO, f"Stopping nfs-server on old NFS node ({old_nfs_server.instance_id})", write_to_changelog=False)
        self.ssm_wait_response([old_nfs_server], "systemctl stop nfs-server")

        # unmount /media/atl on NFS server node
        log_to_ui(self, INFO, f"Unmounting /media/atl on NFS node ({old_nfs_server.instance_id})", write_to_changelog=False)
        self.ssm_wait_response([old_nfs_server], "umount /media/atl")

        # detach EBS volume from old NFS server node
        log_to_ui(self, INFO, f"Detaching EBS restore volume {ebs_restore_vol.volume_id} from NFS node ({old_nfs_server.instance_id})", write_to_changelog=False)
        try:
            ebs_restore_vol.detach_from_instance()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == 'IncorrectState' and "is in the 'available' state" in e.response['Error']['Message']:
                log_to_ui(self, INFO, f"Volume {ebs_restore_vol.volume_id} has already been detached, proceeding", write_to_changelog=False)
            else:
                log_to_ui(self, ERROR, f"Error detaching volume: {e.response['Error']['Code']}: {e.response['Error']['Message']}", write_to_changelog=True)
                return False

        # wait for EBS volume to be available
        log_to_ui(self, INFO, f"Waiting for EBS volume {ebs_restore_vol.volume_id} to be available", write_to_changelog=False)
        ebs_available_waiter = ec2.meta.client.get_waiter("volume_available")
        try:
            ebs_available_waiter.wait(VolumeIds=[ebs_restore_vol.volume_id], WaiterConfig={"Delay": 5, "MaxAttempts": 120})
        except (botocore.exceptions.ClientError, botocore.exceptions.WaiterError) as e:
            error_details = f"{e.response['Error']['Code']}: {e.response['Error']['Message']}" if hasattr(e, "response") else e
            log_to_ui(self, ERROR, f"Error waiting for EBS volume to be available: {error_details}", write_to_changelog=True)
            return False
        else:
            log_to_ui(self, INFO, "EBS volume detached successfully", write_to_changelog=False)

        # execute the changeset to replace the NFS server
        log_to_ui(self, INFO, "Creating stack update change set", write_to_changelog=False)
        change_set: CreateChangeSetOutputTypeDef = self.create_change_set(template=None, params=self.get_params(exclude_passwords=True))
        change_set_name: str = change_set['Id'].split('/')[1]
        change_set_create_complete_waiter = cfn.meta.client.get_waiter('change_set_create_complete')
        change_set_create_complete_waiter.wait(ChangeSetName=change_set_name, StackName=self.stack_name, WaiterConfig={'Delay': 5})
        cfn.meta.client.execute_change_set(ChangeSetName=change_set_name, StackName=self.stack_name)
        log_to_ui(self, INFO, "Change set successfully executed", write_to_changelog=False)

        # wait for EBS volume to be attached
        log_to_ui(self, INFO, f"Waiting for EBS volume {ebs_restore_vol.volume_id} to be re-attached", write_to_changelog=False)
        ebs_reattached_waiter = ec2.meta.client.get_waiter("volume_in_use")
        try:
            ebs_reattached_waiter.wait(VolumeIds=[ebs_restore_vol.volume_id], WaiterConfig={"Delay": 5, "MaxAttempts": 120})
        except (botocore.exceptions.ClientError, botocore.exceptions.WaiterError) as e:
            error_details = f"{e.response['Error']['Code']}: {e.response['Error']['Message']}" if hasattr(e, "response") else e
            log_to_ui(self, ERROR, f"Error waiting for EBS volume to be attached: {error_details}", write_to_changelog=True)
            return False

        # find new NFS server
        log_to_ui(self, INFO, "Finding new NFS server from EBS restore volume attachment details", write_to_changelog=False)
        ebs_restore_vol.reload()
        log.debug(f"EBS restore volume attachment details: {ebs_restore_vol.attachments}")
        new_nfs_server_instance_id = ebs_restore_vol.attachments[0]["InstanceId"]
        if not new_nfs_server_instance_id:
            log_to_ui(self, ERROR, f"EBS volume {ebs_restore_vol.volume_id} is not attached?", write_to_changelog=True)
            return False
        new_nfs_server = ec2.Instance(new_nfs_server_instance_id)
        log_to_ui(self, INFO, f"Found new NFS server ID: {new_nfs_server.instance_id}", write_to_changelog=False)

        # wait for new NFS server node to be "running"
        log_to_ui(self, INFO, f'Waiting for new NFS server node {new_nfs_server.instance_id} to be in the "running" EC2 state', write_to_changelog=False)
        try:
            new_nfs_server.wait_until_running()
        except (botocore.exceptions.ClientError, botocore.exceptions.WaiterError) as e:
            error_details = f"{e.response['Error']['Code']}: {e.response['Error']['Message']}" if hasattr(e, "response") else e
            log_to_ui(self, ERROR, f'Error waiting for NFS server to be in "running" status: {error_details}', write_to_changelog=True)
            return False
        new_nfs_server.reload()
        log_to_ui(self, INFO, f"New NFS server EC2 instance status: {new_nfs_server.state['Name']}", write_to_changelog=False)

        # wait for SSM to consider the node to be "online" and ready to receive commands
        nfs_server_ssm_connection_status: str = "offline"
        log_to_ui(self, INFO, f'Waiting for new NFS server node {new_nfs_server.instance_id} be "online" (ready to receive SSM commands)', write_to_changelog=False)
        while nfs_server_ssm_connection_status != "online":
            time.sleep(3)
            try:
                nfs_server_ssm_connection_status = ssm.describe_instance_information(Filters=[{"Key": "InstanceIds", "Values": [new_nfs_server.instance_id]}])[
                    "InstanceInformationList"
                ][0]["PingStatus"].lower()
            except botocore.exceptions.ClientError as e:
                error_details = f"{e.response['Error']['Code']}: {e.response['Error']['Message']}" if hasattr(e, "response") else e
                log_to_ui(self, ERROR, f'Error waiting for NFS server to be "online": {error_details}', write_to_changelog=True)
            except IndexError:
                pass  # InstanceInformationList is always returned by describe_instance_information, but will be empty if the instance isn't listable yet
        log_to_ui(self, INFO, f"NFS server SSM connection status: {nfs_server_ssm_connection_status}", write_to_changelog=False)

        # wait for nfs-server to be running on new NFS server node
        nfs_server_status: str = "inactive"
        log_to_ui(
            self, INFO, f"Waiting for nfs-server service to be running/active on node {new_nfs_server.instance_id} ({new_nfs_server.private_ip_address})", write_to_changelog=False
        )
        while nfs_server_status != "active":
            time.sleep(3)
            try:
                nfs_server_status = self.ssm_wait_response([new_nfs_server], "systemctl is-active nfs-server")[0]["std_out"].strip()
            except weaver_exceptions.SsmExecutionError as e:
                if e.return_code == 3:
                    pass  # systemctl is-active returns 3 for inactive or unknown service
                else:
                    raise  # re-raise any other SsmExecutionError
        log.debug(f"Status of nfs-server service: {nfs_server_status}")
        log_to_ui(self, INFO, f"Status of nfs-server service: {nfs_server_status}", write_to_changelog=False)

        if not skip_remount:
            # replace NFS server IP in /etc/fstab on all app nodes
            log_to_ui(self, INFO, "Replacing NFS server IP address in /etc/fstab on all application nodes", write_to_changelog=False)
            self.ssm_wait_response(
                stack_nodes,
                rf"sed --in-place --regexp-extended 's/^([0-9]+\.?){{4}}:\/media\/atl/{new_nfs_server.private_ip_address}:\/media\/atl/' /etc/fstab",
            )

            # mount /media/atl on all app nodes
            log_to_ui(self, INFO, "Mounting /media/atl on all application nodes", write_to_changelog=False)
            self.ssm_wait_response(stack_nodes, "mount -a")

        if not skip_app_start:
            # start Atlassian service on all nodes
            log_to_ui(self, INFO, "Starting up {self.app_type} application", write_to_changelog=False)
            self.startup_app(stack_nodes)

            # start cfn-hup on all app nodes
            log_to_ui(self, INFO, "Starting cfn-hup on all instances", write_to_changelog=False)
            self.ssm_wait_response(stack_nodes, "systemctl start cfn-hup")

        # wait for stack to be in status UPDATE_COMPLETE
        cfn_stack.reload()
        log_to_ui(self, INFO, "Waiting for stack update to complete", write_to_changelog=False)
        stack_update_complete_waiter = cfn.meta.client.get_waiter('stack_update_complete')
        stack_update_complete_waiter.wait(StackName=self.stack_name, WaiterConfig={'Delay': 5, 'MaxAttempts': 240})
        log_to_ui(self, INFO, "Stack update is complete", write_to_changelog=False)
        log_to_ui(self, INFO, f"{step_log_msg} - complete", write_to_changelog=False)
        return True

    def spindown_to_zero_appnodes(self, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT):
        if drain_type == weaver_enums.DrainType.DEFAULT and not self.stack_deregistration_delays_are_full_value():
            raise weaver_exceptions.StackDeregistrationDelaysNotFullValueException
        cfn = boto3.client('cloudformation', region_name=self.region)
        spindown_params = self.get_spindown_to_zero_appnodes_paramlist()
        if drain_type == weaver_enums.DrainType.FAST:
            self.modify_deregistration_delays_to_fast_drain_value()
        log_to_ui(self, INFO, f'Spinning {self.stack_name} stack down to 0 nodes', write_to_changelog=True)
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            cfn.update_stack(
                StackName=self.stack_name, Parameters=spindown_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token
            )
        except Exception as e:
            if 'No updates are to be performed' in e.args[0]:
                log_to_ui(self, INFO, 'Stack is already at 0 nodes', write_to_changelog=True)
                return True
            else:
                log.exception('An error occurred spinning down to 0 nodes')
                log_to_ui(self, ERROR, f'An error occurred spinning down to 0 nodes: {e}', write_to_changelog=True)
                return False
        if self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            log_to_ui(self, INFO, 'Successfully spun down to 0 nodes', write_to_changelog=True)
            return True
        return False

    def spinup_to_one_appnode(self, new_version, alt_download_url=''):
        log_to_ui(self, INFO, "Spinning stack up to one app node", write_to_changelog=True)
        # for connie 1 app node and 1 synchrony
        cfn = boto3.client('cloudformation', region_name=self.region)
        spinup_params: List[ParameterTypeDef] = self.get_spinup_to_one_appnode_paramlist()
        if any(param for param in spinup_params if param['ParameterKey'] == 'ProductVersion'):
            spinup_params = self.update_paramlist(spinup_params, 'ProductVersion', new_version)
        else:
            spinup_params = self.update_paramlist(spinup_params, f'{self.get_app_type().title()}Version', new_version)
        spinup_params = self.update_paramlist(spinup_params, 'ProductDownloadUrl', alt_download_url)
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            update_stack = cfn.update_stack(
                StackName=self.stack_name, Parameters=spinup_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token
            )
        except botocore.exceptions.ClientError as e:
            log_to_ui(self, INFO, f'Stack spinup failed: {e}', write_to_changelog=True)
            return False
        if not self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            return False
        log_to_ui(self, INFO, 'Spun up to 1 node, waiting for service to respond', write_to_changelog=False)
        if not self.validate_service_responding():
            return False
        if not self.validate_all_stack_nodes_product_versions(new_version):
            return False
        log_to_ui(self, INFO, f'Updated stack: {update_stack}', write_to_changelog=True)
        return True

    # Sets the node count parameters for each AZ to 1, and updates the product version/download URL all in the same change set.
    # Then waits for the service to be responding.
    def spinup_to_one_appnode_per_az(self, app_type, new_version, alt_download_url=''):
        if not self.preupgrade_az_node_counts:
            log_to_ui(self, INFO, "Could not retrieve pre-upgrade AZ node counts", write_to_changelog=True)
            return False
        log_to_ui(self, INFO, "Spinning stack up to 1 app node per AZ needed", write_to_changelog=True)
        cfn = boto3.client('cloudformation', region_name=self.region)
        spinup_params: List[ParameterTypeDef] = self.get_params()
        for az_node_count_param, az_node_count_value in self.preupgrade_az_node_counts.items():
            if az_node_count_value > 0:
                spinup_params = self.update_paramlist(spinup_params, az_node_count_param, '1')
        if any(param for param in spinup_params if param['ParameterKey'] == 'ProductVersion'):
            spinup_params = self.update_paramlist(spinup_params, 'ProductVersion', new_version)
        else:
            spinup_params = self.update_paramlist(spinup_params, f'{app_type.title()}Version', new_version)
        spinup_params = self.update_paramlist(spinup_params, 'ProductDownloadUrl', alt_download_url)
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            update_stack = cfn.update_stack(
                StackName=self.stack_name, Parameters=spinup_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token
            )
        except botocore.exceptions.ClientError as e:
            log_to_ui(self, INFO, f'Stack spinup failed: {e}', write_to_changelog=True)
            return False
        if not self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            return False
        log_to_ui(self, INFO, 'Spun up to 1 node per AZ needed, waiting for service to respond', write_to_changelog=False)
        if not self.validate_service_responding():
            return False
        log_to_ui(self, INFO, f'Updated stack: {update_stack}', write_to_changelog=True)
        return True

    # This method assumes the ALB is tied to the target group which appears first in the node's auto scaling group's list of target groups
    @cache.cached(timeout=10, make_cache_key=lambda stack, node: f"get_node_registration_state_alb/{stack.stack_name}/{node.id}")
    def get_node_registration_state_alb(self, node) -> Union[str, Literal[False]]:
        app_type = self.get_tag('product', log_msgs=False)
        # Immediately return False for cases we expect the node to not be part of the ALB
        if app_type and 'mesh' in app_type:
            return False
        if node.is_backup_node():
            return False
        # Find the ALB target group from the node's auto scaling group info, then find it's registration state in that target group.
        elbv2 = boto3.client('elbv2', region_name=self.region)
        node_registration_state: str | Literal[False] = False
        try:
            target_group_arn = node.get_auto_scaling_group()['TargetGroupARNs'][0]
        except IndexError as e:
            log.exception(f'Error occurred finding ALB target group for {node.id}')
            log_to_ui(self, ERROR, f'Error occurred finding ALB target group for {node.id}: {e}', write_to_changelog=False)
            log_to_ui(self, WARN, "If this is a backup node, ensure it is tagged 'node_type' with value containing 'backup', or unexpected behaviour may occur")
            return False
        try:
            response = elbv2.describe_target_health(TargetGroupArn=target_group_arn, Targets=[{'Id': node.id}])
            if 'TargetHealthDescriptions' in response:
                node_registration_state = response['TargetHealthDescriptions'][0]['TargetHealth']['State']
                if node_registration_state == 'unused' and response['TargetHealthDescriptions'][0]['TargetHealth']['Reason'] == 'Target.NotRegistered':
                    node_registration_state = 'notregistered'
        except Exception as e:
            log.exception(f'Error occurred getting node registration state for {node.id}')
            log_to_ui(self, ERROR, f'Error occurred getting node registration state for {node.id}: {e}', write_to_changelog=False)
        return node_registration_state

    @cache.cached(timeout=10, make_cache_key=lambda stack, node: f"get_node_registration_state_nlb/{stack.stack_name}/{node.id}")
    def get_node_registration_state_nlb(self, node) -> Union[str, Literal[False]]:
        app_type = self.get_tag('product', log_msgs=False)
        # Immediately return False for cases we expect the node to not be part of the NLB
        if app_type and not (app_type == 'bitbucket' or 'mirror' in app_type):
            return False
        if node.is_backup_node():
            return False
        # Find the NLB target group from the node's auto scaling group info, then find it's registration state in that target group.
        elbv2 = boto3.client('elbv2', region_name=self.region)
        node_registration_state: str | Literal[False] = False
        target_group_arns: List[str] = node.get_auto_scaling_group()['TargetGroupARNs']
        try:
            nlb_ssh_target_group_arn: str = list(filter(lambda arn: arn.split('-')[-1][:3] == 'ssh', target_group_arns))[0]
        except IndexError as e:
            log.exception(f'Error occurred finding NLB target group for {node.id}')
            log_to_ui(self, ERROR, f'Error occurred finding NLB target group for {node.id}: {e}', write_to_changelog=False)
            log_to_ui(self, WARN, "If this is a backup node, ensure it is tagged 'node_type' with value containing 'backup', or unexpected behaviour may occur")
            return False
        try:
            response = elbv2.describe_target_health(TargetGroupArn=nlb_ssh_target_group_arn, Targets=[{'Id': node.id}])
            if 'TargetHealthDescriptions' in response:
                node_registration_state = response['TargetHealthDescriptions'][0]['TargetHealth']['State']
                if node_registration_state == 'unused' and response['TargetHealthDescriptions'][0]['TargetHealth']['Reason'] == 'Target.NotRegistered':
                    node_registration_state = 'notregistered'
        except Exception as e:
            log.exception(f'Error occurred getting node NLB registration state for {node.id}')
            log_to_ui(self, ERROR, f'Error occurred getting node NLB registration state for {node.id}: {e}', write_to_changelog=False)
        return node_registration_state

    # Returns True once the registration states of all nodes in their ALB / NLB (if they exist for the stack) load balancers match a desired state
    def wait_nodes_registration_states(self, nodes: List[Instance], desired_state: str) -> bool:
        # Filter out any backup nodes, which will never be registered to load balancers.
        backup_nodes, nodes_to_check = self.split_backup_from_non_backup_nodes(nodes)
        if len(backup_nodes) > 0:
            log_to_ui(self, INFO, f"Skipping registration states check for backup node(s) {backup_nodes} (these are never registered to load balancers)", write_to_changelog=False)
        if len(nodes_to_check) == 0:
            log_to_ui(self, INFO, 'No nodes require checking registration states', write_to_changelog=False)
            return True
        log_to_ui(self, INFO, f'Waiting for nodes {nodes_to_check} to report {desired_state} in their load balancers', write_to_changelog=False)
        node_state_results = {}  # Stores True/False whether each node's current state in each of its load balancers matches its desired
        for node in nodes_to_check:
            registration_state_alb = self.get_node_registration_state_alb(node)
            if registration_state_alb:
                node_state_results[(node.id, 'ALB')] = registration_state_alb == desired_state
            registration_state_nlb = self.get_node_registration_state_nlb(node)
            if registration_state_nlb:
                node_state_results[(node.id, 'NLB')] = registration_state_nlb == desired_state
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['node_registration_deregistration']
        while not all(node_state_results.values()):
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f"Nodes {nodes_to_check} are not all in desired state '{desired_state}' after {format_timespan(action_timeout)}. Node registration state results are : {node_state_results}",
                    write_to_changelog=True,
                )
                return False
            if 'TESTING' not in current_app.config or current_app.config['TESTING'] is False:
                time.sleep(10)
            for node in nodes_to_check:
                if (node.id, 'ALB') in node_state_results:
                    node_state_results[(node.id, 'ALB')] = self.get_node_registration_state_alb(node) == desired_state
                if (node.id, 'NLB') in node_state_results:
                    node_state_results[(node.id, 'NLB')] = self.get_node_registration_state_nlb(node) == desired_state
        log_to_ui(self, INFO, f'Nodes {nodes_to_check} in all their target groups are in desired state {desired_state}', write_to_changelog=False)
        return True

    # High-level method that fully deregisters a node and ensures its in a 'notregistered' state before moving on.
    def deregister_node(self, node: Instance, fast_drain: bool = False) -> bool:
        if not self.begin_deregistering_node_from_all_target_groups(node, fast_drain):
            log_to_ui(self, ERROR, f'Failed to begin draining node {node.id} ({node.private_ip_address})', write_to_changelog=True)
            return False
        if not self.wait_nodes_registration_states([node], 'notregistered'):
            log_to_ui(self, ERROR, f'Failed to wait for node {node.id} ({node.private_ip_address}) to be deregistered', write_to_changelog=True)
            return False
        return True

    # Registers all stack nodes to the target groups associated with their Autoscaling Group
    def register_all_stack_nodes(self) -> bool:
        log_to_ui(self, INFO, 'Registering all stack nodes to their target groups', write_to_changelog=True)
        nodes = self.get_stacknodes()
        for node in nodes:
            if not self.begin_registering_node_to_all_target_groups(node):
                return False
        if not self.wait_all_healthy_targets():
            return False
        return True

    # Registers a node to the target groups associated with their Autoscaling Group
    def begin_registering_node_to_all_target_groups(self, node: Instance) -> bool:
        target_group_arns: List[str] = node.get_auto_scaling_group()['TargetGroupARNs']  # type: ignore[attr-defined]
        for target_group_arn in target_group_arns:
            try:
                log_to_ui(self, INFO, f'Beginning registration of node {node.id} ({node.private_ip_address}) to target group {target_group_arn}', write_to_changelog=False)
                elbv2 = boto3.client('elbv2', region_name=self.region)
                elbv2.register_targets(TargetGroupArn=target_group_arn, Targets=[{'Id': node.id}])
            except Exception as e:
                log.exception(f'Node {node.id} ({node.private_ip_address}) failed to register')
                log_to_ui(self, ERROR, f'Node {node.id} ({node.private_ip_address}) failed to register: {e}', write_to_changelog=True)
                return False
        return True

    # Registers a select group of nodes to the target groups associated with their Auto Scaling Groups
    def begin_registering_nodes_to_all_target_groups(self, nodes: List[Instance]) -> bool:
        backup_nodes, nodes_to_register = self.split_backup_from_non_backup_nodes(nodes)
        if len(backup_nodes) > 0:
            log_to_ui(self, INFO, f'Skipping registration of backup node(s) {backup_nodes} (these are never registered to load balancers)', write_to_changelog=False)
        if len(nodes_to_register) == 0:
            log_to_ui(self, INFO, 'No nodes require registration', write_to_changelog=False)
            return True
        for node in nodes_to_register:
            if not self.begin_registering_node_to_all_target_groups(node):
                return False
        return True

    # Begins deregistering a node from all target groups associated with it's Autoscaling group it is part of
    # Also checks if we aren't fast draining the node, that the deregistration delay for the stack's load balancers are
    # at their full value.
    def begin_deregistering_node_from_all_target_groups(self, node: Instance, fast_drain: bool = False) -> bool:
        if not fast_drain and not self.stack_deregistration_delays_are_full_value():
            raise weaver_exceptions.StackDeregistrationDelaysNotFullValueException
        target_group_arns: List[str] = node.get_auto_scaling_group()['TargetGroupARNs']  # type: ignore[attr-defined]
        for target_group_arn in target_group_arns:
            if not self.begin_deregistering_node_from_target_group(node, target_group_arn, fast_drain):
                return False
        return True

    # Begins deregistering a node from a target group
    def begin_deregistering_node_from_target_group(self, node: Instance, target_group_arn: str, fast_drain: bool = False) -> bool:
        log_to_ui(self, INFO, f'Beginning deregistration of node {node.id} ({node.private_ip_address}) from target group {target_group_arn}', write_to_changelog=False)
        elbv2 = boto3.client('elbv2', region_name=self.region)
        if fast_drain:
            self.modify_deregistration_delays_to_fast_drain_value()
        try:
            elbv2.deregister_targets(TargetGroupArn=target_group_arn, Targets=[{'Id': node.id}])
        except Exception as e:
            if hasattr(e, 'response') and e.response['Error']['Code'] == 'ValidationError':
                log_to_ui(self, INFO, f'Node {node.id} ({node.private_ip_address}) is not in target group {target_group_arn}, continuing', write_to_changelog=False)
                return True
            log.exception(f'Node {node.id} ({node.private_ip_address}) failed to start deregistering from target group {target_group_arn}')
            log_to_ui(self, ERROR, f'Node {node.id} ({node.private_ip_address}) failed to start deregistering from target group {target_group_arn}: {e}', write_to_changelog=True)
            return False
        log_to_ui(self, INFO, f'Node {node.id} ({node.private_ip_address}) is deregistering from target group {target_group_arn}', write_to_changelog=False)
        return True

    # Begins deregistering a group of nodes from all of their Autoscaling Groups' target groups
    def begin_deregistering_nodes_from_all_target_groups(self, nodes: list[Instance], fast_drain: bool = False):
        backup_nodes, nodes_to_deregister = self.split_backup_from_non_backup_nodes(nodes)
        if len(backup_nodes) > 0:
            log_to_ui(self, INFO, f"Skipping deregistration of backup node(s) {backup_nodes} (these are never registered to load balancers)", write_to_changelog=False)
        if len(nodes_to_deregister) == 0:
            log_to_ui(self, INFO, 'No nodes require deregistration', write_to_changelog=False)
            return True
        for node in nodes_to_deregister:
            if not self.begin_deregistering_node_from_all_target_groups(node, fast_drain):
                return False
        return True

    # High-level method that fully registers a node and ensures its in a 'healthy' state before moving on.
    def register_node(self, node: Instance) -> bool:
        if not self.begin_registering_node_to_all_target_groups(node):
            return False
        if not self.wait_nodes_registration_states([node], 'healthy'):
            return False
        return True

    # Gets the deregistration delay for a specific target group.
    def get_deregistration_delay(self, target_group_arn: str) -> int:
        elbv2 = boto3.client('elbv2', region_name=self.region)
        deregistration_delay_attr_key: str = 'deregistration_delay.timeout_seconds'
        try:
            tg_attributes: List[TargetGroupAttributeTypeDef] = elbv2.describe_target_group_attributes(TargetGroupArn=target_group_arn)['Attributes']
        except botocore.exceptions.ClientError as e:
            log_to_ui(self, ERROR, f'Error describing attributes for target group {target_group_arn}: {e}')
            raise e
        deregistration_delay: int = next(int(attr['Value']) for attr in tg_attributes if attr['Key'] == deregistration_delay_attr_key)
        return deregistration_delay

    # Modifies the deregistration delay for a target group to a target value.
    def modify_deregistration_delay(self, target_group_arn: str, new_deregistration_delay: int):
        elbv2 = boto3.client('elbv2', region_name=self.region)
        deregistration_delay_attr_key: str = 'deregistration_delay.timeout_seconds'
        attr_to_modify: List[TargetGroupAttributeTypeDef] = [{'Key': deregistration_delay_attr_key, 'Value': str(new_deregistration_delay)}]
        try:
            elbv2.modify_target_group_attributes(TargetGroupArn=target_group_arn, Attributes=attr_to_modify)
        except botocore.exceptions.ClientError as e:
            log_to_ui(self, ERROR, f'Error modifying deregistration delay to {new_deregistration_delay}s for target group {target_group_arn}: {e}', write_to_changelog=True)
            log_to_ui(
                self,
                WARN,
                'Please manually double check the stack load balancer deregistration delays are their expected values (specified in the CFN template)',
                write_to_changelog=False,
            )
            raise e

    # Checks if the deregistration delay for a load balancer is at its 'full' drain value
    # We consider the 'full' drain value as the value specified as a CFN parameter.
    def lb_deregistration_delay_is_full_value(self, load_balancer: weaver_enums.LoadBalancer) -> bool:
        current_deregistration_delay: int = self.get_deregistration_delay(self.get_load_balancer_tg_arn(load_balancer))
        try:
            cfn_set_delay: int = self.get_cfn_param_deregistration_delay(load_balancer)
        except weaver_exceptions.StackParameterNotFound:
            # If no CFN param is set, then we have no point of reference so assume it is full to avoid blocking actions.
            return True
        if current_deregistration_delay != cfn_set_delay:
            log_to_ui(
                self,
                ERROR,
                f'Current deregistration delay for {load_balancer.value} is {current_deregistration_delay}s which does not match expected value {cfn_set_delay}s',
                write_to_changelog=True,
            )
            return False
        return True

    # Checks if the deregistration delays for all load balancers in the stack are their 'full' drain value
    # We consider the 'full' drain value as either the value specified as a CFN parameter, or if that doesn't exist
    # do a best effort check of if it's not set to the fast drain value.
    def stack_deregistration_delays_are_full_value(self) -> bool:
        deregistration_delays_are_full_value: bool = True
        if self.has_alb() and not self.lb_deregistration_delay_is_full_value(weaver_enums.LoadBalancer.ALB):
            deregistration_delays_are_full_value = False
            # We want to log all load balancers with incorrect deregistration delays so don't return False here.
        if self.has_nlb() and not self.lb_deregistration_delay_is_full_value(weaver_enums.LoadBalancer.NLB):
            deregistration_delays_are_full_value = False
            # We want to log all load balancers with incorrect deregistration delays so don't return False here.
        if not deregistration_delays_are_full_value:
            log_to_ui(self, INFO, 'Either manually change back to the full value(s), or wait for any existing fast drain actions to complete', write_to_changelog=False)
            return False
        return True

    # Checks if a target group's deregistration delay is above the fast drain threshold.
    def check_target_group_deregistration_delay_above_fast_threshold(self, target_group_arn: str) -> bool:
        current_deregistration_delay: int = self.get_deregistration_delay(target_group_arn)
        return current_deregistration_delay > self.fast_deregistration_delay

    # Modifies all stack load balancer deregistration delays to the 'fast' drain value.
    # Note: this is intentionally designed to possibly execute when delays are already at the fast value to save AWS API calls.
    def modify_deregistration_delays_to_fast_drain_value(self):
        if self.has_alb():
            alb_tg_arn: str = self.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.ALB)
            self.modify_deregistration_delay(alb_tg_arn, self.fast_deregistration_delay)
        if self.has_nlb():
            nlb_tg_arn: str = self.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.NLB)
            self.modify_deregistration_delay(nlb_tg_arn, self.fast_deregistration_delay)

    # Restore the deregistration delay for a load balancer
    # This intentionally might be called when no restoring is needed, to allow for better code design
    def restore_lb_deregistration_delay_if_needed(self, load_balancer: weaver_enums.LoadBalancer):
        try:
            cfn_set_delay: int = self.get_cfn_param_deregistration_delay(load_balancer)
        except weaver_exceptions.StackParameterNotFound:
            # Fast drain actions should have been blocked anyway so Weaver should not have modified the deregistration delay, so nothing to do here
            return
        tg_arn: str = self.get_load_balancer_tg_arn(load_balancer)
        self.modify_deregistration_delay(tg_arn, cfn_set_delay)

    # Restores the original deregistration delay values for all of the stack's load balancers
    # This intentionally might be called when no restoring is needed, to allow for better code design
    def restore_original_deregistration_delays_if_needed(self):
        if self.has_alb():
            self.restore_lb_deregistration_delay_if_needed(weaver_enums.LoadBalancer.ALB)
        if self.has_nlb():
            self.restore_lb_deregistration_delay_if_needed(weaver_enums.LoadBalancer.NLB)

    # Returns two separate lists of backup and non-backup nodes
    def split_backup_from_non_backup_nodes(self, nodes: list[Instance]) -> tuple[list[Instance], list[Instance]]:
        backup_nodes: list[Instance] = []
        non_backup_nodes: list[Instance] = []
        for node in nodes:
            if node.is_backup_node():  # type: ignore[attr-defined]
                backup_nodes.append(node)
            else:
                non_backup_nodes.append(node)
        return backup_nodes, non_backup_nodes

    def get_index_health(self, node, personal_access_token):
        stack = boto3.client('cloudformation', self.region).describe_stacks(StackName=self.stack_name)['Stacks'][0]
        port = [param['ParameterValue'] for param in stack['Parameters'] if param['ParameterKey'] == 'TomcatDefaultConnectorPort'][0]
        context_path = [param['ParameterValue'] for param in stack['Parameters'] if param['ParameterKey'] == 'TomcatContextPath'][0]
        self.session = BaseUrlSession(base_url=f'http://{node}:{port}{context_path}/')
        self.session.headers = {
            'Authorization': f'Bearer {personal_access_token}',
        }
        try:
            response = self.session.get('rest/indexanalyzer/1/state?maxResults=1')
            if response.ok:
                index_data = json.loads(response.text)
                return float(index_data['indexHealth'])
            if response.status_code == 503:
                # Jira is in Maintenance, ie reindexing or restoring the index from snapshot
                return 503
            else:
                error_msg = f'Something went wrong: {response.status_code} {response.reason} '
                if response.status_code == 401:
                    error_msg += 'The credentials you entered are incorrect or do not have administrator access'
                elif response.status_code == 404:
                    error_msg += 'The origin server did not find a current representation for the target resource or is not willing to disclose that one exists'
                log_to_ui(self, ERROR, f"Index health on node {node} could not be retrieved: {error_msg}", write_to_changelog=False)
                return error_msg
        except requests.exceptions.ConnectionError:
            return 'Node could not be contacted'
        except (KeyError, ValueError):
            error_msg = index_data['indexHealth'] if 'indexHealth' in index_data else index_data
            log_to_ui(self, ERROR, f"Index health on node {node} could not be retrieved: {error_msg}", write_to_changelog=False)
            return error_msg
        except Exception:
            raise

    def get_index_health_all_nodes(self, personal_access_token):
        nodes_and_health = []
        for node in self.get_stacknodes():
            health = self.get_index_health(node.private_ip_address, personal_access_token)
            nodes_and_health.append({'node_ip': node.private_ip_address, 'index_health': health})
        return nodes_and_health

    def wait_stack_action_complete(self, in_progress_state, client_request_token):
        log_to_ui(self, INFO, 'Waiting for stack action to complete', write_to_changelog=False)
        try:
            self.stack_id = boto3.client('cloudformation', self.region).describe_stacks(StackName=self.stack_name)['Stacks'][0]['StackId']
            stack_state = self.check_stack_state()
        except ClientError as e:
            if 'does not exist' in e.response['Error']['Message'] and in_progress_state == "DELETE_IN_PROGRESS":
                return True
            else:
                log.exception('Error getting stack state')
                log_to_ui(self, ERROR, f'Error getting stack state: {e}', write_to_changelog=True)
        logged_events: list[str] = []
        logged_events = self.get_event_logs(client_request_token, logged_events)
        while 'IN_PROGRESS' in stack_state or stack_state in (in_progress_state, 'throttled'):
            time.sleep(10)
            stack_state = self.check_stack_state()
            logged_events = self.get_event_logs(client_request_token, logged_events)
        if 'ROLLBACK' in stack_state:
            log_to_ui(self, ERROR, f'Stack action was rolled back: {stack_state}', write_to_changelog=True)
            return False
        elif 'FAILED' in stack_state:
            log_to_ui(self, ERROR, f'Stack action failed: {stack_state}', write_to_changelog=False)
            return False
        self.get_event_logs(client_request_token, logged_events)
        return True

    def validate_service_responding(self):
        app_type = self.get_tag('product', log_msgs=False)
        if app_type and 'mesh' in app_type:
            log_to_ui(self, INFO, 'Mesh service will be available once the nodes report SERVING (or UNIMPLEMENTED for Mesh < 1.4)', write_to_changelog=False)
            return self.wait_all_serving_mesh_nodes()
        log_to_ui(self, INFO, 'Waiting for service to reply on /status', write_to_changelog=False)
        service_state = self.check_service_status()
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['validate_service_responding']
        while service_state not in READY_TO_SERVE_STATUSES:
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f'{self.stack_name} failed to come up after {format_timespan(action_timeout)}. ' f'Status endpoint is returning: {service_state}',
                    write_to_changelog=True,
                )
                return False
            time.sleep(60)
            service_state = self.check_service_status()
        self.get_service_url()
        log_to_ui(self, INFO, f'{self.service_url}status is now reporting {service_state}', write_to_changelog=True)
        return True

    # Doesn't do any product type checks - just computes true / false if the versions are equal
    # for stack version >= 10, or node version is 4 major versions ahead for stack version < 10
    def jsmVersionEquality(self, node_version: str, stack_version: str) -> bool:
        node_major_version, _, node_version_suffix = node_version.partition(".")
        node_major_version = int(node_major_version)
        if node_major_version >= 10:
            # Versions for JSM and JSW are aligned from 10 onwards
            return node_version == stack_version
        # Otherwise ensure node version is 4 major versions ahead
        stack_major_version, _, stack_version_suffix = stack_version.partition(".")
        stack_major_version = int(stack_major_version)
        return node_major_version == stack_major_version + 4 and node_version_suffix == stack_version_suffix

    # Check if a one or more nodes' product version matches the version we expect them to be running on
    def validate_nodes_product_version(self, nodes: List[Instance], expected_version: str) -> bool:
        incorrect_nodes: List[Instance] = []
        jira_product: str = self.get_param_value("JiraProduct") if self.get_app_type() == "jira" else ""
        for node in nodes:
            node_product_version: str = self.check_instance_product_version(node)
            if not node_product_version:
                # Either node product version is unimplemented for this product or there was an issue retrieving it
                # Just skip this check for this node to avoid blocking further steps
                continue
            if jira_product == "ServiceDesk":
                if not self.jsmVersionEquality(node_product_version, expected_version):
                    incorrect_nodes.append(node)
            elif node_product_version != expected_version:
                incorrect_nodes.append(node)
        if len(incorrect_nodes) > 0:
            log_to_ui(self, ERROR, f"Node(s) are running on an unexpected version: {[node.private_ip_address for node in incorrect_nodes]}", write_to_changelog=True)
            log_to_ui(self, ERROR, "Please check node startup logs", write_to_changelog=True)
            return False
        return True

    def validate_all_stack_nodes_product_versions(self, expected_version: str) -> bool:
        # This may get called when spinning nodes up, so we need to make sure we aren't pulling a stale list of stack nodes
        stack_nodes: List[Instance] = self.get_stacknodes(force=True)
        return self.validate_nodes_product_version(stack_nodes, expected_version)

    def validate_node_responding(self, node, primary_stack=False):
        log_to_ui(self, INFO, f'Waiting for node {node.private_ip_address} to reply to healthcheck', write_to_changelog=False)
        result = self.check_node_status(node, primary_stack)
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['validate_node_responding']
        while result not in READY_TO_SERVE_STATUSES:
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f'{node.private_ip_address} failed to come up after {format_timespan(action_timeout)}. Status endpoint is returning: {result}',
                    write_to_changelog=True,
                )
                return False
            result = self.check_node_status(node, primary_stack)
            time.sleep(10)
        log_to_ui(self, INFO, f'Startup result for {node.private_ip_address}: {result}', write_to_changelog=False)
        return True

    # Periodically checks if a select group of nodes are reporting ready to serve statuses. Errors after a configured timeout if not all nodes are ready.
    def validate_nodes_responding(self, nodes: List[Instance], primary_stack: bool = False) -> bool:
        step_log_msg: str = f'Validating nodes {nodes} are responding'
        log_to_ui(self, INFO, f'{step_log_msg}', write_to_changelog=False)
        node_ready_results = {node.id: self.check_node_status(node, primary_stack) in READY_TO_SERVE_STATUSES for node in nodes}
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['validate_node_responding']
        while not all(node_ready_results.values()):
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f'Some nodes failed to come up after {format_timespan(action_timeout)}. Current status: {node_ready_results}',
                    write_to_changelog=True,
                )
                return False
            time.sleep(60)
            node_ready_results = {node.id: self.check_node_status(node, primary_stack) in READY_TO_SERVE_STATUSES for node in nodes}
        log_to_ui(self, INFO, f'{step_log_msg} - complete', write_to_changelog=False)
        return True

    # Periodically checks if all stack nodes are reporting ready to serve statuses. Errors after a configured timeout if not all nodes are ready.
    def validate_all_stack_nodes_responding(self):
        log_to_ui(self, INFO, 'Validating all stack nodes are responding', write_to_changelog=False)
        nodes: list[Instance] = self.get_stacknodes()
        node_ready_results = {node.id: self.check_node_status(node) in READY_TO_SERVE_STATUSES for node in nodes}
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['validate_node_responding']
        while not all(node_ready_results.values()):
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f'Some nodes failed to come up after {format_timespan(action_timeout)}. Current status: {node_ready_results}',
                    write_to_changelog=True,
                )
                return False
            time.sleep(60)
            node_ready_results = {node.id: self.check_node_status(node) in READY_TO_SERVE_STATUSES for node in nodes}
        log_to_ui(self, INFO, 'Validating all stack nodes are responding - complete', write_to_changelog=False)
        return True

    def wait_all_healthy_targets(self):
        log_to_ui(self, INFO, 'Waiting for registered targets in each target group to report healthy status', write_to_changelog=False)
        stack_asgs = self.get_stack_asgs(force=True)
        target_group_arns = set(tg_arn for asg in stack_asgs for tg_arn in asg['TargetGroupARNs'])
        tg_health_results = {tg_arn: self.check_all_healthy_targets(tg_arn) for tg_arn in target_group_arns}
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['validate_node_responding']
        while not all(tg_health_results.values()):
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f'Target group(s) failed to register all targets as "healthy" after {format_timespan(action_timeout)}. Current status: {tg_health_results}',
                    write_to_changelog=True,
                )
                return False
            time.sleep(60)
            for tg_arn in target_group_arns:
                tg_health_results[tg_arn] = self.check_all_healthy_targets(tg_arn)
        log_to_ui(self, INFO, 'All registered targets in all target groups are healthy', write_to_changelog=False)
        return True

    def wait_all_serving_mesh_nodes(self):
        # force getting stack nodes to ensure we have an up-to-date list
        node_status_results = {node.id: self.check_node_status(node).upper() == 'SERVING' for node in self.get_stacknodes(force=True)}
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['validate_node_responding']
        while not all(node_status_results.values()):
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f'Mesh failed to detect all nodes as "SERVING" after {format_timespan(action_timeout)}. Current status: {node_status_results}',
                    write_to_changelog=True,
                )
                return False
            time.sleep(60)
            node_status_results = {node.id: self.check_node_status(node).upper() == 'SERVING' for node in self.get_stacknodes(force=True)}
        log_to_ui(self, INFO, 'All mesh nodes are "SERVING"', write_to_changelog=False)
        return True

    def check_service_status(self, logMsgs=True):
        try:
            if not self.get_service_url():
                return 'Timed Out'
        except tenacity.RetryError:
            return 'Timed Out'
        if logMsgs:
            log_to_ui(self, INFO, f' ==> checking service status at {self.service_url}status', write_to_changelog=False)
        try:
            service_status = requests.get(self.service_url + 'status', timeout=5)
            if service_status.status_code == requests.codes.ok:
                json_status = service_status.json()
                if 'state' in json_status:
                    status = json_status['state']
            else:
                status = str(service_status.status_code) + ": " + service_status.reason[:21] if service_status.reason else str(service_status.status_code)
            if logMsgs:
                log_to_ui(self, INFO, f' ==> service status is: {status}', write_to_changelog=False)
            return status
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
            if logMsgs:
                log_to_ui(self, INFO, 'Service status check timed out', write_to_changelog=False)
        except json.decoder.JSONDecodeError:
            log.error(f'Parsing status response from {self.service_url} as JSON failed: {service_status.text}')
            if logMsgs:
                log_to_ui(self, WARN, f'Service status check failed: returned {service_status.status_code} and response was empty / not in JSON format', write_to_changelog=False)
        return 'Timed Out'

    @cache.cached(timeout=10, make_cache_key=lambda stack: f"check_stack_state/{stack.stack_id}")
    def check_stack_state(self):
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            stack_state = cfn.describe_stacks(StackName=self.stack_id)
        except Exception as e:
            if hasattr(e, 'response'):
                if 'Throttling' in e.response['Error']['Message'] or 'Rate exceeded' in e.response['Error']['Message']:
                    log_to_ui(self, WARN, f'Stack actions are being throttled: {e}', write_to_changelog=False)
                    return 'throttled'
                if 'does not exist' in e.response['Error']['Message']:
                    log_to_ui(self, INFO, f'Stack {self.stack_name} does not exist', write_to_changelog=False)
                    return
            log.exception('Error checking stack state')
            log_to_ui(self, ERROR, 'Error checking stack state', write_to_changelog=False)
            return
        state = stack_state['Stacks'][0]['StackStatus']
        return state

    def check_all_healthy_targets(self, target_group_arn):
        target_group_healthy = False
        try:
            elbv2 = boto3.client('elbv2', region_name=self.region)
            health_descriptions = elbv2.describe_target_health(TargetGroupArn=target_group_arn)['TargetHealthDescriptions']
            health_per_target = [target['TargetHealth']['State'] == 'healthy' for target in health_descriptions]
            target_group_healthy = all(health_per_target)
            target_group_name = target_group_arn.split("/")[1]
            target_group_stats = f'{health_per_target.count(True)} out of {len(health_per_target)} targets healthy'
            log_to_ui(self, INFO, f' ==> checking target group {target_group_name}: {target_group_stats}', write_to_changelog=False)
        except Exception as e:
            log.exception(f'Error occurred getting target group health for {target_group_arn}')
            log_to_ui(self, ERROR, f'Error occurred getting target group health for {target_group_arn}: {e}', write_to_changelog=False)
        return target_group_healthy

    def check_node_status(self, node, primary_stack=False):
        status = 'Unknown'
        app_type = node.get_app_type()
        if app_type and 'mesh' in app_type:
            with grpc.insecure_channel(target=f"{node.private_ip_address}:7777", options=[("grpc.enable_retries", 0)]) as channel:
                stub = pb2_grpc.HealthStub(channel)
                request = pb2.HealthCheckRequest()
                try:
                    response = stub.Check(request, timeout=5)
                except grpc.RpcError as rpc_error:
                    log.error(f"Error retrieiving gRPC health status for mesh node {node.private_ip_address}: {rpc_error.code().name}: {rpc_error.details()}")
                    if rpc_error.code().name == 'DEADLINE_EXCEEDED':
                        return 'Timed out'
                    return rpc_error.code().name
                else:
                    return str(response).split(':')[1].strip()
        else:
            try:
                client = self.boto_session.client('elbv2')
                if primary_stack:
                    # assume primary stack has one asg for code simplicity
                    target_group_arns = self.get_primary_stack_asgs()[0]['TargetGroupARNs']
                else:
                    target_group_arns = node.get_auto_scaling_group()['TargetGroupARNs']
                target_group = next(tg for tg in client.describe_target_groups(TargetGroupArns=target_group_arns)['TargetGroups'] if 'HealthCheckPath' in tg)
                port = target_group["Port"] if target_group["HealthCheckPort"] == 'traffic-port' else target_group["HealthCheckPort"]
                status_check_url = f'{target_group["HealthCheckProtocol"].lower()}://{node.private_ip_address}:{port}{target_group["HealthCheckPath"]}'
            except Exception as e:
                log.exception('Error retrieving healthcheck details from target group')
                log_to_ui(self, ERROR, f'Error retrieving healthcheck details from target group: {e}', write_to_changelog=False)
            try:
                node_status_response = requests.get(status_check_url, timeout=5)
                node_status_response.raise_for_status()
                if app_type == 'synchrony':
                    status = node_status_response.text.strip()
                else:
                    node_status = node_status_response.json()
                    if 'state' in node_status:
                        status = node_status['state']
                    else:
                        log_to_ui(self, ERROR, f'Node status not in expected format: {node_status}', write_to_changelog=False)
            except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout):
                status = 'Timed out'
            except requests.exceptions.ConnectionError:
                status = 'No listener'
            except requests.exceptions.HTTPError as e:
                try:
                    # quick check for a JSON response, as apps might respond with error code for things like MAINTENANCE
                    status = node_status_response.json()['state']
                except json.decoder.JSONDecodeError:
                    # fall back to responding with status code and reason, if available
                    status = f'{e.response.status_code}'
                    if e.response.reason:
                        status += f': {e.response.reason}'
                    log.error(f'Status request to {status_check_url} returned an HTTP error: {status} - {node_status_response.text}')
            except json.decoder.JSONDecodeError:
                log.error(f'Parsing status response from {status_check_url} as JSON failed: {node_status_response.text}')
                status = 'Invalid JSON'
            except Exception as e:
                log.exception(f'Error checking node status from {status_check_url}')
                log_to_ui(self, ERROR, f'Error checking node status: {e}', write_to_changelog=False)
            return status

    def check_instance_product_version(self, instance: Instance) -> str:
        app_version = ""
        app_type = instance.get_app_type()  # type: ignore[attr-defined]
        context_path = self.get_param_value("TomcatContextPath")
        port = self.get_param_value("TomcatDefaultConnectorPort")
        match app_type:
            case "jira":
                version_check_path = "rest/api/2/serverInfo"
            case "confluence":
                version_check_path = "rest/applinks/1.0/manifest"
            case "crowd":
                version_check_path = "rest/admin/1.0/server-info"
            case "bitbucket" | "bitbucket_mirror":
                version_check_path = "rest/api/latest/application-properties"
            case _:
                version_check_path = ""
        if not version_check_path:
            return ""
        full_version_check_path = f"{context_path}/{version_check_path}" if context_path else version_check_path
        try:
            version_check_response = requests.get(f"http://{instance.private_ip_address}:{port}/{full_version_check_path}", timeout=5)
            version_check_response.raise_for_status()
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
            # i.e., no listener; no need to log this as we don't for status checks either
            return ""
        except Exception as e:
            log.error(f"Unable to retrieve app version from {full_version_check_path} for node {instance.private_ip_address}: {e}")
            return ""
        try:
            match app_type:
                case "jira" | "crowd" | "bitbucket" | "bitbucket_mirror":
                    app_version = version_check_response.json()["version"]
                case "confluence":
                    version_node = ElementTree.fromstring(version_check_response.text).find("version")
                    if version_node is not None and hasattr(version_node, "text"):
                        app_version = str(version_node.text)
        except Exception as e:
            log.error(f"Unable to parse response for app version from node {instance.private_ip_address}: {e}")
        return app_version

    # Ensure any temporary modifications to the stack, its properties or its running services during a Weaver action are restored to their original state.
    # Nothing here can rely on data persisting between separate instances of this class with the same stack name / region (due to current code design).
    def restore_stack_state_if_needed(self, action_name: str, deregistration_delays: bool = True):
        log_to_ui(self, INFO, 'Restoring stack state', write_to_changelog=False)
        if action_name == 'upgrade':
            self.toggle_cfn_hup('start')
        if deregistration_delays:
            self.restore_original_deregistration_delays_if_needed()
        log_to_ui(self, INFO, 'Restoring stack state - complete', write_to_changelog=False)

    def get_basic_stack_info(self) -> dict[str, Any]:
        stack_info: dict[str, Any] = {}
        action_in_progress = utils.get_stack_action_in_progress(self.stack_name)
        stack_info['action_in_progress'] = action_in_progress if action_in_progress else 'none'
        cfn_stack = boto3.client('cloudformation', region_name=self.region).describe_stacks(StackName=self.stack_name)['Stacks'][0]
        stack_info['stack_status'] = cfn_stack['StackStatus']
        app_type = self.get_tag('product', log_msgs=False)
        if (
            app_type
            and 'mesh' not in app_type
            and cfn_stack['StackStatus']
            in (
                'CREATE_COMPLETE',
                'UPDATE_COMPLETE',
                'UPDATE_IN_PROGRESS',
                'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
                'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS',
                'UPDATE_ROLLBACK_IN_PROGRESS',
                'UPDATE_ROLLBACK_COMPLETE',
            )
        ):
            stack_info['service_status'] = self.check_service_status(logMsgs=False)
        stack_info['product'] = app_type if app_type else 'Unknown - not tagged'
        stack_info['version'] = self.get_param_value('ProductVersion', cfn_stack['Parameters'])
        return stack_info

    @cache.cached(timeout=5, make_cache_key=lambda stack, instance, allow_api_calls=True: f"__get_stack_node_details_builder/{stack.stack_name}/{instance.id}")
    def __get_stack_node_details_builder(self, instance: Instance, allow_api_calls=True):
        instance_info = {
            'arch': instance.architecture,
            'autoscaling_group_name': instance.get_auto_scaling_group()['AutoScalingGroupName'],  # type: ignore[attr-defined]
            'az': instance.subnet.availability_zone if instance.subnet and allow_api_calls else 'unknown',
            'id': instance.id,
            'ip': instance.private_ip_address,
            'service': instance.get_app_type(),  # type: ignore[attr-defined]
            'service_version': self.check_instance_product_version(instance) if allow_api_calls else '',
            'state': instance.state['Name'],
            'status': self.check_node_status(instance) if allow_api_calls else 'unknown',
            'type': instance.instance_type,
            'uptime': "{}d {}h {}m".format(*instance.get_uptime()),  # type: ignore[attr-defined]
        }
        # Some nodes (e.g. backup ones) won't ever be registered to an ALB, and some stacks don't have an ALB (e.g. Bitbucket Mesh).
        registration_status_alb = self.get_node_registration_state_alb(instance)
        if registration_status_alb:
            instance_info['registration_status_alb'] = registration_status_alb if allow_api_calls else 'unknown'
        # Some nodes (e.g. backup ones) won't ever be registered to an NLB, and some stacks don't have an NLB (e.g. Jira, Confluence, Crowd).
        registration_status_nlb = self.get_node_registration_state_nlb(instance)
        if registration_status_nlb:
            instance_info['registration_status_nlb'] = registration_status_nlb if allow_api_calls else 'unknown'
        return instance_info

    def __asg_sort_order(self, asg_name):
        asg_sort_order_map = {'ClusterNodeGroup': 0, 'ClusterBotNodeGroup': 1, 'SynchronyClusterNodeGroup': 2, 'NodeGroup': 3}
        return next((position for name_fragment, position in asg_sort_order_map.items() if name_fragment in asg_name), 99)

    def get_stack_node_details(self) -> list[dict[str, str]]:
        instances = self.get_stacknodes()
        nodes = []
        with concurrent.futures.ThreadPoolExecutor() as executor:
            future_timeout = 10  # seconds; int or float
            # kick off the parallel tasks; keep the tasks in a mapping pointing back to the instance the task is for (used later in case task doesn't complete within timeout)
            future_to_instance_map = {executor.submit(self.__get_stack_node_details_builder, instance): instance for instance in instances}

            # wait for all the node status info parallel tasks to be done or timed-out
            done, not_done = concurrent.futures.wait(future_to_instance_map, timeout=future_timeout)
            for future in done:
                # for any tasks that completed, check for unhandled exceptions; otherwise add resulting node info to stack_info
                try:
                    result = future.result()
                except Exception as e:
                    log.exception("Unhandled exception building instance info")
                    nodes.append({'autoscaling_group_name': 'Unknown', 'error_class': type(e).__name__, 'error_details': str(e)})
                else:
                    nodes.append(result)
            if len(not_done):
                # log the number of tasks that didn't complete (if any)
                log.error(
                    f'{self.stack_name} get_stack_node_details: {len(not_done)} of {len(future_to_instance_map)} instance builder tasks did not complete within timeout ({future_timeout} seconds)'
                )
                # for any tasks that didn't complete within the timeout:
                for future in not_done:
                    # 1. check to see if maybe it _did_ finish, perhaps just after the timeout (if the result is here we might as well use it)?
                    try:
                        result = future.result()
                    # 2. ensure any exceptions raised by the task are logged
                    except Exception:
                        log.exception(f"{self.stack_name} get_stack_node_details: instance builder task failure exception")
                    else:
                        nodes.append(result)
                        continue
                    # 3. ref back to the instance and try to build the node info again with just the known EC2 instance data that shouldn't require additional API calls
                    try:
                        instance = future_to_instance_map[future]
                        nodes.append(self.__get_stack_node_details_builder(instance, allow_api_calls=False))
                    except Exception as e:
                        log.exception("Unhandled exception building instance info")
                        nodes.append({'autoscaling_group_name': 'Unknown', 'error_class': type(e).__name__, 'error_details': str(e)})
        # at this point; the nodes will be in whatever order they completed their parallel tasks in
        # sort the nodes by their ASG name (custom order defined by __asg_sort_order) and the last octet in their IP address (if it has one)
        return sorted(
            nodes,
            key=lambda node: (self.__asg_sort_order(node['autoscaling_group_name']), node['autoscaling_group_name'], int(node['ip'].split('.')[3]) if 'ip' in node else 99),
        )

    def __get_stack_info_builder(self, info_type: str) -> dict[str, Any]:
        # for each info_type, return a dict with the key of the expected key in
        # the response and the corresponding value; exception to this is the
        # "basic" case, where we return get_basic_stack_info directly
        match info_type:
            case "basic":
                return self.get_basic_stack_info()
            case "clone_defaults":
                return {"clone_defaults": self.get_clone_defaults()}
            case "nodes":
                return {"nodes": self.get_stack_node_details()}
            case "params":
                return {"params": self.get_params()}
            case "tags":
                return {"tags": self.get_tags()}
            case "template":
                tags = self.get_tags()
                return {
                    "template": {
                        "repo": tags.get("repository", ""),
                        "name": tags.get("template", ""),
                    }
                }
            case "termination_protection":
                return {"termination_protection": self.has_termination_protection()}
            case "zdu_compatibility":
                return {"zdu_compatibility": self.get_zdu_compatibility()}
            case _:
                # we shouldn't ever hit this case, but just in... case
                # ensure the default return is still a dict, albeit an empty one
                return {}

    def get_stack_info(self, expands: list[str]) -> dict[str, Any]:
        stack_info: dict[str, Any] = {}
        # if no expands were requested, don't spool up an executor just to
        # retrieve the "basic" stack info; return that info now and be done
        if not expands:
            return dict(sorted(self.__get_stack_info_builder("basic").items()))
        # ensure the "basic" stack info is always included in the response
        info_types = ["basic"] + expands
        # similar to how we retrieve node info in get_stack_node_details, spool
        # out to futures-managed threads, but in this case, use Flask-Executor
        # to submit the threads so the calls to __get_stack_info_builder are
        # wrapped in the application context
        future_to_info_map = {executor.submit(self.__get_stack_info_builder, info_type): info_type for info_type in info_types}
        # no timeouts in this config; we just process the results as they
        # complete (we didn't previously have any timeouts on this routine)
        for future in concurrent.futures.as_completed(future_to_info_map):
            try:
                result = future.result()
            except Exception:
                log.exception("Unhandled exception retrieving stack info")
            else:
                stack_info = stack_info | result
        # since the results will come back in whatever order they completed in,
        # go ahead and sort the results for consistency's sake
        return dict(sorted(stack_info.items()))

    def get_template_params(self, action, template_name):  # noqa: C901 (too complex)
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            stack_details = cfn.describe_stacks(StackName=self.stack_name)
        except botocore.exceptions.ClientError:
            log.exception('Error occurred getting stack parameters')
            return False
        stack_params = stack_details['Stacks'][0]['Parameters']
        # load the template params, groups, and labels
        template_file = open(utils.get_template_file(template_name), "r")
        yaml = YAML(typ='safe', pure=True)
        yaml.Constructor.add_multi_constructor(u'!', utils.general_constructor)
        template = yaml.load(template_file)
        response: GetTemplateParamsResponse = {'groups': {}, 'labels': {}, 'params': []}
        with suppress(KeyError):
            template_param_groups = template['Metadata']['AWS::CloudFormation::Interface']['ParameterGroups']
            for group in template_param_groups:
                response['groups'][group['Label']['default']] = group['Parameters']
        with suppress(KeyError):
            template_param_labels = template['Metadata']['AWS::CloudFormation::Interface']['ParameterLabels']
            for label in template_param_labels:
                response['labels'][label] = template_param_labels[label]['default']
        for group in template_param_groups:
            response['groups'][group['Label']['default']] = group['Parameters']
        for label in template_param_labels:
            response['labels'][label] = template_param_labels[label]['default']
        # Remove old stack params that are no longer on the template
        # To do this we need to build a new list
        template_params = template['Parameters']
        for stack_param in stack_params:
            if stack_param['ParameterKey'] in template_params:
                # include the default value from the template if it differs from the existing parameter value
                template_default_value = str(template_params[stack_param['ParameterKey']].get('Default', ''))
                if template_default_value and template_default_value != stack_param['ParameterValue']:
                    stack_param['DefaultValue'] = template_params[stack_param['ParameterKey']]['Default']  # type: ignore[typeddict-unknown-key]
                response['params'].append(stack_param)  # type: ignore[arg-type]
            else:
                print("Parameter not found: " + stack_param['ParameterKey'])
        # Add defaults for region specific params on clone
        if action == 'clone':
            region_specific_parameters: List[str] = ['ExternalSubnets', 'InternalSubnets', 'KmsKeyArn', 'SSLCertificateARN', 'VPC']
            for stack_param in stack_params:
                if stack_param['ParameterKey'] in region_specific_parameters:
                    stack_param['ParameterValue'] = template_params[stack_param['ParameterKey']]['Default'] if 'Default' in template_params[stack_param['ParameterKey']] else ''
        # Add new params from the template to the stack params
        for param in template_params:
            if param not in [stack_param['ParameterKey'] for stack_param in stack_params]:
                response['params'].append({'ParameterKey': param, 'ParameterValue': template_params[param]['Default'] if 'Default' in template_params[param] else ''})
            if 'AllowedValues' in template_params[param]:
                next(compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param)['AllowedValues'] = template_params[param]['AllowedValues']
                next(compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param)['Default'] = (
                    template_params[param]['Default'] if 'Default' in template_params[param] else ''
                )
            if 'AllowedPattern' in template_params[param]:
                next(compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param)['AllowedPattern'] = template_params[param][
                    'AllowedPattern'
                ]
            if 'MinValue' in template_params[param]:
                next(compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param)['MinValue'] = template_params[param]['MinValue']
            if 'MaxValue' in template_params[param]:
                next(compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param)['MaxValue'] = template_params[param]['MaxValue']
            if 'MinLength' in template_params[param]:
                next(compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param)['MinLength'] = template_params[param]['MinLength']
            if 'MaxLength' in template_params[param]:
                next(compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param)['MaxLength'] = template_params[param]['MaxLength']
            compared_param = next((compared_param for compared_param in response['params'] if compared_param['ParameterKey'] == param), None)
            if compared_param and 'Description' in template_params[param]:
                compared_param['ParameterDescription'] = template_params[param]['Description']
        return response

    def spinup_remaining_nodes(self, new_version):
        log_to_ui(self, INFO, 'Spinning up any remaining nodes in stack', write_to_changelog=True)
        cfn = boto3.client('cloudformation', region_name=self.region)
        spinup_params = self.get_spinup_to_full_node_count_paramlist()
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            cfn.update_stack(
                StackName=self.stack_name, Parameters=spinup_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token
            )
        except Exception as e:
            log.exception('Error occurred spinning up remaining nodes')
            log_to_ui(self, ERROR, f'Error occurred spinning up remaining nodes: {e}', write_to_changelog=True)
            return False
        if self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            log_to_ui(self, INFO, "Stack restored to full node count", write_to_changelog=True)
        if not self.validate_all_stack_nodes_product_versions(new_version):
            return False
        return True

    def spinup_all_nodes(self, new_version, alt_download_url=''):
        log_to_ui(self, INFO, 'Spinning up all nodes in stack', write_to_changelog=True)
        cfn = boto3.client('cloudformation', region_name=self.region)
        spinup_params = self.get_spinup_to_full_node_count_paramlist()
        if any(param for param in spinup_params if param['ParameterKey'] == 'ProductVersion'):
            spinup_params = self.update_paramlist(spinup_params, 'ProductVersion', new_version)
        else:
            spinup_params = self.update_paramlist(spinup_params, f'{self.get_app_type().title()}Version', new_version)
        spinup_params = self.update_paramlist(spinup_params, 'ProductDownloadUrl', alt_download_url)
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            cfn.update_stack(
                StackName=self.stack_name, Parameters=spinup_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token
            )
        except Exception as e:
            log.exception('Error occurred spinning up all nodes')
            log_to_ui(self, ERROR, f'Error occurred spinning up all nodes: {e}', write_to_changelog=True)
            return False
        if self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            log_to_ui(self, INFO, "Stack restored to full node count", write_to_changelog=True)
        return True

    # Check if a stack split into AZs' current node counts match their preupgrade values.
    def check_az_node_counts_match_preupgrade(self) -> bool:
        # Check if the stack is split into AZs or not
        if self.preupgrade_az_node_counts:
            current_params: List[ParameterTypeDef] = self.get_params()
            for az_node_count_param, preupgrade_az_node_count in self.preupgrade_az_node_counts.items():
                current_az_node_count: int = int(self.get_param_value(az_node_count_param, current_params))
                if current_az_node_count < preupgrade_az_node_count:
                    return False
            # If we reach here, no AZ node count parameters are less than their preupgrade value.
            return True
        else:
            log_to_ui(self, ERROR, 'The method "check_az_node_counts_match_preupgrade" is not supported for stacks not split into AZs', write_to_changelog=True)
            raise weaver_exceptions.StackUsingUnsupportedMethodException()

    # Spins up an extra node in each AZ, so long as it doesn't push that AZs' node count past its preupgrade value.
    def spinup_extra_node_per_az(self) -> bool:
        cfn = boto3.client('cloudformation', region_name=self.region)
        spinup_params: List[ParameterTypeDef] = self.get_params()
        for az_node_count_param, preupgrade_az_node_count in self.preupgrade_az_node_counts.items():
            current_az_node_count: int = int(self.get_param_value(az_node_count_param, spinup_params))
            if current_az_node_count < preupgrade_az_node_count:
                spinup_params = self.update_paramlist(spinup_params, az_node_count_param, str(current_az_node_count + 1))
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            update_stack = cfn.update_stack(
                StackName=self.stack_name, Parameters=spinup_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token
            )
        except botocore.exceptions.ClientError as e:
            log_to_ui(self, INFO, f'Stack spinup failed: {e}', write_to_changelog=True)
            return False
        if not self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            return False
        log_to_ui(self, INFO, 'Spun up an extra node per AZ needed, waiting for service to respond', write_to_changelog=False)
        if not self.validate_service_responding():
            return False
        log_to_ui(self, INFO, f'Updated stack: {update_stack}', write_to_changelog=True)
        return True

    # Spins up the remaining stack nodes in the cluster, up to one node per AZ at a time.
    def spinup_remaining_nodes_per_az_incremental(self) -> bool:
        step_log_msg: str = "Spinning up remaining stack nodes, up to 1 app node per AZ at a time"
        if not self.preupgrade_az_node_counts:
            log_to_ui(self, INFO, "Could not retrieve pre-upgrade AZ node counts", write_to_changelog=True)
            return False
        log_to_ui(self, INFO, f"{step_log_msg}", write_to_changelog=True)
        while not self.check_az_node_counts_match_preupgrade():
            if not self.spinup_extra_node_per_az():
                log_to_ui(self, ERROR, f"{step_log_msg} - failed", write_to_changelog=True)
                return False
        log_to_ui(self, INFO, f"{step_log_msg} - complete", write_to_changelog=True)
        return True

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def get_stack_resource_list(self, force=False):
        if self.stack_resources and not force:
            return self.stack_resources
        else:
            log.debug(f'Getting stack info{"; update is forced" if force else ""}')
        cfn = self.boto_session.client('cloudformation')
        stack_resources = []
        try:
            paginator = cfn.get_paginator('list_stack_resources')
            page_iterator = paginator.paginate(StackName=self.stack_name)
            for page in page_iterator:
                stack_resources.extend(page['StackResourceSummaries'])
        except botocore.exceptions.ClientError as e:
            if 'does not exist' in e.response['Error']['Message']:
                raise weaver_exceptions.StackDoesNotExistException(f'Stack {self.stack_name} does not exist', stack_name=self.stack_name)
            else:
                log.exception('An error occurred retrieving stack resources')
                log_to_ui(self, ERROR, f'An error occurred retrieving stack resources: {e}', write_to_changelog=True)
        except KeyError as e:
            raise e
        except Exception as e:
            log.exception('An error occurred retrieving stack resources')
            log_to_ui(self, ERROR, f'An error occurred retrieving stack resources: {e}', write_to_changelog=True)
        self.stack_resources = stack_resources
        return stack_resources

    def get_stack_resource_by_logical_id(self, logical_id: str, force: bool = False) -> StackResourceSummaryTypeDef | None:
        stack_resource_list: List[StackResourceSummaryTypeDef] = self.get_stack_resource_list(force)
        for stack_resource in stack_resource_list:
            if stack_resource['LogicalResourceId'] == logical_id:
                return stack_resource
        return None

    @cache.cached(timeout=60, make_cache_key=(lambda stack, force=False: f"get_stack_asgs/{stack.stack_name}"), forced_update=utils.force_cache_update)
    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def get_stack_asgs(self, force=False) -> List[AutoScalingGroupTypeDef]:
        if self.stack_asgs and not force:
            return self.stack_asgs
        else:
            log.debug(f'Getting stack autoscaling groups{"; update is forced" if force else ""}')
        stack_asgs = []
        try:
            asg_names = [
                resource['PhysicalResourceId'] for resource in self.get_stack_resource_list(force=force) if resource['ResourceType'] == 'AWS::AutoScaling::AutoScalingGroup'
            ]
            if asg_names:
                client = self.boto_session.client('autoscaling')
                stack_asgs = client.describe_auto_scaling_groups(AutoScalingGroupNames=asg_names)['AutoScalingGroups']
        except (weaver_exceptions.StackDoesNotExistException, KeyError):
            raise
        except Exception as e:
            log.exception('An error occurred retrieving autoscaling group details')
            log_to_ui(self, ERROR, f'An error occurred retrieving autoscaling group details: {e}', write_to_changelog=True)
        self.stack_asgs = stack_asgs
        return stack_asgs

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def get_stack_node_target_group_arns(self, force=False) -> List[str]:
        if self.stack_node_target_group_arns and not force:
            return self.stack_node_target_group_arns
        stack_asgs: List[AutoScalingGroupTypeDef] = self.get_stack_asgs(force=force)
        node_target_group_arns: Set[str] = {target_group_arn for stack_asg in stack_asgs for target_group_arn in stack_asg['TargetGroupARNs']}
        self.stack_node_target_group_arns = list(node_target_group_arns)
        return self.stack_node_target_group_arns

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def get_stacknodes(self, force=False) -> list[Instance]:
        if self.stack_nodes and not force:
            return self.stack_nodes
        else:
            log.debug(f'Getting stack nodes{"; update is forced" if force else ""}')
        nodes = []
        asgs = self.get_stack_asgs(force=force)
        instance_ids = [instance['InstanceId'] for asg in asgs for instance in asg['Instances']]
        if instance_ids:
            filters: list[FilterTypeDef] = [
                {'Name': 'instance-id', 'Values': instance_ids},
                # every instance state except "terminated"
                {'Name': 'instance-state-name', 'Values': ['pending', 'running', 'shutting-down', 'stopping', 'stopped']},
            ]
            try:
                ec2 = self.boto_session.resource('ec2')
                nodes = list(ec2.instances.filter(Filters=filters))
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == 'RequestLimitExceeded':
                    log.exception('RequestLimitExceeded received during get_stacknodes')
                    log_to_ui(self, ERROR, 'RequestLimitExceeded received during get_stacknodes', write_to_changelog=False)
                    raise e
        self.stack_nodes = nodes
        return nodes

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def get_primary_stack_asgs(self, force=False):
        if self.primary_stack_asgs and not force:
            return self.primary_stack_asgs
        else:
            log.debug(f'Getting primary stack autoscaling groups{"; update is forced" if force else ""}')
        primary_stack_asgs = []
        try:
            cfn = self.boto_session.client('cloudformation')
            primary_stack_resources = cfn.describe_stack_resources(StackName=self.get_primary_stack_name())['StackResources']
        except Exception as e:
            log.exception('An error occurred retrieving primary stack resource details')
            log_to_ui(self, ERROR, f'An error occurred retrieving primary stack resource details: {e}', write_to_changelog=True)
        try:
            autoscaling = self.boto_session.client('autoscaling')
            primary_stack_asg_names = [resource['PhysicalResourceId'] for resource in primary_stack_resources if resource['ResourceType'] == 'AWS::AutoScaling::AutoScalingGroup']
            primary_stack_asgs = autoscaling.describe_auto_scaling_groups(AutoScalingGroupNames=primary_stack_asg_names)['AutoScalingGroups']
        except Exception as e:
            log.exception('An error occurred retrieving primary stack autoscaling group details')
            log_to_ui(self, ERROR, f'An error occurred retrieving primary stack autoscaling group details: {e}', write_to_changelog=True)
        self.primary_stack_asgs = primary_stack_asgs
        return primary_stack_asgs

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def get_primary_stack_name(self, force: bool = False):
        if self.primary_stack_name and not force:
            return self.primary_stack_name
        else:
            log.debug(f'Getting primary stack name{"; update is forced" if force else ""}')
        # search for primary stack name
        regex_pattern = r'^.*PrimaryStack$'
        matched_params = self.search_param_keys(regex_pattern)
        if len(matched_params) != 1:
            log_to_ui(
                self,
                ERROR,
                f"An error occurred retrieving primary stack name: more than one or no CloudFormation parameters matched pattern '{regex_pattern}'",
                write_to_changelog=True,
            )
            return False
        self.primary_stack_name = matched_params[0]['ParameterValue']
        return self.primary_stack_name

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def get_primary_stack_nodes(self, force=False):
        if self.primary_stack_nodes and not force:
            return self.primary_stack_nodes
        else:
            log.debug(f'Getting primary stack nodes{"; update is forced" if force else ""}')
        nodes = []
        primary_stack_asgs = self.get_primary_stack_asgs(force=force)
        instance_ids = [instance['InstanceId'] for asg in primary_stack_asgs for instance in asg['Instances']]
        if instance_ids:
            filters: list[FilterTypeDef] = [
                {'Name': 'instance-id', 'Values': instance_ids},
                # every instance state except "terminated"
                {'Name': 'instance-state-name', 'Values': ['pending', 'running', 'stopping', 'stopped']},
            ]
            try:
                ec2 = self.boto_session.resource('ec2')
                nodes = list(ec2.instances.filter(Filters=filters))
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == 'RequestLimitExceeded':
                    log.exception('RequestLimitExceeded received during get_primary_stack_nodes')
                    log_to_ui(self, ERROR, 'RequestLimitExceeded received during get_primary_stack_nodes', write_to_changelog=False)
                    raise e
        self.primary_stack_nodes = nodes
        return nodes

    # Get a list of the ARNs of the stack's Auto Scaling Groups in order of sizes (number of stack nodes they contain).
    def get_stack_auto_scaling_group_arns_by_size(self, reverse: bool = False) -> list[str]:
        asg_sizes = self.get_stack_auto_scaling_group_sizes()
        sorted_asg_sizes = sorted(asg_sizes, key=lambda x: asg_sizes.get(x, -1), reverse=reverse)
        return sorted_asg_sizes

    # Get the sizes of the stack's Auto Scaling Groups (number of stack nodes they contain).
    def get_stack_auto_scaling_group_sizes(self) -> dict[str, int]:
        asg_sizes: dict[str, int] = {}
        nodes: list[Instance] = self.get_stacknodes()
        for node in nodes:
            asg_arn: str = node.get_auto_scaling_group()['AutoScalingGroupARN']  # type: ignore[attr-defined]
            if asg_arn not in asg_sizes:
                asg_sizes[asg_arn] = 1
            else:
                asg_sizes[asg_arn] += 1
        return asg_sizes

    def group_nodes_by_health(self, nodes: List[Instance]) -> tuple[list[Instance], list[Instance]]:
        healthy_nodes: list[Instance] = []
        unhealthy_nodes: list[Instance] = []
        for node in nodes:
            if self.check_node_status(node) in READY_TO_SERVE_STATUSES:
                healthy_nodes.append(node)
            else:
                unhealthy_nodes.append(node)
        return healthy_nodes, unhealthy_nodes

    def shutdown_app(self, nodes: List[Instance]) -> bool:
        cmd_id_list = []
        for node in nodes:
            app_type = node.get_app_type()  # type: ignore[attr-defined]
            if 'mesh' in app_type:
                app_type = 'mesh'
            elif 'mirror' in app_type:
                app_type = 'bitbucket'
            log_to_ui(self, INFO, f'Shutting down {app_type} on {node.id} ({node.private_ip_address})', write_to_changelog=True)
            cmd = f'service {app_type} stop'
            cmd_id_list.append(self.ssm_send_command(node.id, cmd))
        for cmd_id in cmd_id_list:
            result = self.wait_for_cmd_result(cmd_id)
            if result == 'Failed':
                log_to_ui(self, ERROR, f'Shutdown result for {cmd_id}: {result}', write_to_changelog=True)
            else:
                log_to_ui(self, INFO, f'Shutdown result for {cmd_id}: {result}', write_to_changelog=True)
        return True

    def startup_app(self, nodes, primary_stack=False):
        cmd_id_list = []
        for node in nodes:
            app_type = node.get_app_type()
            if 'mesh' in app_type:
                app_type = 'mesh'
            elif 'mirror' in app_type:
                app_type = 'bitbucket'
            if app_type == 'jira':
                if not self.cleanup_jira_temp_files(str(node.id)):
                    log_to_ui(self, ERROR, f'Failure cleaning up temp files for {node.id}', write_to_changelog=False)
            log_to_ui(self, INFO, f'Starting up {app_type} on {node.id} ({node.private_ip_address})', write_to_changelog=True)
            cmd = f'service {app_type} start'
            cmd_id_list.append(self.ssm_send_command(node.id, cmd))
        for cmd_id in cmd_id_list:
            result = self.wait_for_cmd_result(cmd_id)
            if result == 'Failed':
                log_to_ui(self, ERROR, f'Startup result for {cmd_id}: {result}', write_to_changelog=True)
            else:
                log_to_ui(self, INFO, f'Application started on node {node.id}', write_to_changelog=True)
        # Keeping this check in just in case its possible for the startup command to report successful via SSM, but fail to actually startup the application.
        if not self.validate_nodes_responding(nodes, primary_stack):
            return False
        return True

    def reboot_ec2_node(self, node) -> bool:
        log_to_ui(self, INFO, f'Rebooting EC2 node on {node.id} ({node.private_ip_address})', write_to_changelog=True)
        cmd = 'systemctl reboot'
        cmd_id = self.ssm_send_command(node.id, cmd)
        result = self.wait_for_cmd_result(cmd_id)
        if result == 'Failed':
            log_to_ui(self, ERROR, f'EC2 node reboot result for {cmd_id}: {result}', write_to_changelog=True)
            return False
        else:
            log_to_ui(self, INFO, f'EC2 node reboot result for {cmd_id}: {result}', write_to_changelog=True)
        return True

    def toggle_cfn_hup(self, action):
        nodes = self.get_stacknodes(force=True)
        log_to_ui(
            self, INFO, f'{"Starting" if action == "start" else "Stopping"} cfn-hup on nodes {", ".join([node.private_ip_address for node in nodes])}', write_to_changelog=True
        )
        return self.run_command(nodes, f'systemctl {action} cfn-hup')

    # Retrieves a stack's cfn-init command (assumes all stack nodes use the same command). If the attribute is defined for the stack, immediately return this instead.
    def get_stack_cfn_init_command(self, force: bool = False) -> str | None:
        if not force and self.cfn_init_cmd:
            return self.cfn_init_cmd
        try:
            node: Instance = self.get_stacknodes()[0]
        except Exception:
            log_to_ui(self, ERROR, 'Failed to get stack cfn-init command - stack has no non-terminated nodes', write_to_changelog=True)
            return None
        # create an IMDSv2 session token to query the node's metadata service with; any non-SSM error should just return blank string on stdout
        imdsv2_token_cmd_id = self.ssm_send_command(node.id, 'curl --fail --silent -H "X-aws-ec2-metadata-token-ttl-seconds: 60" -X PUT "http://169.254.169.254/latest/api/token"')
        imdsv2_token_cmd_result = self.wait_for_cmd_result(imdsv2_token_cmd_id)
        if imdsv2_token_cmd_result == 'Failed':  # nosec B105
            log_to_ui(self, ERROR, 'Requesting an IMDSv2 token to retrieve user-data failed', write_to_changelog=True)
            return None
        imdsv2_token = self.ssm_get_command_output(imdsv2_token_cmd_id, node.id)
        if not imdsv2_token:
            log_to_ui(self, ERROR, 'Retrieiving the IMDSv2 token failed', write_to_changelog=True)
            return None
        # extract the cfn-init command pertintent to this node from user-data; provide an alternate command description so the token doesn't appear in logs
        user_data_cmd_id = self.ssm_send_command(
            node.id,
            f'curl -s -H "X-aws-ec2-metadata-token: {imdsv2_token}" http://169.254.169.254/latest/user-data/ | grep cfn-init',
            f'Querying user-data for cfn-init command on node {node.id}',
        )
        user_data_cmd_result = self.wait_for_cmd_result(user_data_cmd_id)
        if user_data_cmd_result == 'Failed':
            log_to_ui(self, ERROR, 'Retrieving cfn-init command via user-data failed', write_to_changelog=True)
            return None
        self.cfn_init_cmd = self.ssm_get_command_output(user_data_cmd_id, node.id)
        if not self.cfn_init_cmd or 'cfn-init' not in self.cfn_init_cmd:
            log_to_ui(self, ERROR, f'Failed to find valid cfn-init command in user-data on node {node.id} ({node.private_ip_address})', write_to_changelog=True)
            return None
        return self.cfn_init_cmd

    def execute_cfn_init(self, node):
        # create an IMDSv2 session token to query the node's metadata service with; any non-SSM error should just return blank string on stdout
        imdsv2_token_cmd_id = self.ssm_send_command(node.id, 'curl --fail --silent -H "X-aws-ec2-metadata-token-ttl-seconds: 60" -X PUT "http://169.254.169.254/latest/api/token"')
        imdsv2_token_cmd_result = self.wait_for_cmd_result(imdsv2_token_cmd_id)
        if imdsv2_token_cmd_result == 'Failed':  # nosec B105
            log_to_ui(self, ERROR, 'Requesting an IMDSv2 token to retrieve user-data failed', write_to_changelog=True)
            return False
        imdsv2_token = self.ssm_get_command_output(imdsv2_token_cmd_id, node.id)
        if not imdsv2_token:
            log_to_ui(self, ERROR, 'Retrieiving the IMDSv2 token failed', write_to_changelog=True)
            return False
        # extract the cfn-init command pertintent to this node from user-data; provide an alternate command description so the token doesn't appear in logs
        user_data_cmd_id = self.ssm_send_command(
            node.id,
            f'curl -s -H "X-aws-ec2-metadata-token: {imdsv2_token}" http://169.254.169.254/latest/user-data/ | grep cfn-init',
            f'Querying user-data for cfn-init command on node {node.id}',
        )
        user_data_cmd_result = self.wait_for_cmd_result(user_data_cmd_id)
        if user_data_cmd_result == 'Failed':
            log_to_ui(self, ERROR, 'Retrieving cfn-init command via user-data failed', write_to_changelog=True)
            return False
        cfn_init_cmd = self.ssm_get_command_output(user_data_cmd_id, node.id)
        if not cfn_init_cmd or 'cfn-init' not in cfn_init_cmd:
            log_to_ui(self, ERROR, f'Failed to find valid cfn-init command in user-data on node {node.id} ({node.private_ip_address})', write_to_changelog=True)
            return False
        # execute cfn-init manually
        if not self.run_command([node], cfn_init_cmd):
            log_to_ui(self, ERROR, f'Failed to execute cfn-init on node {node.id} ({node.private_ip_address})', write_to_changelog=True)
            return False
        return True

    # Executes cfn-init on all of the stack's nodes serially
    def execute_cfn_init_all_stack_nodes(self) -> bool:
        log_to_ui(self, INFO, 'Executing cfn-init on all stack nodes', write_to_changelog=True)
        nodes: list[Instance] = self.get_stacknodes()
        for node in nodes:
            if not self.execute_cfn_init(node):
                log_to_ui(self, INFO, f'Failed to reinitialize node {node.id} ({node.private_ip_address})', write_to_changelog=True)
                return False
        log_to_ui(self, INFO, 'Executing cfn-init on all stack nodes - complete', write_to_changelog=True)
        return True

    # Executes cfn-init on a select group of nodes in parallel
    def execute_cfn_init_parallel(self, nodes: List[Instance]) -> bool:
        step_log_msg: str = f'Executing cfn-init on nodes: {nodes}'
        log_to_ui(self, INFO, f'{step_log_msg}', write_to_changelog=True)
        # Assume the cfn-init command is the same across the entire stack
        cfn_init_cmd = self.get_stack_cfn_init_command()
        if not cfn_init_cmd:
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.run_command(nodes, cfn_init_cmd):
            log_to_ui(self, ERROR, 'Failed to execute cfn-init on nodes', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        log_to_ui(self, INFO, f'{step_log_msg} - complete', write_to_changelog=True)
        return True

    # Re-initialises one node from a list of nodes, and waits for it to report healthy.
    def reinit_one_node_and_wait_healthy(self, nodes: List[Instance], expected_version: str, skip_node_drain: bool = False) -> bool:
        asg_arns_by_size_desc: List[str] = self.get_stack_auto_scaling_group_arns_by_size(reverse=True)
        # Select a node in the Auto Scaling Group with the highest stack node count.
        selected_node: Instance | None = next((node for node in nodes if node.get_auto_scaling_group()['AutoScalingGroupARN'] == asg_arns_by_size_desc[0]), None)  # type: ignore[attr-defined]
        step_log_msg: str = f'Re-initialising {selected_node} and waiting for it to report healthy'
        log_to_ui(self, INFO, f'{step_log_msg}', write_to_changelog=True)
        if not selected_node:
            log_to_ui(self, ERROR, 'Error finding stack node in Auto Scaling Group with highest stack node count', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.execute_cfn_init_parallel([selected_node]):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.validate_node_responding(selected_node):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.validate_nodes_product_version([selected_node], expected_version):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not skip_node_drain and not self.begin_registering_node_to_all_target_groups(selected_node):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.wait_nodes_registration_states([selected_node], 'healthy'):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        log_to_ui(self, INFO, f'{step_log_msg} - complete', write_to_changelog=True)
        return True

    # Re-initialises non-ready-to-serve nodes from a list of nodes, and waits for it to report healthy.
    def reinit_remaining_nodes_and_wait_healthy(self, nodes: List[Instance], expected_version: str, skip_node_drain: bool = False) -> bool:
        filtered_nodes: list[Instance] = self.filter_out_ready_to_serve_nodes(nodes)
        step_log_msg: str = f'Re-initialising {filtered_nodes} and waiting for it to report healthy'
        log_to_ui(self, INFO, f'{step_log_msg}', write_to_changelog=True)
        if not self.execute_cfn_init_parallel(filtered_nodes):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.validate_nodes_responding(filtered_nodes):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.validate_nodes_product_version(filtered_nodes, expected_version):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not skip_node_drain and not self.begin_registering_nodes_to_all_target_groups(filtered_nodes):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.wait_nodes_registration_states(filtered_nodes, 'healthy'):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        log_to_ui(self, INFO, f'{step_log_msg} - complete', write_to_changelog=True)
        return True

    def cleanup_jira_temp_files(self, node_id):
        possible_locations = ['/opt/atlassian/jira*/current/temp/', '/opt/atlassian/jira/temp/']
        results = []
        for path in possible_locations:
            cmd = f'find {path} -type f -delete'
            cmd_id = self.ssm_send_command(node_id, cmd)
            result = self.wait_for_cmd_result(cmd_id)
            if result == 'Success':
                log_to_ui(self, INFO, f'Deleted Jira temp files in {path} on {node_id}', write_to_changelog=False)
                return True
            else:
                log_to_ui(self, WARN, f'Cleanup temp files in {path} result for {cmd_id}: {result}', write_to_changelog=False)
            results.append(result == 'Success')
        if not any(results):
            return False
        return True

    def get_tags(self) -> dict[str, str]:
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            stack = cfn.describe_stacks(StackName=self.stack_name)
        except Exception as e:
            log_to_ui(self, ERROR, f'Error getting tags: {e}', write_to_changelog=True)
            log.exception('Error getting tags')
            return {}
        try:
            tags = utils.tag_list_to_dict(stack['Stacks'][0]['Tags'])
        except KeyError as e:
            log_to_ui(self, ERROR, f'Error parsing tags: {e}', write_to_changelog=True)
            log.exception('Error parsing tags')
            return {}
        return tags

    def get_tag(self, tag_name: str, log_msgs=True) -> str | Literal[False]:
        tags = self.get_tags()
        if tags:
            tag_value = tags.get(tag_name)
            if tag_value:
                if log_msgs and hasattr(self, 'logfile') and self.logfile is not None:
                    log_to_ui(self, INFO, f'Tag \'{tag_name}\' is \'{tag_value}\'', write_to_changelog=False)
                return tag_value.lower()
        if log_msgs and hasattr(self, 'logfile') and self.logfile is not None:
            log_to_ui(self, WARN, f'Tag {tag_name} not found', write_to_changelog=True)
        log_to_ui(self, WARN, f'Tag {tag_name} not found', write_to_changelog=True)
        return False

    # Get the application type for the stack.
    # False represents a CFN stack that has not yet been created.
    def get_app_type(self) -> str:
        if self.app_type:
            return self.app_type
        app_type = self.get_tag('product')
        if app_type:
            # class-defined default value is empty string; if get_tag returns bool False, don't change it
            self.app_type = app_type
        return self.app_type

    @staticmethod
    def get_s3_bucket_name() -> str:
        s3_bucket: str = f"atl-labs-dc-weaver-{boto3.client('sts').get_caller_identity().get('Account')}"
        return s3_bucket

    def is_app_clustered(self):
        clustered = self.get_tag('clustered', False)
        if not clustered:
            log_to_ui(self, WARN, 'App clustering status is unknown (tag is missing from stack); proceeding as if clustered = true', write_to_changelog=False)
            return True
        return True if clustered == 'true' else False

    def get_sql_from_s3(self, stack, sql_dir):
        # try to pull latest from s3
        s3_bucket = self.get_s3_bucket_name()
        try:
            s3 = boto3.client('s3')
            bucket_list = s3.list_objects(Bucket=s3_bucket, Prefix=f'config/{sql_dir}')['Contents']
            if bucket_list:
                if not os.path.exists(sql_dir):
                    os.makedirs(sql_dir)
                for bucket_item in bucket_list:
                    if bucket_item['Size'] > 0:  # this is to catch when s3 sometimes weirdly returns the path as an object
                        sql_file_name = os.path.basename(bucket_item['Key'])
                        s3.download_file(s3_bucket, bucket_item['Key'], f'{sql_dir}{sql_file_name}')
            log_to_ui(self, INFO, f'Retrieved latest SQL for {stack} from {sql_dir}', write_to_changelog=False)
            return True
        except KeyError as e:
            if e.args[0] == 'Contents':
                log.warning(f'No SQL files exist at s3://{s3_bucket}/config/{sql_dir} for stack {stack}')
                log_to_ui(self, WARN, f'No SQL files exist at s3://{s3_bucket}/config/{sql_dir} for stack {stack}', write_to_changelog=True)
                return True
            log.exception(f'Could not retrieve sql from s3 for stack {stack}')
            log_to_ui(self, ERROR, f'Could not retrieve sql from s3 for stack {stack}: {e}', write_to_changelog=False)
            return False
        except ClientError as e:
            error_code = e.response["Error"]["Code"]
            log.exception(f'Could not retrieve sql from s3 for stack {stack}: {error_code}')
            log_to_ui(self, ERROR, f'Could not retrieve sql from s3 for stack {stack}: {error_code}', write_to_changelog=False)
            return False
        except Exception as e:
            log.exception(f'Could not retrieve sql from s3 for stack {stack}')
            log_to_ui(self, ERROR, f'Could not retrieve sql from s3 for stack {stack}: {e}', write_to_changelog=True)
            return False

    def get_sql(self):
        sql_to_run = ''
        # get SQL for the stack this stack was cloned from (ie the master stack)
        cloned_from_stack = self.get_tag('cloned_from')
        if cloned_from_stack:
            cloned_from_stack_sql_dir = f'stacks/{cloned_from_stack}/{cloned_from_stack}-clones-sql.d/'
            self.get_sql_from_s3(cloned_from_stack, cloned_from_stack_sql_dir)
            if Path(cloned_from_stack_sql_dir).exists():
                sql_files = os.listdir(cloned_from_stack_sql_dir)
                if len(sql_files) > 0:
                    sql_to_run = f'---- ***** SQL to run for clones of {cloned_from_stack} *****\n\n'
                    for file in sql_files:
                        sql_file = open(os.path.join(cloned_from_stack_sql_dir, file), "r")
                        sql_to_run = f"{sql_to_run}-- *** SQL from {cloned_from_stack} {sql_file.name} ***\n\n{sql_file.read()}\n\n"
        # download SQL for this stack from S3 for display
        own_sql_dir = f'stacks/{self.stack_name}/local-post-clone-sql.d/'
        self.get_sql_from_s3(self.stack_name, own_sql_dir)
        if Path(own_sql_dir).exists():
            sql_files = os.listdir(own_sql_dir)
            if len(sql_files) > 0:
                sql_to_run = f'{sql_to_run}---- ***** SQL to run for {self.stack_name} *****\n\n'
                for file in sql_files:
                    sql_file = open(os.path.join(own_sql_dir, file), "r")
                    sql_to_run = f"{sql_to_run}-- *** SQL from {sql_file.name} ***\n\n{sql_file.read()}\n\n"
        if len(sql_to_run) > 0:
            return sql_to_run
        return 'No SQL script exists for this stack'

    def get_pre_upgrade_information(self, app_type, upgrade_type=None):
        # get preupgrade version and node counts
        params = self.get_params()
        self.preupgrade_version = self.get_param_value('ProductVersion', params)
        self.preupgrade_alt_download_url = self.get_param_value('ProductDownloadUrl', params)
        if self.is_app_clustered():
            # determine if app nodes are split into AZs or not
            az_node_count_params: List[ParameterTypeDef] = self.search_param_keys(r'^Az\dNodeCount$', params)
            if len(az_node_count_params) > 0:
                self.preupgrade_az_node_counts: dict[str, int] = {
                    az_node_count_param['ParameterKey']: int(az_node_count_param['ParameterValue']) for az_node_count_param in az_node_count_params
                }
                self.preupgrade_app_node_count = sum(self.preupgrade_az_node_counts.values())
            else:
                self.preupgrade_app_node_count = int(self.get_param_value('ClusterNodeCount', params))
                bot_node_count_param_value: str = self.get_param_value('ClusterBotNodeCount', params)
                self.preupgrade_bot_node_count = int(bot_node_count_param_value) if bot_node_count_param_value != '' else 0
                if app_type == 'confluence':
                    synchrony_node_count_param_val: str = self.get_param_value('SynchronyClusterNodeCount', params)
                    self.preupgrade_synchrony_node_count = int(synchrony_node_count_param_val) if synchrony_node_count_param_val != '' else 0
                if self.preupgrade_bot_node_count and upgrade_type != 'zdu':
                    self.preupgrade_listener_rules = self.pop_alb_listener_rules()
            backup_node_count_param_value: str = self.get_param_value('BackupNodeCount', params)
            self.preupgrade_backup_node_count = int(backup_node_count_param_value) if backup_node_count_param_value != '' else 0
        # create changelog
        log_to_changelog(self.changelogfile, f'Pre upgrade version: {self.preupgrade_version}')
        if self.preupgrade_alt_download_url:
            log_to_ui(self, INFO, f'Pre upgrade alternate download URL: {self.preupgrade_alt_download_url}', write_to_changelog=True)

    # Retrieve the stack's ZDU state. Includes retries on failure since sometimes the endpoint is flaky.
    @tenacity.retry(retry_error_callback=return_last_value, retry=tenacity.retry_if_result(is_false_p), stop=tenacity.stop_after_attempt(5), wait=tenacity.wait_exponential(max=8))
    def get_zdu_state(self, app_type: str) -> Union[str, Literal[False]]:
        try:
            zdu_state_api: str = f"rest/{'api/2/cluster/' if app_type == 'jira' else ''}zdu/state"
            response = self.session.get(zdu_state_api, timeout=5)
            if response.status_code != requests.codes.ok:
                log_to_ui(self, ERROR, f'Unable to get ZDU state: {zdu_state_api} returned status code: {response.status_code}', write_to_changelog=True)
                return False
            return response.json()['state']
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
            log.exception('ZDU state check timed out')
            log_to_ui(self, INFO, 'ZDU state check timed out', write_to_changelog=False)
        except Exception as e:
            log.exception('Error occurred getting ZDU state')
            log_to_ui(self, ERROR, f'Could not retrieve ZDU state: {e}', write_to_changelog=True)
        return False

    def enable_zdu_mode(self, app_type):
        try:
            enable_zdu_api: str = f"rest/{'api/2/cluster/' if app_type == 'jira' else ''}zdu/start"
            response = self.session.post(enable_zdu_api, timeout=5)
            if response.status_code not in (requests.codes.created, requests.codes.ok):
                log_to_ui(self, ERROR, f'Unable to enable ZDU mode: {enable_zdu_api} returned {response.status_code}: {response.text}', write_to_changelog=True)
                return False
            action_start = time.time()
            action_timeout = current_app.config['ACTION_TIMEOUTS']['enable_zdu_mode']
            while self.get_zdu_state(app_type) != 'READY_TO_UPGRADE':
                if (time.time() - action_start) > action_timeout:
                    log_to_ui(self, ERROR, f'Stack is not in READY_TO_UPGRADE mode after {format_timespan(action_timeout)} - aborting', write_to_changelog=True)
                    return False
                time.sleep(5)
            log_to_ui(self, INFO, 'ZDU mode enabled', write_to_changelog=True)
            return True
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
            log.exception('Could not enable ZDU mode')
            log_to_ui(self, ERROR, f'Could not enable ZDU mode: {e}', write_to_changelog=True)
        except Exception as e:
            log.exception('Error occurred enabling ZDU mode')
            log_to_ui(self, ERROR, f'Error occurred enabling ZDU mode: {e}', write_to_changelog=True)
        return False

    def cancel_zdu_mode(self, app_type):
        try:
            cancel_zdu_api: str = f"rest/{'api/2/cluster/' if app_type == 'jira' else ''}zdu/cancel"
            response = self.session.post(cancel_zdu_api, timeout=5)
            if response.status_code != requests.codes.created:
                log_to_ui(self, ERROR, f'Unable to cancel ZDU mode: {cancel_zdu_api} returned status code: {response.status_code}', write_to_changelog=True)
                return False
            action_start = time.time()
            action_timeout = current_app.config['ACTION_TIMEOUTS']['cancel_zdu_mode']
            while self.get_zdu_state(app_type) != 'STABLE':
                if (time.time() - action_start) > action_timeout:
                    log_to_ui(self, ERROR, f'Stack is not in STABLE mode after {format_timespan(action_timeout)} - ZDU mode cancel failed', write_to_changelog=True)
                    return False
                time.sleep(5)
            log_to_ui(self, INFO, 'ZDU mode cancelled', write_to_changelog=True)
            return True
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
            log.exception('Could not cancel ZDU mode')
            log_to_ui(self, ERROR, f'Could not cancel ZDU mode: {e}', write_to_changelog=True)
        except Exception as e:
            log.exception('Error occurred cancelling ZDU mode')
            log_to_ui(self, ERROR, f'Error occurred cancelling ZDU mode: {e}', write_to_changelog=True)
        return False

    def approve_zdu_upgrade(self, app_type):
        log_to_ui(self, INFO, 'Approving upgrade and running upgrade tasks', write_to_changelog=True)
        try:
            approve_zdu_api: str = f"rest/{'api/2/cluster/' if app_type == 'jira' else ''}zdu/approve"
            response = self.session.post(approve_zdu_api, timeout=30)
            if response.status_code != requests.codes.ok:
                log_to_ui(self, ERROR, f'Unable to approve upgrade: {approve_zdu_api} returned status code: {response.status_code}', write_to_changelog=True)
                return False
            log_to_ui(self, INFO, 'Upgrade tasks are running, waiting for STABLE state', write_to_changelog=True)
            state = self.get_zdu_state(app_type)
            action_start = time.time()
            action_timeout = current_app.config['ACTION_TIMEOUTS']['approve_zdu_upgrade']
            while state != 'STABLE':
                if (time.time() - action_start) > action_timeout:
                    log_to_ui(
                        self,
                        ERROR,
                        f'Stack is not in STABLE mode after {format_timespan(action_timeout)} - ' 'upgrade tasks may still be running but Weaver is aborting',
                        write_to_changelog=True,
                    )
                    return False
                log_to_ui(self, INFO, f'ZDU state is {state}', write_to_changelog=False)
                time.sleep(5)
                state = self.get_zdu_state(app_type)
            log_to_ui(self, INFO, 'Upgrade tasks complete', write_to_changelog=True)
            return True
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
            log.exception('Could not cancel ZDU mode')
            log_to_ui(self, ERROR, f'Could not approve ZDU mode: {e}', write_to_changelog=True)
        except Exception as e:
            log.exception('Error occurred approving ZDU mode')
            log_to_ui(self, ERROR, f'Error occurred approving ZDU mode: {e}', write_to_changelog=True)
        return False

    def get_zdu_compatibility(self) -> Dict[str, Union[str, bool]]:
        app_type = self.get_tag('product', log_msgs=False)
        if app_type not in ('jira', 'confluence', 'bitbucket'):
            return {'compatible': False, 'reason': f'{app_type} does not have ZDU support in Weaver'}
        nodes = self.get_stacknodes()
        if not len(nodes) > 1:
            return {'compatible': False, 'reason': 'too few nodes'}
        params = self.get_params()
        current_version = version_tuple(self.get_param_value('ProductVersion', params))
        if (app_type == 'bitbucket' and current_version >= ZDU_MINIMUM_BITBUCKET_VERSION) or (app_type == 'confluence' and current_version >= ZDU_MINIMUM_CONFLUENCE_VERSION):
            return {'compatible': True, 'reason': 'Enter a target upgrade version to check ZDU/Rolling upgrade compatibility'}
        elif app_type == 'jira':
            jira_product = self.get_param_value('JiraProduct', params)
            if jira_product == 'ServiceDesk':
                if current_version >= ZDU_MINIMUM_SERVICEDESK_VERSION:
                    return {'compatible': True}
            elif current_version >= ZDU_MINIMUM_JIRACORE_VERSION:
                return {'compatible': True}
        return {'compatible': False, 'reason': f"version {'.'.join(map(str, current_version))} does not support ZDU"}

    def has_termination_protection(self) -> bool:
        # check stack for termination protection
        cfn = boto3.client('cloudformation', region_name=self.region)
        stack_details = cfn.describe_stacks(StackName=self.stack_name)['Stacks'][0]
        if 'EnableTerminationProtection' in stack_details and stack_details['EnableTerminationProtection']:
            return True
        # check ec2 nodes for termination protection
        nodes = self.get_stacknodes()
        for node in nodes:
            response = node.describe_attribute(Attribute='disableApiTermination')
            if response['DisableApiTermination']['Value'] is True:
                return True
        return False

    def delete_from_s3(self, folder):
        s3_bucket = self.get_s3_bucket_name()
        s3 = boto3.client('s3', region_name=self.region)
        paginator = s3.get_paginator('list_objects_v2')
        page_iterator = paginator.paginate(Bucket=s3_bucket, Prefix=folder)
        for files in page_iterator:
            keys_to_delete: list[ObjectIdentifierTypeDef] = []
            if 'Contents' in files:
                for file in files['Contents']:
                    keys_to_delete.append({'Key': file['Key']})
                keys_to_delete.append({'Key': folder})
                response = s3.delete_objects(
                    Bucket=s3_bucket,
                    Delete={'Objects': keys_to_delete},
                )
                if 'Errors' in response:
                    log_to_ui(self, ERROR, 'Failed to delete some files from S3', write_to_changelog=True)

    def get_event_logs(self, client_request_token, logged_events: list[str] = []) -> list[str]:
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            all_events = cfn.describe_stack_events(StackName=self.stack_id)['StackEvents']
            all_events.reverse()
            for event in all_events:
                if 'ClientRequestToken' in event and event['ClientRequestToken'] == client_request_token and event['EventId'] not in logged_events:
                    logline = f"{event['LogicalResourceId']} | {event['ResourceStatus']} | {event['ResourceStatusReason'] if 'ResourceStatusReason' in event else ''}"
                    log_to_ui(self, INFO, logline, write_to_changelog=False)
                    logged_events.append(event['EventId'])
        except Exception as e:
            log.exception('Error getting stack events')
            log_to_ui(self, ERROR, f'Error getting stack events: {e}', write_to_changelog=False)
        return logged_events

    def get_node_cpu(self, node_ip):
        nodes = self.get_stacknodes()
        node_id = None
        try:
            node_id = list(filter(lambda instance: instance.private_ip_address == node_ip, nodes))[0].id
        except (AttributeError, IndexError) as e:
            log.exception('Error getting node information')
            log_to_ui(self, ERROR, f'Error getting node information: {e}', write_to_changelog=True)
            return False
        cloud_watch = boto3.client('cloudwatch', region_name=self.region)
        try:
            response = cloud_watch.get_metric_statistics(
                Namespace='AWS/EC2',
                MetricName='CPUUtilization',
                Dimensions=[{'Name': 'InstanceId', 'Value': node_id}],
                StartTime=datetime.now(UTC) - timedelta(minutes=30),
                EndTime=datetime.now(UTC),
                Period=60,
                Statistics=['Average'],
            )
            cpu_data = {}
            for datapoint in response['Datapoints']:
                seconds_since_epoch = int((datapoint['Timestamp'] - datetime(1970, 1, 1, 0, 0, tzinfo=datapoint['Timestamp'].tzinfo)).total_seconds())
                cpu_data[seconds_since_epoch] = datapoint['Average']
        except Exception as e:
            log.exception('Error getting node CPU')
            log_to_ui(self, ERROR, f'Error getting node CPU: {e}', write_to_changelog=False)
        return cpu_data

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def pop_alb_listener_rules(self):
        alb_listener_rules = []
        alb_listener_arn = [resource['PhysicalResourceId'] for resource in self.get_stack_resource_list() if resource['ResourceType'] == 'AWS::ElasticLoadBalancingV2::Listener'][0]
        if alb_listener_arn:
            client = self.boto_session.client('elbv2')
            log_to_ui(self, INFO, 'Storing ALB listener rules', write_to_changelog=True)
            try:
                alb_listener_rules = [rule for rule in client.describe_rules(ListenerArn=alb_listener_arn)['Rules'] if not rule['IsDefault']]
                log_to_ui(self, INFO, f'ALB listener rules for {alb_listener_arn}: {alb_listener_rules}', write_to_changelog=True)
            except Exception as e:
                log.exception('An error occurred retrieving ALB listener rules')
                log_to_ui(self, ERROR, f'An error occurred retrieving ALB listener rules: {e}', write_to_changelog=True)
            try:
                for rule in alb_listener_rules:
                    log_to_ui(self, INFO, 'Deleting ALB listener rules', write_to_changelog=True)
                    client.delete_rule(RuleArn=rule['RuleArn'])
            except Exception as e:
                log.exception('An error occurred deleting ALB listener rules')
                log_to_ui(self, ERROR, f'An error occurred deleting ALB listener rules: {e}', write_to_changelog=True)
        else:
            log_to_ui(self, WARN, 'No ALB found in stack resources', write_to_changelog=True)
        return alb_listener_rules

    @retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
    def push_alb_listener_rules(self, rules):
        alb_listener_arn = [resource['PhysicalResourceId'] for resource in self.get_stack_resource_list() if resource['ResourceType'] == 'AWS::ElasticLoadBalancingV2::Listener'][0]
        if not alb_listener_arn:
            log_to_ui(self, WARN, 'No ALB found in stack resources', write_to_changelog=True)
            return False
        client = self.boto_session.client('elbv2')
        # check for existing listener rules (maybe created by other node-init automation, e.g. ansible); we don't want to clobber
        alb_listener_rules = [rule for rule in client.describe_rules(ListenerArn=alb_listener_arn)['Rules'] if not rule['IsDefault']]
        if any(alb_listener_rules):
            log_to_ui(self, WARN, 'Found existing listener rules; skipping re-creation of pre-upgrade listener rules', write_to_changelog=True)
            return True
        log_to_ui(self, INFO, f'Re-creating ALB listener rules for {alb_listener_arn}', write_to_changelog=True)
        try:
            for rule in rules:
                for action in rule['Actions']:
                    # get the name of the old target group from the existing forwarding rule
                    old_target_group_name = action['TargetGroupArn'].split('/')[1]
                    # find the ARN of the new target group with the same name from current stack resources
                    new_target_group_arn = next(
                        resource['PhysicalResourceId']
                        for resource in self.get_stack_resource_list(force=True)
                        if resource['ResourceType'] == 'AWS::ElasticLoadBalancingV2::TargetGroup' and f'targetgroup/{old_target_group_name}/' in resource['PhysicalResourceId']
                    )
                    # modify the forward action to point to the new target group
                    if 'TargetGroupArn' in action:
                        action['TargetGroupArn'] = new_target_group_arn
                    if 'ForwardConfig' in action:
                        for tg in action['ForwardConfig']['TargetGroups']:
                            tg['TargetGroupArn'] = new_target_group_arn
                client.create_rule(ListenerArn=alb_listener_arn, Conditions=rule['Conditions'], Priority=int(rule['Priority']), Actions=rule['Actions'])
        except Exception as e:
            log.exception('An error occurred creating ALB listener rules')
            log_to_ui(self, ERROR, f'An error occurred creating ALB listener rules: {e}', write_to_changelog=True)
            return False
        return True

    def suspend_AddToLoadBalancer(self, asg_name=None) -> bool:
        asgs: list[dict[str, str]] = self.get_stack_asgs() if asg_name is None else [{'AutoScalingGroupName': asg_name}]
        try:
            for asg in asgs:
                log_to_ui(self, INFO, f"Suspending AddToLoadBalancer for asg: {asg['AutoScalingGroupName']}", write_to_changelog=True)
                boto3.client('autoscaling', region_name=self.region).suspend_processes(
                    AutoScalingGroupName=asg['AutoScalingGroupName'],
                    ScalingProcesses=[
                        'AddToLoadBalancer',
                    ],
                )
        except Exception:
            log.exception("Unable to suspend autoscaling group's AddToLoadBalancer functionality")
            log_to_ui(self, ERROR, "Unable to suspend autoscaling group's AddToLoadBalancer functionality", write_to_changelog=True)
            return False
        return True

    def resume_AddToLoadBalancer(self, asg_name=None) -> bool:
        asgs: list[dict[str, str]] = self.get_stack_asgs() if asg_name is None else [{'AutoScalingGroupName': asg_name}]
        try:
            for asg in asgs:
                log_to_ui(self, INFO, f"Resuming AddToLoadBalancer for asg: {asg['AutoScalingGroupName']}", write_to_changelog=True)
                boto3.client('autoscaling', region_name=self.region).resume_processes(
                    AutoScalingGroupName=asg['AutoScalingGroupName'],
                    ScalingProcesses=[
                        'AddToLoadBalancer',
                    ],
                )
        except Exception:
            log.exception("Unable to resume autoscaling group's AddToLoadBalancer functionality")
            log_to_ui(
                self,
                ERROR,
                "Unable to resume autoscaling group's AddToLoadBalancer functionality."
                "You will need to manually resume AddToLoadBalancer for the autoscaling group in the AWS console",
                write_to_changelog=True,
            )
            return False
        return True

    def get_ebs_snapshots(self) -> List[Dict[str, str]]:
        ec2 = boto3.client('ec2', region_name=self.region)
        snap_name_format: str = f'{self.stack_name}_ebs_snap_*'
        if self.region != session['region']:
            snap_name_format = f'dr_{snap_name_format}'
        try:
            snapshots = ec2.describe_snapshots(Filters=[{'Name': 'tag-value', 'Values': [snap_name_format]}, {'Name': 'status', 'Values': ['completed']}])
        except botocore.exceptions.ClientError:
            log.exception('Error occurred getting EBS snapshots')
            return []
        snapshot_ids: List[Dict[str, str]] = []
        for snap in snapshots['Snapshots']:
            start_time = str(snap['StartTime'])
            if '.' in start_time:
                start_time = start_time.split('.')[0]
            else:
                start_time = start_time.split('+')[0]
            snapshot_ids.append({'label': f"{start_time} ({snap['SnapshotId']})", 'value': snap['SnapshotId']})
        snapshot_ids = sorted(snapshot_ids, key=lambda x: x['label'], reverse=True)
        return snapshot_ids

    def get_rds_snapshots(self, cloned_from_region) -> List[Dict[str, str]]:
        cfn = boto3.client('cloudformation', region_name=cloned_from_region)
        rds = boto3.client('rds', region_name=self.region)
        snapshot_ids: List[Dict[str, str]] = []

        def rds_snapshot_labelizer(snap):
            return f'{str(snap["SnapshotCreateTime"]).split(".")[0]} ({snap["DBSnapshotIdentifier"]}, {snap["Engine"]} {snap["EngineVersion"]})'

        try:
            # get RDS instance from stack resources
            rds_name: str = cfn.describe_stack_resource(LogicalResourceId='DB', StackName=self.stack_name)['StackResourceDetail']['PhysicalResourceId']
            # get snapshots and append ids to list
            snapshots_response = rds.describe_db_snapshots(DBInstanceIdentifier=rds_name)
            for snap in snapshots_response['DBSnapshots']:
                if 'SnapshotCreateTime' in snap and 'DBSnapshotIdentifier' in snap and snap['Status'] == 'available':
                    snapshot_ids.append({'label': rds_snapshot_labelizer(snap), 'value': snap['DBSnapshotIdentifier']})
            # if there are more than 100 snapshots the response will contain a marker, get the next lot of snapshots and add them to the list
            while 'Marker' in snapshots_response:
                snapshots_response = rds.describe_db_snapshots(DBInstanceIdentifier=rds_name, Marker=snapshots_response['Marker'])
                for snap in snapshots_response['DBSnapshots']:
                    if 'SnapshotCreateTime' in snap and 'DBSnapshotIdentifier' in snap and snap['Status'] == 'available':
                        snapshot_ids.append({'label': rds_snapshot_labelizer(snap), 'value': snap['DBSnapshotIdentifier']})
        except botocore.exceptions.ClientError:
            log.exception('Error occurred getting RDS snapshots')
            return []
        snapshot_ids = sorted(snapshot_ids, key=lambda x: x['label'], reverse=True)
        return snapshot_ids

    def download_scripts_onto_ec2_nodes(self):
        project_root = [folder for folder in Path(__file__).parents if folder.parts[-1] == 'weaver'][0].parent
        scripts_dir = f'{project_root}/scripts/'
        scripts = os.listdir(scripts_dir)
        for script in scripts:
            if not self.upload_file_to_s3(f'{scripts_dir}/{script}', f'scripts/{script}'):
                log_to_ui(self, WARN, 'Uploading scripts to S3 failed; check permissions', write_to_changelog=True)
                return False
        if not self.download_files_from_s3('scripts', '/usr/local/bin'):
            log_to_ui(self, WARN, 'Downloading scripts from S3 failed; check EC2 nodes have the s3:ListBucket IAM permission', write_to_changelog=True)
            return False
        log_to_ui(self, INFO, 'Scripts successfully downloaded to /usr/local/bin', write_to_changelog=True)
        return True

    ##
    # Stack - Major Action Methods
    #

    def upgrade(self, new_version, alt_download_url, startup_primary: bool = True, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT):
        log_to_ui(
            self,
            INFO,
            f'Beginning upgrade for {self.stack_name}{"; fast drain mode" if drain_type == weaver_enums.DrainType.FAST else ""}',
            write_to_changelog=False,
            send_sns_msg=True,
        )
        # get product
        app_type = self.get_app_type()
        if not app_type:
            log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        if not self.drain_type_allowed(drain_type):
            log_to_ui(self, ERROR, f'Stack does not support drain type {drain_type.value}, please select a different type.', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        self.get_pre_upgrade_information(app_type)
        if not self.preupgrade_app_node_count and not self.preupgrade_bot_node_count:
            log_to_ui(self, INFO, 'No nodes in stack to upgrade', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete', write_to_changelog=True, send_sns_msg=True)
            return True
        # for jira with JiraProduct=All cater for custom jsm obr location
        if app_type == "jira":
            jira_product = self.get_param_value('JiraProduct')
            if alt_download_url and jira_product == 'All':
                self.pre_upgrade_param_changes_for_custom_obr(new_version, alt_download_url)
        log_to_changelog(self.changelogfile, f'New version: {new_version}')
        if alt_download_url:
            log_to_ui(self, INFO, f'Alternate download URL: {alt_download_url}', write_to_changelog=True)
        log_to_changelog(self.changelogfile, 'Upgrade is underway')
        if 'mesh' in app_type:
            if not self.upgrade_mesh(new_version, alt_download_url, startup_primary):
                log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        elif self.is_app_clustered():
            if not self.upgrade_dc(new_version, alt_download_url, drain_type):
                log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        else:
            if not self.upgrade_server(new_version, alt_download_url, app_type):
                log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        log_to_ui(self, INFO, f'Upgrade successful for {self.stack_name} at {self.region} to version {new_version}', write_to_changelog=False)
        log_to_ui(self, INFO, 'Upgrade complete', write_to_changelog=True, send_sns_msg=True)
        return True

    def pre_upgrade_param_changes_for_custom_obr(self, new_version, alt_download_url):
        custom_prefix = alt_download_url.split(new_version)[0]
        # Create jsm_obr_location from known path and JSM major version instead of JSW major version
        jsw_version_parts: list = new_version.split('.')
        jsm_version_parts: list = jsw_version_parts[:]
        if int(jsw_version_parts[0]) < 10:
            jsm_version_parts[0] = str(int(jsw_version_parts[0]) - 4)
        jsm_version: str = '.'.join(jsm_version_parts)
        jsm_obr_location: str = f'{custom_prefix}{new_version}/jira-servicedesk-application-{jsm_version}.obr'
        # modify atl_jsd_build and atl_obr_download_url in 'DeploymentAutomationCustomParams'
        deployment_params: str = self.get_param_value('DeploymentAutomationCustomParams')
        # add or modify the 'atl_jsd_build' custom deployment param
        if 'atl_jsd_build' not in deployment_params:
            deployment_params += f' -e atl_jsd_build={jsm_version}'
        else:
            deployment_params = re.sub(r'atl_jsd_build=([^\s]+)', f'atl_jsd_build={jsm_version}', deployment_params)
        # add or modify the 'atl_obr_download_url' custom deployment param
        if 'atl_obr_download_url' not in deployment_params:
            deployment_params += f' -e atl_obr_download_url={jsm_obr_location}'
        else:
            deployment_params = re.sub(r'atl_obr_download_url=([^\s]+)', f'atl_obr_download_url={jsm_obr_location}', deployment_params)
        updated_params = self.update_paramlist(self.get_params(), 'DeploymentAutomationCustomParams', deployment_params)
        log_to_ui(self, INFO, f'DeploymentAutomationCustomParams updated to: {deployment_params}', write_to_changelog=True)
        # now push those changes to cfn before returning to normal upgrade process
        cfn = boto3.client('cloudformation', region_name=self.region)
        client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
        try:
            cfn.update_stack(
                StackName=self.stack_name, Parameters=updated_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token
            )
        except botocore.exceptions.ClientError as e:
            if 'No updates are to be performed' in e.args[0]:
                log_to_ui(self, INFO, f'No parameter changes are required to stack {self.stack_name}', write_to_changelog=True)
            else:
                log.exception(f'Error putting stack parameters - {e.args[0]}, {updated_params}')
                log_to_ui(self, INFO, f'Error putting stack parameters - {e.args[0]}', write_to_changelog=True)
                return False
        # wait for stack action to complete before continuing with upgrade
        if self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            log_to_ui(self, INFO, 'Successfully updated cloudformation parameters for custom obr', write_to_changelog=True)
            return True
        return False

    def upgrade_dc(self, new_version, alt_download_url, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT):
        # check if an NFS server needs replacing, skip app start up steps since nodes will be rebuilt anyway
        if self.check_nfs_server_replacement():
            if not self.replace_nfs_server(skip_remount=True, skip_app_start=True):
                log_to_ui(self, ERROR, 'NFS server replacement failed', write_to_changelog=True)
                log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
                return False
            # no need to shutdown app, is already done as part of NFS server replacement
        else:
            # shutdown app on all nodes
            self.shutdown_app(self.get_stacknodes())
        # spin stack down to 0 nodes
        if not self.spindown_to_zero_appnodes(drain_type):
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        # if fast node drain was toggled on, restore original deregistration delay values
        if drain_type == weaver_enums.DrainType.FAST:
            self.restore_original_deregistration_delays_if_needed()
        if self.get_app_type() == 'bitbucket':
            if not self.spinup_all_nodes(new_version, alt_download_url):
                log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
                return False
        else:
            # spin stack up to 1 node on new release version
            if not self.spinup_to_one_appnode(new_version, alt_download_url):
                log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
                return False
            # spinup remaining nodes in stack if needed
            if self.preupgrade_app_node_count > 1 or self.preupgrade_bot_node_count or self.preupgrade_backup_node_count:
                self.spinup_remaining_nodes(new_version)
        # re-create any listener rules that existed prior to the upgrade
        if self.preupgrade_listener_rules:
            if not self.push_alb_listener_rules(self.preupgrade_listener_rules):
                log_to_ui(self, ERROR, 'ALB listener rules were not re-created!', write_to_changelog=True)
        # wait for target group(s) to report all targets "healthy"
        if not self.wait_all_healthy_targets():
            return False
        if not self.validate_all_stack_nodes_product_versions(new_version):
            return False
        # TODO enable traffic at VTM
        return True

    def upgrade_mesh(self, new_version, alt_download_url, startup_primary: bool = True):
        app_type = self.get_app_type()
        # fail upgrade if product tag does not contain 'mesh'
        if not app_type or 'mesh' not in app_type:
            log_to_ui(self, INFO, 'Stack product tag does not contain "mesh", aborting mesh upgrade', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        # retrieve primary stack nodes
        bb_primary_stack_nodes = self.get_primary_stack_nodes()
        if not bb_primary_stack_nodes:
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        log_to_ui(self, INFO, f'Bitbucket primary stack nodes: {bb_primary_stack_nodes}', write_to_changelog=True)
        # shutdown bitbucket service
        self.shutdown_app(bb_primary_stack_nodes)
        # shutdown mesh
        self.shutdown_app(self.get_stacknodes())
        # spin mesh down to 0 nodes in all AZs
        if not self.spindown_to_zero_appnodes():
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        # spinup all nodes at once
        if not self.spinup_all_nodes(new_version, alt_download_url):
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        # start primary stack bitbucket service unless that's been toggled off
        if startup_primary:
            if not self.startup_app(bb_primary_stack_nodes, primary_stack=True):
                return False
        else:
            log_to_ui(self, WARN, 'Skipping startup of primary cluster', write_to_changelog=True)
        # verify for mesh nodes are all serving
        if not self.wait_all_serving_mesh_nodes():
            return False
        return True

    def upgrade_server(self, new_version, alt_download_url, app_type):
        # check if an NFS server needs replacing, skip app start up steps since nodes will be rebuilt anyway
        if self.check_nfs_server_replacement():
            if not self.replace_nfs_server(skip_remount=True, skip_app_start=True):
                log_to_ui(self, ERROR, 'NFS server replacement failed', write_to_changelog=True)
                log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
                return False
            # no need to shutdown app, is already done as part of NFS server replacement
        else:
            # shutdown app on all nodes
            self.shutdown_app(self.get_stacknodes())
        # update the version in stack parameters
        stack_params = self.get_params()
        if any(param for param in stack_params if param['ParameterKey'] == 'ProductVersion'):
            stack_params = self.update_paramlist(stack_params, 'ProductVersion', new_version)
        elif app_type == 'jira':
            stack_params = self.update_paramlist(stack_params, 'JiraVersion', new_version)
        elif app_type == 'confluence':
            stack_params = self.update_paramlist(stack_params, 'ConfluenceVersion', new_version)
        elif app_type == 'crowd':
            stack_params = self.update_paramlist(stack_params, 'CrowdVersion', new_version)
        # update the alternate download URL in the stack parameters
        stack_params = self.update_paramlist(stack_params, 'ProductDownloadUrl', alt_download_url)
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            cfn.update_stack(StackName=self.stack_name, Parameters=stack_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token)
        except Exception as e:
            if 'No updates are to be performed' in e.args[0]:
                log_to_ui(self, INFO, f'Stack is already at {new_version}', write_to_changelog=True)
            else:
                log.exception('An error occurred updating the version')
                log_to_ui(self, ERROR, f'An error occurred updating the version: {e}', write_to_changelog=True)
                return False
        if self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            log_to_ui(self, INFO, 'Successfully updated version in stack parameters', write_to_changelog=False)
        else:
            log_to_ui(self, INFO, 'Could not update version in stack parameters', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        # terminate the node and allow new one to spin up
        if not self.rolling_rebuild():
            log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        return True

    def upgrade_zdu(self, new_version, alt_download_url, zdu_type, personal_access_token):  # noqa: C901 (too complex)
        if self.check_nfs_server_replacement():
            log_to_ui(
                self,
                ERROR,
                'This stack requires an NFS server replacement, which requires downtime. Please use a non-ZDU upgrade method, or replace the NFS server in a standalone stack update.',
                write_to_changelog=True,
            )
            log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        log_to_ui(self, INFO, f'Beginning upgrade for {self.stack_name}', write_to_changelog=False, send_sns_msg=True)
        # get product
        app_type = self.get_tag('product')
        if not app_type:
            log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        try:
            if not self.get_service_url():
                log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        except tenacity.RetryError:
            log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        self.session = BaseUrlSession(base_url=self.service_url)
        self.session.headers = {
            'X-Atlassian-Token': 'no-check',
            'Authorization': f'Bearer {personal_access_token}',
        }
        self.get_pre_upgrade_information(app_type, 'zdu')
        log_to_changelog(self.changelogfile, f'New version: {new_version}')
        if alt_download_url:
            log_to_ui(self, INFO, f'Alternate download URL: {alt_download_url}', write_to_changelog=True)
        log_to_changelog(self.changelogfile, 'Upgrade is underway')
        # set upgrade mode on
        zdu_state = self.get_zdu_state(app_type)
        if zdu_state != 'READY_TO_UPGRADE':  # if previously put in upgrade mode but upgrade has not begun, skip this
            if zdu_state != 'STABLE':
                log_to_ui(self, INFO, f'Expected STABLE but ZDU state is {zdu_state}', write_to_changelog=True)
                log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.enable_zdu_mode(app_type):
                log_to_ui(self, ERROR, 'Could not enable ZDU mode', write_to_changelog=True)
                log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        # disable cfn-hup service on existing nodes if ZDU will re-use existing nodes; will be re-enabled by cfn-init
        # do this before stack update to prevent collions with cfn-init (via cfn-hup) as a result of stack update
        if zdu_type == 'inplace' and not self.toggle_cfn_hup('stop'):
            log_to_ui(self, INFO, 'Failed to stop cfn-hup on all nodes', write_to_changelog=True)
            log_to_ui(self, ERROR, 'Rolling re-init complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        # for jira with JiraProduct=All cater for custom jsm obr location
        if app_type == "jira":
            jira_product = self.get_param_value('JiraProduct')
            if alt_download_url and jira_product == 'All':
                log_to_ui(self, INFO, 'working out pre-upgrade parameters', write_to_changelog=True)
                self.pre_upgrade_param_changes_for_custom_obr(new_version, alt_download_url)
        # update the version in stack parameters
        stack_params = self.get_params()
        if any(param for param in stack_params if param['ParameterKey'] == 'ProductVersion'):
            stack_params = self.update_paramlist(stack_params, 'ProductVersion', new_version)
        else:
            stack_params = self.update_paramlist(stack_params, 'JiraVersion', new_version)
        # update the alternate download URL in the stack parameters
        stack_params = self.update_paramlist(stack_params, 'ProductDownloadUrl', alt_download_url)
        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            cfn.update_stack(StackName=self.stack_name, Parameters=stack_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token)
        except Exception as e:
            if 'No updates are to be performed' in e.args[0]:
                log_to_ui(self, INFO, f'Stack is already at {new_version}', write_to_changelog=True)
            else:
                log.exception('An error occurred updating the version')
                log_to_ui(self, ERROR, f'An error occurred updating the version: {e}', write_to_changelog=True)
                log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
                self.cancel_zdu_mode(app_type)
                return False
        if self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            log_to_ui(self, INFO, 'Successfully updated version in stack parameters', write_to_changelog=False)
        else:
            log_to_ui(self, INFO, 'Could not update version in stack parameters', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
            self.cancel_zdu_mode(app_type)
            return False
        if zdu_type == 'inplace':
            # re-initialize the nodes one at a time; wait for service to be healthy on each before continuing
            if not self.rolling_reinit():
                log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
        else:
            # terminate the nodes and allow new ones to spin up, wait for index to be recovered before putting into service
            if not self.rolling_rebuild(None, personal_access_token):
                log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)
        state = self.get_zdu_state(app_type)
        if self.preupgrade_version == new_version and state == 'READY_TO_UPGRADE':
            # This is fine since an upgrade didn't actually happen, cancel ZDU mode and report success
            self.cancel_zdu_mode(app_type)
            log_to_ui(self, INFO, 'Upgrade complete', write_to_changelog=True, send_sns_msg=True)
            return True
        action_start = time.time()
        action_timeout = current_app.config['ACTION_TIMEOUTS']['zdu_ready_to_run_upgrade_tasks']
        while state != 'READY_TO_RUN_UPGRADE_TASKS':
            if (time.time() - action_start) > action_timeout:
                log_to_ui(
                    self,
                    ERROR,
                    f'Stack is in state {state} not READY_TO_RUN_UPGRADE_TASKS mode after {format_timespan(action_timeout)} - aborting',
                    write_to_changelog=True,
                    send_sns_msg=True,
                )
                return False
            time.sleep(5)
            state = self.get_zdu_state(app_type)
        # approve the upgrade and allow upgrade tasks to run
        if self.approve_zdu_upgrade(app_type):
            log_to_ui(self, INFO, f'Upgrade successful for {self.stack_name} at {self.region} to version {new_version}', write_to_changelog=False)
            log_to_ui(self, INFO, 'Upgrade complete', write_to_changelog=True, send_sns_msg=True)
            return True
        else:
            log_to_ui(self, ERROR, 'Could not approve upgrade. The upgrade will need to be manually approved or cancelled.', write_to_changelog=True)
            log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True, send_sns_msg=True)

    def destroy(self, delete_changelogs, delete_threaddumps):
        log_to_ui(self, INFO, f'Destroying stack {self.stack_name} in {self.region}', write_to_changelog=not delete_changelogs, send_sns_msg=True)
        cfn = boto3.client('cloudformation', region_name=self.region)
        # at this point we know the stack exists, so set the stack_id now to ensure check_stack_state below uses the correct stack_id
        self.stack_id = boto3.client('cloudformation', self.region).describe_stacks(StackName=self.stack_name)['Stacks'][0]['StackId']
        try:
            cfn.delete_stack(StackName=self.stack_name, ClientRequestToken=g.action_id)
        except botocore.exceptions.ClientError as e:
            log.exception('An error occurred destroying stack')
            log_to_ui(self, ERROR, f'An error occurred destroying stack: {e}', write_to_changelog=not delete_changelogs)
            log_to_ui(self, ERROR, 'Destroy complete - failed', write_to_changelog=not delete_changelogs, send_sns_msg=True)
            return False
        if not self.wait_stack_action_complete('DELETE_IN_PROGRESS', g.action_id):
            log_to_ui(self, ERROR, 'Destroy complete - failed', write_to_changelog=not delete_changelogs, send_sns_msg=True)
            return False
        if not self.check_stack_state() == 'DELETE_COMPLETE':
            log_to_ui(self, ERROR, 'Destroy complete - failed', write_to_changelog=not delete_changelogs, send_sns_msg=True)
            return False
        log_to_ui(self, INFO, f'Destroy successful for stack {self.stack_name}', write_to_changelog=not delete_changelogs)
        if delete_changelogs:
            self.delete_from_s3(f'changelogs/{self.stack_name}')
        if delete_threaddumps:
            self.delete_from_s3(f'diagnostics/{self.stack_name}')
        log_to_ui(self, INFO, 'Destroy complete', write_to_changelog=not delete_changelogs, send_sns_msg=True)
        return True

    def clone(self, product: ProductEnum, template, stack_params, tags, cloned_from):
        log_to_ui(self, INFO, 'Initiating clone', write_to_changelog=True, send_sns_msg=True)
        try:
            # query stackname to see if it exists - if not it will throw a ClientError and we skip the destroy
            self.check_exists()
        except weaver_exceptions.StackDoesNotExistException:
            log_to_ui(self, INFO, f'Stack {self.stack_name} does not exist, destroy not required', write_to_changelog=True)
        else:
            # TODO popup confirming if you want to destroy existing
            if not self.destroy(delete_changelogs=False, delete_threaddumps=False):
                log_to_ui(self, INFO, 'Clone complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        tags["cloned_from"] = cloned_from
        if not self.create(product, template, stack_params, tags):
            log_to_ui(self, INFO, 'Clone complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        log_to_changelog(self.changelogfile, 'Create complete, looking for post-clone SQL')
        if self.get_tag('environment', False) == 'dr':
            log_to_ui(self, INFO, 'Clone environment is DR, skipping post clone SQL/liquibase', write_to_changelog=True)
        else:
            post_clone_sql_result = self.run_sql()
            if post_clone_sql_result:
                log_to_changelog(self.changelogfile, f'SQL complete; restarting {self.stack_name}')
                self.full_restart()
            elif post_clone_sql_result is None:
                log_to_changelog(self.changelogfile, 'SQL complete; skipping full restart')
            else:
                log_to_ui(self, INFO, 'Clone complete - Run SQL failed', write_to_changelog=True, send_sns_msg=True)
                return False
        log_to_ui(self, INFO, 'Clone complete', write_to_changelog=True, send_sns_msg=True)
        return True

    def create_change_set(self, template: dict[str, str] | None, params: list[ParameterTypeDef], tags: dict[str, str] = {}) -> CreateChangeSetOutputTypeDef:
        cfn_resource = boto3.resource('cloudformation', region_name=self.region)
        cfn_stack = cfn_resource.Stack(self.stack_name)

        # stub out the change set request args
        change_set_params = {
            "ChangeSetName": f"{self.stack_name}-{datetime.now().strftime('%Y%m%d-%H%M%S')}",
            "ChangeSetType": "UPDATE",
            "StackName": self.stack_name,
            "Capabilities": ["CAPABILITY_IAM"],
        }

        # log the existing template parameters that were applied to the stack to changelog only as there is no UI log on change set creation
        log_to_changelog(
            self.changelogfile, f"Existing parameters: {[{k: v for k, v in param.items() if k in ('ParameterKey', 'ParameterValue')} for param in cfn_stack.parameters]}"
        )

        # log the passed parameters
        log_to_changelog(self.changelogfile, f"Passed parameters: {[{k: v for k, v in param.items() if k in ('ParameterKey', 'ParameterValue')} for param in params]}")

        # to update tags, we have to send all the existing tags, and we need to exclude duplicates to ensure the correct value is set
        # to make things "easy", transform the existing tags list to a dict, update that dict in-place with the new values, then transform back to list of Key/Value dicts AWS expects
        # if a tag value is empty, treat it as a tag deletion and exclude it from the list
        # (while some services allow empty tag values, Cloudformation is not one of them; our schema requires a Value key, but doesn't enforce a value)
        if tags:
            change_set_tags = utils.tag_list_to_dict(cfn_stack.tags)
            change_set_tags |= tags
            change_set_params["Tags"] = utils.tag_dict_to_list(change_set_tags)

        # if a template was provided, upload it to S3, set the TemplateURL, and save the parameter keys for later comparison
        # otherwise, use the previous template
        if template:
            template_file: Path = utils.get_template_file(template)
            self.upload_file_to_s3(template_file, f"weaver-templates/{template_file.name}")
            change_set_params["TemplateURL"] = f"https://s3.amazonaws.com/{self.get_s3_bucket_name()}/weaver-templates/{template_file.name}"
            template_param_keys = set(utils.get_template_params(template).keys())
        else:
            change_set_params["UsePreviousTemplate"] = True
            template_param_keys = set()

        # create a dict from the passed params; key is the ParameterKey and the value is all remaining key/value pairs of the param
        # (at this point, should just be ParameterValue), e.g.: {"InternalSubnets": {"ParameterValue": "subnet-1234,subnet-5678"}}
        request_params_by_key: dict[str, dict[str, str | bool]] = {param["ParameterKey"]: {key: value for (key, value) in param.items() if key != "ParameterKey"} for param in params}  # type: ignore[misc]

        # iterate through the stack's existing params and determine which need to be included with UsePreviousValue set to true and
        # which might need to be removed/skipped from the stack (i.e. not included in a new template, if provided)
        # any new parameters for a new template will have already been provided in the request, and any invalid/extra parameters
        # will have already been rejected by the API schema validator
        for stack_param in cfn_stack.parameters:
            if template and stack_param["ParameterKey"] not in template_param_keys:
                continue
            if stack_param["ParameterKey"] not in request_params_by_key:
                request_params_by_key[stack_param["ParameterKey"]] = {"UsePreviousValue": True}
            # if param is subnets and the value has not changed from previous (even if the order has), do not pass in changeset, or pass in correct order if there are additional
            elif stack_param['ParameterKey'] in ('InternalSubnets', 'ExternalSubnets', 'Az1Subnets', 'Az2Subnets', 'Az3Subnets'):
                if stack_param['ParameterValue']:
                    orig_subnets_list: List[str] = stack_param['ParameterValue'].split(',')
                    new_subnets_list: List[str] = str(request_params_by_key[stack_param['ParameterKey']]['ParameterValue']).split(',')
                    subnets_to_send: list[str] = []
                    for subnet in orig_subnets_list:
                        subnets_to_send.append(subnet)
                    # append newly added subnets
                    for new_subnet in new_subnets_list:
                        if new_subnet not in orig_subnets_list:
                            subnets_to_send.append(new_subnet)
                    # remove any deleted subnets
                    for orig_subnet in orig_subnets_list:
                        if orig_subnet not in new_subnets_list:
                            subnets_to_send.remove(orig_subnet)
                if subnets_to_send == orig_subnets_list:
                    request_params_by_key[stack_param["ParameterKey"]] = {"UsePreviousValue": True}
                else:
                    request_params_by_key[stack_param["ParameterKey"]] = {"ParameterValue": ",".join(subnets_to_send)}

        # convert our dictionary of params back to a list of dicts as AWS expects
        change_set_params["Parameters"] = [{"ParameterKey": k, **v} for k, v in request_params_by_key.items()]

        log_to_changelog(self.changelogfile, f"Creating change set for stack with params: {[param for param in change_set_params['Parameters']]}")
        try:
            change_set = cfn_resource.meta.client.create_change_set(**change_set_params)
        except botocore.exceptions.ClientError as e:
            log_to_changelog(self.changelogfile, f"An error occurred creating change set: {e}")
            log_to_changelog(self.changelogfile, "Change set creation complete - failed")
            raise
        log_to_changelog(self.changelogfile, f"Change set created {change_set['Id']}")
        return change_set

    def list_change_sets(self) -> list[ChangeSetSummaryTypeDef]:
        cfn = boto3.client("cloudformation", region_name=self.region)
        change_sets: list[ChangeSetSummaryTypeDef] = []
        paginator: ListChangeSetsPaginator = cfn.get_paginator("list_change_sets")
        page_iterator = paginator.paginate(StackName=self.stack_name)
        for page in page_iterator:
            change_sets.extend(page["Summaries"])
        return change_sets

    def get_change_set_details(self, change_set_name: str) -> DescribeChangeSetOutputTypeDef:
        cfn = boto3.client('cloudformation', region_name=self.region)
        change_set_details: DescribeChangeSetOutputTypeDef | None = None
        paginator: DescribeChangeSetPaginator = cfn.get_paginator('describe_change_set')
        page_iterator = paginator.paginate(ChangeSetName=change_set_name, StackName=self.stack_name, IncludePropertyValues=True)
        try:
            for page in page_iterator:
                if change_set_details and "Changes" in change_set_details:
                    change_set_details["Changes"].extend(page["Changes"])
                else:
                    change_set_details = page
        except cfn.exceptions.ChangeSetNotFoundException as e:
            raise weaver_exceptions.StackChangeSetNotFoundException from e
        if not change_set_details:
            raise weaver_exceptions.StackChangeSetNotFoundException
        return change_set_details

    def execute_change_set(self, change_set_name, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT):
        if not self.drain_type_allowed(drain_type):
            log_to_ui(self, ERROR, f'Stack does not support drain type {drain_type.value}, please select a different type.', write_to_changelog=True)
            log_to_ui(self, INFO, 'Change set execution complete - failed', write_to_changelog=True)
            return False
        log_to_ui(
            self,
            INFO,
            f'Beginning stack update via change set execution ({change_set_name}){"; fast drain mode" if drain_type == weaver_enums.DrainType.FAST else ""}',  # nosec B608
            write_to_changelog=True,
            send_sns_msg=True,
        )
        if drain_type == weaver_enums.DrainType.FAST:
            self.modify_deregistration_delays_to_fast_drain_value()

        cfn = boto3.client('cloudformation', region_name=self.region)
        try:
            client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
            cfn.execute_change_set(ChangeSetName=change_set_name, StackName=self.stack_name, ClientRequestToken=client_request_token)
        except Exception as e:
            log.exception('An error occurred applying change set to stack')
            log_to_ui(self, ERROR, f'An error occurred applying change set to stack: {e}', write_to_changelog=True)
            log_to_ui(self, INFO, 'Change set execution complete - failed', write_to_changelog=True)
            return False
        if not self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
            log_to_ui(self, INFO, 'Change set execution complete - failed', write_to_changelog=True)
            return False
        # only check for response from service if stack is server (should always have one node) or if cluster has more than 0 nodes
        cluster_node_count = self.get_param_value('ClusterNodeCount') or 0
        if not self.is_app_clustered() or int(cluster_node_count) > 0:
            log_to_ui(self, INFO, 'Waiting for stack to respond', write_to_changelog=False)
            if not self.validate_service_responding():
                log_to_ui(self, INFO, 'Change set execution complete - failed', write_to_changelog=True)
                return False
        log_to_ui(self, INFO, f'Change set execution ({change_set_name}) complete', write_to_changelog=True, send_sns_msg=True)
        return True

    # This method is intended for cases when the target group drain time is extremely long.
    def terminate_draining_nodes_gracefully(self, shutdown_app: bool = True):
        log_to_ui(self, INFO, "Detecting draining nodes to terminate", write_to_changelog=True)
        # First, calculate the difference between number of current stack nodes and it's desired capacity.
        nodes: List[Instance] = self.get_stacknodes(force=True)
        asgs: List[AutoScalingGroupTypeDef] = self.get_stack_asgs(force=True)
        stack_desired_capacity: int = sum([asg['DesiredCapacity'] for asg in asgs])
        num_to_terminate: int = len(nodes) - stack_desired_capacity
        if num_to_terminate <= 0:
            log_to_ui(self, INFO, "No nodes require terminating", write_to_changelog=True)
            return True
        # Then loop through nodes, identifying 'draining' nodes until we find all that will be terminated.
        nodes_to_terminate: Set[Instance] = set()
        while len(nodes_to_terminate) < num_to_terminate:
            for node in nodes:
                try:
                    # In the future, we could check the health state of all target groups, or generally find nodes which
                    # are 'terminating' in the ASG still not in a 'shutting-down' instance state
                    if self.check_node_is_draining_nlb_ssh_tg(node):
                        nodes_to_terminate.add(node)
                except Exception as e:
                    log_to_ui(self, ERROR, f"An error occurred checking if node is draining {e}", write_to_changelog=True)
                    return False
        log_to_ui(self, INFO, f"Proceeding to terminate {nodes_to_terminate}", write_to_changelog=True)
        if shutdown_app:
            # Shutdown application on all nodes to be terminated.
            self.shutdown_app(list(nodes_to_terminate))
        # Send a terminate signal to those EC2 instances.
        ec2 = boto3.client('ec2', region_name=self.region)
        for node in nodes_to_terminate:
            try:
                ec2.terminate_instances(InstanceIds=[node.id])
            except botocore.exceptions.ClientError as e:
                log_to_ui(self, ERROR, f"An error occurred terminating node {e}", write_to_changelog=True)
                return False
        log_to_ui(self, INFO, "All draining nodes are shutting-down", write_to_changelog=True)
        return True

    # Checks a node is in a 'draining' state in the stack's NLB SSH target group.
    def check_node_is_draining_nlb_ssh_tg(self, node: Instance) -> bool:
        nlb_ssh_tg_arn: str
        nlb_ssh_tg_name: str = f'{self.stack_name}-NLB-ssh'
        nlb_ssh_tg_name_fallback: str = f'{self.stack_name}-ssh'
        elbv2 = boto3.client('elbv2', region_name=self.region)
        try:
            nlb_ssh_tg_arn = elbv2.describe_target_groups(Names=[nlb_ssh_tg_name])['TargetGroups'][0]['TargetGroupArn']
        except botocore.exceptions.ClientError:
            try:
                nlb_ssh_tg_arn = elbv2.describe_target_groups(Names=[nlb_ssh_tg_name_fallback])['TargetGroups'][0]['TargetGroupArn']
            except botocore.exceptions.ClientError as e:
                log_to_ui(self, ERROR, f"An error occurred describing stack resource {e}", write_to_changelog=True)
                raise e
        try:
            node_health_state: TargetHealthStateEnumType = elbv2.describe_target_health(TargetGroupArn=nlb_ssh_tg_arn, Targets=[{'Id': node.id}])['TargetHealthDescriptions'][0][
                'TargetHealth'
            ]['State']
        except Exception as e:
            log_to_ui(self, ERROR, f"An error occurred checking draining status of node {e}", write_to_changelog=True)
            raise e
        return node_health_state == 'draining'

    # Wrapper method that runs whenever a stack update action is started.
    def update(self, change_set_name: str, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
        if not self.execute_change_set(change_set_name, drain_type):
            return False
        log_to_ui(self, INFO, 'Update complete', write_to_changelog=True)
        return True

    def create(self, product: ProductEnum, template, stack_params, tags, *args, **kwargs):
        log_to_ui(self, INFO, f'Creating stack: {self.stack_name}', write_to_changelog=True, send_sns_msg=True)
        # strip passwords for logging
        sanitised_params = [param for param in stack_params if 'password' not in param['ParameterKey'].lower()]
        log_to_ui(self, INFO, f'Creation params (with passwords removed): {sanitised_params}', write_to_changelog=True)
        template_file = utils.get_template_file(template)
        log_to_changelog(self.changelogfile, f'Template is {template}')
        # add weaver-enforced tags; tags will be an empty object as schema-default if request didn't include any user-supplied tags
        tags |= {
            "product": product.name,
            "environment": next((param['ParameterValue'] for param in stack_params if param['ParameterKey'] == 'DeployEnvironment'), 'not-specified'),
            "created_by": utils.get_username(),
        }
        if isinstance(template, dict):
            tags |= {
                "repository": template["repo"],
                "template": template["name"],
            }
        try:
            self.upload_file_to_s3(template_file, f'weaver-templates/{template_file.name}')
            cfn = boto3.client('cloudformation', region_name=self.region)
            # wait for the template to upload to avoid race conditions
            if 'TESTING' not in current_app.config or current_app.config['TESTING'] is False:
                time.sleep(5)
            # TODO spin up to one node first, then spin up remaining nodes
            created_stack = cfn.create_stack(
                StackName=self.stack_name,
                Parameters=stack_params,
                TemplateURL=f"https://s3.amazonaws.com/{self.get_s3_bucket_name()}/weaver-templates/{template_file.name}",
                Capabilities=['CAPABILITY_IAM'],
                Tags=utils.tag_dict_to_list(tags),
                ClientRequestToken=g.action_id,
            )
        except Exception as e:
            log.exception('Error occurred creating stack')
            log_to_ui(self, ERROR, f'Error occurred creating stack: {e}', write_to_changelog=True)
            log_to_ui(self, INFO, 'Create complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        log_to_ui(self, INFO, f'Create has begun: {created_stack}', write_to_changelog=False)
        if not self.wait_stack_action_complete('CREATE_IN_PROGRESS', g.action_id):
            log_to_ui(self, INFO, 'Create complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        log_to_ui(self, INFO, f'Stack {self.stack_name} created, waiting on service responding', write_to_changelog=False)
        if not self.validate_service_responding():
            log_to_ui(self, INFO, 'Create complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        log_to_ui(self, INFO, 'Create complete', write_to_changelog=True, send_sns_msg=True)
        return True

    # Wrapper around all the steps involved in a restart action (full / rolling / single)
    def restart_action(
        self,
        restart_type: weaver_enums.RestartType,
        node_ip: str = "",
        asg_name: str = "",
        drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT,
        thread_dumps: bool = False,
        heap_dumps: bool = False,
    ):
        if thread_dumps:
            self.thread_dump(node_ip, heap_dumps)
        if heap_dumps:
            self.heap_dump(node_ip)
        match restart_type:
            case weaver_enums.RestartType.FULL:
                result = self.full_restart()
            case weaver_enums.RestartType.ROLLING:
                result = self.rolling_restart((asg_name if asg_name else None), drain_type)
            case weaver_enums.RestartType.SINGLE:
                result = self.restart_node(node_ip, drain_type)
        return result

    def rolling_restart(self, asg_name=None, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
        step_log_msg: str = 'Rolling restart'
        log_to_ui(
            self,
            INFO,
            f'Beginning Rolling Restart for {f"auto scaling group {asg_name}" if asg_name else f"stack {self.stack_name}"}{"; fast drain mode" if drain_type == weaver_enums.DrainType.FAST else ""}',
            write_to_changelog=True,
            send_sns_msg=True,
        )
        app_type = self.get_tag('product')
        if not app_type:
            log_to_ui(self, ERROR, 'Could not determine product', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if not self.drain_type_allowed(drain_type):
            log_to_ui(self, ERROR, f'Stack does not support drain type {drain_type.value}, please select a different type.', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        nodes = [node for node in self.get_stacknodes() if node.get_auto_scaling_group()["AutoScalingGroupName"] == asg_name or asg_name is None]  # type: ignore[attr-defined]
        log_to_ui(self, INFO, f'{self.stack_name} nodes are {[{node.id: node.private_ip_address} for node in nodes]}', write_to_changelog=False)
        # determine if app is clustered or has a single node (rolling restart may cause an unexpected outage)
        if not self.is_app_clustered():
            log_to_ui(self, ERROR, 'App is not clustered - rolling restart not supported (use full restart)', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if len(nodes) == 0:
            log_to_ui(self, ERROR, 'Node count is 0: nothing to restart', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        elif len(nodes) == 1:
            log_to_ui(self, ERROR, 'Stack or auto scaling group only has one node - rolling restart not supported (use full restart)', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        # determine if the nodes are healthy or not
        healthy_nodes, unhealthy_nodes = self.group_nodes_by_health(nodes)
        # restart unhealthy nodes first
        for node in itertools.chain(unhealthy_nodes, healthy_nodes):
            if ('mesh' not in app_type and 'mirror' not in app_type) and len(node.get_auto_scaling_group()['Instances']) == 1:  # type: ignore[attr-defined]
                log_to_ui(
                    self,
                    ERROR,
                    f'Auto scaling group {node.get_auto_scaling_group()["AutoScalingGroupName"]} only has one node - rolling restart not supported (use full or single-node restart)',  # type: ignore[attr-defined]
                    write_to_changelog=True,
                )
                continue
            if drain_type != weaver_enums.DrainType.SKIP:
                if not self.deregister_node(node, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            if not self.shutdown_app([node]):
                log_to_ui(self, ERROR, f'Failed to stop application on node {node.id} ({node.private_ip_address})', write_to_changelog=True)
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.startup_app([node]):
                log_to_ui(self, ERROR, f'Failed to start application on node {node.id} ({node.private_ip_address})', write_to_changelog=True)
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.validate_node_responding(node):
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if drain_type != weaver_enums.DrainType.SKIP:
                if not self.register_node(node):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            else:
                if not self.wait_nodes_registration_states([node], 'healthy'):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
        log_to_ui(self, INFO, f'{step_log_msg} - complete', write_to_changelog=True, send_sns_msg=True)
        return True

    def ec2_rolling_reboot(self, asg_name=None, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
        step_log_msg: str = 'EC2 rolling reboot'
        log_to_ui(
            self,
            INFO,
            f'Beginning EC2 Node Reboot for {f"auto scaling group {asg_name}" if asg_name else f"stack {self.stack_name}"}{"; fast drain mode" if drain_type == weaver_enums.DrainType.FAST else ""}',
            write_to_changelog=True,
            send_sns_msg=True,
        )
        app_type = self.get_tag('product')
        if not app_type:
            log_to_ui(self, ERROR, 'Could not determine product', write_to_changelog=True)
            log_to_ui(self, ERROR, 'EC2 rolling reboot complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if not self.drain_type_allowed(drain_type):
            log_to_ui(self, ERROR, f'Stack does not support drain type {drain_type.value}, please select a different type.', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        nodes = [node for node in self.get_stacknodes() if node.get_auto_scaling_group()["AutoScalingGroupName"] == asg_name or asg_name is None]  # type: ignore[attr-defined]
        log_to_ui(self, INFO, f'{self.stack_name} nodes are {[{node.id: node.private_ip_address} for node in nodes]}', write_to_changelog=False)
        # determine if the nodes are healthy or not
        healthy_nodes, unhealthy_nodes = self.group_nodes_by_health(nodes)
        # To verify nodes are started up on the expected version
        expected_version: str = self.get_param_value("ProductVersion")
        # restart unhealthy nodes first
        for node in itertools.chain(unhealthy_nodes, healthy_nodes):
            if drain_type != weaver_enums.DrainType.SKIP:
                if not self.deregister_node(node, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            if not self.shutdown_app([node]):
                log_to_ui(self, INFO, f'Failed to stop application on node {node.id} ({node.private_ip_address})', write_to_changelog=True)
                log_to_ui(self, ERROR, 'EC2 rolling reboot complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.reboot_ec2_node(node):
                log_to_ui(self, INFO, f'Failed to restart EC2 node {node.id} ({node.private_ip_address})', write_to_changelog=True)
                log_to_ui(self, ERROR, 'EC2 rolling reboot complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.validate_node_responding(node):
                log_to_ui(
                    self,
                    INFO,
                    f"Node {node.id} ({node.private_ip_address}) is not responding after {current_app.config['ACTION_TIMEOUTS']['validate_node_responding']}s",
                    write_to_changelog=True,
                )
                log_to_ui(self, INFO, 'EC2 rolling reboot complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.validate_nodes_product_version([node], expected_version):
                log_to_ui(self, ERROR, 'EC2 rolling reboot complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if drain_type != weaver_enums.DrainType.SKIP:
                if not self.register_node(node):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            else:
                if not self.wait_nodes_registration_states([node], 'healthy'):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
        log_to_ui(self, INFO, 'EC2 rolling reboot complete', write_to_changelog=True, send_sns_msg=True)
        return True

    def full_restart(self):
        log_to_ui(self, INFO, f'Beginning Full Restart for {self.stack_name}', write_to_changelog=True, send_sns_msg=True)
        app_type = self.get_tag('product')
        if not app_type:
            log_to_ui(self, ERROR, 'Full restart complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        nodes = self.get_stacknodes()
        if len(nodes) == 0:
            log_to_ui(self, ERROR, 'Node count is 0: nothing to restart', write_to_changelog=True)
            log_to_ui(self, ERROR, 'Full restart complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        log_to_ui(self, INFO, f'{self.stack_name} nodes are {[{node.id: node.private_ip_address} for node in nodes]}', write_to_changelog=False)
        if not self.shutdown_app(nodes):
            log_to_ui(self, ERROR, 'Full restart complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if app_type == 'confluence':
            # we might have a mix of confluence and synchrony nodes; ensure the synchrony nodes are started first
            nodes = sorted(nodes, key=lambda node: node.get_app_type() != 'synchrony')  # type: ignore[attr-defined]
        for node in nodes:
            self.startup_app([node])
        log_to_ui(self, INFO, 'Full restart complete', write_to_changelog=True, send_sns_msg=True)
        return True

    def restart_node(self, node_ip: str, drain_type: weaver_enums.DrainType) -> bool:
        step_log_msg: str = 'Node restart'
        log_to_ui(
            self, INFO, f"Beginning restart on node {node_ip}{'; fast drain mode' if drain_type == weaver_enums.DrainType.FAST else ''}", write_to_changelog=True, send_sns_msg=True
        )
        if not self.drain_type_allowed(drain_type):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        app_type = self.get_tag('product')
        if not app_type:
            log_to_ui(self, ERROR, 'Node restart complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        nodes = self.get_stacknodes()
        try:
            node_to_restart: Instance = next(node for node in nodes if node.private_ip_address == node_ip)
        except StopIteration:
            log_to_ui(self, ERROR, 'Node restart complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if drain_type != weaver_enums.DrainType.SKIP and not self.deregister_node(node_to_restart, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if drain_type == weaver_enums.DrainType.FAST:
            self.restore_original_deregistration_delays_if_needed()
        if not self.shutdown_app([node_to_restart]):
            log_to_ui(self, ERROR, 'Node restart complete - shutdown failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if not self.startup_app([node_to_restart]):
            log_to_ui(self, ERROR, 'Node restart complete - startup failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if drain_type != weaver_enums.DrainType.SKIP and not self.register_node(node_to_restart):
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        log_to_ui(self, INFO, 'Node restart complete', write_to_changelog=True, send_sns_msg=True)
        return True

    def toggle_node_registration(self, node_ip: str, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
        log_to_ui(
            self,
            INFO,
            f"Beginning node registration toggle on node {node_ip}{'; fast drain mode' if drain_type == weaver_enums.DrainType.FAST else ''}",
            write_to_changelog=True,
            send_sns_msg=True,
        )
        step_log_msg: str = 'Node toggle complete'
        nodes = self.get_stacknodes()
        try:
            node_to_toggle: Instance = next(node for node in nodes if node.private_ip_address == node_ip)
        except StopIteration:
            log_to_ui(self, ERROR, 'Node registration toggle complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        if node_to_toggle.is_backup_node():  # type: ignore[attr-defined]
            log_to_ui(self, ERROR, 'Backup nodes are never registered to any load balancers, cannot toggle registration', write_to_changelog=True, send_sns_msg=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
            return False
        try:
            node_to_toggle.get_auto_scaling_group()['TargetGroupARNs'][0]  # type: ignore[attr-defined]
        except IndexError:
            log_to_ui(self, WARN, f'Stack {self.stack_name} does not use ELBV2, skipping wait for node registration', write_to_changelog=True)
            return True
        node_registration_state = {}
        node_registration_state['ALB'] = self.get_node_registration_state_alb(node_to_toggle)
        node_registration_state['NLB'] = self.get_node_registration_state_nlb(node_to_toggle)
        if not node_registration_state['NLB']:
            node_registration_state.pop('NLB')
        log_to_ui(self, INFO, f'Node {node_to_toggle.private_ip_address} registration state is {node_registration_state}', write_to_changelog=True)
        if all([registration_state not in ('notregistered', 'draining') for registration_state in node_registration_state.values()]):
            log_to_ui(self, INFO, 'Node is fully registered, so will be removed from the load balancer(s)', write_to_changelog=True)
            if not self.drain_type_allowed(drain_type):
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.deregister_node(node_to_toggle, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        else:
            log_to_ui(self, INFO, 'Node is not fully registered, so will be added to the load balancer(s)', write_to_changelog=True)
            if not self.register_node(node_to_toggle):
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
        log_to_ui(self, INFO, 'Node toggle complete', write_to_changelog=True, send_sns_msg=True)
        return True

    def rolling_reinit(self, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
        step_log_msg: str = 'Rolling re-init'
        log_to_ui(
            self,
            INFO,
            f"Rolling re-init for stack {self.stack_name} has begun{'; fast drain mode' if drain_type == weaver_enums.DrainType.FAST else ''}",
            write_to_changelog=True,
            send_sns_msg=True,
        )
        if not self.drain_type_allowed(drain_type):
            log_to_ui(self, ERROR, f'Stack does not support drain type {drain_type.value}, please select a different type.', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        nodes = self.get_stacknodes()
        product_version = self.get_param_value("ProductVersion")
        log_to_ui(self, INFO, f'{self.stack_name} nodes are {[{node.id: node.private_ip_address} for node in nodes]}', write_to_changelog=False)
        for node in nodes:
            if drain_type != weaver_enums.DrainType.SKIP:
                if not self.deregister_node(node, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            if not self.shutdown_app([node]):
                log_to_ui(self, INFO, f'Failed to stop application on node {node.id} ({node.private_ip_address})', write_to_changelog=True)
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.execute_cfn_init(node):
                log_to_ui(self, INFO, f'Failed to reinitialize node {node.id} ({node.private_ip_address})', write_to_changelog=True)
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.validate_node_responding(node):
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if not self.validate_nodes_product_version([node], product_version):
                log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            if drain_type != weaver_enums.DrainType.SKIP:
                if not self.register_node(node):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            else:
                if not self.wait_nodes_registration_states([node], 'healthy'):
                    log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
        log_to_ui(self, INFO, f'{step_log_msg} complete', write_to_changelog=True, send_sns_msg=True)
        return True

    # Deregister all stack nodes from their Autoscaling Groups' target groups, wait for nodes to report 'notregistered', then shut down application on all nodes.
    def deregister_all_stack_nodes(self, fast_drain: bool = False) -> bool:
        step_log_msg: str = 'Deregistering and shutting down app on all stack nodes'
        log_to_ui(self, INFO, f'{step_log_msg}', write_to_changelog=True)
        nodes: List[Instance] = self.get_stacknodes()
        if not self.begin_deregistering_nodes_from_all_target_groups(nodes, fast_drain):
            log_to_ui(self, ERROR, 'Failed to drain node all stack nodes', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if not self.wait_nodes_registration_states(nodes, 'notregistered'):
            log_to_ui(self, ERROR, 'Failed to wait for all nodes to be deregistered', write_to_changelog=True)
            log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True)
            return False
        if fast_drain:
            self.restore_original_deregistration_delays_if_needed()
        log_to_ui(self, INFO, f'{step_log_msg} - complete', write_to_changelog=True)
        return True

    def rolling_rebuild(self, asg_name=None, personal_access_token=None, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT):  # noqa: C901 (too complex)
        step_log_msg: str = 'Rolling rebuild complete'
        log_to_ui(
            self,
            INFO,
            f'Rolling rebuild has begun for {f"auto scaling group {asg_name}" if asg_name else "all nodes"}{"; fast drain mode" if drain_type == weaver_enums.DrainType.FAST else ""}',
            write_to_changelog=True,
            send_sns_msg=True,
        )
        if not self.drain_type_allowed(drain_type):
            log_to_ui(self, ERROR, f'Stack does not support drain type {drain_type.value}, please select a different type.', write_to_changelog=True)
            log_to_ui(self, INFO, 'Upgrade complete - failed', write_to_changelog=True)
            return False
        old_nodes = [node for node in self.get_stacknodes() if node.get_auto_scaling_group()["AutoScalingGroupName"] == asg_name or asg_name is None]  # type: ignore[attr-defined]
        new_nodes = []
        app_type = self.get_tag('product')
        if app_type == 'confluence':
            # we might have a mix of confluence and synchrony nodes; ensure the confluence nodes are rebuilt first
            old_nodes = sorted(old_nodes, key=lambda node: node.get_app_type() == 'synchrony')  # type: ignore[attr-defined]
        log_to_ui(self, INFO, f'Old nodes: {[{node.id: node.private_ip_address} for node in old_nodes]}', write_to_changelog=True)
        try:
            # suspend AddToLoadbalancer functionality for rebuilds that check index health
            # this stops nodes being added back to the load balancer by the asg before they have a healthy index
            if app_type == 'jira' and personal_access_token is not None:
                if not self.suspend_AddToLoadBalancer(asg_name):
                    log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            for node in old_nodes:
                if drain_type != weaver_enums.DrainType.SKIP:
                    if not self.deregister_node(node, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
                        log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                        return False
                # destroy the node and wait for another to be created to replace it
                log_to_ui(self, INFO, f'Replacing {node.get_app_type()} node {node.id} ({node.private_ip_address})', write_to_changelog=True)  # type: ignore[attr-defined]
                node.terminate()
                nodes = [node for node in self.get_stacknodes(force=True) if node.get_auto_scaling_group()["AutoScalingGroupName"] == asg_name or asg_name is None]  # type: ignore[attr-defined]
                waiting_for_new_node_creation = True
                replacement_node: Instance | None = None
                action_start = time.time()
                action_timeout = current_app.config['ACTION_TIMEOUTS']['node_initialisation']
                while waiting_for_new_node_creation:
                    for node in nodes:
                        # check the node id against the old nodes
                        if node.id not in set(map(lambda instance: instance.id, old_nodes)):
                            # if the node is new, track it
                            if node not in new_nodes:
                                # check for IP, break out if not assigned yet
                                if node.private_ip_address is None:
                                    break
                                # otherwise, store the node and proceed
                                replacement_node = node
                                log_to_ui(self, INFO, f'New node: {replacement_node.id} ({replacement_node.private_ip_address})', write_to_changelog=True)
                                new_nodes.append(replacement_node)
                                waiting_for_new_node_creation = False
                    if (time.time() - action_start) > action_timeout:
                        log_to_ui(self, ERROR, f'New node failed to be created after {format_timespan(action_timeout)} - aborting', write_to_changelog=True)
                        log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
                        return False
                    if waiting_for_new_node_creation:
                        time.sleep(30)
                        nodes = [
                            node for node in self.get_stacknodes(force=True) if node.get_auto_scaling_group(force=True)["AutoScalingGroupName"] == asg_name or asg_name is None  # type: ignore[attr-defined]
                        ]
                if not replacement_node:
                    log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
                # wait for the new node to come up
                if app_type == 'jira' and personal_access_token is not None:
                    # When a new node comes up, it'll automatically be registered (AWS behaviour) - so explicitly deregister it again
                    log_to_ui(self, INFO, f'Deregistering node {replacement_node.id} ({replacement_node.private_ip_address}) to wait for index recovery', write_to_changelog=False)
                    if not self.deregister_node(replacement_node, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
                        log_to_ui(
                            self,
                            ERROR,
                            f'Node {replacement_node.id} ({replacement_node.private_ip_address}) failed to \
                            deregister while waiting for index to restore; you will need to manually \
                            deregister the node and watch for index health',
                            write_to_changelog=True,
                            send_sns_msg=True,
                        )
                        log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                        return False
                if not self.validate_node_responding(replacement_node):
                    log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
                product_version = self.get_param_value("ProductVersion")
                if not self.validate_nodes_product_version([replacement_node], product_version):
                    log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
                if app_type == 'jira' and personal_access_token is not None:
                    if not utils.wait_index_health(self, replacement_node.private_ip_address, personal_access_token):
                        log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
                        return False
                    if not self.register_node(replacement_node):
                        log_to_ui(self, ERROR, f'{step_log_msg} - failed', write_to_changelog=True, send_sns_msg=True)
                        return False
                elif not self.wait_nodes_registration_states([replacement_node], 'healthy'):
                    log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
                    return False
            # resume AddToLoadBalancer functionality, do not fail action if this fails - warning in log is sufficient
            if app_type == 'jira' and personal_access_token is not None:
                self.resume_AddToLoadBalancer(asg_name)
            log_to_ui(self, INFO, 'Rolling rebuild complete', write_to_changelog=True, send_sns_msg=True)
            log_to_changelog(self.changelogfile, f'New nodes are: {[{node.id: node.private_ip_address} for node in new_nodes]}')
            return True
        except Exception as e:
            log.exception('An error occurred during rolling rebuild')
            log_to_ui(self, ERROR, f'An error occurred during rolling rebuild: {e}', write_to_changelog=True)
            log_to_ui(self, ERROR, 'Rolling rebuild complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False

    def thread_dump(self, node_ip=False, alsoHeaps=False):
        heaps_to_come_log_line = ''
        if alsoHeaps == 'true':
            heaps_to_come_log_line = ', heap dumps to follow'
        log_to_ui(self, INFO, f'Beginning thread dumps on {self.stack_name}{heaps_to_come_log_line}', write_to_changelog=False)
        nodes = self.get_stacknodes()
        if node_ip:
            nodes = list(filter(lambda instance: instance.private_ip_address == node_ip, nodes))
        log_to_ui(self, INFO, f'Thread dumps requested on {[{node.id: node.private_ip_address} for node in nodes]}', write_to_changelog=False)
        node_results = self.run_command(nodes, '/usr/local/bin/j2ee_thread_dump', return_results=True)
        if any([node_result['result'] for node_result in node_results if node_result['result'] != 'Success']):
            log_to_ui(self, WARN, 'Thread dumps failed, copying scripts to /usr/local/bin and retrying', write_to_changelog=True)
            if self.download_scripts_onto_ec2_nodes():
                log_to_ui(self, INFO, 'Retrying thread dumps', write_to_changelog=True)
                node_results = self.run_command(nodes, '/usr/local/bin/j2ee_thread_dump', return_results=True)
        for node_result in node_results:
            log_level = INFO if node_result['result'] == 'Success' else WARN
            log_to_ui(self, log_level, f"Result for {node_result['node_ip']} was {node_result['result']}", write_to_changelog=True)
        log_to_ui(self, INFO, 'Successful thread dumps can be downloaded from the main Diagnostics page', write_to_changelog=False)
        log_to_ui(self, INFO, 'Thread dumps complete', write_to_changelog=True)
        return True

    def heap_dump(self, node_ip=False):
        log_to_ui(self, INFO, f'Beginning heap dumps on {self.stack_name}', write_to_changelog=False)
        nodes = self.get_stacknodes()
        node_results = []
        if node_ip:
            nodes = list(filter(lambda instance: instance.private_ip_address == node_ip, nodes))
        log_to_ui(self, INFO, f'Heap dumps requested on {[{node.id: node.private_ip_address} for node in nodes]}', write_to_changelog=False)
        # Wait for each heap dump to finish before starting the next, to avoid downtime
        for node in nodes:
            result = self.ssm_send_and_wait_response(node.id, '/usr/local/bin/j2ee_heap_dump_live')
            if result != 'Success':
                log_to_ui(self, WARN, 'Heap dumps failed, copying scripts to /usr/local/bin and retrying', write_to_changelog=True)
                if self.download_scripts_onto_ec2_nodes():
                    log_to_ui(self, INFO, 'Retrying heap dumps', write_to_changelog=True)
                    result = self.ssm_send_and_wait_response(node.id, '/usr/local/bin/j2ee_heap_dump_live')
            node_results.append({'node_ip': node.private_ip_address, 'result': result})
            if 'TESTING' not in current_app.config or current_app.config['TESTING'] is False and len(nodes) > 1:
                time.sleep(30)  # give node time to recover and rejoin cluster
        for node_result in node_results:
            log_level = INFO if node_result['result'] == 'Success' else WARN
            log_to_ui(self, log_level, f"Result for {node_result['node_ip']} was {node_result['result']}", write_to_changelog=True)
        log_to_ui(self, INFO, 'Successful heap dumps can be downloaded from the main Diagnostics page', write_to_changelog=False)
        log_to_ui(self, INFO, 'Heap dumps complete', write_to_changelog=True)
        return True

    def get_s3_file_links(self, file_type: weaver_enums.DiagnosticFileType = weaver_enums.DiagnosticFileType.THREAD_DUMP, node_ip: str = "") -> list[str]:
        urls_split = []
        urls: list[str] = []
        s3_bucket = self.get_s3_bucket_name()
        try:
            s3 = boto3.client('s3', region_name=self.region)
            paginator = s3.get_paginator('list_objects_v2')
            page_iterator = paginator.paginate(Bucket=s3_bucket, Prefix=f'diagnostics/{self.stack_name}/')
            for page in page_iterator:
                if 'Contents' in page:
                    for item in page['Contents']:
                        try:
                            url: str = s3.generate_presigned_url(ClientMethod='get_object', Params={'Bucket': s3_bucket, 'Key': item['Key']})
                            if 'thread_dumps' in url and file_type == weaver_enums.DiagnosticFileType.THREAD_DUMP:
                                search_timestamp = match.group(0) if (match := re.search(r'(\d{8}_\d{6})', url)) is not None else ''
                                obj_timestamp = datetime.strptime(search_timestamp, '%Y%m%d_%H%M%S')
                                ipaddr = match.group(0) if (match := re.search(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', url)) is not None else ''
                                if node_ip and node_ip != ipaddr:
                                    continue
                                obj_ip = ip_address(ipaddr)
                                urls_split.append({'date': obj_timestamp, 'ip': obj_ip, 'orig_url': url})
                            elif 'support' in url and file_type == weaver_enums.DiagnosticFileType.SUPPORT_ZIP:
                                search_timestamp = match.group(0) if (match := re.search(r'(\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2})', url)) is not None else ''
                                obj_timestamp = datetime.strptime(search_timestamp, '%Y-%m-%d-%H-%M-%S')
                                search_ipaddr = match.group(0) if (match := re.search(r'(\d{1,3}-\d{1,3}-\d{1,3}-\d{1,3})', url)) is not None else ''
                                ipaddr = search_ipaddr.replace('-', '.')
                                if node_ip and node_ip != ipaddr:
                                    continue
                                obj_ip = ip_address(ipaddr)
                                urls_split.append({'date': obj_timestamp, 'ip': obj_ip, 'orig_url': url})
                            elif 'heap_dump' in url and file_type == weaver_enums.DiagnosticFileType.HEAP_DUMP:
                                search_timestamp = match.group(0) if (match := re.search(r'(\d{8}_\d{6})', url)) is not None else ''
                                obj_timestamp = datetime.strptime(search_timestamp, '%Y%m%d_%H%M%S')
                                ipaddr = match.group(0) if (match := re.search(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', url)) is not None else ''
                                if node_ip and node_ip != ipaddr:
                                    continue
                                obj_ip = ip_address(ipaddr)
                                urls_split.append({'date': obj_timestamp, 'ip': obj_ip, 'orig_url': url})
                        except Exception:
                            log.exception('Error occurred parsing file links from S3 bucket')
            sorted_results = sorted(urls_split, key=lambda k: (k['date'], k['ip']), reverse=True)
            urls = [str(result['orig_url']) for result in sorted_results]
        except Exception as e:
            if hasattr(e, 'response') and e.response['Error']['Code'] == 'NoSuchBucket':
                print(f"S3 bucket '{s3_bucket}' has not yet been created")
            log.exception('Error occurred getting S3 file links')
        return urls

    def run_sql(self):
        sql_to_run = self.get_sql()
        if sql_to_run != 'No SQL script exists for this stack':
            log_to_ui(self, INFO, 'Running post clone SQL', write_to_changelog=True)
            nodes = self.get_stacknodes()
            db_conx_string = 'PGPASSWORD=${ATL_DB_PASSWORD} /usr/bin/psql -v ON_ERROR_STOP=1 -h ${ATL_DB_HOST} -p ${ATL_DB_PORT} -U postgres -w ${ATL_DB_NAME}'
            cloned_from_stack = self.get_tag('cloned_from')
            if cloned_from_stack:
                # on node, grab cloned_from sql from s3
                self.run_command(
                    [nodes[0]],
                    f"aws s3 sync s3://{self.get_s3_bucket_name()}/config/stacks/{cloned_from_stack}/{cloned_from_stack}-clones-sql.d /tmp/{cloned_from_stack}-clones-sql.d",
                )
                # run that sql
                if not self.run_command(
                    [nodes[0]],
                    f'source /etc/atl; for file in `ls /tmp/{cloned_from_stack}-clones-sql.d/*.sql`;do {db_conx_string} -a -f $file >> /var/log/cloned_from_sql.out 2>&1; done',
                ):
                    log_to_ui(self, ERROR, 'Running SQL script failed', write_to_changelog=True)
                    return False
            # on node, grab local-post-clone sql from s3
            self.run_command([nodes[0]], f"aws s3 sync s3://{self.get_s3_bucket_name()}/config/stacks/{self.stack_name}/local-post-clone-sql.d /tmp/local-post-clone-sql.d")
            # run that sql
            if not self.run_command(
                [nodes[0]], f'source /etc/atl; for file in `ls /tmp/local-post-clone-sql.d/*.sql`;do {db_conx_string} -a -f $file >> /var/log/local_post_clone_sql.out 2>&1; done'
            ):
                log_to_ui(self, ERROR, 'Running SQL script failed', write_to_changelog=True)
        else:
            log_to_ui(self, INFO, 'No post clone SQL files found', write_to_changelog=True)
            return None
        log_to_ui(self, INFO, 'Run SQL complete', write_to_changelog=True)
        return True
