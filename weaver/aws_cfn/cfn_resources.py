import logging

import boto3
import botocore
from flask import session, current_app
from retry import retry

log = logging.getLogger('app_log')


@retry(botocore.exceptions.ClientError, tries=5, delay=2, backoff=2)
def get_cfn_stacknames(region):
    if region is None:
        region = session['region']
    stack_name_list = []
    try:
        cfn = boto3.client('cloudformation', region)
        stack_list = cfn.list_stacks(StackStatusFilter=current_app.config['VALID_STACK_STATUSES'])
        session['credentials'] = True
        for stack in stack_list['StackSummaries']:
            stack_name_list.append(stack['StackName'])
    except botocore.exceptions.NoCredentialsError:
        session['credentials'] = False
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == 'RequestLimitExceeded':
            log.exception('RequestLimitExceeded received during get_cfn_stacknames')
        else:
            log.exception(e.args[0])
        raise e
    except botocore.exceptions.EndpointConnectionError as e:
        log.exception(e.args[0])
        raise e
    except Exception as e:
        log.exception(e.args[0])
        raise e
    return sorted(stack_name_list)
