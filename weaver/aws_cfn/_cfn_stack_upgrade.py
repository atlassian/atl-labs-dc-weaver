# Contains methods related to upgrading AWS CFN stacks
# NOTE: This is a test for a refactoring idea (see https://stackoverflow.com/questions/47561840/how-can-i-separate-the-functions-of-a-class-into-multiple-files/47562412#47562412),
# most upgrade methods still exist in cfn_stack.py.
import boto3
import logging
from datetime import datetime
from logging import ERROR, INFO, WARN
from typing import List, TYPE_CHECKING

import weaver.enums as weaver_enums
from weaver.weaver_logging.weaver_logging import log_to_changelog, log_to_ui

READY_TO_SERVE_STATUSES = ('FIRST_RUN', 'OK', 'RUNNING', 'SERVING', 'SYNCHRONIZED', 'UNIMPLEMENTED')

log = logging.getLogger('app_log')

# To enable VSCode autocomplete without creating a circular import issue: https://stackoverflow.com/a/69911395
if TYPE_CHECKING:
    from cfn_stack import CfnStack
    from mypy_boto3_autoscaling.type_defs import InstanceTypeDef


##
# Micro helper methods
#


def select_not_ready_node(self: 'CfnStack', nodes, asg_arn: str | None = None):
    for node in nodes:
        if self.check_node_status(node) not in READY_TO_SERVE_STATUSES:
            if not asg_arn or node.get_auto_scaling_group()['AutoScalingGroupARN'] == asg_arn:
                return node
    return None


# Filters out nodes from a list which are already in a "ready-to-serve" state.
def filter_out_ready_to_serve_nodes(self: 'CfnStack', nodes):
    filtered_nodes: List[InstanceTypeDef] = []
    for i in range(len(nodes)):
        node: InstanceTypeDef = nodes[i]
        if self.check_node_status(node) not in READY_TO_SERVE_STATUSES:
            filtered_nodes.append(node)
    return filtered_nodes


##
#  Macro helpful abstraction methods
#


# Updates "ProductVersion" and "ProductDownloadURL" CFN parameters for the stack, in preparation for an upgrade
def update_version_and_product_download_url(self: 'CfnStack', new_version: str, product_download_url: str) -> bool:
    # update the version in stack parameters
    stack_params = self.get_params()
    if any(param for param in stack_params if param['ParameterKey'] == 'ProductVersion'):
        stack_params = self.update_paramlist(stack_params, 'ProductVersion', new_version)
    else:
        stack_params = self.update_paramlist(stack_params, f'{self.app_type.title()}Version', new_version)
    # update the alternate download URL in the stack parameters
    stack_params = self.update_paramlist(stack_params, 'ProductDownloadUrl', product_download_url)
    cfn = boto3.client('cloudformation', region_name=self.region)
    try:
        client_request_token = f'{self.stack_name}-{datetime.now().strftime("%Y%m%d-%H%M%S")}'
        cfn.update_stack(StackName=self.stack_name, Parameters=stack_params, UsePreviousTemplate=True, Capabilities=['CAPABILITY_IAM'], ClientRequestToken=client_request_token)
    except Exception as e:
        if 'No updates are to be performed' in e.args[0]:
            log_to_ui(self, INFO, f'Stack is already at {new_version}', write_to_changelog=True)
            return True
        else:
            log.exception('An error occurred updating the version')
            log_to_ui(self, ERROR, f'An error occurred updating the version: {e}', write_to_changelog=True)
            return False
    # wait for update to complete
    if not self.wait_stack_action_complete('UPDATE_IN_PROGRESS', client_request_token):
        log_to_ui(self, ERROR, 'Could not update product version and/or product download url in stack parameters', write_to_changelog=True)
        return False
    log_to_ui(self, INFO, 'Successfully updated product version and product download url in stack parameters', write_to_changelog=False)
    return True


# Re-init all nodes at once and then validate the service is responding on all
def upgrade_in_place_reinit_all_nodes(self: 'CfnStack', new_version: str, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
    stack_nodes: List[InstanceTypeDef] = self.get_stacknodes()
    if not self.execute_cfn_init_parallel(stack_nodes):
        return False
    if not self.validate_nodes_responding(stack_nodes):
        return False
    if not self.validate_nodes_product_version(stack_nodes, new_version):
        return False
    if drain_type != weaver_enums.DrainType.SKIP and (self.has_alb() or self.has_nlb()):
        if not self.begin_registering_node_to_all_target_groups(stack_nodes):
            return False
        if not self.wait_nodes_registration_states(stack_nodes, 'healthy'):
            return False
    return True


# # Re-init one node and wait to report healthy in its load balancers, then do the same with the rest of the nodes.
def upgrade_in_place_reinit_one_then_remaining_nodes(self: 'CfnStack', new_version: str, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
    stack_nodes: List[InstanceTypeDef] = self.get_stacknodes()
    if not self.reinit_one_node_and_wait_healthy(stack_nodes, new_version, skip_node_drain=(drain_type == weaver_enums.DrainType.SKIP)):
        return False
    if not self.reinit_remaining_nodes_and_wait_healthy(stack_nodes, new_version, skip_node_drain=(drain_type == weaver_enums.DrainType.SKIP)):
        return False
    return True


##
#  Main upgrade methods
#


# Performs an in-place upgrade of a CFN stack (nodes are re-initalised rather than rebuilt)
def upgrade_in_place(
    self: 'CfnStack', new_version: str, alt_download_url: str, startup_primary: bool = True, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT
) -> bool:
    log_to_ui(self, INFO, f'Beginning in-place upgrade for {self.stack_name}', write_to_changelog=False, send_sns_msg=True)
    # get product
    self.app_type = self.get_tag('product')
    if not self.app_type:
        log_to_ui(self, ERROR, 'Stack is not tagged with "product"', write_to_changelog=True)
        log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
        return False
    self.get_pre_upgrade_information(self.app_type)
    # check if there are nodes in the stack
    if not self.preupgrade_app_node_count and not self.preupgrade_bot_node_count:
        log_to_ui(self, INFO, 'No nodes in stack to upgrade', write_to_changelog=True)
        log_to_ui(self, INFO, 'Upgrade complete', write_to_changelog=True, send_sns_msg=True)
        return True
    # logging start of upgrade info
    log_to_changelog(self.changelogfile, f'New version: {new_version}')
    if alt_download_url:
        log_to_ui(self, INFO, f'Alternate download URL: {alt_download_url}', write_to_changelog=True)
    log_to_changelog(self.changelogfile, 'Upgrade is underway')
    if not self.upgrade_in_place_stack_preparation_steps(new_version, alt_download_url, drain_type):
        log_to_ui(self, ERROR, 'Upgrade in place stack preparation steps failed', write_to_changelog=True)
        log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
        return False
    if not self.upgrade_in_place_reinit_nodes(new_version, drain_type):
        log_to_ui(self, ERROR, 'Upgrade in place reinit nodes failed', write_to_changelog=True)
        log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
        return False
    if not self.upgrade_in_place_cleanup(startup_primary):
        log_to_ui(self, ERROR, 'Upgrade in place cleanup failed', write_to_changelog=True)
        log_to_ui(self, ERROR, 'Upgrade complete - failed', write_to_changelog=True)
        return False
    log_to_ui(self, INFO, f'Upgrade successful for {self.stack_name} at {self.region} to version {new_version}', write_to_changelog=False)
    log_to_ui(self, INFO, 'Upgrade complete', write_to_changelog=True, send_sns_msg=True)
    return True


# Everything we need to do to the service get ready to re-init nodes onto the new upgraded version.
def upgrade_in_place_stack_preparation_steps(
    self: 'CfnStack', new_version: str, alt_download_url: str, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT
) -> bool:
    app_type: str = self.get_app_type()
    # NOTE: cfn-hup MUST be stopped before any CFN update or else a hook will auto fire a cfn-init we don't want
    if not self.toggle_cfn_hup('stop'):
        log_to_ui(self, ERROR, 'Failed to stop cfn-hup on all nodes', write_to_changelog=True)
        return False
    # check if an NFS server needs replacing
    if self.check_nfs_server_replacement() and not self.replace_nfs_server(skip_app_start=True):
        log_to_ui(self, ERROR, 'NFS server replacement failed', write_to_changelog=True)
        return False
    if 'mesh' in app_type:
        # retrieve primary stack nodes
        bb_primary_stack_nodes = self.get_primary_stack_nodes()
        if not bb_primary_stack_nodes:
            return False
        log_to_ui(self, INFO, f'Bitbucket primary stack nodes: {bb_primary_stack_nodes}', write_to_changelog=True)
        # shutdown bitbucket service
        self.shutdown_app(bb_primary_stack_nodes)
    stack_nodes: List[InstanceTypeDef] = self.get_stacknodes()
    if not self.update_version_and_product_download_url(new_version, alt_download_url):
        log_to_ui(self, ERROR, 'Failed to update version and/or product download url', write_to_changelog=True)
        return False
    # Bitbucket Mesh currently has no draining supported by Weaver
    # TODO: implement this (seems to be an app thing, there's no load balancer for Mesh)
    if 'mesh' not in app_type and drain_type != weaver_enums.DrainType.SKIP:
        if not self.begin_deregistering_nodes_from_all_target_groups(stack_nodes, fast_drain=(drain_type == weaver_enums.DrainType.FAST)):
            log_to_ui(self, ERROR, 'Failed to drain node all stack nodes', write_to_changelog=True)
            return False
        if not self.wait_nodes_registration_states(stack_nodes, 'notregistered'):
            log_to_ui(self, ERROR, 'Failed to wait for all nodes to be deregistered', write_to_changelog=True)
            return False
        if drain_type == weaver_enums.DrainType.FAST:
            self.restore_original_deregistration_delays_if_needed()
    if not self.shutdown_app(stack_nodes):
        log_to_ui(self, ERROR, 'Failed to shutdown all stack nodes', write_to_changelog=True)
        return False
    return True


# Bring up a service onto a new upgraded version
def upgrade_in_place_reinit_nodes(self: 'CfnStack', new_version: str, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT) -> bool:
    app_type: str = self.get_app_type()
    if not app_type:
        log_to_ui(self, ERROR, "Please ensure stack has a 'product' tag", write_to_changelog=True)
        return False
    if 'bitbucket' in app_type and 'mirror' not in app_type:
        if not self.upgrade_in_place_reinit_all_nodes(new_version, drain_type):
            return False
    else:
        if not self.upgrade_in_place_reinit_one_then_remaining_nodes(new_version, drain_type):
            return False
    return True


# Any final tasks we need to do before calling the upgrade complete
# NOTE: starting cfn-hup happens in a general stack state restore method to ensure all error paths are covered too
def upgrade_in_place_cleanup(self: 'CfnStack', startup_primary: bool = True) -> bool:
    app_type: str = self.get_app_type()
    if not app_type:
        log_to_ui(self, ERROR, "Please ensure stack has a 'product' tag", write_to_changelog=True)
        return False
    if 'bitbucket' in app_type and 'mesh' in app_type:
        if startup_primary:
            # start primary stack bitbucket service
            bb_primary_stack_nodes = self.get_primary_stack_nodes()
            if not bb_primary_stack_nodes:
                return False
            if not self.startup_app(bb_primary_stack_nodes, primary_stack=True):
                return False
        else:
            log_to_ui(self, WARN, 'Skipping startup of primary cluster', write_to_changelog=True)
    return True
