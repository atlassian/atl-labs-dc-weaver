from datetime import timedelta


class BaseConfig(object):
    ########
    ## Defaults
    DEBUG = False
    TESTING = False
    PRODUCTS = ["Bitbucket", "Confluence", "Crowd", "Jira"]
    VALID_STACK_STATUSES = [
        'CREATE_COMPLETE',
        'UPDATE_COMPLETE',
        'UPDATE_ROLLBACK_COMPLETE',
        'CREATE_IN_PROGRESS',
        'DELETE_IN_PROGRESS',
        'UPDATE_IN_PROGRESS',
        'ROLLBACK_IN_PROGRESS',
        'ROLLBACK_COMPLETE',
        'ROLLBACK_FAILED',
        'DELETE_FAILED',
        'UPDATE_ROLLBACK_FAILED',
        'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS',
        'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
        'UPDATE_ROLLBACK_IN_PROGRESS',
    ]
    ########
    ## User configuration properties
    REGIONS = [('us-east-1', 'N.Virginia'), ('us-west-2', 'Oregon'), ('ap-southeast-2', 'Sydney')]
    ANALYTICS = True

    ##
    ## Configurable timeouts for actions (in seconds)
    ACTION_TIMEOUTS = {
        'approve_zdu_upgrade': 3600,
        'cancel_zdu_mode': 300,
        'check_index_health': 10800,
        'enable_zdu_mode': 300,
        'generate_support_zip': 300,
        'node_initialisation': 3600,
        'node_registration_deregistration': 3600,
        'validate_node_responding': 3600,
        'validate_service_responding': 3600,
        'zdu_ready_to_run_upgrade_tasks': 600,
        'zdu_upgrade_tasks_complete': 3600,
    }

    ## Default parameters for cloning a stack
    # Enter defaults for desired parameters to be applied to all stacks
    # fmt: off
    CLONE_DEFAULTS = {
        'all': {
            'ClusterNodeCount': '1',
            'CustomDnsName': '',
            'DBMultiAZ': 'false',
            'DeployEnvironment': 'stg',
            'MailEnabled': 'false',
            'Monitoring': 'false',
            'TomcatScheme': 'http'
        },
        'foj-stg': {
            'ClusterNodeCount': '4',
            'CustomDnsName': 'mystack.mycompany.com'
        },
        # To create stack specific defaults, add a param with the stackname, as below
        # When cloning a stack of this name, these defaults will be applied over the top of the parameters above
        # 'mystack' = {
        #     'ClusterNodeMin': '2',
        #     'ClusterNodeMax': '2',
        #     'CustomDnsName': 'mystack.mycompany.com'
        # },
    }
    # fmt: on

    # Percentage of index health considered acceptable
    ACCEPTABLE_INDEX_HEALTH = 98

    # For a given Weaver deployment environment, which environment stacks is it allowed to run actions on.
    ACCESSIBLE_ENVIRONMENTS = {'dev': ['dev'], 'dr': ['dr'], 'stg': ['stg'], 'prod': ['prod']}

    # List of allowed domains for use where an alternate product URL has been specified
    ALTERNATE_DOWNLOAD_URL_ALLOWED_DOMAINS = [
        "www.atlassian.com",
        "packages.atlassian.com",
        "product-downloads.atlassian.com",
    ]

    # Default URL for gravatar
    # To use a custom URL, specify the username, md5_of_email, email_address parameter in {}s for it to be injected for the user, eg
    # https://mycompany.com/avatars/{username}?size=Small
    AVATAR_URL = 'https://www.gravatar.com/avatar/{md5_of_email}.jpg?s=80&d=mp'

    # disable SAML by default
    NO_SAML = True

    # Default SNS region
    SNS_REGION = 'us-east-1'

    # Executor config
    EXECUTOR_PROPAGATE_EXCEPTIONS = True
    EXECUTOR_PUSH_APP_CONTEXT = True

    ## Stack locking
    # Lock stack actions so only one can be performed at a time
    STACK_LOCKING = False

    # Default StackName to 16 characters
    # Set user defined StackName max characters length to 16 due to AWS generated appending string to Target Group exceeding 32 chars max limit
    STACKNAME_LENGTH = '16'

    # Create a SQLalchemy db for SAML session and token key storage
    SQLALCHEMY_DATABASE_URI = 'sqlite:///weaver.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # suppress warning messages

    # Flask-Caching
    CACHE_DEFAULT_TIMEOUT = 60
    CACHE_KEY_PREFIX = "weaver_cache_"
    CACHE_TYPE = "NullCache"  # disable caching by default

    # Manage session config (Flask-Session)
    SESSION_COOKIE_SECURE = True  # Browsers will only send cookies with requests over HTTPS if the cookie is marked “secure”
    SESSION_KEY_PREFIX = 'session:'
    SESSION_SQLALCHEMY_TABLE = 'sessions'
    SESSION_TYPE = 'sqlalchemy'
    SESSION_SQLALCHEMY_TABLE_EXISTS = True

    # Manage access token expiration (API tokens only)
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=15)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=90)
    JWT_ERROR_MESSAGE_KEY = "error_msg"

    # Platform (k8s or cfn)
    PLATFORM = 'cfn'


# This configuration is used by run.py in any environment where ATL_ENVIRONMENT is not defined (i.e., local environments)
class DebugConfig(BaseConfig):
    DEBUG = True
    SESSION_COOKIE_SECURE = False
    ACCESSIBLE_ENVIRONMENTS = {'local': ['dev', 'dr', 'stg', 'prod']}


# This configuration is used by tests
class TestingConfig(DebugConfig):
    NO_SAML = True
    TESTING = True
