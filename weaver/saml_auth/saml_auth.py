#!/usr/bin/env python3

from __future__ import annotations

import getpass
import json
import logging
import sys

from os import getenv, path

import botocore
import flask_saml
from flask import current_app, Blueprint, request, session
from ruamel import yaml

saml_blueprint = Blueprint('saml_auth', __name__)

log = logging.getLogger('app_log')

PERMISSIONS_FILE_NAME = "permissions.yaml"
PERMISSIONS_FILE_NAME_LEGACY = "permissions.json"
ROUTES_WITH_NO_STACKNAME = {'stacklistmethods', 'docreatetoken', 'dogitpull', 'utilcfntemplatemethods'}


def configure_saml(ssm_client, app):
    if getenv('SAML_METADATA_URL'):
        app.config['SAML_METADATA_URL'] = getenv('SAML_METADATA_URL')
    else:
        try:
            saml_protocol = ssm_client.get_parameter(Name='atl_weaver_saml_metadata_protocol', WithDecryption=True)
            saml_url = ssm_client.get_parameter(Name='atl_weaver_saml_metadata_url', WithDecryption=True)
            app.config['SAML_METADATA_URL'] = f"{saml_protocol['Parameter']['Value']}://{saml_url['Parameter']['Value']}"
        except botocore.exceptions.NoCredentialsError:
            log.exception('No credentials - please authenticate with Cloudtoken')
            sys.exit(1)
        except KeyError:
            log.exception('SAML is configured but there is no SAML metadata URL in the parameter store - exiting')
            sys.exit(1)
    flask_saml.FlaskSAML(app)
    log.info('SAML auth configured')


def load_permissions_file(file_path: str) -> dict:
    try:
        permissions = yaml.YAML(typ="safe", pure=True).load(open(file_path, 'r'))
    except Exception:
        log.exception(f'Unable to load permissions file {file_path}')
        permissions = {}
    return permissions


def load_legacy_permissions_file(file_path: str) -> dict:
    try:
        with open(file_path) as json_data:
            permissions = json.load(json_data)
    except Exception:
        log.exception(f'Unable to load legacy permissions file {file_path}')
        permissions = {}
    return permissions


def get_saml_user_and_groups() -> tuple[str, list[str]]:
    """
    Grab user_name and groups from the session; provide fallbacks for local dev; filters the returned SAML groups by the
    list of groups configured in our permissions file(s)
    """
    if "saml" in session:
        configured_permissions_groups = get_configured_permissions_groups()
        return (
            session["saml"]["subject"],
            [group for group in session["saml"]["attributes"]["memberOf"] if group in configured_permissions_groups],
        )
    else:
        return getpass.getuser(), []


def get_configured_permissions_groups() -> set[str]:
    """
    Returns a set of the groups configured in permissions.yaml and permissions.json; used for filtering a user's SAML
    groups against the configured permissions groups for embedding in an API "refresh" token
    """
    groups = set()
    yaml_permissions_file_path = path.join(current_app.instance_path, PERMISSIONS_FILE_NAME)
    if path.exists(yaml_permissions_file_path):
        yaml_permissions = load_permissions_file(yaml_permissions_file_path)
        if yaml_permissions:
            for group in yaml_permissions["groups"]:
                groups.add(group["name"])
    json_permissions_file_path = path.join(current_app.instance_path, PERMISSIONS_FILE_NAME_LEGACY)
    if path.exists(json_permissions_file_path):
        json_permissions = load_legacy_permissions_file(json_permissions_file_path)
        if json_permissions:
            for group in json_permissions.values():
                groups.add(group["group"][0])
    return groups


def check_user_permissions(weaver_method_name, req_region, stack_name) -> bool:
    """
    Checks a given request against permissions.yaml for access privileges on that route
    """
    # load permissions on every request
    permissions = {}
    permissions_file_path = path.join(current_app.instance_path, PERMISSIONS_FILE_NAME)
    if path.exists(permissions_file_path):
        permissions = load_permissions_file(permissions_file_path)
    if not permissions:
        return False
    # check user permissions
    # loop through groups one by one
    for group in permissions['groups']:
        # if user is a member of this group
        if group['name'] in session['saml']['attributes']['memberOf']:
            # loop through the roles for the group one by one
            for group_role in group['roles']:
                # if any of the regions in the group_role's regions are wildcard * or this region
                # the user can assume this role in this region
                # or if there is no region in the request, eg for admin functions like git pull
                if any([req_region is None or region == '*' or region == req_region for region in group['roles'][group_role]['regions']]):
                    # retrieve the details of this role
                    weaver_role = next(role for role in permissions['roles'] if group_role == role['name'])
                    # if any of the http_methods the role can perform are wildcard * or this http_method
                    # the user can perform this http_method in this region
                    if any([http_method == '*' or http_method == request.method.lower() for http_method in weaver_role['http_methods']]):
                        # if any of the allowed_resources the role can act on are wildcard * or this resource
                        # the user can perform actions on this resource
                        if any([weaver_method == '*' or weaver_method.lower() == weaver_method_name for weaver_method in weaver_role['weaver_methods']]):
                            # many utility methods do not have stack_name arg so proceed
                            if weaver_method_name in ROUTES_WITH_NO_STACKNAME:
                                return True
                            # if any of the allowed_stacks the role can act on are wildcard * or this stack_name
                            # the user can perform actions on this stack
                            if any([allowed_stack == '*' or allowed_stack == stack_name for allowed_stack in group['roles'][group_role]['stacks']]):
                                if request.method != 'GET':
                                    log.info(f"User {session['saml']['subject']} is authorised to act on {weaver_method_name} for {stack_name}")
                                return True
    return False


def check_user_permissions_legacy(weaver_method_name, req_region, stack_name) -> bool:
    """
    Checks a given request against permissions.json for access privileges on that route
    """
    # load permissions on every request
    json_perms = {}
    permissions_file_path = path.join(current_app.instance_path, PERMISSIONS_FILE_NAME_LEGACY)
    if path.exists(permissions_file_path):
        json_perms = load_legacy_permissions_file(permissions_file_path)
    if not json_perms:
        return False
    # most gets were unrestricted in the past so allow all gets except thread dump links and support zip links
    # this will allow gets on new weaver methods (eg StackListMethods/StackMethods)
    # without having permissions.yaml implemented
    if request.method == 'GET' and weaver_method_name not in ('dogitpull', 'getthreaddumplinks', 'getsupportziplinks'):
        return True
    for keys in json_perms:  # loop through groups
        if json_perms[keys]['group'][0] in session['saml']['attributes']['memberOf']:  # if user member of group
            if session['region'] in json_perms[keys]['region'] or '*' in json_perms[keys]['region']:  # if group can act in region
                if weaver_method_name in json_perms[keys]['action'] or '*' in json_perms[keys]['action']:  # if action allowed
                    if stack_name and (stack_name in json_perms[keys]['stack'] or '*' in json_perms[keys]['stack']):
                        log.info(f"User {session['saml']['subject']} is authorised to perform {weaver_method_name} on {stack_name}")
                        return True
    return False
