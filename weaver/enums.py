## Custom Enums
#

import enum


class DiagnosticFileType(enum.Enum):
    THREAD_DUMP = 0
    SUPPORT_ZIP = 1
    HEAP_DUMP = 2


class DiagnosticType(enum.Enum):
    CPU = 'cpu'
    HEAP_DUMP = 'heap_dump'
    INDEX_HEALTH = 'index_health'
    SUPPORT_ZIP = 'support_zip'
    THREAD_DUMP = 'thread_dump'


class PostMethodDiagnosticType(enum.Enum):
    HEAP_DUMP = 'heap_dump'
    SUPPORT_ZIP = 'support_zip'
    THREAD_DUMP = 'thread_dump'


# Defines how nodes are drained when deregistering or destroying them.
class DrainType(enum.Enum):
    FAST = 'fast'  # Temporarily adjust node drain time to a small number.
    SKIP = 'skip'  # Skip node drain altogether.
    DEFAULT = 'default'  # Leave node drain configuration as defined by infrastructure.


# The types of load balancers Weaver currently supports.
class LoadBalancer(enum.Enum):
    ALB = 'ALB'  # Application Load Balancer
    NLB = 'NLB'  # Network Load Balancer


class RestartType(enum.Enum):
    FULL = 'full'
    ROLLING = 'rolling'
    SINGLE = 'single'


# Types of upgrades to apply to stacks. Currently incomplete.
class UpgradeType(enum.Enum):
    IN_PLACE = 'IN_PLACE'
    DEFAULT = 'DEFAULT'
