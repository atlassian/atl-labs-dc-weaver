import concurrent.futures
import re
import time
from datetime import datetime
from logging import getLogger, INFO
from typing import Any

import pytz
from flask import current_app
from kubernetes import client, config
from kubernetes.stream import stream

from weaver import utils
from weaver.Stack import Stack
from weaver.weaver_logging import log_to_ui

log = getLogger('app_log')


def get_namespaces():
    config.load_kube_config()
    core_api = client.CoreV1Api()
    namespaces = core_api.list_namespace(_request_timeout=10)
    namespace_names = []
    for namespace in namespaces.items:
        namespace_names.append(namespace.metadata.name)
    return namespace_names


def get_pods_for_stateful_set(stateful_set_name):
    config.load_kube_config()
    core_api = client.CoreV1Api()
    all_pods = core_api.list_pod_for_all_namespaces()
    pods = []
    for pod in all_pods.items:
        if re.search(fr"^{stateful_set_name}-\d+$", pod.metadata.name):
            pods.append(pod)
    return pods


def get_app_type(node):
    return next(container for container in node.spec.containers if container.name in ('jira', 'confluence')).name


class StatefulSet(Stack):
    """An object describing a Kubernetes StatefulSet"""

    def __init__(self, stack_name, region):
        Stack.__init__(self, stack_name, region)
        self.namespace = self.get_stateful_set_namespace()

    def get_stack_info(self):
        stack_info: dict[str, Any] = {}
        try:
            config.load_kube_config()
            apps_api = client.AppsV1Api()
            # get nodes
            pods = self.get_stacknodes()

            stack_info['nodes'] = []
            with concurrent.futures.ThreadPoolExecutor() as executor:
                future_timeout = 10  # seconds; int or float
                # kick off the parallel tasks; keep the tasks in a mapping pointing back to the pod the task is for (used later in case task doesn't complete within timeout)
                future_to_instance_map = {executor.submit(self.__get_stack_info_instance_builder, pod): pod for pod in pods}

                # go get the rest of the stack info on the main thread while the pod status info is collected
                action_in_progress = utils.get_stack_action_in_progress(self.stack_name)
                stack_info['action_in_progress'] = action_in_progress if action_in_progress else 'none'
                stack = apps_api.read_namespaced_stateful_set(self.stack_name, self.namespace)
                stack_info['stack_status'] = f'ready replicas: {stack.status.ready_replicas}'
                stack_info['service_status'] = 'unknown'
                stack_info['product'] = stack.spec.template.metadata.labels['app.kubernetes.io/name']
                stack_info['version'] = 'unknown'

                # wait for all the node status info parallel tasks to be done or timed-out
                done, not_done = concurrent.futures.wait(future_to_instance_map, timeout=future_timeout)
                for future in done:
                    # for any tasks that completed, check for unhandled exceptions; otherwise add resulting node info to stack_info
                    try:
                        result = future.result()
                    except Exception as e:
                        log.exception("Unhandled exception building instance info")
                        stack_info['nodes'].append({'autoscaling_group_name': 'Unknown', 'error_class': type(e).__name__, 'error_details': str(e)})
                    else:
                        stack_info['nodes'].append(result)
                if len(not_done):
                    # log the number of tasks that didn't complete (if any)
                    log.error(
                        f'{self.stack_name} get_stack_info: {len(not_done)} of {len(future_to_instance_map)} instance builder tasks did not complete within timeout ({future_timeout} seconds)'
                    )
                    # for any tasks that didn't complete within the timeout, ref back to the instance and try to build the node info again with just the known EC2 instance data that shouldn't require additional API calls
                    for future in not_done:
                        try:
                            instance = future_to_instance_map[future]
                            stack_info['nodes'].append(self.__get_stack_info_instance_builder(instance, allow_api_calls=False))
                        except Exception as e:
                            log.exception("Unhandled exception building instance info")
                            stack_info['nodes'].append({'autoscaling_group_name': 'Unknown', 'error_class': type(e).__name__, 'error_details': str(e)})
            # at this point; the nodes will be in whatever order they completed their parallel tasks in
            # sort the nodes by their ASG name (custom order defined by asg_sort_order_map) and the last octet in their IP address (if it has one)
            # asg_sort_order_map = {'ClusterNodeGroup': 0, 'ClusterBotNodeGroup': 1, 'SynchronyClusterNodeGroup': 2}
            # asg_sort_order = lambda asg_name: next((position for name_fragment, position in asg_sort_order_map.items() if name_fragment in asg_name), 99)
            # stack_info['nodes'] = sorted(
            #     stack_info['nodes'], key=lambda node: (asg_sort_order(node['autoscaling_group_name']), int(node['ip'].split('.')[3]) if 'ip' in node else 99)
            # )
        except Exception as e:
            log.exception('Error getting stack info')
            return f'Error getting stack info: {e}'
        return stack_info

    def __get_stack_info_instance_builder(self, pod, allow_api_calls=True):
        uptime_total_seconds = (datetime.now(pytz.utc) - pod.status.start_time).total_seconds()
        days, hours_remainder = divmod(uptime_total_seconds, 3600 * 24)
        hours, minutes_remainder = divmod(hours_remainder, 3600)
        minutes, seconds = divmod(minutes_remainder, 60)

        registration_status = '-'
        if pod.status.container_statuses is not None:
            product_container_status = next(container_status for container_status in pod.status.container_statuses if container_status.name in ('jira', 'confluence'))
            registration_status = 'ready' if product_container_status.ready else 'not ready'

        product_container = next(container for container in pod.spec.containers if container.name in ('jira', 'confluence'))

        return {
            'arch': 'unknown',
            'autoscaling_group_name': '',
            'az': 'unknown',
            'id': pod.spec.node_name,
            'ip': f'{pod.spec.hostname}: {pod.status.pod_ip}',
            'registration_status': registration_status,
            'service': product_container.name,
            'state': pod.status.phase,
            'status': 'unknown',
            'type': 'unknown',
            'uptime': f'{int(days)}d {int(hours)}h {int(minutes)}m',
        }

    def get_stacknodes(self, force=False):
        if self.stack_nodes and not force:
            return self.stack_nodes
        else:
            log.debug(f'Getting stack nodes{"; update is forced" if force else ""}')
            self.stack_nodes = get_pods_for_stateful_set(self.stack_name)
            return self.stack_nodes

    def get_service_url(self):
        pass

    def get_stateful_set_namespace(self):
        if hasattr(self, 'namespace'):
            return self.namespace
        config.load_kube_config()
        apps_api = client.AppsV1Api()
        stateful_sets = apps_api.list_stateful_set_for_all_namespaces(_request_timeout=10)
        for stateful_set in stateful_sets.items:
            if stateful_set.metadata.name == self.stack_name:
                return stateful_set.metadata.namespace

    def thread_dump(self, node_ip=False, alsoHeaps=False):
        heaps_to_come_log_line = ''
        if alsoHeaps == 'true':
            heaps_to_come_log_line = ', heap dumps to follow'
        log_to_ui(self, INFO, f'Beginning thread dumps on {self.stack_name}{heaps_to_come_log_line}', write_to_changelog=False)
        nodes = self.get_stacknodes()
        if node_ip:
            nodes = list(filter(lambda node: node.status.pod_ip == node_ip, nodes))
        log_to_ui(self, INFO, f'Thread dumps requested on {[{node.spec.hostname} for node in nodes]}', write_to_changelog=False)
        self.run_command(nodes, '/opt/atlassian/support/thread-dumps.sh')
        log_to_ui(self, INFO, 'Successful thread dumps can be downloaded from the main Diagnostics page', write_to_changelog=False)
        log_to_ui(self, INFO, 'Thread dumps complete', write_to_changelog=True)
        return True

    def heap_dump(self, node_ip=False):
        log_to_ui(self, INFO, f'Beginning heap dumps on {self.stack_name}', write_to_changelog=False)
        nodes = self.get_stacknodes()
        if node_ip:
            nodes = list(filter(lambda node: node.status.pod_ip == node_ip, nodes))
        log_to_ui(self, INFO, f'Heap dumps requested on {[{node.spec.hostname} for node in nodes]}', write_to_changelog=False)
        # Wait for each heap dump to finish before starting the next, to avoid downtime
        for node in nodes:
            self.run_command([node], '/opt/atlassian/support/heap-dump.sh')
            if ('TESTING' not in current_app.config or current_app.config['TESTING'] is False) and len(nodes) > 1:
                # TODO make this a status check when that is implemented
                time.sleep(30)  # give node time to recover and rejoin cluster
        log_to_ui(self, INFO, 'Heap dumps complete', write_to_changelog=True)
        return True

    def run_command(self, nodes, cmd, log_msgs=True):
        responses = []
        config.load_kube_config()
        core_api = client.CoreV1Api()
        for node in nodes:
            if log_msgs:
                log_to_ui(self, INFO, f'Running command {cmd} on {node.spec.hostname}', write_to_changelog=False)
            response = stream(
                core_api.connect_get_namespaced_pod_exec,
                name=node.spec.hostname,
                namespace=self.namespace,
                container=get_app_type(node),
                command=['/bin/sh', '-c', cmd],
                stderr=True,
                stdin=True,
                stdout=True,
                tty=True,
            )
            if log_msgs:
                log_to_ui(self, INFO, response, write_to_changelog=False)
            responses.append(response)
        return responses

    # TODO: Add support for heap dumps
    def get_s3_file_links(self):
        thread_dumps = []
        for node in self.get_stacknodes():
            product = get_app_type(node)
            str_response = self.run_command([node], f'ls /var/atlassian/application-data/{product}/thread_dumps/', log_msgs=False)
            thread_dump_folders = str.replace(str_response[0], '\r\n', ' ').split()
            for folder in thread_dump_folders:
                thread_dumps.append({'namespace': self.namespace, 'pod': node.spec.hostname, 'product': product, 'dump': folder})
        return thread_dumps

    # Implement later. :)
    def copy_support_zip_to_s3(self, zip_filenames: dict[str, str] = {}):
        pass

    def get_tag(self, tag_name: str, log_msgs=True):
        pass
