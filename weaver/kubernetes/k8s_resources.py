import json

import kubernetes
from flask import session
from kubernetes import client, config


def get_k8s_stacknames():
    config.load_kube_config()
    apps_api = client.AppsV1Api()
    stateful_set_names = []
    try:
        stateful_sets = apps_api.list_stateful_set_for_all_namespaces(_request_timeout=10)
        session['credentials'] = True
        for stateful_set in stateful_sets.items:
            stateful_set_names.append(stateful_set.metadata.name)
    except kubernetes.client.exceptions.ApiException as e:
        if json.loads(e.body)['reason'] == 'Forbidden':
            session['credentials'] = False
        else:
            raise e
    return sorted(stateful_set_names)
