import json
import logging
import re
import time

from abc import abstractmethod, ABC
from flask import current_app
from humanfriendly import format_timespan
from logging import ERROR, INFO
from requests_toolbelt.sessions import BaseUrlSession
from typing import Any, Union
from enum import Enum

from weaver.weaver_logging import log_to_ui


log = logging.getLogger('app_log')


class Stack(ABC):
    """An object describing a stack of resources that combine to make a product deployment"""

    def __init__(self, stack_name, region):
        self.stack_name = stack_name
        self.logfile = None
        if region == '':
            error_string = 'No region defined for stack - your session may have timed out'
            log_to_ui(self, ERROR, error_string, write_to_changelog=False)
            raise ValueError(error_string)
        self.app_type: str = ''
        self.region = region
        self.stack_id = None
        self.changelogfile = None

        self.primary_stack_nodes = None
        self.primary_stack_name = None

        self.preupgrade_version: str = ''
        self.preupgrade_alt_download_url: str = ''
        self.preupgrade_app_node_count: int = 0
        self.preupgrade_az_node_counts = None
        self.preupgrade_backup_node_count: int = 0
        self.preupgrade_synchrony_node_count: int = 0
        self.preupgrade_bot_node_count: int = 0
        self.preupgrade_listener_rules = []

        self.stack_resources = []
        self.stack_nodes = []
        self.service_url: str = ''

    @abstractmethod
    def get_stack_info(self, expands: list[str]) -> dict[str, Any]:
        pass

    @abstractmethod
    def get_stacknodes(self, force=False):
        pass

    @abstractmethod
    def get_service_url(self):
        pass

    @abstractmethod
    def thread_dump(self):
        pass

    @abstractmethod
    def run_command(self, nodes, cmd, return_results=False):
        pass

    @abstractmethod
    def get_s3_file_links(self) -> list[str]:
        pass

    @abstractmethod
    def copy_support_zip_to_s3(self, zip_filenames: dict[str, str] = {}):
        pass

    @abstractmethod
    def get_tag(self, tag_name: str, log_msgs=True):
        pass

    def get_clone_defaults(self) -> dict[str, str]:
        clone_defaults = {}
        if 'all' in current_app.config['CLONE_DEFAULTS']:
            # shallow copy should be sufficient here; we don't support nested parameter definitions in clone defaults today
            clone_defaults = current_app.config['CLONE_DEFAULTS']['all'].copy()
        if self.stack_name in current_app.config['CLONE_DEFAULTS']:
            clone_defaults.update(current_app.config['CLONE_DEFAULTS'][self.stack_name])
        return clone_defaults

    # https://confluence.atlassian.com/support/create-a-support-zip-using-the-rest-api-in-server-applications-947857090.html
    def generate_support_zip(self, personal_access_token: str) -> Union[str, bool]:
        log_to_ui(self, INFO, f'Beginning generate Support Zip on {self.stack_name}', write_to_changelog=False)
        self.get_service_url()  # Generate the actual service_url link
        url = self.service_url
        app_type = self.get_tag('product')
        self.session = BaseUrlSession(base_url=f'{url}')
        self.session.headers.update(
            {
                'X-Atlassian-Token': 'no-check',
                'Authorization': f'Bearer {personal_access_token}',
            }
        )  # X-Atlassian-Token prevents "XSRF check failed".
        try:
            if app_type not in ['bitbucket', 'confluence', 'crowd', 'jira']:
                log_to_ui(self, ERROR, 'Only Bitbucket, Confluence, Crowd and Jira are supported.', write_to_changelog=True)
                log_to_ui(self, ERROR, 'Generate Support Zip complete - failed', write_to_changelog=True, send_sns_msg=True)
                return False
            response = self.session.post('rest/troubleshooting/latest/support-zip/cluster?os_authType=basic')
            if response.ok:
                response_content = json.loads(response.text)
                task_id: str = response_content['clusterTaskId']
                task_completed: bool = self.check_support_zip_completed(task_id)
                action_start = time.time()
                action_timeout = current_app.config['ACTION_TIMEOUTS']['generate_support_zip']
                while not task_completed:
                    if (time.time() - action_start) > action_timeout:
                        log_to_ui(
                            self,
                            ERROR,
                            f"{self.stack_name} failed to generate Support Zip bundle after {format_timespan(action_timeout)}. Generate Support Zip status is: {task_completed}",
                            write_to_changelog=True,
                        )
                        return False
                    time.sleep(5)
                    task_completed = self.check_support_zip_completed(task_id)
                zip_filenames: dict[str, str] = self.get_support_zip_filenames(task_id)
                self.copy_support_zip_to_s3(zip_filenames)
                log_to_ui(self, INFO, 'Successful Support Zip files can be downloaded from the main Diagnostics page', write_to_changelog=False)
                log_to_ui(self, INFO, 'Generating Support Zip complete', write_to_changelog=True)
                return True
            else:
                error_msg = f'Something went wrong: {response.status_code} {response.reason} '
                if response.status_code == 401:
                    error_msg += 'The credentials you entered are incorrect or do not have administrator access'
                log_to_ui(self, ERROR, f"Unable to generate Support Zip: {error_msg}", write_to_changelog=False)
                return error_msg
        except Exception as e:
            log.exception('An error occurred generating Support Zip')
            log_to_ui(self, ERROR, f'An error occurred generating Support Zip: {e}', write_to_changelog=True)
            log_to_ui(self, ERROR, 'Generate Support Zip complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False

    def check_support_zip_completed(self, task_id: str) -> bool:
        try:
            response = self.session.get(f'rest/troubleshooting/latest/support-zip/status/cluster/{task_id}?os_authType=basic')
            if response.ok:
                response_content = json.loads(response.text)
                tasks_completed: bool = True
                for task in response_content['tasks']:
                    node_id: str = task['nodeId']
                    status: str = task['status']
                    log_to_ui(self, INFO, f'Status of generating Support Zip {task_id} on node {node_id} is {status}', write_to_changelog=False)
                    if status != "SUCCESSFUL":
                        tasks_completed = False
                return tasks_completed
            else:
                error_msg = f'Something went wrong: {response.status_code} {response.reason} '
                if response.status_code == 401:
                    error_msg += 'The credentials you entered are incorrect or do not have administrator access'
                log_to_ui(self, ERROR, f"Unable to check Support Zip status: {error_msg}", write_to_changelog=False)
                return False
        except Exception as e:
            log.exception('An error occurred checking Support Zip progress')
            log_to_ui(self, ERROR, f'An error occurred checking Support Zip status: {e}', write_to_changelog=True)
            log_to_ui(self, ERROR, 'Generate Support Zip complete - failed', write_to_changelog=True, send_sns_msg=True)
            return False

    def get_support_zip_filenames(self, task_id: str) -> dict[str, str]:
        try:
            response = self.session.get(f'rest/troubleshooting/latest/support-zip/status/cluster/{task_id}?os_authType=basic')
            if response.ok:
                response_content = json.loads(response.text)
                zip_filenames = {}
                for task in response_content['tasks']:
                    # Can't rely on "nodeId" as Confluence and Bitbucket return a non-IP string
                    ip_search = match.group(0) if (match := re.search(r'(\d{1,3}-\d{1,3}-\d{1,3}-\d{1,3})', task['fileName'])) is not None else ''
                    node_ip = ip_search.replace('-', '.')
                    full_file_path = task['progressMessage'].replace('It was saved to ', '').replace('.zip.', '.zip')
                    zip_filenames[node_ip] = full_file_path
                return zip_filenames
            else:
                error_msg = f'Something went wrong: {response.status_code} {response.reason} '
                if response.status_code == 401:
                    error_msg += 'The credentials you entered are incorrect or do not have administrator access'
                log_to_ui(self, ERROR, f"Unable to get Support Zip filenames: {error_msg}", write_to_changelog=False)
                return {}
        except Exception as e:
            log.exception('An error occurred getting Support Zip filenames')
            log_to_ui(self, ERROR, f'An error occurred getting Support Zip filenames: {e}', write_to_changelog=True)
            log_to_ui(self, ERROR, 'Generate Support Zip complete - failed', write_to_changelog=True, send_sns_msg=True)
            return {}
