#!/usr/bin/env python3

from flask_caching import Cache
from flask_executor import Executor
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_session import Session

cache = Cache()
db = SQLAlchemy()
executor = Executor()
jwt = JWTManager()
ma = Marshmallow()
migrate = Migrate()
sess = Session()
