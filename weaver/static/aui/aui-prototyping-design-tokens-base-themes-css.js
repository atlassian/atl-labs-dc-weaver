/*!!
 * @atlassian/aui - Atlassian User Interface library
 * @version v9.12.0
 * @link https://aui.atlassian.com
 * @license Apache-2.0
 * @author Atlassian Pty Ltd.
 */
!function(e,o){"object"==typeof exports&&"object"==typeof module?module.exports=o():"function"==typeof define&&define.amd?define("@atlassian/aui",[],o):"object"==typeof exports?exports["@atlassian/aui"]=o():(e.AJS=e.AJS||{},e.AJS.DesignTokens=o())}(window,(()=>(()=>{"use strict";var e={r:e=>{"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})}},o={};return e.r(o),o})()));
//# sourceMappingURL=aui-prototyping-design-tokens-base-themes-css.js.map