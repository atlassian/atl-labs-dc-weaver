function onReady() {
    readyTheTemplate();
    $("#stack-name-input").hide();
    var products = document.getElementsByClassName("selectProductOption");
    for (var i = 0; i < products.length; i++) {
        products[i].addEventListener("click", function (data) {
            var product = data.target.text;
            $("#productSelector").text(product);
            document.getElementById("templateSelector").textContent = 'Select Template';
            getTemplates(product);
            resetForm();
        }, false);
    }
}

function resetForm() {
    $("#stack-name-input").hide();
    $("#paramsList").html("");
    $("#tagsList").hide();
    disableActionButton();
}
