function onReady() {
    $("#stackInformation").hide();
    const stacks = document.getElementsByClassName("selectStackOption");
    const platform = $("meta[name=platform]").attr("value");
    for (const stack of stacks) {
        stack.addEventListener("click", function (data) {
            $("#threadDumpBtn").attr("aria-disabled", false);
            $("#threadDumpBtn").removeAttr("disabled");
            $("#dlThreadDumpBtn").attr("aria-disabled", false);
            $("#dlThreadDumpBtn").removeAttr("disabled");
            $("#heapDumpBtn").attr("aria-disabled", false);
            $("#heapDumpBtn").removeAttr("disabled");
            $("#dlHeapDumpBtn").attr("aria-disabled", false);
            $("#dlHeapDumpBtn").removeAttr("disabled");
            selectStack(data.target.text);
            emptyAsgAndNodeListsAndCpuChart();
            listNodes();
        }, false);
    }
}

function takeDiagnostics(diagnostic_type) {
    const stack_name = $("#stackName").text();
    const requestBody = {"diagnostic_type": diagnostic_type}
    const node = $("#nodeSelector").text();
    if (diagnostic_type === "support_zip") {
        requestBody["personal_access_token"] = getAuthDetails()["personal_access_token"]
    } else {
        // Weaver API does not support generating support ZIPs for specific nodes
        // Assume a string with 3 .'s means a specific node was chosen, otherwise take diagnostics on all nodes (don't include node_ip in request body)
        if ((node.match(/\./g) || []).length == 3) {
            requestBody["node_ip"] = node
        }
    }
    send_http_request(`${baseUrl}/api/v1/stack/${stack_name}/diagnostics?region=${region}`, "POST", {
        data: JSON.stringify(requestBody),
        onreadystatechange: (responseText) => redirectToLog(stack_name, JSON.parse(responseText)['action_id'])
    })
}

// Assumes input is formatted as required by the Weaver API route
function downloadDiagnostics(diagnostic_type) {
    const stack_name = $("#stackName").text();
    const nodeSelectorText = $("#nodeSelector").text();
    const diagnostic_type_pretty = diagnostic_type.replace("_", " ")
    // Assume a string with 3 .'s means a specific node was chosen, otherwise fetch links all nodes (don't include node_ip as a query param)
    let node_ip = null
    if ((nodeSelectorText.match(/\./g) || []).length == 3) {
        node_ip = nodeSelectorText
    }
    replaceModalContents(modals.PROGRESS, {text: `Loading ${diagnostic_type_pretty}s for ${(node_ip ?? stack_name)}...`});
    appendToModalContents(modals.PROGRESS, "<aui-progressbar indeterminate></aui-progressbar>")
    showModal(modals.PROGRESS);
    const full_url = `${baseUrl}/api/v1/stack/${stack_name}/diagnostics?region=${region}&diagnostic_types=${diagnostic_type}` + (node_ip ? `&node_ip=${node_ip}` : "")
    send_http_request(full_url, "GET", {
        onreadystatechange: (responseText) => displayDiagnosticsToDownload(JSON.parse(responseText)[`${diagnostic_type}s`], diagnostic_type, node_ip === null)
    })
}

function displayDiagnosticsToDownload(diagnostics, diagnostic_type, all_nodes) {
    const platform = $("meta[name=platform]").attr("value");
    if (platform === 'k8s') {
        if (diagnostic_type === "heap_dump") {
            displayK8sHeapdumpInstructions()
        } else if (diagnostic_type === "support_zip") {
            displayK8sSupportZipInstructions(support_zips)
        } else if (diagnostic_type === "thread_dump") {
            displayK8sThreaddumpInstructions(thread_dumps)
        }
    } else {
        displayDlUrls(diagnostics, diagnostic_type, all_nodes)
    }
}

function displayK8sThreaddumpInstructions(thread_dumps) {
    const command = "<pre><code>kubectl cp -c &lt;product&gt; &lt;namespace&gt;/&lt;pod&gt;:/var/atlassian/application-data/&lt;product&gt;/thread_dumps/&lt;thread_dump_folder&gt; &lt;local-location&gt;</code></pre>"

    let thread_dump_list = '';
    for (const thread_dump in thread_dumps) {
        thread_dump_list += thread_dumps[thread_dump]['pod']
        thread_dump_list += ": "
        thread_dump_list += thread_dumps[thread_dump]['dump']
        thread_dump_list += thread_dump < thread_dumps.length ? "<br>" : ''
    }
    replaceModalContents(modals.CONFIRM, {title: "Kubernetes thread dumps", text: "Use kubectl to download thread dumps directly from the pods:"});
    appendToModalContents(modals.CONFIRM, command)
    appendToModalContents(modals.CONFIRM, thread_dump_list);
    showModal(modals.CONFIRM);
}

// TODO: Add support for K8s
function displayK8sHeapdumpInstructions() {
    replaceModalContents(modals.ERROR, {title: "Kubernetes heap dumps", text: "Currently unsupported."});
    showModal(modals.ERROR);
}

function showIndexHealthModal() {
    setModalOkFunction(modals.AUTH, checkIndexHealth);
    showModal(modals.AUTH);
}

function checkIndexHealth() {
    var stack_name = $("#stackName").text();
    var node = $("#nodeSelector").text();
    let node_ip = ""
    // Assume a string with 3 .'s means a specific node was chosen, otherwise check index health for all nodes
    if ((node.match(/\./g) || []).length == 3) {
        node_ip = node
    }
    const stack_personal_access_token = getAuthDetails()["personal_access_token"]
    const full_url = `${baseUrl}/api/v1/stack/${stack_name}/diagnostics?region=${region}&diagnostic_types=index_health` + (node_ip ? `&node_ip=${node_ip}` : "")
    send_http_request(full_url, "GET", {
        headers: {"stack-personal-access-token": stack_personal_access_token},
        onreadystatechange: (response) => {
            var indexHealth = JSON.parse(response)["index_health"];
            var text = "";
            if (indexHealth.length === 0)
                text = "No index data was returned"
            for (node in indexHealth) {
                if (text.length > 0 )
                    text+= "<br>"
                text += indexHealth[node]['node_ip'] + ": " + indexHealth[node]['index_health']
                if (!isNaN(parseFloat(indexHealth[node]['index_health'])))
                    text += "%"
            }
            replaceModalContents(modals.CONFIRM, {title: "Index Health", text: text});
            showModal(modals.CONFIRM);
        }
    })
    replaceModalContents(modals.PROGRESS, {text: `Getting index health for ${stack_name}...`});
    appendToModalContents(modals.PROGRESS, "<aui-progressbar indeterminate></aui-progressbar>")
    showModal(modals.PROGRESS);
}

function showGenerateSupportZipModal() {
    setModalOkFunction(modals.AUTH, () => takeDiagnostics('support_zip'));
    showModal(modals.AUTH);
}

function displayK8sSupportZipInstructions(support_zips) {
    // ! For the most part should be right? * UNTESTED *

    const command = "<pre><code>kubectl cp -c &lt;product&gt; &lt;namespace&gt;/&lt;pod&gt;:/var/atlassian/application-data/&lt;product&gt;/export/&lt;filename&gt;</code></pre>"

    let support_zip_list = '';
    for (const support_zip in support_zips) {
        support_zip_list += support_zips[support_zip]['pod']
        support_zip_list += ": "
        support_zip_list += support_zips[support_zip]['dump']
        support_zip_list += support_zip < support_zips.length ? "<br>" : ''
    }
    replaceModalContents(modals.CONFIRM, {title: "Kubernetes support zips", text: "Use kubectl to download support zips directly from the pods:"});
    appendToModalContents(modals.CONFIRM, command)
    appendToModalContents(modals.CONFIRM, support_zip_list);
    showModal(modals.CONFIRM);
}

function displayDlUrls(urls, dlType, all_nodes) {
    if (dlType === "thread_dump") {
        sectionID = "#threaddump-dialog"
        divID = "threaddump-dialog-content"
    } else if (dlType === "support_zip") {
        sectionID = "#supportzip-dialog"
        divID = "supportzip-dialog-content"
    } else if (dlType === "heap_dump") {
        sectionID = "#heapdump-dialog"
        divID = "heapdump-dialog-content"
    }
    const displayDialog = document.getElementById(divID);
    while (displayDialog.firstChild) {
        displayDialog.removeChild(displayDialog.firstChild);
    }
    if (urls.length === 0) {
        const text = document.createElement("p");
        text.appendChild(document.createTextNode(`No downloadable ${dlType.replace("_", " ")}s available for this ${(all_nodes ? 'stack' : 'node')}.`));
        document.getElementById(divID).appendChild(text);
    } else {
        const ul = document.createElement("UL");
        ul.className = "aui-list-truncate";

        for (const url in urls) {
            const li = document.createElement("LI");
            const anchor = document.createElement("A");
            let stringUrl = urls[url].substr(urls[url].lastIndexOf("/")+1);
            stringUrl = stringUrl.substr(0, stringUrl.indexOf("?"));
            const text = document.createTextNode(stringUrl);
            anchor.appendChild(text);
            anchor.href=urls[url];
            li.appendChild(anchor);
            ul.appendChild(li);
        }
        document.getElementById(divID).appendChild(ul);
    }
    hideModal('progress');
    AJS.dialog2(sectionID).show();
}
