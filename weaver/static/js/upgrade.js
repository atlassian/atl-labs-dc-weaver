function onReady() {

    $('.selectStackOption').click((event) => {
        disableActionButton();
        $("#action-button").off('click');
        const stack_name = $(event.currentTarget).text();
        selectStack(stack_name);
        checkOptions(stack_name);
        $("#upgradeVersionSelector").removeAttr("disabled");
        $("#alternateDownloadUrl").removeAttr("disabled");
    });

    $('input[name="zduType"]').prop('disabled', true);
    $('#zduCheckBox').click((event) => $('input[name="zduType"]').prop('disabled', !event.currentTarget.checked));
    $('input[name="inPlaceUpgradeOption"]').prop('disabled', true);
    $('#inPlaceUpgradeCheckbox').click((event) => {
        // Enable all in-place upgrade options when that upgrade type is selected
        $('input[name="inPlaceUpgradeOption"]').prop('disabled', !event.currentTarget.checked)
        // Disable/enable options not used for in-place upgrades
        $('#fastNodeDrainCheckbox').prop('disabled', event.currentTarget.checked)
    });

    $('#alternateDownloadUrl').on('input change', (event) => {
        // get the field value from the event
        const altUrl = event.currentTarget.value;
        const versionregex = /\d+\.\d+\.\d+-?[^\s/.]*/;
        const parsedVersion = altUrl.match(versionregex);

        // populate the field or clear it
        if (altUrl && parsedVersion) {
            $('#upgradeVersionSelector').val(parsedVersion[0]);
        } else {
            $('#upgradeVersionSelector').val('');
        }

        // trigger change to force version field validation
        $('#upgradeVersionSelector').change();
    });

    // If Bitbucket or Confluence, validate version for ZDU
    $('#upgradeVersionSelector').on('input change', (event) => {
        const product = window.stackInfo.product;
        if (product === 'bitbucket' || product === 'confluence') {
            resetZDUPanel();
            // get the field value from the event
            const upgrade_version = event.currentTarget.value.split('.');
            const major_version = window.stackInfo.version.split('.').slice(0, 2);

            // check Bitbucket compatibility
            if (product === 'bitbucket') {
                // if upgrade version is the same major version
                if (upgrade_version[0] === major_version[0] && upgrade_version[1] === major_version[1]) {
                    $("#zduCheckDiv").attr("style", "display:none");
                    $("#performZDUDiv").removeAttr("style");
                } else {
                    $("#zduCheckLbl").text(`Bitbucket can only perform Rolling Upgrades within the ${major_version[0]}.${major_version[1]} range`);
                }
            }
            // check Confluence compatibility
            else if (product === 'confluence') {
                // if upgrade version is the same, or the next, major version
                if (upgrade_version[0] === major_version[0] && (upgrade_version[1] === major_version[1] || parseInt(upgrade_version[1]) === parseInt(major_version[1]) + 1)) {
                    $("#zduCheckDiv").attr("style", "display:none");
                    $("#performZDUDiv").removeAttr("style");
                } else {
                    $("#zduCheckLbl").text(`Confluence can only perform Rolling Upgrades within the ${major_version[0]}.${major_version[1]} range, or to the next major version (ie ${major_version[0]}.${parseInt(major_version[1]) + 1})`);
                }
            }
        }
    });

    AJS.$('#upgradeForm').on('aui-valid-submit', (event) => {
        disableActionButton();
        beginUpgrade();
        event.preventDefault();
    });

    // Version checking not currently working (and only worked for Confluence in the past). Leaving so we can fix for public.
    // if (action === "upgrade") {
    //     document.getElementById("versionCheckButton").addEventListener("click", function (data) {
    //         var version = $("#upgradeVersionSelector").val();
    //         var url = 'https://s3.amazonaws.com/atlassian-software/releases/confluence/atlassian-confluence-' + version + '-linux-x64.bin';
    //         $.ajax({
    //             url: url,
    //             type: 'HEAD',
    //             headers: {'Access-Control-Allow-Origin': url},
    //             complete: function (xhr) {
    //                 switch (xhr.status) {
    //                     case 200:
    //                         $("#versionExists").html(getStatusLozenge("Valid"));
    //                         $("#action-button").attr("aria-disabled", false);
    //                         break;
    //                     case 403:
    //                     default:
    //                         $("#versionExists").html(getStatusLozenge("Invalid"));
    //                 }
    //             },
    //         });
    //     });
    // }
}

function resetZDUPanel() {
    $("#zduCheckDiv").removeAttr("style");
    $("#zduCheckLbl").text("Checking Zero Downtime/Rolling Upgrade compatibility ...");
    $("#performZDUDiv").attr("style", "display:none");
}

function resetOtherOptionsPanel() {
    $("#otherOptionsCheckDiv").removeAttr("style");
    $("#otherOptionsCheckLbl").text("Checking other options available ...");
    $('#fastNodeDrainDiv').attr("style", "display:none");
    $('#inPlaceUpgradeDiv').hide();
    $('#startupPrimaryDiv').hide();
    $('#inPlaceUpgradeCheckbox').prop('checked', false);
    $('#fastNodeDrainCheckbox').prop('checked', false);
    $('#skipNodeDrainCheckbox').prop('checked', false);
    $('#startupPrimaryCheckbox').prop('checked', false);
}

function checkOptions(stack_name) {
    resetZDUPanel();
    resetOtherOptionsPanel()
    send_http_request(`${baseUrl}/api/v1/stack/${stack_name}?region=${region}&expand=zdu_compatibility`, "GET", {onreadystatechange: displayOptions});
}

function displayOptions(responseText) {
    const stack_info = JSON.parse(responseText);
    if (stack_info['zdu_compatibility']['compatible'] === true) {
        if ('reason' in stack_info['zdu_compatibility']) {
            $("#zduCheckLbl").text(`${stack_info['zdu_compatibility']['reason']}`);
        } else {
            $("#zduCheckDiv").attr("style", "display:none");
            $("#performZDUDiv").removeAttr("style");
        }
    } else {
        $("#zduCheckLbl").text(`Stack is not ZDU/Rolling Upgrade compatible: ${stack_info['zdu_compatibility']['reason']}`);
    }
    // display other options allowed
    const product = stack_info['product'];
    let no_other_options = true;
    if (product.includes('bitbucket')) {
        $('#inPlaceUpgradeDiv').show();
        if (product.includes('mesh')) {
            // Mesh stacks do not have load balancers
            $('#skipNodeDrainDiv').hide();
            // Mesh upgrade involves shutting down the primary cluster
            // Default to restarting at the end, but checkbox to toggle this off is there to save time if chaining with
            // a primary cluster upgrade
            $('#startupPrimaryDiv').show();
            $('#startupPrimaryCheckbox').prop('checked', true);
        } else {
            $('#skipNodeDrainDiv').show();
        }
        // Default in-place upgrade to skip draining nodes from load balancer(s).
        $('#skipNodeDrainCheckbox').prop('checked', true);
        no_other_options = false;
    }
    if (product.includes('bitbucket') && !product.includes('mesh')) {
        $('#fastNodeDrainDiv').removeAttr("style");
        no_other_options = false;
    }
    if (no_other_options) {
        $("#otherOptionsCheckLbl").text("No other options available");
    } else {
        $("#otherOptionsCheckDiv").attr("style", "display:none");
    }
    // set the action button function and enable it
    $("#action-button").click(() => $("#upgradeForm").submit());
    enableActionButton();
}

function beginUpgrade() {
    if (document.getElementById("zduCheckBox").checked) {
        setModalOkFunction(modals.AUTH, performUpgrade);
        showModal(modals.AUTH);
    }
    else {
        performUpgrade();
    }
}

function performUpgrade() {
    const drain_type = $('#skipNodeDrainCheckbox').is(':checked') && !$('#skipNodeDrainCheckbox').is(':disabled')
        ? 'SKIP'
        : $('#fastNodeDrainCheckbox').is(':checked') && !$('#fastNodeDrainCheckbox').is(':disabled')
        ? 'FAST'
        : 'DEFAULT'
    const startupPrimary = $('#startupPrimaryCheckbox').is(':checked')
    const upgradeType = $('#inPlaceUpgradeCheckbox').is(':checked') ? 'IN_PLACE' : 'DEFAULT';
    const zdu = document.getElementById("zduCheckBox").checked;
    const zduType = $('input[name="zduType"]:checked').val();
    const version = $("#upgradeVersionSelector").val();
    const altDownloadUrl = $("#alternateDownloadUrl").val();
    const stackName = scrapePageForStackName();
    const requestBody = {"new_version": version}

    if (altDownloadUrl) {
        requestBody["alternate_download_url"] = altDownloadUrl;
    }

    if (zdu) {
        requestBody["zdu"] = true;
        requestBody["zdu_type"] = zduType;
        requestBody["auth"] = getAuthDetails();
    }

    requestBody['drain_type'] = drain_type
    if ($('#startupPrimaryDiv').is(':visible')){
        requestBody['startup_primary'] = startupPrimary
    }
    requestBody['upgrade_type'] = upgradeType

    send_http_request(`${baseUrl}/api/v1/stack/${stackName}/upgrade?region=${region}`, "POST", {
        data: JSON.stringify(requestBody),
        onreadystatechange: (responseText) => {
            redirectToLog(stackName, JSON.parse(responseText)['action_id']);
        }
    });
}
