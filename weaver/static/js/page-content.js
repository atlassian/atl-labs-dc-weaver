var baseUrl = window.location.protocol + "//" + window.location.host;
var region = $("meta[name=region]").attr("value");
var action = window.location.pathname.substr(1);

function selectStack(stack_name) {
  $("#stackSelector").text(stack_name);
  $("#stackName").text(stack_name);
  $("#pleaseSelectStackMsg").hide();
  $("#stackInformation").parent().show();
  $("#stackInformation").show();

  // clean up stack info
  removeElementsByClass("aui-lozenge");
  $("#product").html("Product: ");
  $("#serviceStatus").html("Service status: ");
  $("#stackState").html("Stack status: ");
  $("#currentAction").html("Action in progress: ");
  $("#currentVersion").html("Current version: ");
  $("#nodes").html("");
  $("#nodesCount").html("");

  updateStackInfo(stack_name);
}

function updateStackInfo(stack_name, stack_region) {
  if (stack_name === 'actionreadytostart') return;
  if (!stack_region) stack_region = region;

  // show spinners if no content
  if ($("#product").length && $("#product").html().length <= 10)
    $("#product").html(
        "Product: <aui-spinner size=\"small\" ></aui-spinner>");
  if ($("#serviceStatus").find('span.aui-lozenge').length == 0)
    $("#serviceStatus").html(
      "Service status: <aui-spinner size=\"small\" ></aui-spinner>");
  if ($("#stackState").find('span.aui-lozenge').length == 0)
    $("#stackState").html(
      "Stack status: <aui-spinner size=\"small\" ></aui-spinner>");
  if ($("#currentAction").find('span.aui-lozenge').length == 0)
    $("#currentAction").html(
        "Action in progress: <aui-spinner size=\"small\" ></aui-spinner>");
  if ($("#currentVersion").length && $("#currentVersion").html().length <= 17)
    $("#currentVersion").html(
      "Current version: <aui-spinner size=\"small\" ></aui-spinner>");
  if ($("#nodesCount").length && $("#nodesCount").html().length == 0)
    $("#nodesCount").html("");
  if ($("#nodes").length && $("#nodes").html().length <= 4)
    $("#nodes").html("<aui-spinner size=\"small\" ></aui-spinner>");

  const expand = [];
  const viewing_action_progress = window.location.pathname.includes('actionprogress');
  // only request node data if the action is not clone, but if the action _is_ clone, still request node data if we're viewing the clone action
  if (action !== "clone" || viewing_action_progress) {
    expand.push("nodes");
  }
  // if we're not viewing action progress, we need to request tagging data
  // (stored on window and referenced by methods in template-params.js)
  // someday we'll refactor the frontend to remove this mess, but that day is not today
  if (!viewing_action_progress) {
    expand.push("tags");
  }

  send_http_request(`${baseUrl}/api/v1/stack/${stack_name}?region=${stack_region}&expand=${expand.join(",")}`, "GET", {onreadystatechange: displayStackInfo});
}

function displayStackInfo(responseText) {
  var stackInfo = JSON.parse(responseText);
  window.stackInfo = stackInfo;
  if (stackInfo['product'].toLowerCase().includes('mesh')) {
    $("#serviceStatus").hide();
    switch (action) {
      case 'togglenode':
        $("#toggleNodeMainFlexContent").html("Toggle node is not supported: Mesh does not use load balancers");
        disableActionButton()
        break;
      case 'runsql':
        $("#runSqlTextArea").html("Running SQL is not supported: Mesh does not have a database");
        disableActionButton()
    }
  } else {
    $("#serviceStatus").show();
  }
  if (action === 'destroy' && (stackInfo['stack_status'] === 'does not exist' || (stackInfo.hasOwnProperty('error_msg') && stackInfo['error_msg'].includes("not found")))) {
      $("#stackState").html("Stack status: " + getStatusLozenge("DELETE_COMPLETE"));
      $("#stackPanel").find("aui-spinner").remove();
  } else {
    // remove any existing/visible product warning tooltip that might've existed and been triggered while the product was unknown (i.e., on stack create)
    AJS.$('#product > span.aui-icon').tooltip('destroy');
    $("#product").html("Product: " + stackInfo['product']);
    if (stackInfo['product'].toLowerCase().includes('unknown')) {
      $("#product").append(`<span class="aui-icon aui-icon-small aui-iconfont-warning"></span>`);
      AJS.$('#product > span.aui-icon').tooltip({
        title: 'This stack is not tagged with a <code>product</code> tag; some actions will be unavailable. Use the <a href="/update">Update</a> action to apply a <code>product</code> tag if this stack was created outside Weaver.',
        gravity: 's'
      });
    }
    $("#stackState").html("Stack status: " + getStatusLozenge(stackInfo['stack_status']));
    $("#serviceStatus").html("Service status: " + ('service_status' in stackInfo ? getStatusLozenge(stackInfo['service_status']) : 'unknown'));
    $("#currentVersion").html("Current version: " + stackInfo['version']);
    displayActionInProgress(stackInfo['action_in_progress']);
    displayNodes(stackInfo['nodes']);
    if (action === 'diagnostics' ) {
      if ($("#product").text().toLowerCase().includes('jira')) {
        $("#checkIndexHealthBtn").removeAttr("disabled");
        $("#checkIndexHealthBtn").attr("aria-disabled", false);
      }
      if (! $("#product").text().toLowerCase().includes('mesh')) {
        $('#supportZipBtn').removeAttr("title");
        $('#dlSupportZipBtn').removeAttr("title");
        $("#supportZipBtn").attr("aria-disabled", false);
        $("#supportZipBtn").removeAttr("disabled");
        $("#dlSupportZipBtn").attr("aria-disabled", false);
        $("#dlSupportZipBtn").removeAttr("disabled");
      } else {
        $('#supportZipBtn').prop('title', 'Not supported for Mesh');
        $('#dlSupportZipBtn').prop('title', 'Not supported for Mesh');
        $("#supportZipBtn").attr("aria-disabled", true);
        $("#supportZipBtn").attr("disabled", true);
        $("#dlSupportZipBtn").attr("aria-disabled", true);
        $("#dlSupportZipBtn").attr("disabled", true);
      }
    } else if (["clone", "update"].includes(action)) {
      selectDefaultTemplate();
    }
  }
}

function displayActionInProgress(actionInProgress) {
  $("#currentAction").html("Action in progress: " + getStatusLozenge(
    actionInProgress, "moved"));
  if (actionInProgress.toLowerCase() !== "none" && window.location.href.indexOf("/admin/") === -1) {
    $("#currentAction").append(
      `<a id="unlockStackButton" class="aui-button aui-button-compact" href="${baseUrl}/admin">
         <span class="aui-icon aui-icon-small aui-iconfont-unlock">Unlock this stack</span>
       </a>`
    );
  }
}

function displayNodes(nodes) {
  $("#nodes").html("");
  if (!nodes || !nodes[0]) {
    $("#nodes").html("None");
    $("#nodesCount").html(0);
    return;
  }
  $('#nodesCount').html(nodes.length);
  $('#nodesCount').trigger('nodeCountChanged');

  if ($("meta[name=platform]").attr("value").toLowerCase() === 'k8s')
    buildNodeInfoK8s(nodes);
  else
    buildNodeInfoCfn(nodes);
}

function buildNodeInfoK8s(nodes) {
  buildNodeInfo(nodes);
}

// for cfn group the nodes by their autoscaling group name
function buildNodeInfoCfn(nodes) {
  const groupedNodes = nodes.reduce((groups, node) => {
    let asg_name = node.autoscaling_group_name
    if (!groups[asg_name]) {
      groups[asg_name] = []
    }
    groups[asg_name].push(node)
    return groups
  }, {});

  for (const [asg_name, nodes] of Object.entries(groupedNodes)) {
    $("#nodes").append(`<div class="asgName">${asg_name}</div>`);
    buildNodeInfo(nodes, asg_name);
  }
}

function buildNodeInfo(nodes, asg_name='') {
  for (const node of nodes) {
    if (node.error_class) {
      $("#nodes").append('<div class="node clearfix">' +
        '<div class="nodeDetails">' +
          `<div class="error">${node.error_class}</div>` +
          `<div class="errorDetails">${node.error_details}</div>` +
        '</div>' +
        '<div class="instanceDetails">Unhandled exception retrieving node</div>' +
      '</div>');
      continue;
    }

    let registration_status_alb = "";
    if (node.registration_status_alb) {
      switch (node.registration_status_alb.toUpperCase()) {
        case "HEALTHY":
          registration_status_alb = "REGISTERED";
          break;
        case "INITIAL":
          registration_status_alb = "REGISTERING";
          break;
        case "NOTREGISTERED":
          registration_status_alb = "DEREGISTERED";
          break;
        default:
          registration_status_alb = node.registration_status_alb.toUpperCase()
      }
      ;
    }

    let registration_status_nlb = "";
    if (node.registration_status_nlb) {
      switch (node.registration_status_nlb.toUpperCase()) {
        case "HEALTHY":
          registration_status_nlb = "REGISTERED";
          break;
        case "INITIAL":
          registration_status_nlb = "REGISTERING";
          break;
        case "NOTREGISTERED":
          registration_status_nlb = "DEREGISTERED";
          break;
        default:
          registration_status_nlb = node.registration_status_nlb.toUpperCase()
      }
      ;
    }

    let node_service_version = "";
    let node_service_extended_version = "";
    if (node.service_version) {
      const node_service_version_parts = node.service_version.split("-");
      if (node_service_version_parts.length > 1) {
        node_service_version = node_service_version_parts[0]
        node_service_extended_version = node.service_version
      } else {
        node_service_version = node.service_version
      }
    }

    const alb_status_div = (registration_status_alb) ? `<div class="albStatusWrapper"><span class="aui-lozenge">ALB</span>${getStatusLozenge(registration_status_alb)}</div>` : ''
    const nlb_status_div = (registration_status_nlb) ? `<div class="nlbStatusWrapper"><span class="aui-lozenge">NLB</span>${getStatusLozenge(registration_status_nlb)}</div>` : ''

    $("#nodes").append(`<div id="node-${node.id}" class="node clearfix">` +
      '<div class="nodeDetails">' +
        `<div class="asgName hidden">${asg_name}</div>` +
        `<div class="nodeIp">${node.ip}</div>` +
        `<div class="instanceId">${node.id}</div>` +
      '</div>' +
      '<div class="statusLozenges">' +
        `<div class="nodeStateWrapper"><span class="aui-lozenge">EC2</span>${getStatusLozenge(node.state)}</div>` +
        `<div class="serviceStatusWrapper"><span class="aui-lozenge">${node_service_version || node.service || 'svc'}</span>${getStatusLozenge(node.status)}</div>` +
        `${alb_status_div}` +
        `${nlb_status_div}` +
      '</div>' +
      '<div class="instanceDetails">' +
        // `<span class="aui-icon aui-icon-small aui-iconfont-page-filled"></span>${node.type} (${node.arch})` +
        `<span class="aui-icon aui-icon-small aui-iconfont-page-filled"></span>${node.type}` +
        `<span class="aui-icon aui-icon-small aui-iconfont-world"></span>${node.az}` +
        `<span class="aui-icon aui-icon-small aui-iconfont-recent-filled"></span>${node.uptime}` +
      '</div>' +
    '</div>');
    if (!node.service) {
      $(`#node-${node.id} .nodeIp`).append(`<span class="aui-icon aui-icon-small aui-iconfont-warning"></span>`);
      AJS.$(`#node-${node.id} .nodeIp > span.aui-icon`).tooltip({
        title: 'This instance is not tagged with a <code>product</code> tag. Some information may be incomplete / inaccurate.',
        gravity: 's'
      });
    }
    if (node_service_extended_version) {
      $(`#node-${node.id} .statusLozenges .serviceStatusWrapper .aui-lozenge:first`).prepend(`<span class="aui-icon aui-icon-small aui-iconfont-info-circle"></span>`);
      AJS.$(`#node-${node.id} .statusLozenges .serviceStatusWrapper .aui-lozenge:first`).tooltip({
        title: node_service_extended_version,
        gravity: 's'
      });
    }
    if (node_service_version && (window.stackInfo.version != node.service_version)) {
      if (!(window.stackInfo.product === 'jira' && isFourMajorVersionsAheadPreTen(window.stackInfo.version, node.service_version))) {
        $(`#node-${node.id} .statusLozenges .serviceStatusWrapper .aui-lozenge:first`).addClass("aui-lozenge-moved")
      }
    }
  }
}

function getStatusLozenge(text, cssClass) {
  if (cssClass) {
    if (text.toLowerCase() === 'none') {
      cssClass = "success";
    }
  } else {
    var cssClass = "";
    text = text.trim();
    text = text.replace(/"/g, "");
    if (text.toUpperCase().includes("READY REPLICAS")) {
      if (text.toUpperCase().includes("None")) {
        cssClass = "moved"
      } else {
        cssClass = "success"
      }
    } else {
      switch (text.toUpperCase()) {
        case "CREATE_COMPLETE":
        case "FIRST_RUN":
        case "LISTENING":
        case "NONE":
        case "OK":
        case "READY":
        case "REGISTERED":
        case "RUNNING":
        case "SERVING":
        case "SYNCHRONIZED":
        case "UPDATE_COMPLETE":
        case "VALID":
          cssClass = "success";
          break;
        case "BOOTSTRAPPED":
        case "BOOTSTRAPPING":
        case "CREATE_IN_PROGRESS":
        case "DELETE_IN_PROGRESS":
        case "DRAINING":
        case "METADATA_SYNCHRONIZED":
        case "NOT READY":
        case "PENDING":
        case "REGISTERING":
        case "STARTING":
        case "STOPPING":
        case "SYNCHRONIZING":
        case "TIMED OUT":
        case "UNIMPLEMENTED":
        case "UPDATE_IN_PROGRESS":
        case "UPDATE_COMPLETE_CLEANUP_IN_PROGRESS":
          cssClass = "moved";
          break;
        case "ERROR":
        case "INVALID":
        case "SHUTTING-DOWN":
        case "STOPPED":
        case "TERMINATED":
        default:
          cssClass = "removed";
      }
    }
  }
  let optional_title = ''
  if (text === 'UNIMPLEMENTED') {
    optional_title = ' title="Mesh versions prior to 1.4 do not implement a gRPC status check. ' +
        'UNIMPLEMENTED (vs UNAVAILABLE) indicates that the service is running, but we cannot ascertain its status."'
  }
  return `<span class="aui-lozenge aui-lozenge-${cssClass}"${optional_title}>${text}</span>`;
}

function isFourMajorVersionsAheadPreTen(stack_version, node_version) {
  stack_version_parts = stack_version.split(".")
  node_version_parts = node_version.split(".")
  // Convert the major version into an int so we can do maths
  // Don't bother with the other parts, testing equality of strings works
  // (last part might not be covertable to an int if its a non-standard version anyway)
  stack_major_version = parseInt(stack_version_parts[0])
  node_major_version = parseInt(node_version_parts[0])
  return (node_major_version < 10) && ((stack_major_version + 4) === node_major_version) && (stack_version_parts[1] === node_version_parts[1]) && (stack_version_parts[2] === node_version_parts[2])
}

function onReady() {
  // empty function for errors, overridden by each action
}

document.addEventListener('DOMContentLoaded', function() {
  $("#stackInformation").hide();
}, false);

AJS.toInit(function () {
  onReady();
}, false);

// handles changing the aui-* classes on the paramsForm on window resize events
(function($) {
  const $window = $(window);
  const $paramsForm = $("#paramsForm");
  const $stackInfo = $(".page-container-flex-secondary");
  const labelTogglePoint = $stackInfo.length ? 1488 : 1200;

  $window.resize(() => {
    if ($window.width() < labelTogglePoint) {
      $paramsForm.removeClass('long-label').addClass('top-label');
    } else {
      $paramsForm.removeClass('top-label').addClass('long-label');
    }
  }).trigger('resize');
})(jQuery);
