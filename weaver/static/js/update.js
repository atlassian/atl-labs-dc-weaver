function onReady() {
    readyTheTemplate();

    // Add event listener for stack dropdown
    var stacks = document.getElementsByClassName("selectStackOption");
    for (var i = 0; i < stacks.length; i++) {
        stacks[i].addEventListener("click", function (data) {
            templateHandler(data.target.text);
        });
    }

    // if the waiting/progress modal is dismissed for any reason, cancel the request and re-enable the action button
    AJS.dialog2("#modal-progress-dialog").on("hide", function() {
        if (typeof changeSetRequest !== "undefined") {
            changeSetRequest.abort();
        }
        enableActionButton();
    });

    // if the changeset modal is dismissed for any reason, re-enable the action button
    AJS.dialog2("#modal-changeset-dialog").on("hide", ()=> enableActionButton());
}

function createChangeSet(stackName, jsonRequestBody) {
    prependToModalContents(modals.PROGRESS, "Please wait while the change set is generated...");
    showModal(modals.PROGRESS);
    if (typeof changeSetRequest !== "undefined") {
        changeSetRequest.abort();
    }
    changeSetRequest = send_http_request(`${baseUrl}/api/v1/stack/${stackName}/changeset?region=${region}`, "PUT", {
        data: jsonRequestBody,
        onreadystatechange: (response) => {
            let changeSetName;
            try {
                changeSetName = JSON.parse(response)['change_set_name'];
            } catch(e) {
                replaceModalContents(modals.ERROR, {text: "We've encountered an error creating this change set: <p><pre><code>Unexpected response from server: " + response + "</code></pre></p>"});
                showModal(modals.ERROR);
                return;
            }
            if (changeSetName === undefined) {
                replaceModalContents(modals.ERROR, {text: "We've encountered an error creating this change set: <p><pre><code>Unexpected response from server: " + response + "</code></pre></p>"});
                showModal(modals.ERROR);
                return;
            }
            changeSetStatusChecker(stackName, changeSetName);
        }
    });
}

function changeSetStatusChecker(stackName, changeSetName) {
    send_http_request(`${baseUrl}/api/v1/stack/${stackName}/changeset?region=${region}`, "GET", {
        onreadystatechange: (response) => {
            const changeSets = JSON.parse(response)['change_sets'];
            const changeSet = changeSets.filter((cs) => cs.change_set_name === changeSetName)[0];
            if (changeSet.status === 'CREATE_COMPLETE') {
                presentChangeSetForExecution(stackName, changeSetName);
            } else if (changeSet.status === 'FAILED') {
                replaceModalContents(modals.ERROR, {text: `Change set failed to create: ${changeSet.status_reason}`});
                hideModal(modals.CHANGESET);
                showModal(modals.ERROR);
            } else {
                setTimeout(changeSetStatusChecker, 5000, stackName, changeSetName);
            }
        }
    });
}

function presentChangeSetForExecution(stackName, changeSetName) {
    send_http_request(`${baseUrl}/api/v1/stack/${stackName}/changeset?region=${region}&change_set_name=${changeSetName}`, "GET", {
        onreadystatechange: (response) => {
            const changeSetDetails = JSON.parse(response);
            populateChangesetModal(stackName, changeSetDetails);
            setModalOkFunction(modals.CHANGESET, () => {
                let drain_type = $('#fastNodeDrainCheckbox').is(':checked') ? 'FAST' : 'DEFAULT'
                send_http_request(`${baseUrl}/api/v1/stack/${stackName}?region=${region}`, "PATCH", {
                    data: JSON.stringify({"change_set_name": changeSetName, "drain_type": drain_type}),
                    onreadystatechange: (responseText) => {
                        redirectToLog(stackName, JSON.parse(responseText)['action_id']);
                    }
                });
            });
            showModal(modals.CHANGESET);
            disableActionButton();  // ensure the action button stays disabled; the hiding of the progress modal will re-enable it
        }
    });
}

function populateChangesetModal(stackName, changeSetDetails) {
    const product = window.stackInfo.product;
    // remove previous rows and hide warning
    $(".table-data").remove();
    $('#changesetDeletionWarning').hide();

    // fill in changeset and stack name placeholders in modal
    $("#changesetName").text(changeSetDetails["change_set_name"]);
    $("#changesetStackname").text(stackName);

    // populate the table with data from the response
    changeSetDetails.changes.forEach((change) => {
        let actionClass = '';
        let replacementClass = '';

        switch(change.action) {
            case 'Add':
                actionClass = 'aui-lozenge-success';
                break;
            case 'Modify':
                actionClass = 'aui-lozenge-inprogress aui-lozenge-subtle';
                break;
            case 'Remove':
                actionClass = 'aui-lozenge-removed';
                break;
        }

        switch(change.replacement) {
            case 'True':
                replacementClass = 'aui-lozenge-removed';
                break;
            case 'False':
                replacementClass = 'aui-lozenge-subtle';
                break;
            case 'Conditional':
                replacementClass = 'aui-lozenge-moved';
                break;
        }

        $('#changesetDescription table').append('<tr class="table-data">' +
            '<td>' + change.logical_resource_id + '</td>' +
            '<td><code>' + change.resource_type + '</code></td>' +
            '<td><span class="aui-lozenge ' + actionClass + '">' + change.action + '</span></td>' +
            '<td>' + change.scope.join(", ") + '</td>' +
            '<td>' + (change.replacement ? ('<span class="aui-lozenge ' + replacementClass + '">' + change.replacement + '</span>') : '-') + '</td>' +
        '</tr>');
    });

    // show a warning banner if we have any resources being replaced or removed
    if (changeSetDetails.changes.some((change) => change.replacement === 'True' || change.action === 'Remove')) {
        $('#changesetDeletionWarning').show();
    }

    // show checkbox to drain nodes fast if product is bitbucket mirror / main cluster and changeset includes a modified Auto Scaling Group
    if (product.includes('bitbucket') && !product.includes('mesh') && changeSetDetails.changes.some((change) => change.resource_type === 'AWS::AutoScaling::AutoScalingGroup' && change.action === 'Modify')) {
        $('#fastNodeDrainDiv').removeAttr('style')
        // and add a tooltip
        AJS.$('#fastNodeDrainLabel').tooltip({
            title: 'Check this box if scaling down the number of stack nodes, and the stack has long node drain times set.',
            gravity: 's'
        });
    }
}
