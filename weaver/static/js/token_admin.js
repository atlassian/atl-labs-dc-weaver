function onReady() {
  $("#stackInformation").parent().hide();
  $("#stackSelector").hide();
  $("#action-button").on("click", createToken);
  $("#activeTokensTable").on("click", "button.token-revoke", revokeToken);
  enableActionButton();
}

function createToken() {
  send_http_request("/api/v1/token/refresh", "POST", {onreadystatechange: (responseText) => {
    var token = JSON.parse(responseText);
    $('#tokenExpiry').text(token.expires_at);
    $('#tokenContent').text(token.refresh_token);
    showModal(modals.CONFIRM);
    $("#activeTokensTable tbody").prepend(`
      <tr>
        <td>${token.jti}</td>
        <td><time datetime="${token.created_at}">${token.created_at}</time></td>
        <td><time datetime="${token.expires_at}">${token.expires_at}</time></td>
        <td><button class="aui-button aui-button-link token-revoke">Revoke</button></td>
      </tr>
    `);
  }});
}

function revokeToken(event) {
  const currentRow = $(event.currentTarget).parents('tr');
  const jti = $("td:first", currentRow).text();
  send_http_request("/api/v1/token/refresh", "DELETE", {
    data: JSON.stringify({"jti": jti}),
    functionParams: currentRow,
    onreadystatechange: (responseText, currentRow) => {
      var response = JSON.parse(responseText);
      if (response.message === "Token revoked") {
        currentRow.fadeOut('fast', () => currentRow.remove());
      }
    }
  });
}
