function onReady() {
    readyTheTemplate();

    // Add event listener for stack dropdown
    var stacks = document.getElementsByClassName("selectStackOption");
    for (var i = 0; i < stacks.length; i++) {
        stacks[i].addEventListener("click", function (data) {
            templateHandler(data.target.text);
        });
    }

    $("#RegionVal").auiSelect2({
        minimumResultsForSearch: 5
    }).change((event) => {
        const clone_region = event.currentTarget.value;
        const stackname = document.getElementById("stackSelector").innerText;
        getEbsSnapshots(clone_region, stackname);
        getRdsSnapshots(clone_region, stackname);
        getVPCs(clone_region);
        getKmsKeys(clone_region);
        getSslCerts(clone_region);
    });

    $('#getCloneDefaults-button').click(function(event) {
        if (!event.currentTarget.hasAttribute('disabled')) {
            getCloneDefaults();
        }
    });
}

function getCloneDefaults(){
    var stack_name = $("#StackNameVal").val();
    if (!stack_name) {
        displayAUIFlag('Please enter a stack name', 'info');
        return;
    }
    send_http_request(`${baseUrl}/api/v1/stack/${stack_name}?region=${region}&expand=clone_defaults`, "GET", {onreadystatechange: applyCloneDefaults});
}

function applyCloneDefaults(responseText) {
    const stack_info = JSON.parse(responseText);
    const stack_name = $("#StackNameVal").val();

    if (stack_info['clone_defaults'].length === 0) {
        displayAUIFlag(`No defaults exist for ${stack_name}`, "error");
        return;
    }

    for (const [param, value] of Object.entries(stack_info['clone_defaults'])) {
        let element = $(`#${param}Val`);
        if (element.val() !== value) {
            makeSelections(element, value);
            element.trigger("change");
        }
    }

    displayAUIFlag(`Defaults for ${stack_name} have been applied`, "success");
}

// declaring this here allows us to close it programmatically once assigned to prevent multiple alerts from stacking
let dbSnapshotFlag;

function checkRdsSnapshotVersionCompatibility(cloneRegion) {
    let selectedRdsSnapshotEngine;
    let selectedRdsSnapshotEngineVersion;
    const dbEngineVersion = $('#DBEngineVersionVal').val();

    // close any existing flag
    if (dbSnapshotFlag !== undefined) dbSnapshotFlag.close();

    // try to parse the DB version from the snapshot label of the selected snapshot; warn if we can't parse it that we won't be able to validate snapshot/DB version compatibility
    try {
        const selectedRdsSnapshotEngineInfo = $('#DBSnapshotNameVal').select2('data').text.match(/(\w+)\s([\d.]+)\)$/);
        selectedRdsSnapshotEngine = selectedRdsSnapshotEngineInfo[1];
        selectedRdsSnapshotEngineVersion = selectedRdsSnapshotEngineInfo[2];
    } catch {
        dbSnapshotFlag = AJS.flag({
            type: 'warning',
            title: 'Unknown database version for snapshot',
            body: '<p>We were unable to parse a version for the selected RDS snapshot; validation of compatibility with "DB Engine Major Version" is not possible for this snapshot. Perhaps snapshots are still loading?</p>',
            close: 'manual'
        });
        return;
    }

    // if the DB version and snapshot version are the same (i.e., user specifies a minor version for "DB Engine Major Version"), we don't need to make any requests to determine compatibilty
    if (dbEngineVersion === selectedRdsSnapshotEngineVersion) {
        dbSnapshotFlag = AJS.flag({
            type: 'success',
            title: 'Compatible snapshot version',
            body: '<p>"DB Engine Major Version" and snapshot DB engine version are compatible (exact match).</p>',
            close: 'auto'
        });
        return;
    }

    queryParams = generateQueryString({
        region: cloneRegion,
        dbEngine: selectedRdsSnapshotEngine,
        dbEngineVersion: dbEngineVersion,
        snapshotEngineVersion: selectedRdsSnapshotEngineVersion
    });

    send_http_get_request(`${baseUrl}/getDbVersionCompatibilityForSnapshot${queryParams}`, (response) => {
        if (response.length) {
            dbSnapshotFlag = AJS.flag(JSON.parse(response));
        }
    });
}
