// JS helpers
function countOccurences(stringToSearch, searchTerm) {
    var count = 0;
    var position = stringToSearch.indexOf(searchTerm);
    while (position > -1) {
        ++count;
        position = stringToSearch.indexOf(searchTerm, ++position);
    }
    return count;
}

function removeElementsByClass(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
}

function notify(message) {
    if ("Notification" in window) {
        if (Notification.permission === "granted") {
            var notification = new Notification('Weaver', {body: message, icon: '/static/img/Atlassian-vertical-blue@2x-rgb.png'});
        }
        else if (Notification.permission !== "denied") {
            Notification.requestPermission().then(function (permission) {
                if (permission === "granted") {
                    var notification = new Notification('Weaver', {body: message, icon: '/static/img/Atlassian-vertical-blue@2x-rgb.png'});
                }
            });
        }
    }
}

// Find and return the value from a JSON object inside an array of JSON objects (case insensitive)
var getObjectFromArrayByValue = function (array, key, value) {
    var obj = array.filter(function (object) {
        return object[key].toLowerCase() === value.toLowerCase();
    });
    return obj[0].Value;
};

// create HTML elements from a string
function createElementsFromString(htmlString) {
    const template = document.createElement("template");
    template.innerHTML = htmlString.trim();
    return template.content.firstChild;
}

// build a querystring from an object of params
function generateQueryString(params) {
    return "?" + Object.keys(params).map((key) => {
        return `${key}=${encodeURIComponent(params[key])}`
    }).join("&");
}

// Modal window types
const modals = {
    AUTH: 'auth',
    CHANGESET: 'changeset',
    CONFIRM: 'confirm',
    ERROR: 'error',
    PROGRESS: 'progress'
}

// API helpers

function send_http_request(url, http_method, options) {
    // get optional function parameters
    const onreadystatechange = options.onreadystatechange
    const optionalData = options.data
    const optionalFunctionParams = options.functionParams
    const optionalHeaders = options.headers ?? {}
    // build the http request
    var httpRequest = new XMLHttpRequest();
    httpRequest.open(http_method, url, true);
    httpRequest.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    for (const [key, value] of Object.entries(optionalHeaders)) {
        httpRequest.setRequestHeader(key, value)
    }
    httpRequest.addEventListener("load", processResponse);
    if (onreadystatechange) {
        httpRequest.onreadystatechange = function () {
            if (httpRequest.readyState === XMLHttpRequest.DONE && [200, 202, 404].includes(httpRequest.status) && httpRequest.responseText !== "false\n") {
                onreadystatechange(httpRequest.responseText, optionalFunctionParams);
            }
        };
    }
    // send the http request with optional data
    httpRequest.send(optionalData);
    return httpRequest;
}

// deprecated, use send_http_request() instead
function send_http_get_request(url, onreadystatechange, optionalFunctionParams) {
    var getRequest = new XMLHttpRequest();
    getRequest.open("GET", url, true);
    getRequest.setRequestHeader("Content-Type", "text/xml");
    getRequest.addEventListener("load", processResponse);
    if (onreadystatechange) {
        getRequest.onreadystatechange = function () {
            if (getRequest.readyState === XMLHttpRequest.DONE && getRequest.status === 200) {
                if (optionalFunctionParams)
                    onreadystatechange(getRequest.responseText, optionalFunctionParams);
                else
                    onreadystatechange(getRequest.responseText);
            }
        };
    }
    getRequest.send();
}

// deprecated, use send_http_request() instead
function send_http_post_request(url, data, onreadystatechange) {
    var postRequest = new XMLHttpRequest();
    postRequest.open("POST", url, true);
    postRequest.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    postRequest.addEventListener("load", processResponse);
    if (onreadystatechange) {
        postRequest.onreadystatechange = function () {
            if (postRequest.readyState === XMLHttpRequest.DONE && postRequest.status === 200 && postRequest.responseText !== "false\n")
                onreadystatechange(postRequest.responseText);
        };
    }
    postRequest.send(data);
    return postRequest;
}

function createSelect(parameterKey, defaultValue, dropdownOptions, allowMultiple=false) {
    const selectElem = $("<select/>", {
        id: parameterKey + "Val",
        name: parameterKey + "DropdownDiv",
        multiple: allowMultiple
    });
    addSelectOptions(selectElem, defaultValue, dropdownOptions);
    return selectElem;
}

function addSelectOptions(elem, defaultValue, dropdownOptions) {
    const hasLabels = dropdownOptions && (typeof dropdownOptions[0] === "object" ? true : false);
    elem.append("<option/>");
    $.each(dropdownOptions, function(index, value) {
        elem.append($("<option/>", {
            text: hasLabels ? value["label"] : value,
            value: hasLabels ? value["value"] : value,
            selected: hasLabels ? (
                        isDefaultValue(defaultValue, value["label"]) ||
                        isDefaultValue(defaultValue, value["value"])
                      ) : isDefaultValue(defaultValue, value)
        }));
    });
    elem.trigger('change');
}

function isDefaultValue(defaultValue, currentOption) {
    if (defaultValue === undefined || defaultValue === null || defaultValue.length === 0) {
        return false;
    } else if (typeof defaultValue === "object") {
        return defaultValue.includes(currentOption);
    } else {
        return defaultValue.toString() === currentOption.toString();
    }
}

function updateSelect(parameterKey, defaultValue, dropdownOptions) {
    let selectElem = $(`#${parameterKey}Val`);
    selectElem.select2("val", null);
    selectElem.empty();
    addSelectOptions(selectElem, defaultValue, dropdownOptions);
}

function makeSelections(element, value) {
    if (element.length === 0) return;
    if ("select2" in element.data()) {
        let s2data = element.data()["select2"];
        if (element.is("input") && "tags" in s2data["opts"]) {
            seps = new RegExp(`${s2data["opts"]["tokenSeparators"].join("|")}`, "g");
            element.select2("val", value.split(seps));
        } else if (element.is("select")) {
            if (element.attr("multiple") !== undefined) {
                element.select2("val", value.split(s2data["opts"]["separator"]));
            } else {
                element.select2("val", value);
            }
        }
    } else if (element.is("input") || element.is("select")) {
        element.val(value);
    } else if (element.is("textarea")) {
        const separator = getTextAreaSeparator(element.attr("id").split(/Val$/)[0]);
        element.val(value.replaceAll(separator, "\n"));
    } else if (element.is("a")) {
        element.text(value);
    } else if (element.is("aui-select")) {
        element[0].value = value;
    }
}

function createTextArea(param) {
    // set the separator for this field
    const separator = getTextAreaSeparator(param.ParameterKey);

    // get a count of the number of params in the field; used to estimate the starting height of the textarea
    const numParams = (param.ParameterValue.match(separator) || []).length + 2;

    // build the element
    const element = $("<textarea/>", {
        class: "textarea",
        id: param.ParameterKey + "Val",
        name: param.ParameterKey + "DropdownDiv"})
    // set the default content of the textarea; replace configured separators with newlines for readability
    // newlines will be replaced with spaces when we read this field's input on form submission (sendParamsAsJson)
        .val(param.ParameterValue.replaceAll(separator, "\n"))
    // set a starting estimated height by using the number of params and the body element's line-height
        .css('height', `${numParams * $("body").css("line-height").split("px")[0]}px`)
    // when the textarea receives input or is changed (clone params, etc), change the height of the element to be
    // 10px taller than the height of the content (should remove need to see/use scrollbar)
        .on('change input', (event) => {
            event.currentTarget.style.height = "";
            event.currentTarget.style.height = (event.currentTarget.scrollHeight + 10) + "px";
        });
    return element;
}

function getTextAreaSeparator(parameterKey) {
    let separator = null;
    if (parameterKey === "CatalinaOpts") {
        // only spaces followed by a hyphen/minus
        separator = new RegExp(" (?=-)", "g");
    } else if (parameterKey === "DeploymentAutomationCustomParams") {
        // spaces not preceeded by "-e"
        separator = new RegExp("(?<!-e) ", "g");
    }
    return separator;
}

function createInputParameter(param, fieldset) {
    var div = document.createElement("DIV");
    div.className = "field-group param-field-group";
    div.id = param.ParameterKey + "Div";
    div.name = "parameter";

    var label = document.createElement("LABEL");
    label.className = "paramLbl";
    label.htmlFor = param.ParameterKey + "Val";
    label.innerHTML = param.ParameterLabel;
    div.appendChild(label);

    var actionRegion = action === "clone" ? $("#RegionVal").select2("val") : region;
    var actionStack = ["clone", "update"].includes(action) ? document.getElementById("stackSelector").innerText : "";
    var select2opts = {
        formatNoMatches: function () { return "Still loading, or no matches..."; },
        minimumResultsForSearch: 5
    }

    if (param.AllowedValues) {
        var input = createSelect(param.ParameterKey, param.ParameterValue, param['AllowedValues'], false);
    } else if (/^(?:Cluster|NFS|Synchrony)\w+InstanceType$/.test(param.ParameterKey)) {
        var input = createSelect(param.ParameterKey, null, null, false);
        ec2InstanceTypeParams.push(param);
        select2opts["placeholder"] = "Loading available instance types...";
        if (param.ParameterKey !== "NFSNodeInstanceType") {
            input.change(() => displaySuggestedHeapSize(param.ParameterKey));
        }
    } else if (/^DB\w*InstanceClass$/.test(param.ParameterKey)) {
        var input = createSelect(param.ParameterKey, null, null, false);
        rdsInstanceTypeParams.push(param);
        select2opts["placeholder"] = "Set a DB engine version to load available instance types...";
        select2opts["formatNoMatches"] = () => "Still loading, or no instance classes available for the supplied DB Engine Major Version...";
    } else if (param.ParameterKey.endsWith("Subnets")) {
        var input = createSelect(param.ParameterKey, null, null, true);
        select2opts["closeOnSelect"] = false;
        select2opts["formatNoMatches"] = () => "Still loading, or no VPC is selected...";
    } else if (param.ParameterKey === "VPC") {
        var input = createSelect(param.ParameterKey, null, null, false);
        getVPCs(actionRegion, param.ParameterValue);
        input.change((event) => getSubnets(event.currentTarget.value, action === "clone" ? $("#RegionVal").select2("val") : actionRegion));
    } else if (param.ParameterKey === "KmsKeyArn") {
        var input = createSelect(param.ParameterKey, null, null, false);
        getKmsKeys(actionRegion, param.ParameterValue);
        select2opts["placeholder"] = "Select a KMS key..."
        select2opts["allowClear"] = true;
    } else if (param.ParameterKey === "SSLCertificateARN") {
        var input = createSelect(param.ParameterKey, null, null, false);
        getSslCerts(actionRegion, param.ParameterValue);
        input.change((event) => {
            const selectedSslCert = $(event.currentTarget).select2("val");
            const selectedTomcatScheme = $("#TomcatSchemeVal").select2("val");
            const existingTomcatScheme = $("#TomcatSchemeVal").data("param")["ParameterValue"];
            if (action === "update" && selectedSslCert === "" && selectedTomcatScheme === existingTomcatScheme) {
                $("#TomcatSchemeVal").select2("val", existingTomcatScheme);
            } else {
                $("#TomcatSchemeVal").select2("val", selectedSslCert === "" ? "http" : "https");
            }
        });
        select2opts["placeholder"] = "Select an SSL certificate...";
        select2opts["allowClear"] = true;
    } else if (action === "clone" && param.ParameterKey === "DBSnapshotName") {
        // render this as a select widget and retrieve the list of snapshots if the user is creating a clone
        // otherwise, we let this case fall through to the final `else` below and create a normal text input
        // with the existing value (e.g., stack updates on existing clones)
        var input = createSelect(param.ParameterKey, null, null, false);
        getRdsSnapshots(actionRegion, actionStack);
        input.change(() => setTimeout(checkRdsSnapshotVersionCompatibility, 0, actionRegion));
        select2opts["placeholder"] = "Select an RDS snapshot...";
    } else if (action === "clone" && param.ParameterKey === "EBSSnapshotId") {
        // render this as a select widget and retrieve the list of snapshots if the user is creating a clone
        // otherwise, we let this case fall through to the final `else` below and create a normal text input
        // with the existing value (e.g., stack updates on existing clones)
        var input = createSelect(param.ParameterKey, null, null, false);
        getEbsSnapshots(actionRegion, actionStack);
        select2opts["placeholder"] = "Select an EBS snapshot...";
    } else if (param.ParameterKey === "CatalinaOpts" || param.ParameterKey === "DeploymentAutomationCustomParams") {
        var input = createTextArea(param);
    } else {
        var input = document.createElement("INPUT");
        input.className = "text";
        input.id = param.ParameterKey + "Val";
        input.value = param.ParameterValue;
        var pwd_re = new RegExp("^.*(P|p)assword$")

        if (pwd_re.test(param.ParameterKey)) {
            if (action === 'clone' || action === 'create') {
                input.type = "password";
                input.value = "";
                // This improves user friendliness by blocking the clone action on the template screen rather than erroring on stack create on the action logs screen
                input.required = isPasswordRequired(param)
            } else if (action === 'update'){
                // don't display passwords in the update action
                return;
            }
        } else if (param.ParameterKey === "MeshRegistrationUser") {
            if (action === 'update') {
                // assume credential will be unchanged on stack update
                return;
            }
        } else if (param.ParameterKey === "DBEngineVersion") {
            input.addEventListener("change", () => {
                if (rdsInstanceTypeParams.length) {
                    getRdsInstanceTypes(rdsInstanceTypeParams);
                }
                if (action === 'clone') {
                    setTimeout(checkRdsSnapshotVersionCompatibility, 0, actionRegion);
                }
            });
        }
        if (param.ParameterKey === "LatestAmiId" && param.ParameterValue && param.DefaultValue && param.ParameterValue !== param.DefaultValue) {
            AJS.flag({
                type: "warning",
                title: "Non-default value for Latest AMI ID",
                body: `<p>The current value for <b>Latest AMI ID</b> differs from the template default value:</p>
                      <pre><code>${param.DefaultValue}</code></pre>
                      <p>The use of a non-default value for this field can cause service instability or outages; proceed at your own risk or consult with your system administrator(s).</p>
                      <ul class="aui-nav-actions-list">
                        <li><a onclick="document.getElementById('${input.id}').scrollIntoView({behavior: 'smooth', block: 'center'}); document.getElementById('${input.id}').classList.add('highlight'); setTimeout(() => document.getElementById('${input.id}').classList.remove('highlight'), 5000);">Show Field</a></li>
                        <li><a onclick="document.getElementById('${input.id}').value = '${param.DefaultValue}'; this.closest('.aui-flag').close();">Use Default Value</a></li>
                        <li><a onclick="this.closest('.aui-flag').close();">Dismiss</a></li>
                      </ul>`,
                close: "manual"
            });
        }
        if (param.AllowedPattern) {
            input.setAttribute("data-aui-validation-field", "");
            input.setAttribute("data-aui-validation-pattern-msg","Value must satisfy regular expression pattern: " + param.AllowedPattern);
            input.pattern = param.AllowedPattern;
        }
        if (param.MinValue) {
            input.setAttribute("data-aui-validation-field", "");
            input.type = "number";
            input.min = param.MinValue;
        }
        if (param.MaxValue) {
            input.setAttribute("data-aui-validation-field", "");
            input.type = "number";
            input.max = param.MaxValue;
        }
        if (param.MinLength) {
            input.setAttribute("data-aui-validation-field", "");
            input.minLength = param.MinLength;
        }
        if (param.MaxLength) {
            input.setAttribute("data-aui-validation-field", "");
            input.maxLength = param.MaxLength;
        }
    }
    if (typeof input !== 'undefined') {
        $(div).append(input);
        if (input instanceof jQuery && (input.is("select") || input.is("input") && input.attr("type") === "hidden")) {
            input.data('param', param);
            input.auiSelect2(select2opts);
        }
    }
    if (param.ParameterKey.startsWith('JvmHeap')) {
        var description = document.createElement("DIV");
        description.id = `helper-text-${param.ParameterKey}`;
        description.className = "description";
        div.appendChild(description);
    }
    if (param.ParameterDescription) {
        var description = document.createElement("DIV");
        description.className = "description";
        description.innerText = param.ParameterDescription;
        div.appendChild(description);
    }
    fieldset.append(div);
}

function isPasswordRequired(param) {
    var allowed_pattern_re = null
    if (param.AllowedPattern) {
        allowed_pattern_re = new RegExp(param.AllowedPattern)
    }
    // Say the password is required if the parameter has a min length > 0 or the allowed pattern regex doesn't allow an empty string
    if (param.MinLength && parseInt(param.MinLength) > 0 || allowed_pattern_re && !allowed_pattern_re.test("")) {
        return true
    } else {
        return false
    }
}

function enableActionButton() {
    $("#action-button").attr("aria-disabled", false);
    $('#action-button').attr('disabled', false);
}

function disableActionButton() {
    $("#action-button").attr("aria-disabled", true);
    $('#action-button').attr('disabled', true);
}

function appendWarningToolTip(selector, message) {
    $(selector).append(`<span class="aui-icon aui-icon-small aui-iconfont-warning"></span>`);
    AJS.$(`${selector} > span.aui-icon`).tooltip({
        title: message,
        gravity: 's'
    });
}

// Modal dialog helpers
function replaceModalContents(modalType, {title, text}) {
    if (title) {
        var modalTitleElement = $("#modal-" + modalType + "-title");
        modalTitleElement.html(title);
    }

    if (text) {
        var modalContentsElement = $("#modal-" + modalType + "-contents");
        modalContentsElement.html("<p>" + text + "</p>");
    }
}

function appendToModalContents(modalType, text) {
    var modalContentsElement = $("#modal-" + modalType + "-contents");
    modalContentsElement.append("<p>" + text + "</p>");
}

function prependToModalContents(modalType, text) {
    var modalContentsElement = $("#modal-" + modalType + "-contents");
    if ($("#modal-" + modalType + "-contents:contains('" + text + "')").length === 0)
        modalContentsElement.prepend("<p>" + text + "</p>");
}

function setModalOkFunction(modalType, ok_function) {
    $("#modal-" + modalType + "-ok-btn").off("click");
    $("#modal-" + modalType + "-ok-btn").on("click", ok_function);
}

function showModal(modalType) {
    // hide all other modals
    for (const type in modals) {
        if (modals[type] !== modalType) {
            hideModal(modals[type])
        }
    }
    // add keypress listener; Enter bound to the Ok button and Escape to close the modal
    $("#modal-" + modalType + "-dialog").on("keydown", function(e) {
        switch (e.key) {
            case "Enter":
                $("#modal-" + modalType + "-ok-btn").click();
                break;
            case "Escape":
                hideModal(modalType);
                break;
        }
    });
    // show requested modal
    AJS.dialog2("#modal-" + modalType + "-dialog").show();
}

function hideModal(modalType) {
    // remove keydown listener on the modal
    $("#modal-" + modalType + "-dialog").unbind("keydown");
    // hide the modal
    AJS.dialog2("#modal-" + modalType + "-dialog").hide();
}

// Weaver common functions
function checkAuthenticated() {
    if ($("meta[name=credentials]").attr("value").toLowerCase() === 'false') {
        displayAUIFlag('No credentials - please authenticate with Cloudtoken', 'error', 'manual');
    }
}

function scrapePageForStackName() {
    var stack_name = $("#stackName").text();
    if (!stack_name) {
        stack_name = $("#StackNameVal").val();
    }
    return stack_name;
}

function getAuthDetails() {
    var authDetails = {};
    authDetails["personal_access_token"] = $('#personal_access_token').val();
    return authDetails;
}

function generateRequestBody() {
    // generate a dict containing stack_name, region, and a random UUID based on the current unix time, with numbers and letters
    return {
        "stack_name": scrapePageForStackName(),
        "region": region,
        "id": (parseInt(Math.random() * 100) + Date.now()).toString(16),
    }
}

function redirectToLog(stack_name, action_id, extra_params) {
    // Wait a mo for action to begin in backend
    setTimeout(function () {
        // Redirect to action progress screen
        let url = [baseUrl, '/actionprogress/', action, "?stack=", stack_name, "&id=", action_id].join('')
        if (extra_params)
            url += extra_params;
        window.location = url;
    }, 1000);
}

function processResponse() {
    if (![200, 202].includes(this.status)) {
        // Flask's flash messages only display on a page reload
        // the only time we flash without a page reload is on 403 Forbidden, so reload needs to be triggered manually
        if (this.status === 403) {
            location.reload();
            return;
        }
        // ignore 404s from get stack info on create/clone/destroy
        // 404s will be returned when the stack doesn't exist yet, or has been deleted (in case of clones/destroys)
        const stack_name = scrapePageForStackName();
        if ((["create", "clone", "destroy"].includes(action)) && this.responseURL.includes(`/api/v1/stack/${stack_name}?region=`) && this.status === 404) {
            return;
        }
        // else display a popup modal containing the error details
        const title = [this.status, this.statusText].join(': ');
        let text = ["Error returned by", this.responseURL, "<br><br>"].join(' ');
        try {
            const json_response = JSON.parse(this.responseText);
            if (json_response.hasOwnProperty("error_msg") && typeof json_response["error_msg"] === "object") {
                // currently, webargs validation errors from API v1 routes will return an object representation of the validation issues
                // if error_msg is an object, odds are we have one of these responses; for now just parse error_msg back to a formatted string with indentation
                text += `<code style="display: block; white-space: pre-wrap;">${JSON.stringify(json_response["error_msg"], null, 2)}</code>`
            } else {
                text += json_response['error_msg'] || json_response['message'] || `<code style="display: block; white-space: pre-wrap;">${JSON.stringify(json_response, null, 2)}</code>`;
            }
        } catch {
            text += this.responseText;
        }
        text += "<br><br>You may need to <a><span onclick='location.reload();'>reload</span></a> the page.";
        replaceModalContents(modals.ERROR, {title, text});
        showModal(modals.ERROR);
        enableActionButton();
    }
}

function displayAUIFlag(message, category, closes = 'auto') {
    // useful when we don't want a page reload which is required for flask's 'flash'
    AJS.flag({
         type: category,
         body: message,
         close: closes
    });
}

// https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
function Base64Encode(str, encoding = 'utf-8') {
    var bytes = new (typeof TextEncoder === "undefined" ? TextEncoderLite : TextEncoder)(encoding).encode(str);
    return base64js.fromByteArray(bytes);
}

function Base64Decode(str, encoding = 'utf-8') {
    var bytes = base64js.toByteArray(str);
    return new (typeof TextDecoder === "undefined" ? TextDecoderLite : TextDecoder)(encoding).decode(bytes);
}

// Common functions for node selection and CPU display
function addNodeSelectorWithCpuChart() {
    var stacks = document.getElementsByClassName("selectStackOption");
    for (var i = 0; i < stacks.length; i++) {
        stacks[i].addEventListener("click", function (data) {
            var stack_name = data.target.text;
            selectStack(stack_name);
            emptyAsgAndNodeListsAndCpuChart();
            listNodes();
        }, false);
    }
}

function emptyAsgAndNodeListsAndCpuChart() {
    $("#nodesList").empty();
    $("#asgList").empty();
    const nodeSelector = $("#nodeSelector");
    nodeSelector.length && nodeSelector.text(action === 'diagnostics' ? "All Nodes" : "Select Node");
    const asgSelector = $("#asgSelector");
    asgSelector.length && asgSelector.text('Select ASG');
    $("#cpuChartDiv").empty();
    $("#cpuChartDiv").append("<canvas id='cpuChart' hidden>");
}

function listAsgs() {
    // Add a spinner
    addAuiSpinner('#asgList', 'small');
    // If nodes not yet in stack info, sleep 1s
    if ($('#nodes .node').length === 0 && $('#nodes').text() !== 'None') {
        setTimeout(function() {
            listAsgs()
        }, 1000);
        return;
    }
    // Get ASGs
    const asgs = new Set($('#nodes .asgName').map((index, element) => $(element).text()));
    $("#asgList").empty();
    // Add each ASG to dropdown
    if (asgs.size > 1) {
        $("#asgList").append('<li><a class="selectAsgOption">All Auto Scaling groups</a></li>');
    }
    for (var asg of asgs) {
        $("#asgList").append(`<li><a class="selectAsgOption">${asg}</a></li>`);
    };
    // Add onClick event listener to each ASG
    $("#asgList .selectAsgOption").click((event) => {
        const selectedAsg = event.currentTarget.innerText;
        $("#asgSelector").text(selectedAsg);
        enableActionButton();
    });
    // auto-select the first ASG (it'll either be 'All' or a single ASG)
    $("#asgList .selectAsgOption").first().click();
}

function listNodes() {
    // Add a spinner
    addAuiSpinner('#nodesList', 'small');
    // If nodes not yet in stack info, sleep 1s
    if ($('#nodes .node').length === 0 && $('#nodes').text() !== 'None') {
        setTimeout(function() {
            listNodes()
        }, 1000);
        return;
    }
    // Get nodes
    const nodes = $('#nodes .node .nodeDetails').map((index, element) => {
        return {
            ip_address: $(element).children('.nodeIp').text(),
            autoscaling_group: $(element).children('.asgName').text()
        }
    });
    $("#nodesList").empty();
    // Add 'All Nodes' if Diagnostics
    if (action === 'diagnostics') {
        $("#nodesList").append('<li><a class="selectNodeOption">All Nodes</a></li>');
    }
    // Add each node with a valid IP to dropdown
    // ipv4 regex from ip-regex node.js package (https://www.npmjs.com/package/ip-regex)
    const ip_regex = new RegExp('^(?:25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]\\d|\\d)(?:\\.(?:25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]\\d|\\d)){3}$')
    for (const node of nodes) {
        if (ip_regex.test(node.ip_address)) {
            $("#nodesList").append(`<li><a class="selectNodeOption">${node.ip_address}</a></li>`);
        }
    };
    // Add onClick event listener to each node
    $("#nodesList .selectNodeOption").click((event) => {
        const selectedNode = event.currentTarget.innerText;
        $("#nodeSelector").text(selectedNode);
        $("#takeThreadDumps").removeAttr("disabled");
        $("#takeHeapDumps").removeAttr("disabled");
        $("#drainNodes").removeAttr("disabled");
        $("#fastNodeDrain").removeAttr("disabled");
        $("#cpuChartDiv").empty();
        // TODO: Add support for displaying all nodes' CPU on the frontend (API already supports this)
        // Or maybe just the top few + average
        if (selectedNode !== 'All Nodes') {
            $("#cpuChartDiv").append("<canvas id='cpuChart' class='cpuChartEmpty' hidden>");
            drawChart([], [], '');
            getNodeCPU(selectedNode);
        }
        enableActionButton();
    });
}

function getNodeCPU(node) {
    var stack_name = scrapePageForStackName();
    send_http_request(`${baseUrl}/api/v1/stack/${stack_name}/diagnostics?region=${region}&diagnostic_types=cpu&node_ip=${node}`, "GET", {
        onreadystatechange: (responseText) => displayNodeCPU(JSON.parse(responseText)["cpu"][0]["cpu"])
    })
}

function drawChart(timestamps_sorted, cpu_sorted, border_color) {
    var ctx = document.getElementById('cpuChart').getContext('2d');
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: timestamps_sorted,
            datasets: [{
                label: 'CPU Utilization over the last 30 minutes',
                backgroundColor: 'rgba(255, 255, 255, 0)',
                borderColor: border_color,
                data: cpu_sorted
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 100
                    }
                }]
            }
        }
    });
}

function displayNodeCPU(cpu_data_dict) {
    var timestamps = Object.keys(cpu_data_dict);
    timestamps.sort();
    var timestamps_sorted = [];
    var cpu_sorted = [];
    for (var i = 0; i < timestamps.length; i++) {
        var date = new Date(parseInt(timestamps[i]) * 1000);
        var month = date.getMonth() + 1;
        var day_num = date.getDate().toString().length === 1 ? "0" + date.getDate() : date.getDate();
        var hours = date.getHours().toString().length === 1 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes().toString().length === 1 ? "0" + date.getMinutes() : date.getMinutes();
        timestamps_sorted.push(month + "/" + (day_num) + " " + hours + ":" + minutes);
        cpu_sorted.push(cpu_data_dict[timestamps[i]]);
    }

    var border_color = '#00875A';
    for (var i = cpu_sorted.length - 6; i < cpu_sorted.length; i++) {
        if (cpu_sorted[i] > 80) {
            border_color = '#DE3505';
            break;
        }
    }
    drawChart(timestamps_sorted, cpu_sorted, border_color);
    $("#cpuChart").removeClass("cpuChartEmpty");
    $("#cpuChart").removeAttr("hidden");
}

// replaces the contents of `element` with an AUI spinner of `size`; only adds the spinner if it doesn't already exist
function addAuiSpinner(selector, size) {
    elem = $(selector);
    if (elem.find('.aui-spinner').length === 0) {
        elem.html(`<aui-spinner size="${size}"/>`);
    }
}

document.addEventListener('DOMContentLoaded', function() {
  checkAuthenticated();
}, false);
