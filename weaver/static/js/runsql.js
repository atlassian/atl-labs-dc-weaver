function onReady() {
    $("#action-button").on("click", performRunSql);
    $("#stackInformation").hide();
    var stacks = document.getElementsByClassName("selectStackOption");

    for (var i = 0; i < stacks.length; i++) {
        stacks[i].addEventListener("click", function (data) {
            $("#log").css("background", "rgba(0,20,70,.08)");
            selectStack(data.target.text);
            enableActionButton();
            send_http_get_request(baseUrl + "/getsql/" + region + "/" + data.target.text, displaySQL);
        });
    }
}

function displaySQL(responseText) {
    $("#log").css("background", "rgba(0,0,0,0)");

    $("#log").contents().find('body').html(responseText
        .split('\n').join('<br />'));
}

function performRunSql() {
    var stack_name = scrapePageForStackName();
    var url = [baseUrl, 'dorunsql', region, stack_name].join('/');
    const requestBody = generateRequestBody();
    send_http_post_request(url, JSON.stringify(requestBody));
    redirectToLog(stack_name, requestBody["id"]);
}
