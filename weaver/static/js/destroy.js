function onReady() {
    $('.selectStackOption').click((event) => {
        selectStack($(event.currentTarget).text());
        enableActionButton();
    });
    $("#action-button").on("click", confirmDeletion);
}

function confirmDeletion() {
    var stackName = scrapePageForStackName();
    $("#stack-to-delete").text(stackName);
    send_http_request(`${baseUrl}/api/v1/stack/${stackName}?region=${region}&expand=termination_protection`, "GET", {onreadystatechange: displayDestroyModal});
}

function displayDestroyModal(responseText) {
    const stack_info = JSON.parse(responseText);
    const stackName = scrapePageForStackName();
    if (stack_info['termination_protection']) {
        const title = 'Cannot delete ' + stackName;
        const text = 'Either the Cloudformation stack or at least one EC2 node has termination protection enabled. ' +
            'Please disable termination protection prior to deletion.';
        replaceModalContents(modals.ERROR, {title, text});
        showModal(modals.ERROR);
    } else {
        // Add action to OK button
        setModalOkFunction(modals.CONFIRM, function() {
            const requestBody = {};
            requestBody['delete_changelogs'] = $("#deleteChangelogs").is(":checked");
            requestBody['delete_threaddumps'] = $("#deleteThreadDumps").is(":checked");
            send_http_request(`${baseUrl}/api/v1/stack/${stackName}?region=${region}`, "DELETE", {
                data: JSON.stringify(requestBody),
                onreadystatechange: (responseText) => {
                    redirectToLog(stackName, JSON.parse(responseText)['action_id']);
                }
            });
        });
        showModal(modals.CONFIRM);
    }
}
