var origParams;
var stack_name;
var externalSubnets;
var internalSubnets;
var changeSetRequest;

let ec2InstanceTypeParams = [];
let rdsInstanceTypeParams = [];
let ec2InstanceTypesResponseStr = '';
let numMaxTags;

const targetJvmHeapParamNameMap = {
  'ClusterNodeInstanceType': 'JvmHeap',
  'ClusterBotNodeInstanceType': 'JvmHeapBots',
  'SynchronyNodeInstanceType': 'JvmHeapSynchrony'
}

function readyTheTemplate() {
  var actionButton = document.getElementById("action-button");
  actionButton.addEventListener("click", function(data) {
    $("#paramsForm").submit();
  });

  AJS.$('#paramsForm').on('aui-valid-submit', function(event) {
    disableActionButton();
    sendParamsAsJson();
    event.preventDefault();
  });

  $('#getPreviousParams-button').click(function(event) {
    if (!event.currentTarget.hasAttribute('disabled')) {
      populatePreviousValues();
    }
  });

  document.querySelector("#tagsList .tag-field-button-add").addEventListener('click', tagButtonHandler);
}

function templateHandler(selected_stack_name) {
  stack_name = selected_stack_name;
  $("#aui-message-bar").hide();
  selectStack(stack_name);
  setTemplateDropdownStatus("busy");
  disableActionButton();
}

function setTemplateDropdownStatus(status) {
  const templateDropdownButton = document.getElementById("templateSelector");
  if (status === "idle") {
    templateDropdownButton.idle();
    templateDropdownButton.removeAttribute("disabled");
    templateDropdownButton.removeAttribute("aria-disabled");
  } else if (status === "busy") {
    templateDropdownButton.busy();
    templateDropdownButton.setAttribute("disabled", true);
    templateDropdownButton.setAttribute("aria-disabled", true);
  }
}

function getTemplates(template_type, callbackOpts) {
  setTemplateDropdownStatus("busy");
  send_http_request(`${baseUrl}/api/v1/util/cfn_templates?product=${template_type.toLowerCase()}`, "GET", callbackOpts ? callbackOpts : {onreadystatechange: displayTemplates});
}

function displayTemplates(responseText) {
  const templateDropdownButton = document.getElementById("templateSelector");
  const templateDropdown = document.getElementById("templates");

  // Remove existing templates
  templateDropdown.replaceChildren();

  // Add each template to dropdown; add onClick event listener to each template
  const templates = JSON.parse(responseText);
  for (const template of templates) {
    if (action === "clone" && !template.name.toLowerCase().includes("clone")) { continue; }
    const templateElem = createElementsFromString(`<li><a class="selectTemplateOption">${template.repo}: ${template.name}</a></li>`);
    templateElem.querySelector(":scope a.selectTemplateOption").addEventListener("click", function(event) {
      const selectedTemplate = event.target.text;
      templateDropdownButton.textContent = selectedTemplate;
      if (action === 'create') {
        getTemplateParams(selectedTemplate);
      } else {
        selectTemplateForStack(stack_name, selectedTemplate);
      }
    }, false);
    templateDropdown.appendChild(templateElem);
  }
  setTemplateDropdownStatus("idle");
}

function selectDefaultTemplate() {
  // Get templates for the product and call back to display them
  const product = window.stackInfo.tags.product ? window.stackInfo.tags.product : "";
  if (!product) {
    AJS.flag({
      type: "warning",
      title: "Unknown product",
      body: '<p>This stack is not tagged with a <code>product</code> tag; Weaver is unable to determine which templates should be listed for this stack. Use the <a href="/update">Update</a> action to apply a <code>product</code> tag if this stack was created outside Weaver.</p>',
      close: "manual"
    });
    // don't bother requesting templates for a stack we can't query for
    return;
  }
  getTemplates(product, {onreadystatechange: displayTemplatesAndSelectDefault})
}

function displayTemplatesAndSelectDefault(responseText) {
  displayTemplates(responseText);

  // Find template that the stack was created with
  const templateString = window.stackInfo.tags.repository + ": " + window.stackInfo.tags.template;

  // Search each template in the list, when matched perform its click() function
  $.each($('.selectTemplateOption'), function () {
    if (action === 'update') {
      if (templateString === this.textContent)
        this.click();
    } else if (action === 'clone') {
      if (templateString.replace('.template.yaml', 'Clone.template.yaml') === this.textContent)
        this.click();
    }
  });
}

function getTemplateParams(template) {
  $("#paramsList").html("");
  $("#tagsList").hide();
  $(".tags-container").html("");
  $(".page-container-flex").prepend("<aui-spinner size=\"large\"></aui-spinner>");
  $("#stack-name-input").hide();

  var repo = template.split(": ")[0];
  var template_name = template.split(": ")[1];
  send_http_get_request(`${baseUrl}/getTemplateParams/${repo}/${template_name}`, displayTemplateParams);
}

function displayTemplateParams(responseText) {

  const response = JSON.parse(responseText);
  const paramsList = $("#paramsList");

  const templateGroups = response.groups;
  const templateLabels = response.labels;
  const templateParams = response.params;

  ec2InstanceTypeParams = [];
  rdsInstanceTypeParams = [];

  // store these on the global so they can be referenced by sendParamsAsJson
  origParams = JSON.parse(responseText).params;

  $(".page-container-flex > aui-spinner").remove();

  // Store subnets
  for (const param of templateParams) {
    const paramValue = param.ParameterValue
    switch (param.ParameterKey) {
      case "ExternalSubnets":
        externalSubnets = paramValue.includes(",") ? paramValue.split(",") : undefined;
      case "InternalSubnets":
        internalSubnets = paramValue.includes(",") ? paramValue.split(",") : undefined;
      case "Az1Subnets":
        az1subnets = paramValue;
      case "Az2Subnets":
        az2subnets = paramValue;
      case "Az3Subnets":
        az3subnets = paramValue;
    }
  }

  // For each group of params...
  for (const [groupName, groupParamKeys] of Object.entries(templateGroups)) {
    const sectionHeader = $(`<h4>${groupName}</h4>`);
    const fieldset = $("<fieldset/>");

    // Look for the param; if it exists, remove it from the list so we can catch any params that aren't grouped
    for (paramKey of groupParamKeys) {
      const paramIndex = templateParams.findIndex(param => param.ParameterKey === paramKey);
      if (paramIndex > -1) {
        const param = templateParams[paramIndex];
        param.ParameterLabel = param.ParameterKey in templateLabels ? templateLabels[param.ParameterKey] : param.ParameterKey;
        templateParams.splice(paramIndex, 1);
        createInputParameter(param, fieldset);
      }
    }

    paramsList.append(sectionHeader);
    paramsList.append(fieldset);
  }

  // handle any remaining parameters that weren't defined in a grouping
  if (templateParams.length > 0) {
    templateParams.sort(function(a, b) {
      return a.ParameterKey.localeCompare(b.ParameterKey)
    });
    const fieldset = $("<fieldset/>");
    for (const param of templateParams) {
      param.ParameterLabel = param.ParameterKey in templateLabels ? templateLabels[param.ParameterKey] : param.ParameterKey;
      createInputParameter(param, fieldset);
    }
    templateGroups && paramsList.append($(`<h4>Other</h4>`));
    paramsList.append(fieldset);
  }

  if (ec2InstanceTypeParams.length) {
    getEc2InstanceTypes(ec2InstanceTypeParams);
  }
  if (rdsInstanceTypeParams.length) {
    getRdsInstanceTypes(rdsInstanceTypeParams);
  }

  if (window.stackInfo?.tags && action != "clone") {
    for (const [key, value] of Object.entries(window.stackInfo.tags)) {
      addTagRow(key, value);
    };
  }

  numMaxTags = 50;

  if (["create", "clone"].includes(action)) {
    // for now(?), we don't list the default tags on the create/clone screen, so
    // decrement the number of available tags by the number we add by default
    // product + environment + created_by + repository + template + service_name = 6
    numMaxTags = numMaxTags - 6;
    if(action == "clone") {
      // cloned_from
      numMaxTags--;
    }
  }

  updateTagInterface();

  if (document.getElementById("clone-params"))
    $("#clone-params").show();
  $("#stack-name-input").show();
  $("#paramsForm").show();
  $("#tagsList").show();
  enableExtraActions();
  selectOptionsWaiter(enableActionButton, 500);
}

function selectOptionsWaiter(callback, interval) {
  const checkSelects = setInterval(() => {
    const allSelectElements = document.querySelectorAll('select');
    const allHaveOptions = Array.from(allSelectElements).every(select => select.querySelector('option'));
    if (allHaveOptions) {
      clearInterval(checkSelects);
      callback();
    }
  }, interval);
}

function selectTemplateForStack(stackToRetrieve, templateName) {
  $("#paramsList").html("");
  $("#tagsList").hide();
  $(".tags-container").html("");
  if (document.getElementById("clone-params"))
    $("#clone-params").hide();
  $(".page-container-flex").prepend("<aui-spinner size=\"large\"></aui-spinner>");

  $("#stackSelector").text(stackToRetrieve);
  $("#stackName").text(stackToRetrieve);

  send_http_get_request(`${baseUrl}/getStackParams/${action}/${region}/${stackToRetrieve}/${templateName}`, displayTemplateParams);
}

function getEc2InstanceTypes(params) {
  const actionRegion = action === "clone" ? $("#RegionVal").select2("val") : region;
  for (const param of params) {
    $(`#${param.ParameterKey}Val`).select2("val", null).empty();
  }
  // if the response has already been cached on the window, just supply that data directly to the callback and skip the request
  if (ec2InstanceTypesResponseStr.length) {
    displayInstanceTypes(ec2InstanceTypesResponseStr, params)
  } else {
    send_http_get_request(`${baseUrl}/getEc2InstanceTypes/${actionRegion}`, displayInstanceTypes, params);
  }
}

function getRdsInstanceTypes(params) {
  const actionRegion = action === "clone" ? $("#RegionVal").select2("val") : region;
  for (const param of params) {
    $(`#${param.ParameterKey}Val`).select2("val", null).empty();
  }

  // future templates might support other DB engines; API design includes 'engine' in the request pattern, but there's nowhere to
  // grab that value from currently (it's hard-coded in the CFN template); hard-code it here for now
  const dbEngine = 'postgres';
  const dbEngineVersionElem = $('#DBEngineVersionVal');

  // only try to request the DB instance types if a valid engine version has been supplied (rely on pattern supplied for aui-validation
  // to know if the value supplied is legit; we have to test ourselves because aui-validation event loop also happens on-change, and we
  // might be checking the validity before aui-validation has a chance to complete its evaluation)
  const validDbVersionRegExp = new RegExp(dbEngineVersionElem.attr('pattern'));
  if (dbEngineVersionElem.length && validDbVersionRegExp.test(dbEngineVersionElem.val())) {
    send_http_get_request(`${baseUrl}/getRdsInstanceTypes/${actionRegion}/${dbEngine}/${dbEngineVersionElem.val()}`, displayInstanceTypes, params);
  }
}

// shared function for updating EC2 and RDS instance fields
function displayInstanceTypes(responseText, params) {
  // save the raw response to the window; it'll persist until the page is reloaded, which is long enough to prevent the need to reload
  // the instance types again until that point (e.g., prevents needing to reload the instance types if user changes product/template).
  // only do this for the EC2 instance types as RDS instance types are dependent on the RDS engine version
  if (params.every(param => param.ParameterKey !== 'DBInstanceClass')) {
    ec2InstanceTypesResponseStr = responseText;
  }

  const instanceTypes = JSON.parse(responseText);

  if (!instanceTypes.length) {
    AJS.flag({
      type: "error",
      title: "No instance classes available",
      body: `<p>No instance classes could be retrieved for fields:</p>
             <ul>${params.map(param => `<li>${param.ParameterLabel}</li>`).join('')}</ul>
             <p>Check logs and/or AWS service status${params.some(param => param.ParameterKey.startsWith('DB')) ? ', or your selected DB Major Engine Version' : ''}.</p>`,
      close: "manual"
   });
   return;
  }

  for (const param of params) {
    // current CFN templates expect value of "none" for DBReadReplicaInstanceClass if no replica DB is desired; prepend that as an option before populating the field
    if (param.ParameterKey === "DBReadReplicaInstanceClass" && instanceTypes.length) {
      instanceTypes.unshift({label: 'none', value: 'none'});
    }

    // handle the case where either the default or existing value for an instance type isn't available; warn the user and ensure the field will not be pre-populated
    let defaultValue = param.ParameterValue;
    if (!instanceTypes.map((i) => i.value).includes(defaultValue)) {
      AJS.flag({
         type: "warning",
         title: "Unavailable instance class",
         body: `<p>Existing or default instance class "${defaultValue}" is not available in this region
                ${param.ParameterKey.startsWith('DB') ? ' or compatible with the selected DB Major Engine Version' : ''};
                no value will be pre-selected for field "${param.ParameterLabel}"!</p>`,
         close: "manual"
      });
      defaultValue = null;
    }
    updateSelect(param.ParameterKey, defaultValue, instanceTypes);
    if (defaultValue && param.ParameterKey in targetJvmHeapParamNameMap){
      displaySuggestedHeapSize(param.ParameterKey);
    }
  }
}

function calculateSuggestedHeapSize(ram) {
  const ramInMb = ram * 1024;
  const ramToLeaveFree = ramInMb <= 4096 ? 2048 : 3072;
  const suggestedHeapInMb = Math.max(Math.min(Math.floor(ramInMb - ramToLeaveFree), 12288), 0);
  if (suggestedHeapInMb % 1024 === 0) {
    return `${(suggestedHeapInMb / 1024)}g`
  } else {
    return `${(suggestedHeapInMb)}m`
  }
}

function displaySuggestedHeapSize(instanceTypeParam) {
  const instanceTypeElem = `#${instanceTypeParam}Val`
  const selectedInstanceType = $(instanceTypeElem).select2("val");
  const selectedInstanceTypeData = $(instanceTypeElem).select2("data");
  if (selectedInstanceType && selectedInstanceTypeData) {
    const selectedInstanceTypeRamText = selectedInstanceTypeData.text.split(',').find((elem) => elem.includes('RAM')).trim();
    const selectedInstanceTypeRam = parseFloat(selectedInstanceTypeRamText);
    const suggestedHeapSize = calculateSuggestedHeapSize(selectedInstanceTypeRam);
    $(`#helper-text-${targetJvmHeapParamNameMap[instanceTypeParam]}`).html(`
      <strong>Suggested value</strong> for <code>${selectedInstanceType}</code> with ${selectedInstanceTypeRamText}: ${suggestedHeapSize}
    `);
    if (action === "create") {
      $(`#${targetJvmHeapParamNameMap[instanceTypeParam]}Val`).val(suggestedHeapSize);
    }
    $(`#${targetJvmHeapParamNameMap[instanceTypeParam]}Val`)
      .attr('data-aui-validation-maxheap', selectedInstanceTypeRam)
      .trigger('change');
  }
}

AJS.toInit(function () {
  AJS.formValidation.register(['maxheap'], function(field) {
    const heapVal = parseInt(field.el.value);
    const heapUnit = field.el.value.charAt(field.el.value.length - 1);
    const heapValInGb = heapUnit === 'g' ? heapVal : (heapVal / 1024);
    if (heapValInGb >= parseInt(field.args('maxheap'))) {
        field.invalidate('Heap size cannot be larger than (or equal to) available RAM!');
    } else {
        field.validate();
    }
  });
}, false);

function getEbsSnapshots(clone_region, stackToRetrieve) {
  $("#EBSSnapshotIdVal").select2("val", null).empty();
  send_http_get_request(`${baseUrl}/getEbsSnapshots/${clone_region}/${stackToRetrieve}`, displayEbsSnapshots);
}

function displayEbsSnapshots(responseText) {
  const ebsSnaps = JSON.parse(responseText);
  if (!ebsSnaps.length) {
    AJS.flag({
       type: "error",
       title: "No snapshots found!",
       body: `<p>No EBS snapshots found in <code>${$("#RegionVal").select2("val").replaceAll("-", "‑")}</code>; check console or Weaver logs for errors, or try a different region.</p>`,
       close: "auto"
    });
  }
  updateSelect("EBSSnapshotId", (ebsSnaps.length ? ebsSnaps[0].label : ''), ebsSnaps);
}

function getRdsSnapshots(clone_region, stackToRetrieve) {
  $("#DBSnapshotNameVal").select2("val", null).empty();
  send_http_get_request(`${baseUrl}/getRdsSnapshots/${clone_region}/${stackToRetrieve}?clonedfrom_region=${region}`, displayRdsSnapshots);
}

function displayRdsSnapshots(responseText) {
  const rdsSnaps = JSON.parse(responseText);
  if (!rdsSnaps.length) {
    AJS.flag({
       type: "error",
       title: "No snapshots found!",
       body: `<p>No RDS snapshots were found in <code>${$("#RegionVal").select2("val").replaceAll("-", "‑")}</code>; check console or Weaver logs for errors, or try a different region.</p>`,
       close: "manual"
    });
  }
  updateSelect("DBSnapshotName", (rdsSnaps.length ? rdsSnaps[0].label : ''), rdsSnaps);
}

function getKmsKeys(region, existingKmsKeyArn) {
  $("#KmsKeyArnVal").select2("val", null).empty();
  send_http_get_request(`${baseUrl}/getKmsKeys/${region}`, displayKmsKeys, existingKmsKeyArn);
}

function displayKmsKeys(responseText, existingKmsKeyArn) {
  var kmsKeys = JSON.parse(responseText);
  var existingKmsKey = kmsKeys.find(key => key.value === existingKmsKeyArn);
  var existingKmsKeyAlias = typeof existingKmsKey !== 'undefined' ? existingKmsKey.label : '';
  updateSelect("KmsKeyArn", existingKmsKeyAlias, kmsKeys);
}

function getSslCerts(region, existingSSLCertificateARN) {
  $("#SSLCertificateARNVal").select2("val", null).empty();
  send_http_get_request(`${baseUrl}/getSslCerts/${region}`, displaySslCerts, existingSSLCertificateARN);
}

function displaySslCerts(responseText, existingSSLCertificateARN) {
  var sslCerts = JSON.parse(responseText);
  var existingSslCert = sslCerts.find(cert => cert.value === existingSSLCertificateARN);
  var existingSslCertAlias = typeof existingSslCert !== 'undefined' ? existingSslCert.label : '';
  updateSelect("SSLCertificateARN", existingSslCertAlias, sslCerts);
}

function getVPCs(vpc_region, existingVpc) {
  var functionParams = {
    vpc_region: vpc_region,
    existingVpc: existingVpc
  };

  send_http_get_request(`${baseUrl}/getVpcs/${vpc_region}`, displayVPCs, functionParams);
}

function displayVPCs(responseText, functionParams) {
  var vpcs = JSON.parse(responseText);
  var existingVpc = functionParams.existingVpc;

  updateSelect("VPC", existingVpc, vpcs);
}

function getSubnets(vpc, region) {
  if (vpc !== "")
    send_http_get_request(`${baseUrl}/getSubnetsForVpc/${region}/${vpc}`, displaySubnets);
}

function displaySubnets(responseText, functionParams) {
  const subnets = JSON.parse(responseText);
  updateSelect("ExternalSubnets", externalSubnets, subnets);
  updateSelect("InternalSubnets", internalSubnets, subnets);
  updateSelect("Az1Subnets", az1subnets, subnets);
  updateSelect("Az2Subnets", az2subnets, subnets);
  updateSelect("Az3Subnets", az3subnets, subnets);
}

function sendParamsAsJson() {
  const actionParamsArray = [];
  const stackNameForAction = action === "update" ? $("#stackSelector").text() : $("#StackNameVal").val();
  const origParamsByKey = Object.fromEntries(origParams.map(param => [param.ParameterKey, param]));

  $(".param-field-group").each((index, element) => {
    const inputElem = $(element).find("> :input").first();
    const paramKey = inputElem.attr("id").split(/Val$/)[0];
    let value;

    // exclude non-template fields from the actionParamsArray
    if (["Region", "StackName"].includes(paramKey)) return true;

    if ("select2" in inputElem.data()) {
      const s2data = inputElem.data().select2;
      const selections = inputElem.select2("val");
      if (selections === null) {
        value = '';
      } else if (Array.isArray(selections)) {
        value = selections.join(s2data.opts.separator);
      } else {
        value = selections;
      }
    } else if (inputElem.is("input") || inputElem.is("select")) {
      value = inputElem.val();
    } else if (inputElem.is("textarea")) {
      // replace newlines (if added for readability on field creation/population or by user) with spaces
      value = inputElem.val().replaceAll("\n", " ");
    }

    if (action === "update") {
      // for updates, only send new parameters or parameters with differing values
      if (!(origParamsByKey.hasOwnProperty(paramKey) && origParamsByKey[paramKey]["ParameterValue"] === value)) {
        actionParamsArray.push({
          parameter_key: paramKey,
          parameter_value: value
        });
      }
    } else {
      actionParamsArray.push({
        parameter_key: paramKey,
        parameter_value: value
      });
    }
  });

  const [template_repo, template_name] = $("#templateSelector").text().split(":").map(elem => elem.trim());

  let product_val;
  if (action === "create") {
    product_val = $("#productSelector").text().toLowerCase();
    // get bitbucket sub product
    if (product_val === 'bitbucket') {
      if (template_name.toLowerCase().includes('mesh')) {
        product_val = product_val + '_mesh'
      } else if (template_name.toLowerCase().includes('mirror')) {
        product_val = product_val + '_mirror'
      }
    }
  }
  else {
    product_val = window.stackInfo.product.toLowerCase();
  }

  let requestBody = {
    params: actionParamsArray,
    template: {repo: template_repo, name: template_name},
    tags: parseTagsForAction()
  };
  if (action === "clone") {
    requestBody["cloned_from"] = $("#stackSelector").text();
  }
  if (["create", "clone"].includes(action)) {
    requestBody["product"] = product_val.toLowerCase();
  }

  // store values for re-use during this session
  sessionStorage.setItem('p', Base64Encode(JSON.stringify({
    action: action,
    stack: stackNameForAction,
    params: actionParamsArray,
    tags: requestBody.tags
  })));

  if (action === 'update') {
    createChangeSet(stackNameForAction, JSON.stringify(requestBody));
  } else {
    action_region = action === "clone" ? $("#RegionVal").select2("val") : region;
    send_http_request(`${baseUrl}/api/v1/stack/${stackNameForAction}?region=${action_region}`, "PUT", {
      data: JSON.stringify(requestBody),
      onreadystatechange: (responseText) => {
        const appendRegion = action === "clone" ? `&region=${action_region}` : null;
        redirectToLog(stackNameForAction, JSON.parse(responseText)['action_id'], extra_params=appendRegion);
      }
    });
  }
}

function enableExtraActions() {
  $('#extraActionsDropdown aui-item-link').each(function() {
    $(this).attr("disabled", false).attr("aria-disabled", false);
  })
}

function populatePreviousValues() {
  var currentStackName = scrapePageForStackName();
  if (currentStackName === "") {
    AJS.flag({
       type: 'info',
       body: 'Please enter a stack name',
       close: 'auto'
    });
    return;
  }

  var storedBlob = sessionStorage.getItem('p');
  if (storedBlob === null) {
    AJS.flag({
       type: 'info',
       title: 'No previous values',
       body: 'Previous values only persist through your current browser session (your current tab or window).',
       close: 'auto'
    });
    return;
  }

  try {
    var storedValues = JSON.parse(Base64Decode(storedBlob));
  } catch(e) {
    AJS.flag({
       type: 'error',
       title: 'No previous values',
       body: 'Unable to parse previous values (malformed); check console?',
       close: 'auto'
    });
    throw e;
  }

  const same_action = storedValues['action'] === action;
  const same_stackname = storedValues['stack'] === currentStackName;

  if (same_action && same_stackname) {
    storedValues['params'].forEach((param) => {
      let element = $(`#${param.parameter_key}Val`);
      if (element.val() !== param.parameter_value) {
        makeSelections(element, param.parameter_value);
        element.trigger('change');
      }
    });
    const allTagRows = document.querySelectorAll("#tagsList .tag-field-group")
    for (const [key, value] of Object.entries(storedValues.tags)) {
      const existingTagKeyRow = Array.from(allTagRows).filter((tagRow) => tagRow.querySelector(":scope input.tag-field-input-key").value == key)[0];
      if (existingTagKeyRow && value) {
        // tag exists and we have a saved value; update it
        existingTagKeyRow.querySelector(":scope input.tag-field-input-value").value = value;
      } else if (value) {
        // tag does not exist but we have a saved value; add it
        addTagRow(key, value);
      } else if (existingTagKeyRow) {
        // tag exists but no saved value; remove it
        existingTagKeyRow.parentNode.remove();
      }
      updateTagInterface();
    };
    AJS.flag({
       type: 'success',
       body: 'Previous values used for stack name "' + currentStackName + '" and action "' + action + '" have been applied.',
       close: 'auto'
    });
  } else {
    AJS.flag({
       type: 'error',
       body: 'No previous values matching this stack name and action were found!',
       close: 'auto'
    });
  }
}

function addTagRow(key="", value="") {
  const newTagRow = createElementsFromString(`<fieldset>
    <div class="field-group tag-field-group">
      <div class="tags-flex-container">
        <div class="tags-flex-item">
          <input
            type="text"
            class="text tag-field-input-key"
            ${key ? `value="${key}"` : ""}
            data-aui-validation-field minlength="1" pattern="^(?!aws:).+$" data-aui-validation-pattern-msg="Value must satisfy regular expression pattern: <code>^(?!aws:).+$</code>"
          >
        </div>
        <div class="tags-flex-item">
          <input
            type="text"
            class="text tag-field-input-value"
            ${value ? `value="${value}"` : ""}
            data-aui-validation-field maxlength="256"
          >
        </div>
        <div>
          <button type="button" class="aui-button tag-field-button-remove" aria-label="Remove tag">
            <span class="aui-icon aui-icon-small aui-iconfont-cross" aria-hidden="true"></span>
          </button>
        </div>
      </div>
    </div>
  </fieldset>`);
  newTagRow.querySelector(":scope button.tag-field-button-remove").addEventListener("click", tagButtonHandler);
  document.querySelector("#tagsList .tags-container").append(newTagRow);
}

function updateTagInterface() {
  const addTagButton = document.querySelector("#tagsList .tag-field-button-add");
  const numTagsRemaining = numMaxTags - $(".tags-container").children().length;
  let numTagsRemainingMessage;

  if (numTagsRemaining == 0) {
    numTagsRemainingMessage = "You have reached the limit of 50 tags";
    addTagButton.setAttribute("disabled", true);
    addTagButton.setAttribute("aria-disabled", true);
  } else {
    numTagsRemainingMessage = `You can add ${numTagsRemaining} more tag(s)`;
    addTagButton.removeAttribute("disabled");
    addTagButton.removeAttribute("aria-disabled");
  }
  document.querySelector("#tagsList .buttons-container .description").textContent = numTagsRemainingMessage;
}

function tagButtonHandler(event) {
  const tag_action = event.currentTarget.classList.contains("tag-field-button-add") ? "add" : "remove";
  if (tag_action == "add") {
    addTagRow();
  } else {
    event.currentTarget.closest("fieldset").remove();
  }
  updateTagInterface();
}

function parseTagsForAction() {
  // read all the tags from the form into a hash of key/value pairs
  const tagsFromForm = Object.fromEntries(
    Array.from(
      document.querySelectorAll("#tagsList .tag-field-group")).map((tagRow) => {
        return [
          tagRow.querySelector(":scope input.tag-field-input-key").value,
          tagRow.querySelector(":scope input.tag-field-input-value").value
        ]
      }
    )
    // form validation should prevent this, but just in case, go ahead and filter out any rows with empty keys
    .filter((tagData) => Boolean(tagData[0]))
  );

  let tagsToSubmit = {};
  if (action === "update") {
    // build an object that represents the intended difference from the existing stack tags:
    // 1. add keys that don't exist
    // 2. modify keys that do exist but have different values
    // 3. mark keys for removal by including them with a blank value
    // ... and trim values prior to submission
    for (const [key, value] of Object.entries(tagsFromForm)) {
      // 1. add any tags that don't exist in the existing set of stack tags
      if (!(key in stackInfo.tags)) {
        tagsToSubmit[key] = value.trim();
      }

      // 2. add any tags that exist, but have differing values
      if (key in stackInfo.tags) {
        if(stackInfo.tags[key] != value) {
          tagsToSubmit[key] = value.trim();
        }
      }
    }

    // 3. add - with a blank value, for removal – any tags that exist on the stack but aren't in the set of submitted tags
    for (const [key, value] of Object.entries(stackInfo.tags)) {
      if (!(key in tagsFromForm)) {
        tagsToSubmit[key] = "";
      }
    }
  } else {
    // otherwise (create or clone), the provided tags are the tags we should submit
    tagsToSubmit = tagsFromForm;
  }

  return tagsToSubmit;
}
