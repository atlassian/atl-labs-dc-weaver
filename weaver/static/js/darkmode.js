// Dark mode switcher
(function toggleDarkMode() {
    function darkness(val = null) {
        const storageKey = 'aui-theme';
        try {
            if (val != null) {
                localStorage.setItem(storageKey, !!val ? 'dark' : '');
            }
            return localStorage.getItem(storageKey) === 'dark';
        } catch (e) {
            console.error('No local storage access');
        }
        return false;
    }

    function toggleDarkness(el, state) {
        el.classList.toggle('aui-iconfont-lightbulb', state);
        el.classList.toggle('aui-iconfont-lightbulb-filled', !state);
        document.body.classList.toggle(darkModeClass, state);
    }

    const darkModeClass = 'aui-theme-dark';
    const isDark = darkness();
    const iconElem = document.getElementById('switch-theme');
    toggleDarkness(iconElem, isDark);

    iconElem.addEventListener('click', (e) => {
        const darkOn = !document.body.classList.contains(darkModeClass);
        const result = darkness(darkOn);
        toggleDarkness(iconElem, result);
    });
}());
