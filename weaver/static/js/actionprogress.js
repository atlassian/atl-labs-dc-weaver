var refreshLogsTimer;
var refreshStackInfoInterval;

function onReady() {
    $("#action-button").hide();
    $("#stackSelector").hide();

    // fix action string
    if (action.indexOf("/") !== -1)
        action = action.substr(action.indexOf("/") + 1);

    const params = new URL(window.location).searchParams;
    const stack_name = params.get("stack");
    const id = params.get("id");
    if (params.has("region"))
        region = params.get("region");
    selectStack(stack_name);
    refreshLogs(stack_name, 2000, action, id);
    refreshStackInfo(stack_name, region);
    $("body").addClass("actionprogress");
}

function action_complete(this_action) {
    var logText = $("#log").contents().text().toLowerCase();
    return countOccurences(logText
            .replace(/ restart/g, 'restart')
            .replace(/run sql/g, 'runsql')
            .replace(/changeset execution/g, 'update')
            .replace(/(thread|heap) dumps/g, 'diagnostics'),
        (this_action.toLowerCase() + " complete")) >= 1;
}

// Refresh the logs while the action is still underway
function refreshLogs(stack_name, refresh_interval, this_action, id) {
    refreshLogsTimer = setTimeout(function () {
        getLogs(stack_name, id);
        // Stop once action is complete
        if (action_complete(this_action)) {
            notify(this_action + " is complete");
            clearTimeout(refreshLogsTimer);
        } else {
            // Otherwise keep refreshing
            refreshLogs(stack_name, 5000, this_action, id);
        }
    }, refresh_interval)
}

function refreshStackInfo(stack_name, region) {
    // Refresh stack info every 10s
    refreshStackInfoInterval = setInterval(function () {
        updateStackInfo(stack_name, region);
    }, 10000);
}

function getLogs(stack_name, action_id) {
    if (stack_name === 'actionreadytostart') return;

    $("#log").css("background", "rgba(0,20,70,.08)");

    // API v1 uses full UUID4 generated on the backend for action ids; prior to this we used generateRequestBody in utils.js
    action_id_is_uuid4 = action_id.split("-").map((seg) => {
        return parseInt(seg, 16).toString(16) === seg;
    }).length === 5;

    // Use the action API endpoint if the action ID is a full-length and valid UUID; use the old getLogs endpoint otherwise
    if (action_id_is_uuid4) {
        send_http_request(`${baseUrl}/api/v1/stack/${stack_name}/action?action_id=${action_id}&region=${region}`, "GET", {onreadystatechange: displayActionLogs});
    } else {
        const requestBody = {};
        requestBody["id"] = action_id;
        requestBody["stack_name"] = stack_name;
        send_http_post_request(baseUrl + "/getLogs", JSON.stringify(requestBody), displayLogs);
    }
}

function displayLogs(responseText) {
    var userHasScrolled = false;
    if ($("#log").contents().find('body').scrollTop() + $("#log").height() < $("#log").contents().height())
        userHasScrolled = true;

    $("#log").css("background", "rgba(0,0,0,0)");

    // If getting the logs has blipped, don't overwrite legitimate logging
    if ((countOccurences($("#log").contents().text(), "No current status for") !== 1 &&
        countOccurences($("#log").contents().text(), "Waiting for logs") !== 1)
        &&
        (countOccurences(responseText, "No current status for") === 1 ||
            countOccurences(responseText, "Waiting for logs") === 1))
        return;

    $("#log").contents().find('body').html(responseText
        .split('",').join('<br />')
        .split('\\n').join('<br />')
        .split('"').join('')
        .trim());

    if (! userHasScrolled)
        $("#log").contents().find('body').scrollTop(9999999999);
}

function displayActionLogs(responseText) {
    const response = JSON.parse(responseText);
    let userHasScrolled = false;
    if ($("#log").contents().find('body').scrollTop() + $("#log").height() < $("#log").contents().height())
        userHasScrolled = true;

    $("#log").css("background", "rgba(0,0,0,0)");

    // If getting the logs has blipped, don't overwrite legitimate logging
    if ((countOccurences($("#log").contents().text(), "No log found for") !== 1 &&
            countOccurences($("#log").contents().text(), "Waiting for logs") !== 1)
        &&
        (response["log_lines"].filter(line => line.includes("No log found for")).length === 1 ||
            response["log_lines"].filter(line => line.includes("Waiting for logs")).length === 1))
        return;

    $("#log").contents().find('body').html(response["log_lines"].join('<br />'));

    if (! userHasScrolled)
        $("#log").contents().find('body').scrollTop(9999999999);
}
