function onReady() {
    $('input[name="drainType"]').prop('disabled', true);
    $('#drainNodes').click((event) => $('input[name="drainType"]').prop('disabled', !event.currentTarget.checked));
    $('.selectStackOption').click((event) => {
        const stack_name = $(event.currentTarget).text();
        selectStack(stack_name);
        $("#takeThreadDumps").removeAttr("disabled");
        $("#takeHeapDumps").removeAttr("disabled");
        emptyAsgAndNodeListsAndCpuChart();
        $("#drainNodes").removeAttr("disabled");
        $('input[name="drainType"]').prop('disabled', !$('#drainNodes').is(':checked'));
        if (action === 'rollingrestart') {
            listAsgs();
        } else if (action === 'restartnode') {
            listNodes();
        } else {
            enableActionButton();
        }
    });
    appendWarningToolTip("#fastNodeDrainDiv", message='Long running connections may get interrupted, use with caution.')

    $("#action-button").on("click", performRestart);
}

function performRestart() {
    const stack_name = scrapePageForStackName();
    const asg_name = $('#asgSelector').text().trim();
    const node_ip = $("#nodeSelector").text().trim();

    const drainNodes = $("#drainNodes").is(':checked');
    const takeThreadDumps = $("#takeThreadDumps").is(':checked');
    const takeHeapDumps = $("#takeHeapDumps").is(':checked');

    const drainType = !drainNodes ? 'skip' : $('input[name="drainType"]:checked').val()
    const url = `${baseUrl}/api/v1/stack/${stack_name}/restart?region=${region}`


    const requestBody = {
        'thread_dumps': takeThreadDumps,
        'heap_dumps': takeHeapDumps
    }

    if (action === 'rollingrestart') {
        requestBody['restart_type'] = 'rolling'
        if (asg_name != "All Auto Scaling groups") {
            requestBody['asg_name'] = asg_name
        }
        requestBody['drain_type'] = drainType
    } else if (action === 'restartnode') {
        requestBody['restart_type'] = 'single'
        requestBody['node_ip'] = node_ip
        requestBody['drain_type'] = drainType
    } else {
        requestBody['restart_type'] = 'full'
    }

    if (! takeThreadDumps) {
        // Add action to button, then show thread dump warning modal
        setModalOkFunction(modals.CONFIRM, function () {
            send_http_request(url, "POST", {
                data: JSON.stringify(requestBody),
                onreadystatechange: (responseText) => redirectToLog(stack_name, JSON.parse(responseText)['action_id'])
            })
        });
        showModal(modals.CONFIRM);
    } else {
        send_http_request(url, "POST", {
            data: JSON.stringify(requestBody),
            onreadystatechange: (responseText) => redirectToLog(stack_name, JSON.parse(responseText)['action_id'])
        })
    }
}
