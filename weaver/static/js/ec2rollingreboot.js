function onReady() {
  $('input[name="drainType"]').prop('disabled', true);
  $('#drainNodes').click((event) => $('input[name="drainType"]').prop('disabled', !event.currentTarget.checked));
  $('.selectStackOption').click((event) => {
    selectStack($(event.currentTarget).text());
    $("#drainNodes").removeAttr("disabled");
    $('input[name="drainType"]').prop('disabled', !$('#drainNodes').is(':checked'));
    listAsgs();
  });
  appendWarningToolTip("#fastNodeDrainDiv", message='Long running connections may get interrupted, use with caution.')
  $("#action-button").click(performEc2RollingReboot);
}

function performEc2RollingReboot() {
  const stack_name = scrapePageForStackName();
  const asg_name = $('#asgSelector').text().trim();
  const drainNodes = $("#drainNodes").is(':checked');
  const drain_type = !drainNodes ? 'SKIP' : $('input[name="drainType"]:checked').val()
  const url = [baseUrl, 'doec2rollingreboot', region, stack_name, asg_name, drain_type];
  const requestBody = generateRequestBody();
  send_http_post_request(url.join('/'), JSON.stringify(requestBody));
  redirectToLog(stack_name, requestBody["id"]);
}
