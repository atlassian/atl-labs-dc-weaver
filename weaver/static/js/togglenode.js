function onReady() {
    addNodeSelectorWithCpuChart();
    $("#action-button").on("click", performNodeToggle);
    appendWarningToolTip("#fastNodeDrainDiv", message='Long running connections may get interrupted, use with caution.')
}

function  performNodeToggle() {
    var stack_name = scrapePageForStackName();
    var url = [baseUrl, 'dotogglenode', region, stack_name, $("#nodeSelector").text(), $("#fastNodeDrain").is(':checked') ? 'FAST' : 'DEFAULT', $("#takeThreadDumps").is(':checked'), $("#takeHeapDumps").is(':checked')].join('/');
    const requestBody = generateRequestBody();
    send_http_post_request(url, JSON.stringify(requestBody));
    redirectToLog(stack_name, requestBody["id"]);
}
