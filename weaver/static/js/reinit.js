function onReady() {
  $('input[name="drainType"]').prop('disabled', true);
  $('#drainNodes').click((event) => $('input[name="drainType"]').prop('disabled', !event.currentTarget.checked));
  $('.selectStackOption').click((event) => {
    const stack_name = $(event.currentTarget).text();
    selectStack(stack_name);
    $("#drainNodes").removeAttr("disabled");
    $('input[name="drainType"]').prop('disabled', !$('#drainNodes').is(':checked'));
    enableActionButton();
  });
  appendWarningToolTip("#fastNodeDrainDiv", message='Long running connections may get interrupted, use with caution.')

  $("#action-button").click(performNodeReinit);
}

function performNodeReinit() {
  const drainNodes = $("#drainNodes").is(':checked');
  const drainType = !drainNodes ? 'skip' : $('input[name="drainType"]:checked').val()
  const stack_name = scrapePageForStackName();
  const url = `${baseUrl}/api/v1/stack/${stack_name}/reinit?region=${region}`
  const requestBody = {
    'drain_type': drainType
  }
  send_http_request(url, "POST", {
    data: JSON.stringify(requestBody),
    onreadystatechange: (responseText) => redirectToLog(stack_name, JSON.parse(responseText)['action_id'])
  })
}
