function onReady() {
  $("#action-button").on("click", confirmCheckIndexHealth);
  $('input[name="drainType"]').prop('disabled', true);
  $('#drainNodes').click((event) => $('input[name="drainType"]').prop('disabled', !event.currentTarget.checked));
  $("#nodesCount").bind('nodeCountChanged', function(e) {
    var number = parseInt($("#nodesCount").text());
    if (number > 1) {
      $("#rebuildSingleNodeWarning").hide();
    } else {
      $("#rebuildSingleNodeWarning").show();
    }
  })

  $('.selectStackOption').click((event) => {
    const stack_name = $(event.currentTarget).text();
    selectStack(stack_name);
    $("#drainNodes").removeAttr("disabled");
    $('input[name="drainType"]').prop('disabled', !$('#drainNodes').is(':checked'));
    emptyAsgAndNodeListsAndCpuChart();
    listAsgs();
  });
  appendWarningToolTip("#fastNodeDrainDiv", message='Long running connections may get interrupted, use with caution.')

}

function confirmCheckIndexHealth() {
  if ($("#product").text().toLowerCase() === 'product: jira') {
    if (parseInt($("#nodesCount").text()) === 1) {
      // if single node show a warning that the index won't be restored
      setModalOkFunction(modals.CONFIRM, performNodeRebuild);
      showModal(modals.CONFIRM);
    } else {
      // if multi node ask for admin creds to check index health
      setModalOkFunction(modals.AUTH, performNodeRebuild);
      showModal(modals.AUTH);
    }
  } else {
    // if product is not Jira there is no index health checking - just proceed
    performNodeRebuild();
  }
}

function performNodeRebuild() {
  const drainNodes = $("#drainNodes").is(':checked');
  const drainType = !drainNodes ? 'skip' : $('input[name="drainType"]:checked').val()
  const stack_name = scrapePageForStackName();
  const asg_name = $('#asgSelector').text().trim();
  const url = `${baseUrl}/api/v1/stack/${stack_name}/rebuild?region=${region}`
  const requestBody = {
    'drain_type': drainType
  }
  if (asg_name != "All Auto Scaling groups") {
    requestBody['asg_name'] = asg_name
  }
  personal_access_token = getAuthDetails()["personal_access_token"];
  send_http_request(url, "POST", {
    headers: {"stack-personal-access-token": personal_access_token},
    data: JSON.stringify(requestBody),
    onreadystatechange: (responseText) => redirectToLog(stack_name, JSON.parse(responseText)['action_id'])
  })
}
