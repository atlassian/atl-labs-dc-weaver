function onReady() {
    const stacks = document.getElementsByClassName("selectStackOption");
    $("#action-button").hide();
    const params = new URL(window.location).searchParams;
    const region = params.get("region");
    const id = params.get("id") === null ? 'latest' : params.get("id");

    for (var i = 0; i < stacks.length; i++) {
        stacks[i].addEventListener("click", function (data) {
            const stack_name = data.target.text;
            clearTimeout(refreshLogsTimer);
            clearInterval(refreshStackInfoInterval);
            selectStack(stack_name);
            getLogs(stack_name, id);
            updateStackInfo(stack_name);
            refreshLogs(stack_name, 2000, action, id);
            refreshStackInfo(stack_name, region);
        }, false);
    }
}
