#!/usr/bin/env python3

from __future__ import annotations

# imports
import glob
import logging

from contextlib import suppress
from logging import ERROR
from os import getenv
from pathlib import Path
from typing import Callable
from git.exc import GitCommandError

from flask import abort, current_app, g, request, session
from flask.views import MethodView
from ruamel.yaml import YAML
from webargs import fields, validate

from weaver.api.helpers import auth_required, use_kwargs, is_valid_aws_region, is_valid_weaver_region
from weaver.aws_cfn.cfn_stack import CfnStack
from weaver.custom_types import GetTemplateParamsResponse
from weaver.extensions import cache, db, executor
from weaver.kubernetes.k8s_stack import StatefulSet
from weaver import models
import weaver.utils as utils
import weaver.enums as weaver_enums
import weaver.exceptions as weaver_exceptions
from weaver.weaver_logging.weaver_logging import log_to_ui, create_changelog

log = logging.getLogger('app_log')

#
# common webargs
#

region_arg = {
    "region": fields.Str(required=True, validate=[is_valid_aws_region, is_valid_weaver_region]),
}

#
# common methods
#


def init_stack_abort_if_doesnt_exist(stack_name: str, region: str) -> CfnStack | StatefulSet:
    try:
        stack = utils.init_stack(stack_name, region)
        stack.check_exists()
    except weaver_exceptions.StackDoesNotExistException as e:
        abort(404, f"Stack {e.stack_name} not found")
    return stack


def create_action_abort_if_action_in_progress(stack: CfnStack | StatefulSet, action_type: str):
    try:
        g.action_id = utils.create_action(stack, action_type)
    except weaver_exceptions.StackActionInProgressException as e:
        abort(409, f'Action {e.action_name} is already in-progress for stack {stack.stack_name}')
    except Exception as e:
        msg = f"Error starting action '{action_type}' for stack {stack.stack_name} in {stack.region}"
        log.exception(msg)
        log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
        abort(500, f"{msg}: {e}")


def submit_action_with_finaliser(stack: CfnStack | StatefulSet, action_type, action_method: Callable, action_params: dict):
    try:
        action_exec = executor.submit(action_method, **action_params)
        action_exec.add_done_callback(utils.finish_action)
    except Exception as e:
        msg = f"Error executing action '{action_type}' for {stack.stack_name} in {stack.region}"
        log.exception(msg)
        log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
        stack.restore_stack_state_if_needed(action_type)
        abort(500, f"{msg}: {e}")


#
# REST Endpoint classes
#


# Get a list of stacks in the requested region
class StackListMethods(MethodView):
    @auth_required
    @use_kwargs(region_arg, location="query")
    def get(self, region):
        try:
            return utils.get_stacks(region)
        except Exception as e:
            abort(500, e)


class StackMethods(MethodView):
    decorators = [auth_required, use_kwargs(region_arg, location="query")]  # type: ignore[list-item]

    @cache.cached(timeout=5, query_string=True)
    def get(self, stack_name, region):
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            if 'expand' in request.args and 'clone_defaults' in request.args.get('expand', ''):
                return {'clone_defaults': stack.get_clone_defaults()}
            else:
                abort(404, f'Stack {e.stack_name} not found')
        try:
            expands: list[str] = list(filter(None, request.args.get('expand', '').split(',')))
            stack_info = stack.get_stack_info(expands)
            return stack_info
        except Exception as e:
            msg = f"Error retrieving info for stack {stack_name} in {region}"
            log.exception(msg)
            abort(500, f"{msg}: {e}")

    @use_kwargs(
        {
            "delete_changelogs": fields.Boolean(load_default=False),
            "delete_threaddumps": fields.Boolean(load_default=False),
        }
    )
    def delete(self, stack_name, region, delete_changelogs, delete_threaddumps):
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            abort(404, f'Stack {e.stack_name} not found')
        try:
            g.action_id = utils.create_action(stack, 'destroy')
        except weaver_exceptions.StackActionInProgressException as e:
            abort(409, f'Action {e.action_name} is already in-progress for stack {stack_name}')
        except Exception as e:
            msg = f"Error starting action 'destroy' for stack {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            abort(500, f"{msg}: {e}")
        try:
            delete_exec = executor.submit(stack.destroy, delete_changelogs, delete_threaddumps)
            delete_exec.add_done_callback(utils.finish_action)
        except Exception as e:
            msg = f"Error executing action 'destroy' for stack {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            abort(500, f"{msg}: {e}")
        else:
            return {'action_id': g.action_id, 'message': 'Stack deletion started'}, 202

    @use_kwargs(
        {
            "change_set_name": fields.String(required=True, validate=validate.Length(min=1, error="Field cannot be blank.")),
            "drain_type": fields.Enum(weaver_enums.DrainType, load_default='DEFAULT'),
        }
    )
    def patch(self, stack_name, region, change_set_name, drain_type: weaver_enums.DrainType = weaver_enums.DrainType.DEFAULT):
        # TODO implement validator to ensure changeset exists and is in a valid state before attempting patch?
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            abort(404, f'Stack {e.stack_name} not found')
        try:
            stack.get_change_set_details(change_set_name)
        except weaver_exceptions.StackChangeSetNotFoundException:
            abort(404, f"Change set '{change_set_name}' does not exist for stack {stack_name}")
        try:
            g.action_id = utils.create_action(stack, 'update')
        except weaver_exceptions.StackActionInProgressException as e:
            abort(409, f'Action {e.action_name} is already in-progress for stack {stack_name}')
        except Exception as e:
            msg = f"Error starting action 'update' for stack {stack_name} in {region} with change set {change_set_name}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            abort(500, f"{msg}: {e}")
        try:
            action_exec = executor.submit(stack.update, change_set_name, drain_type)
            action_exec.add_done_callback(utils.finish_action)
        except Exception as e:
            msg = f"Error executing action 'update' for {stack_name} in {region} with change set {change_set_name}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            stack.restore_stack_state_if_needed('update')
            abort(500, f"{msg}: {e}")
        else:
            return {"action_id": g.action_id, "message": "Stack update started"}, 202

    @use_kwargs(models.StackPutRequestBodySchema())
    def put(self, stack_name, region, product, template, params, tags, cloned_from):
        action_type = "clone" if cloned_from else "create"
        deploy_env: str = next((param['ParameterValue'] for param in params if param['ParameterKey'] == 'DeployEnvironment'), '')
        stack = utils.init_stack(stack_name, region)
        if action_type == "create":
            try:
                stack.check_exists()
            except weaver_exceptions.StackDoesNotExistException:
                pass
            else:
                abort(409, f"Stack {stack_name} already exists")
        try:
            g.action_id = utils.create_action(stack, action_type, deploy_env)
        except weaver_exceptions.StackActionInProgressException as e:
            abort(409, f"Action {e.action_name} is already in-progress for stack {stack_name}")
        except Exception as e:
            msg = f"Error starting action '{action_type}' for stack {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            abort(500, f"{msg}: {e}")
        try:
            action_method = getattr(stack, action_type)
            action_exec = executor.submit(action_method, product, template, params, tags, cloned_from)
            action_exec.add_done_callback(utils.finish_action)
        except Exception as e:
            msg = f"Error executing action {action_type} for {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            abort(500, f"{msg}: {e}")
        else:
            return {"action_id": g.action_id, "message": f"Stack {action_type} started"}, 202


class StackActionMethods(MethodView):
    @auth_required
    @use_kwargs(
        {
            **region_arg,
            **{"action_id": fields.Str()},
        },
        location="query",
    )
    def get(self, stack_name, region, action_id=None):
        if action_id is None:
            return models.StackActionsResponseSchema(many=True).dump(
                db.session.query(models.StackAction).filter_by(stack_name=stack_name, region=region).order_by(models.StackAction.started_at.desc()).limit(20)
            )
        action: models.StackAction = db.get_or_404(models.StackAction, action_id, description="Action ID Not Found")  # type: ignore[attr-defined]
        if action.stack_name != stack_name or action.region != region:
            abort(400, f"Action ID {action_id} is not an action for stack {stack_name} in {region}")
        try:
            log_lines = utils.get_action_log(action.stack_name, action.id)
        except Exception as e:
            abort(500, e)
        else:
            action.log_lines = [line for line in log_lines.split('\n') if line]
        return models.StackActionResponseSchema().dump(action)


class StackChangeSetMethods(MethodView):
    decorators = [
        auth_required,
        use_kwargs(region_arg, location="query"),  # type: ignore[list-item]
    ]

    @use_kwargs({"change_set_name": fields.String(load_default=None)}, location="query")
    def get(self, stack_name, region, change_set_name):
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            abort(404, f'Stack {e.stack_name} not found')
        if change_set_name:
            try:
                change_set_details = stack.get_change_set_details(change_set_name)
            except weaver_exceptions.StackChangeSetNotFoundException:
                abort(404, f"Change set '{change_set_name}' does not exist for stack {stack_name}")
            except Exception as e:
                msg = f"An error occurred retrieving details for change set '{change_set_name}'"
                log.exception(msg)
                abort(500, f"{msg}: {e}")
            return models.StackChangeSetSchema().dump(change_set_details)
        else:
            try:
                change_sets = stack.list_change_sets()
            except Exception as e:
                msg = "An error occurred retrieving change sets"
                log.exception(msg)
                abort(500, f"{msg}: {e}")
            return {"change_sets": models.StackChangeSetSchema().dump(change_sets, many=True)}

    @use_kwargs(models.StackChangesetPutRequestBodySchema())
    def put(self, stack_name, region, template, params, tags):
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            abort(404, f"Stack {e.stack_name} not found")
        try:
            stack.changelogfile = create_changelog(stack_name, "create_changeset")
            change_set = stack.create_change_set(template, params, tags)
        except Exception as e:
            msg = "An error occurred creating change set"
            log.exception(msg)
            abort(500, f"{msg}: {e}")
        return models.StackChangeSetSchema().dump(change_set), 202


class StackDiagnosticsMethods(MethodView):
    decorators = [
        auth_required,
        use_kwargs(region_arg, location="query"),  # type: ignore[list-item]
    ]

    @use_kwargs({"stack_personal_access_token": fields.String(load_default=None, data_key="stack-personal-access-token", load_only=True)}, location="headers")
    @use_kwargs(models.StackDiagnosticsGetRequestQuerySchema(), location="query")
    def get(self, stack_name, region, diagnostic_types, node_ip, stack_personal_access_token):
        # TODO: Handle invalid personal access token better (currently just reports node could not be connected to)
        # in the index_health res
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            abort(404, f'Stack {e.stack_name} not found')
        res = {}
        # If node_ip is supplied, validate that it's in the stack's list of nodes
        if node_ip and not stack.has_node(node_ip):
            abort(404, f'Node {node_ip} not found for stack {stack.stack_name}')
        # TODO: Be able to filter thread/heap dumps + support zips by node IP. And maybe date filtering and pagination options.
        # TODO: Fetch S3 links once per API call rather than once for each diagnostic type that returns S3 links.
        if weaver_enums.DiagnosticType.CPU in diagnostic_types:
            if not node_ip:
                # Retrieve CPU for all nodes
                res["cpu"] = [{"node_ip": node.private_ip_address, "cpu": stack.get_node_cpu(node.private_ip_address)} for node in stack.get_stacknodes()]
            else:
                res["cpu"] = [{"node_ip": node_ip, "cpu": stack.get_node_cpu(node_ip)}]
        if weaver_enums.DiagnosticType.HEAP_DUMP in diagnostic_types:
            res["heap_dumps"] = stack.get_s3_file_links(file_type=weaver_enums.DiagnosticFileType.HEAP_DUMP, node_ip=node_ip)
        if weaver_enums.DiagnosticType.INDEX_HEALTH in diagnostic_types:
            if not node_ip:
                # Retrieve index health for all nodes
                res["index_health"] = stack.get_index_health_all_nodes(stack_personal_access_token)
            else:
                res["index_health"] = [{"node_ip": node_ip, "index_health": stack.get_index_health(node_ip, stack_personal_access_token)}]
        if weaver_enums.DiagnosticType.SUPPORT_ZIP in diagnostic_types:
            res["support_zips"] = stack.get_s3_file_links(file_type=weaver_enums.DiagnosticFileType.SUPPORT_ZIP, node_ip=node_ip)
        if weaver_enums.DiagnosticType.THREAD_DUMP in diagnostic_types:
            res["thread_dumps"] = stack.get_s3_file_links(file_type=weaver_enums.DiagnosticFileType.THREAD_DUMP, node_ip=node_ip)
        return res

    @use_kwargs(models.StackDiagnosticsPostRequestBodySchema())
    def post(self, stack_name, region, diagnostic_type, node_ip, personal_access_token):
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            abort(404, f'Stack {e.stack_name} not found')
        if node_ip and not stack.has_node(node_ip):
            abort(404, f'Node {node_ip} not found for stack {stack.stack_name}')
        action_type: str = 'diagnostics'
        try:
            g.action_id = utils.create_action(stack, action_type)
        except weaver_exceptions.StackActionInProgressException as e:
            abort(409, f"Action {e.action_name} is already in-progress for stack {stack_name}")
        except Exception as e:
            msg = f"Error starting action '{action_type}' for stack {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            abort(500, f"{msg}: {e}")
        action_params: dict = {}
        if diagnostic_type == weaver_enums.PostMethodDiagnosticType.HEAP_DUMP:
            action_method = stack.heap_dump
            if node_ip:
                action_params['node_ip'] = node_ip
        elif diagnostic_type == weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP:
            action_method = stack.generate_support_zip
            action_params['personal_access_token'] = personal_access_token
        elif diagnostic_type == weaver_enums.PostMethodDiagnosticType.THREAD_DUMP:
            action_method = stack.thread_dump
            if node_ip:
                action_params['node_ip'] = node_ip
        try:
            action_exec = executor.submit(action_method, **action_params)
            action_exec.add_done_callback(utils.finish_action)
        except Exception as e:
            msg = f"Error executing action '{action_type}' for {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            stack.restore_stack_state_if_needed(action_type)
            abort(500, f"{msg}: {e}")
        else:
            return {"action_id": g.action_id, "message": f"Stack {action_type} started"}, 202


class StackUpgradeMethods(MethodView):
    decorators = [
        auth_required,
        use_kwargs(region_arg, location="query"),  # type: ignore[list-item]
    ]

    @use_kwargs(models.StackUpgradeRequestBodySchema())
    def post(self, stack_name, region, new_version, alternate_download_url, zdu, zdu_type, auth, drain_type, startup_primary, upgrade_type):
        try:
            stack = utils.init_stack(stack_name, region)
            stack.check_exists()
        except weaver_exceptions.StackDoesNotExistException as e:
            abort(404, f"Stack {e.stack_name} not found")
        try:
            g.action_id = utils.create_action(stack, 'upgrade')
        except weaver_exceptions.StackActionInProgressException as e:
            abort(409, f'Action {e.action_name} is already in-progress for stack {stack_name}')
        except Exception as e:
            msg = f"Error starting action 'upgrade' for stack {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            abort(500, f"{msg}: {e}")
        action_params = {
            "new_version": new_version,
            "alt_download_url": alternate_download_url,
        }
        if zdu:
            # TODO: enhance get_zdu_compatibility to take the new_version; move logic for checking specific-version ZDU
            # compatibilty for Bitbucket and Confluence from upgrade.js to get_zdu_compatibility (this check currently
            # only ensures Confluence and Bitbucket are above their minimum compatible versions)
            zdu_compat = stack.get_zdu_compatibility()
            if not zdu_compat.get('compatible'):
                abort(400, zdu_compat['reason'])
            action_method = stack.upgrade_zdu
            action_params["zdu_type"] = zdu_type
            action_params["personal_access_token"] = auth["personal_access_token"]
        elif upgrade_type == weaver_enums.UpgradeType.IN_PLACE:
            action_method = stack.upgrade_in_place
            action_params["drain_type"] = drain_type
            action_params["startup_primary"] = startup_primary
        else:
            action_method = stack.upgrade
            action_params["drain_type"] = drain_type
            action_params["startup_primary"] = startup_primary
        try:
            action_exec = executor.submit(action_method, **action_params)
            action_exec.add_done_callback(utils.finish_action)
        except Exception as e:
            msg = f"Error executing action 'upgrade' for {stack_name} in {region}"
            log.exception(msg)
            log_to_ui(stack, ERROR, f"{msg}: {e}", write_to_changelog=True)
            stack.restore_stack_state_if_needed('upgrade')
            abort(500, f"{msg}: {e}")
        else:
            return {"action_id": g.action_id, "message": "Stack upgrade started"}, 202


class UtilCfnTemplateMethods(MethodView):
    @auth_required
    @use_kwargs(
        {
            "product": fields.Enum(models.ProductEnum, load_default=None),
            "repo_name": fields.Str(load_default=''),
        },
        location="query",
    )
    def get(self, product: models.ProductEnum | None, repo_name: str):
        templates = utils.list_templates(product, repo_name)
        if not templates:
            abort(404, "Template(s) not found")
        return templates


class StackRestartMethods(MethodView):
    @auth_required
    @use_kwargs(region_arg, location="query")
    @use_kwargs(models.StackRestartRequestBodySchema())
    def post(self, stack_name, region, restart_type, node_ip, asg_name, drain_type, thread_dumps, heap_dumps):
        stack = init_stack_abort_if_doesnt_exist(stack_name, region)
        if node_ip and not stack.has_node(node_ip):
            abort(404, f"Node '{node_ip}' not found for stack {stack.stack_name}")
        if asg_name and not stack.has_asg(asg_name):
            abort(404, f"Autoscaling group '{asg_name}' not found for stack {stack.stack_name}")
        match restart_type:
            case weaver_enums.RestartType.FULL:
                action_type = "fullrestart"
            case weaver_enums.RestartType.ROLLING:
                action_type = "rollingrestart"
            case weaver_enums.RestartType.SINGLE:
                action_type = "restartnode"
        create_action_abort_if_action_in_progress(stack, action_type)
        action_method = stack.restart_action
        action_params = {"restart_type": restart_type, "node_ip": node_ip, "asg_name": asg_name, "drain_type": drain_type, "thread_dumps": thread_dumps, "heap_dumps": heap_dumps}
        submit_action_with_finaliser(stack, action_type, action_method, action_params)
        return {"action_id": g.action_id, "message": f"Stack {action_type} started"}, 202


class StackRebuildMethods(MethodView):
    @auth_required
    @use_kwargs(region_arg, location="query")
    @use_kwargs({"stack_personal_access_token": fields.String(load_default=None, data_key="stack-personal-access-token", load_only=True)}, location="headers")
    @use_kwargs(models.StackRebuildRequestBodySchema())
    def post(self, stack_name, region, asg_name, drain_type, stack_personal_access_token):
        stack = init_stack_abort_if_doesnt_exist(stack_name, region)
        if asg_name and not stack.has_asg(asg_name):
            abort(404, f"Autoscaling group '{asg_name}' not found for stack {stack.stack_name}")
        action_type: str = 'rollingrebuild'
        create_action_abort_if_action_in_progress(stack, action_type)
        action_method = stack.rolling_rebuild
        action_params = {"drain_type": drain_type}
        if asg_name:
            action_params["asg_name"] = asg_name
        if stack_personal_access_token:
            action_params["personal_access_token"] = stack_personal_access_token
        submit_action_with_finaliser(stack, action_type, action_method, action_params)
        return {"action_id": g.action_id, "message": f"Stack {action_type} started"}, 202


class StackReinitMethods(MethodView):
    @auth_required
    @use_kwargs(region_arg, location="query")
    @use_kwargs(models.StackReinitRequestBodySchema())
    def post(self, stack_name, region, drain_type):
        stack = init_stack_abort_if_doesnt_exist(stack_name, region)
        action_type: str = 'rollingreinit'
        create_action_abort_if_action_in_progress(stack, action_type)
        action_method = stack.rolling_reinit
        action_params = {"drain_type": drain_type}
        submit_action_with_finaliser(stack, action_type, action_method, action_params)
        return {"action_id": g.action_id, "message": f"Stack {action_type} started"}, 202


###
## Deprecated methods
#


class DoEc2RollingReboot(MethodView):
    @auth_required
    def post(self, region, stack_name, asg_name, drain_type):
        drain_type = weaver_enums.DrainType[drain_type]
        mystack = utils.init_stack(stack_name, region)
        action_name: str = 'ec2rollingreboot'
        if not utils.store_current_action(mystack, action_name, id=request.get_json()['id']):
            return False
        try:
            restart_args = {'drain_type': drain_type}
            if asg_name and asg_name != 'All Auto Scaling groups':
                restart_args['asg_name'] = asg_name
            mystack.ec2_rolling_reboot(**restart_args)
        except weaver_exceptions.StackDeregistrationDelaysNotFullValueException as e:
            log.exception('Error occurred doing rolling EC2 node reboot')
            log_to_ui(mystack, ERROR, f'Error occurred doing rolling EC2 node reboot: {e}', write_to_changelog=True)
            # Don't restore deregistration delays in case a fast drain action is running.
            mystack.restore_stack_state_if_needed(action_name, deregistration_delays=False)
        except Exception as e:
            log.exception('Error occurred doing rolling EC2 node reboot')
            log_to_ui(mystack, ERROR, f'Error occurred doing rolling EC2 node reboot: {e}', write_to_changelog=True)
            mystack.restore_stack_state_if_needed(action_name)
        else:
            mystack.restore_stack_state_if_needed(action_name)
        utils.clear_current_action(mystack)


class DoToggleNode(MethodView):
    @auth_required
    def post(self, region, stack_name, node_ip, drain_type, threads, heaps):
        drain_type = weaver_enums.DrainType[drain_type]
        mystack = utils.init_stack(stack_name, region)
        action_name: str = 'togglenode'
        if not utils.store_current_action(mystack, action_name, id=request.get_json()['id']):
            return False
        try:
            if threads == 'true':
                mystack.thread_dump(node_ip=node_ip, alsoHeaps=heaps)
            if heaps == 'true':
                mystack.heap_dump(node_ip=node_ip)
            mystack.toggle_node_registration(node_ip, drain_type)
        except weaver_exceptions.StackDeregistrationDelaysNotFullValueException as e:
            log.exception('Error occurred toggling node registration')
            log_to_ui(mystack, ERROR, f'Error occurred toggling node registration: {e}', write_to_changelog=True)
            # Don't restore deregistration delays in case a fast drain action is running.
            mystack.restore_stack_state_if_needed(action_name, deregistration_delays=False)
        except Exception as e:
            log.exception('Error occurred toggling node registration')
            log_to_ui(mystack, ERROR, f'Error occurred toggling node registration: {e}', write_to_changelog=True)
            mystack.restore_stack_state_if_needed(action_name)
        else:
            mystack.restore_stack_state_if_needed(action_name)
        utils.clear_current_action(mystack)


class DoRunSql(MethodView):
    @auth_required
    def post(self, region, stack_name):
        mystack = utils.init_stack(stack_name, region)
        if not utils.store_current_action(mystack, 'runsql', id=request.get_json()['id']):
            return False
        try:
            outcome = mystack.run_sql()
        except Exception as e:
            log.exception('Error occurred running SQL')
            log_to_ui(mystack, ERROR, f'Error occurred running SQL: {e}', write_to_changelog=True)
            utils.clear_current_action(mystack)
            return False
        utils.clear_current_action(mystack)
        return outcome


class GetLogs(MethodView):
    def post(self):
        stack_name = request.get_json()['stack_name']
        action_id: str = request.get_json()['id']
        if action_id == 'latest':
            log = utils.get_latest_log(stack_name)
        else:
            log = utils.get_action_log(stack_name, action_id)
        return log if log else f'No current logs for {stack_name} action {action_id}'


class GetSysLogs(MethodView):
    def get(self):
        with open(Path('logs/weaver.log'), 'r') as logfile:
            try:
                return logfile.read()
            except Exception:
                log.exception('Error occurred getting system logs')


class GetGitBranch(MethodView):
    def get(self, template_repo):
        repo = utils.get_git_repo_base(template_repo)
        if repo.head.is_detached:
            return 'Detached HEAD'
        else:
            return repo.active_branch.name


class GetGitCommitDifference(MethodView):
    def get(self, template_repo):
        repo = utils.get_git_repo_base(template_repo)
        if not repo.head.is_detached:
            for remote in repo.remotes:
                remote.fetch(env=dict(GIT_SSH_COMMAND=getenv('GIT_SSH_COMMAND', 'ssh -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -i /home/weaver/gitkey')))
        return utils.get_git_commit_difference(repo)


class DoGitPull(MethodView):
    @auth_required
    def get(self, template_repo, stack_name):
        repo = utils.get_git_repo_base(template_repo)
        if template_repo == 'Weaver (requires restart)':
            log.info('Updating Weaver')
            log.info(f'Stashing: {repo.git.stash()}')
            try:
                log.info(f'Pulling: {repo.git.pull()}')
            except GitCommandError as e:
                if "permission denied" in e.stderr.lower():
                    with repo.git.custom_environment(GIT_SSH_COMMAND='ssh -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -i /home/weaver/gitkey'):
                        log.info(f'Pulling with gitkey: {repo.git.pull()}')
                else:
                    raise
            result = 'Weaver updated successfully'
        else:
            result = repo.git.reset('--hard', f'origin/{repo.active_branch.name}')
        log.info(result)
        return result


class GetGitRevision(MethodView):
    def get(self, template_repo):
        repo = utils.get_git_repo_base(template_repo)
        result = utils.get_git_revision(repo)
        log.info(result)
        return result[:7]


class GetTemplateParams(MethodView):
    def get(self, repo_name, template_name):
        if 'atlassian-aws-deployment' in repo_name:
            template_file = open(f'atlassian-aws-deployment/templates/{template_name}', 'r')
        else:
            for file in glob.glob(f'custom-templates/{repo_name}/**/*.yaml'):
                if template_name in file:
                    template_file = open(file, 'r')
        yaml = YAML(typ='safe', pure=True)
        yaml.Constructor.add_multi_constructor(u'!', utils.general_constructor)
        template = yaml.load(template_file)
        response: GetTemplateParamsResponse = {'groups': {}, 'labels': {}, 'params': []}
        with suppress(KeyError):
            template_param_groups = template['Metadata']['AWS::CloudFormation::Interface']['ParameterGroups']
            for group in template_param_groups:
                response['groups'][group['Label']['default']] = group['Parameters']
        with suppress(KeyError):
            template_param_labels = template['Metadata']['AWS::CloudFormation::Interface']['ParameterLabels']
            for label in template_param_labels:
                response['labels'][label] = template_param_labels[label]['default']
        template_params = template['Parameters']
        for param in template_params:
            response['params'].append(
                {
                    'ParameterKey': param,
                    'ParameterValue': template_params[param]['Default'] if 'Default' in template_params[param] else '',
                    'ParameterDescription': template_params[param]['Description'] if 'Description' in template_params[param] else '',
                }
            )
            # Add validation values to params to send to the front end
            if 'AllowedValues' in template_params[param]:
                next(param_to_send for param_to_send in response['params'] if param_to_send['ParameterKey'] == param)['AllowedValues'] = template_params[param]['AllowedValues']
            if 'AllowedPattern' in template_params[param]:
                next(param_to_send for param_to_send in response['params'] if param_to_send['ParameterKey'] == param)['AllowedPattern'] = template_params[param]['AllowedPattern']
            if 'MinValue' in template_params[param]:
                next(param_to_send for param_to_send in response['params'] if param_to_send['ParameterKey'] == param)['MinValue'] = template_params[param]['MinValue']
            if 'MaxValue' in template_params[param]:
                next(param_to_send for param_to_send in response['params'] if param_to_send['ParameterKey'] == param)['MaxValue'] = template_params[param]['MaxValue']
            if 'MinLength' in template_params[param]:
                next(param_to_send for param_to_send in response['params'] if param_to_send['ParameterKey'] == param)['MinLength'] = template_params[param]['MinLength']
            if 'MaxLength' in template_params[param]:
                next(param_to_send for param_to_send in response['params'] if param_to_send['ParameterKey'] == param)['MaxLength'] = template_params[param]['MaxLength']
        return response


class GetTemplateParamsForStack(MethodView):
    def get(self, action, region, stack_name, template_name):
        mystack = utils.init_stack(stack_name, region)
        return mystack.get_template_params(action, template_name)


class GetSql(MethodView):
    def get(self, region, stack_name):
        mystack = utils.init_stack(stack_name, region)
        return mystack.get_sql()


class GetStackActionInProgress(MethodView):
    def get(self, region, stack_name):
        action = utils.get_stack_action_in_progress(stack_name)
        return action if action else 'None'


class ClearStackActionInProgress(MethodView):
    def get(self, region, stack_name):
        mystack = utils.init_stack(stack_name, region)
        utils.clear_current_action(mystack)
        return True


class GetEbsSnapshots(MethodView):
    def get(self, region, stack_name):
        mystack = utils.init_stack(stack_name, region)
        return mystack.get_ebs_snapshots()


class GetRdsSnapshots(MethodView):
    def get(self, region, stack_name):
        mystack = utils.init_stack(stack_name, region)
        return mystack.get_rds_snapshots(request.args.get('clonedfrom_region'))


class GetLockedStacks(MethodView):
    def get(self):
        locked_stacks = []
        if 'LOCKS' in current_app.config:
            locked_stacks = list(current_app.config['LOCKS'].keys())
        return locked_stacks


class GetGitRepos(MethodView):
    def get(self):
        repos = ['atlassian-aws-deployment', 'Weaver (requires restart)']
        custom_template_folder = Path('custom-templates')
        if custom_template_folder.exists():
            for directory in glob.glob(f'{custom_template_folder}/*'):
                repos.append(directory.split('/')[1])
        repos.sort()
        return repos


class SetStackLocking(MethodView):
    def post(self, lock):
        current_app.config['STACK_LOCKING'] = lock
        session['stack_locking'] = lock
        return lock


class WeaverStatus(MethodView):
    def get(self):
        return {'state': 'RUNNING'}


class DoWeaverRestart(MethodView):
    @auth_required
    def get(self, stack_name):
        log.warning('Weaver restart has been triggered')
        return str(utils.restart_weaver())
