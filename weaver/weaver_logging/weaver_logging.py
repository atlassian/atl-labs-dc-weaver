import errno
import logging
import os
from datetime import datetime
from logging import getLevelName

import boto3

from flask import g

from weaver.sns import sns

##
# Logging functions
#

log = logging.getLogger('app_log')


def create_action_log(stack, action, action_id, timestamp=None):
    # old API using store_current_action will not pass a timestamp; new routes using create_action do pass a timestamp
    if not timestamp:
        timestamp = datetime.now()
    filename = f'stacks/{stack.stack_name}/logs/{timestamp.strftime("%Y%m%d-%H%M%S")}_{stack.stack_name}_{action}_{action_id}.action.log'
    create_logfile_and_path(filename)
    return filename


def create_changelog(stack_name, action, action_id=None, timestamp=None):
    # old API using store_current_action will not pass a timestamp; new routes using create_action do pass a timestamp
    # also, PUT request of StackChangeSetMethods currently calls this method directly without a timestamp
    if not timestamp:
        timestamp = datetime.now()
    filename_parts = [
        timestamp.strftime("%Y%m%d-%H%M%S"),
        stack_name,
        action,
        action_id,
    ]
    filepath = f'stacks/{stack_name}/logs/{"_".join([part for part in filename_parts if part])}.change.log'
    create_logfile_and_path(filepath)
    return filepath


def create_logfile_and_path(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    open(filename, 'w').close()


def log_to_ui(stack, level, message, write_to_changelog=False, send_sns_msg=False):
    if stack.logfile is not None:
        logline = f'{datetime.now().strftime("%Y-%m-%d %X")} {getLevelName(level)} {message}'
        log.log(level, f"[{g.action_id}] {message}" if hasattr(g, 'action_id') else message)
        with open(stack.logfile, 'a') as logfile:
            logfile.write(f'{logline}\n')
        # consider carefully whether this msg needs to go into the changelog
        # if more appropriate, use log_change directly to log an altered message to the changelog
        if write_to_changelog:
            log_to_changelog(stack.changelogfile, message)
        if send_sns_msg:
            sns.send_sns_msg(stack, message)


def log_to_changelog(changelogfile, message):
    if changelogfile is not None:
        logline = f'{datetime.now()} {message} \n'
        with open(changelogfile, 'a') as logfile:
            logfile.write(logline)


def save_changelog(stack):
    changelog = os.path.relpath(stack.changelogfile)
    changelog_filename = os.path.basename(stack.changelogfile)
    s3 = boto3.resource('s3', region_name=stack.region)
    s3.meta.client.upload_file(changelog, stack.get_s3_bucket_name(), f'changelogs/{stack.stack_name}/{changelog_filename}')
