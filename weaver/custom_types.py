from typing import NotRequired
from typing_extensions import TypedDict


class ParameterTypeDefWithoutKey(TypedDict):
    ParameterValue: NotRequired[str]
    ResolvedValue: NotRequired[str]
    UsePreviousValue: NotRequired[bool]


class OverloadedParamTypeDef(TypedDict):
    AllowedPattern: NotRequired[str]
    AllowedValues: NotRequired[list[bool | int | str]]
    Default: NotRequired[str]
    DefaultValue: NotRequired[str]
    Description: NotRequired[str]
    MaxLength: NotRequired[int]
    MaxValue: NotRequired[int]
    MinLength: NotRequired[int]
    MinValue: NotRequired[int]
    NoEcho: NotRequired[bool]
    ParameterDescription: NotRequired[str]
    ParameterKey: NotRequired[str]
    ParameterValue: NotRequired[str]
    ResolvedValue: NotRequired[str]
    UsePreviousValue: NotRequired[bool]


class GetTemplateParamsResponse(TypedDict):
    groups: dict[str, list[str]]
    labels: dict[str, str]
    params: list[OverloadedParamTypeDef]
