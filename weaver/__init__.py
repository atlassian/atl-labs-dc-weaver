import logging
import os
from logging.handlers import RotatingFileHandler

import boto3
import botocore

from flask import Flask

from weaver.extensions import cache, db, executor, jwt, ma, migrate, sess
from weaver.api.routes import api_blueprint
from weaver.aws_resources import get_available_regions
from weaver.aws_cfn.cfn_stack import aws_cfn_stack_blueprint
from weaver.main import main as main_blueprint
from weaver import saml_auth
from weaver.token_auth import token_blueprint
from weaver.version import __version__


log = logging.getLogger('app_log')


def create_app(config_class):
    # create log directory
    log_dir = 'logs'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # init loggers
    access_log_handler = RotatingFileHandler(f'{log_dir}/weaver_access.log', maxBytes=10000000, backupCount=5)
    access_log_handler.setLevel(logging.INFO)
    access_log = logging.getLogger('werkzeug')
    access_log.addHandler(access_log_handler)

    app_log_handler = RotatingFileHandler(f'{log_dir}/weaver.log', maxBytes=10000000, backupCount=5)
    app_log_handler.setLevel(logging.INFO)
    log_formatter = logging.Formatter('%(asctime)s [%(process)d] [%(levelname)s] [%(thread)d] [%(threadName)s] %(message)s', '[%Y-%m-%d %H:%M:%S %z]')
    app_log_handler.setFormatter(log_formatter)
    app_log = logging.getLogger('app_log')
    app_log.setLevel(logging.INFO)
    app_log.addHandler(app_log_handler)

    # create and initialize app
    log.info(f'Starting Atlassian Labs Data Center Weaver v{__version__} in {os.getenv("ATL_ENVIRONMENT", "unknown or local")} environment')
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config_class)
    try:
        app.config.from_pyfile("weaver-deployment.cfg")
    except FileNotFoundError:
        log.warning(f'No weaver-deployment.cfg config file found in instance folder; using defaults from class {config_class}')
    try:
        app.config.from_pyfile("weaver.cfg")
    except FileNotFoundError:
        log.warning(f'No weaver.cfg instance-specific config file found in instance folder; using defaults from class {config_class}')
    app.json.sort_keys = False  # type: ignore[attr-defined]
    app.config['LOCKS'] = {}

    # write all logging that is not werkzeug (requests) to the app log
    app.logger.addHandler(app_log_handler)

    # get current region and create SSM client to read parameter store params
    ssm_client = boto3.client('ssm', region_name=os.getenv('REGION', 'us-east-1'))
    app.config['SECRET_KEY'] = 'REPLACE_ME'  # nosec B105
    try:
        key = ssm_client.get_parameter(Name='atl_weaver_secret_key', WithDecryption=True)
        app.config['SECRET_KEY'] = key['Parameter']['Value']
    except botocore.exceptions.NoCredentialsError:
        log.error('No credentials - please authenticate with Cloudtoken')
    except Exception:
        log.error('No secret key in parameter store')

    # read all available regions from EC2 and populate config
    app.config["AVAILABLE_REGIONS"] = get_available_regions()
    if not app.config["AVAILABLE_REGIONS"]:
        log.error('List of available regions could not be loaded from AWS; check credentials and/or AWS service status')

    with app.app_context():
        # create SAML URL if saml enabled
        if not os.getenv('NO_SAML') and not app.config.get('NO_SAML'):
            saml_auth.configure_saml(ssm_client, app)
        else:
            log.info('SAML auth is not configured')

        db.init_app(app)
        app.config["SESSION_SQLALCHEMY"] = db
        migrate.init_app(app, db)
        ma.init_app(app)
        executor.init_app(app)
        jwt.init_app(app)
        sess.init_app(app)
        cache.init_app(app)

        # clear any existing cache
        cache.clear()

    # Register Blueprints
    app.register_blueprint(api_blueprint)
    app.register_blueprint(main_blueprint)
    app.register_blueprint(aws_cfn_stack_blueprint)
    app.register_blueprint(saml_auth.saml_blueprint, url_prefix='/saml')
    app.register_blueprint(token_blueprint)

    return app
