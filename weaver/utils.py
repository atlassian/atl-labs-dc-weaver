from __future__ import annotations

import getpass
import glob
import re
import time

import git
import logging
import psutil
import subprocess  # nosec B404

from datetime import datetime
from flask import current_app, g, session
from flask_jwt_extended import get_jwt, verify_jwt_in_request
from jwt.exceptions import ExpiredSignatureError
from logging import INFO, WARN, ERROR
from os import getenv, getppid, walk
from os.path import dirname
from pathlib import Path
from ruamel import yaml
from typing import Any, Literal, List, Optional, Union, Iterator, Sequence, TYPE_CHECKING

from humanfriendly import format_timespan

import weaver.exceptions as weaver_exceptions
from weaver.extensions import db
from weaver.models import StackAction, ProductEnum
from weaver.weaver_logging import log_to_ui, create_changelog, create_action_log, log_to_changelog, save_changelog

if TYPE_CHECKING:
    from weaver.Stack import Stack
    from concurrent.futures import Future
    from mypy_boto3_cloudformation.type_defs import TagTypeDef

####
##  Common functions for use across Weaver
##

log = logging.getLogger('app_log')

##
# Mappings
#

pytest_returncode_msgs: list = [
    'All tests were collected and passed successfully',
    'Tests were collected and run but some of the tests failed',
    'Test execution was interrupted by the user',
    'Internal error happened while executing tests',
    'pytest command line usage error',
    'No tests were collected',
]

##
# Common Weaver functions
#


def list_templates(product: ProductEnum | None, repo_name: str) -> list[dict[str, str]]:
    templates = []
    default_template_folder = Path('atlassian-aws-deployment')
    custom_template_folder = Path('custom-templates')

    re_search_pattern = []
    # filter by product, if provided
    if product:
        # for product names with underscores, split by the underscore and use all segments as search tokens
        for segment in product.name.split("_"):
            re_search_pattern.append(fr"(?=.*{segment})")
    re_search_pattern.append(r".*\.template\.yaml$")

    folders_to_search = [default_template_folder]
    if custom_template_folder.exists():
        folders_to_search.extend(custom_template_folder.iterdir())

    for base_search_path in folders_to_search:
        # filter by repo_name, if provided
        if repo_name and base_search_path.name != repo_name:
            continue
        # TODO: remove and use native Path.walk() after upgrade to Python 3.12
        # TODO?: remove and use native Path.rglob(resolve_symlinks=True) after upgrade to Python 3.13
        for root, dirs, files in pathlib_walk(base_search_path):
            for file in files:
                if re.match(''.join(re_search_pattern), file, flags=re.IGNORECASE) and 'deprecated' not in root.parts:
                    templates.append({"repo": base_search_path.name, "name": file})
    templates = sorted(templates, key=lambda x: (x["repo"], x["name"]))
    return templates


# TODO: remove support for string format after all routes/actions calling this method have been refactored
def get_template_file(template_blob: str | dict[str, str]) -> Path:
    template_folder: str | Path
    if isinstance(template_blob, dict):
        template_folder = template_blob['repo']
        template_name = template_blob['name']
    else:
        template_folder, template_name = template_blob.split(': ')
    if 'atlassian-aws-deployment' in template_folder:
        template_folder = Path('atlassian-aws-deployment/templates')
    else:
        template_folder = Path() / 'custom-templates' / template_folder
    return list(template_folder.glob(f"**/{template_name}"))[0]


def get_template_params(template: dict[str, str]) -> dict[str, dict[str, Any]]:
    yaml.add_multi_constructor('!', general_constructor, Loader=yaml.SafeLoader)
    return yaml.YAML(typ="safe", pure=True).load(open(get_template_file(template), 'r'))['Parameters']


def get_action_log(stack_name, id):
    try:
        log_file = glob.glob(f'stacks/{stack_name}/logs/*_{stack_name}_*_{id}.action.log')[0]
    except IndexError:
        return f'No log found for {stack_name} action with id: {id}'
    with open(log_file, 'r') as logfile:
        try:
            return logfile.read()
        except Exception:
            log.exception(f'Error occurred getting log for {stack_name} action #{id}')
    return ""


def get_latest_log(stack_name):
    log_filenames = glob.glob(f'stacks/{stack_name}/logs/*_{stack_name}_*.action.log')
    logs_by_time = {}
    if len(log_filenames) > 0:
        for log_filename in log_filenames:
            str_timestamp = log_filename[log_filename.rfind('/') + 1 : log_filename.find('_')]
            datetime_timestamp = datetime.strptime(str_timestamp, '%Y%m%d-%H%M%S')
            logs_by_time[log_filename] = datetime_timestamp
        sorted_logs = sorted(logs_by_time, key=lambda x: logs_by_time.get(x, -1), reverse=True)
        with open(sorted_logs[0], 'r') as logfile:
            try:
                return logfile.read()
            except Exception:
                log.exception(f'Error occurred getting log for {stack_name}')
    return False


def get_nice_action_name(action):
    switcher = {
        'admin': 'Admin',
        'clone': 'Clone',
        'create': 'Create',
        'destroy': 'Destroy',
        'diagnostics': 'Diagnostics',
        'ec2rollingreboot': 'EC2 Rolling Reboot',
        'fullrestart': 'Full Restart',
        'restartnode': 'Single-Node Restart',
        'rollingrestart': 'Rolling Restart',
        'rollingrebuild': 'Rebuild Nodes',
        'rollingreinit': 'Re-initialize Nodes',
        'runsql': 'Run SQL',
        'syslog': 'System Logs',
        'togglenode': 'Toggle Node Registration',
        'token': 'Token Admin',
        'update': 'Update',
        'upgrade': 'Upgrade',
        'viewlog': 'Action Logs',
    }
    return switcher.get(action, '')


def restart_weaver():
    # get the parent process ID (the gunicorn master, not the worker)
    process = psutil.Process(getppid())
    pid = str(process.pid)
    log.warning(f'Weaver restarting via admin function on pid {pid}')
    if 'gunicorn' in str(process.cmdline()):
        subprocess.run(['/bin/kill', '-HUP', pid])  # nosec B603
        return True
    else:
        log.warning('*** Restarting only supported in gunicorn. Please restart/reload manually ***')
        return False


def get_weaver_settings():
    # use first region in config.py if no region selected (eg first load)
    if 'region' not in session:
        session['region'] = current_app.config['REGIONS'][0][0]
    session['stacks'] = get_stacks()
    session['avatar_url'] = current_app.config['AVATAR_URL']
    session['weaver_version'] = session.get('weaver_version') or f'{get_weaver_revision(git.Repo(Path(dirname(current_app.root_path))))}'
    session['platform'] = current_app.config['PLATFORM'] if 'PLATFORM' in current_app.config else 'cfn'
    session['products'] = current_app.config['PRODUCTS']
    session['regions'] = current_app.config['REGIONS']
    session['stack_locking'] = current_app.config['STACK_LOCKING']
    session['stackname_length'] = current_app.config['STACKNAME_LENGTH']


def get_username():
    try:
        if 'saml' in session:
            return session['saml']['subject']
        elif verify_jwt_in_request(optional=True):
            return get_jwt()['sub']
        else:
            return getpass.getuser()
    except ExpiredSignatureError as e:
        # token is expired; we don't care (this path is not used for authenticating), we just need the token's creator/user
        return e.jwt_data['sub']  # type: ignore[attr-defined]
    except Exception:
        log.exception('Failed to retrieve username from SAML, JWT or the OS')
        return False


def get_stacks(region=None):
    from weaver.aws_cfn.cfn_resources import get_cfn_stacknames
    from weaver.kubernetes.k8s_resources import get_k8s_stacknames

    if 'PLATFORM' in current_app.config and current_app.config['PLATFORM'] == 'k8s':
        return get_k8s_stacknames()
    else:
        return get_cfn_stacknames(region)


def init_stack(stack_name, region):
    from weaver.aws_cfn.cfn_stack import CfnStack as CfnStack
    from weaver.kubernetes.k8s_stack import StatefulSet as K8sStack

    return K8sStack(stack_name, region) if 'PLATFORM' in current_app.config and current_app.config['PLATFORM'] == 'k8s' else CfnStack(stack_name, region)


def get_stack_action_in_progress(stack_name):
    if 'LOCKS' in current_app.config and stack_name in current_app.config['LOCKS']:
        return current_app.config['LOCKS'][stack_name]
    return False


def check_stack_environment_accessible(stack: Stack, action_name: str, stack_env: str):
    if not stack_env:
        stack_env = stack.get_tag('environment', log_msgs=False)
    if not stack_env and action_name not in ('create', 'clone'):
        # There are no tags on the stack object at the beginning of a create/clone
        raise weaver_exceptions.StackMissingTag("Stack needs an 'environment' tag to be supported by Weaver")
    weaver_env: str = getenv('ATL_ENVIRONMENT', 'local')
    if 'ACCESSIBLE_ENVIRONMENTS' not in current_app.config:
        raise weaver_exceptions.MissingConfiguration("Config settings is missing 'ACCESSIBLE_ENVIRONMENTS'")
    if weaver_env not in current_app.config['ACCESSIBLE_ENVIRONMENTS']:
        raise weaver_exceptions.MissingConfiguration(f"Config setting 'ACCESSIBLE_ENVIRONMENTS' is missing entries for Weaver env '{weaver_env}'")
    accessible_envs: List[str] = current_app.config['ACCESSIBLE_ENVIRONMENTS'][weaver_env]
    if stack_env not in accessible_envs:
        raise weaver_exceptions.StackEnvironmentInaccessible(
            f"Config setting 'ACCESSIBLE_ENVIRONMENTS' does not permit accessing stacks with environment '{stack_env}', please try another Weaver instance that is configured to support this environment."
        )


def create_action(stack: Stack, action_name: str, stack_env: str = '', changelog=True) -> str:
    check_stack_environment_accessible(stack, action_name, stack_env)
    if current_app.config['STACK_LOCKING']:
        action_in_progress = get_stack_action_in_progress(stack.stack_name)
        if action_in_progress:
            raise weaver_exceptions.StackActionInProgressException(action_name=action_in_progress)
    current_app.config['LOCKS'][stack.stack_name] = action_name
    action = StackAction(
        user=get_username() or 'unknown',
        stack_name=stack.stack_name,
        region=stack.region,
        action=action_name,
        status="IN_PROGRESS",
    )
    # store the action now so we can reference the id and started_at timestamp in log file creation below
    db.session.add(action)
    db.session.flush()
    action.log_file = stack.logfile = create_action_log(stack, action_name, action.id, action.started_at)
    if changelog:
        action.change_log_file = stack.changelogfile = create_changelog(stack.stack_name, action_name, action.id, action.started_at)
        actor = get_username()
        if actor:
            log_to_changelog(stack.changelogfile, f'Action triggered by {actor}')
    # store the updated record with the logfiles
    db.session.commit()
    return action.id


def finish_action(future: Future):
    action: StackAction = db.session.get(StackAction, g.action_id)  # type: ignore[attr-defined]
    stack = init_stack(action.stack_name, action.region)
    stack.logfile = action.log_file
    stack.changelogfile = action.change_log_file
    try:
        result = future.result()
    except weaver_exceptions.StackDeregistrationDelaysNotFullValueException as e:
        result = False
        # There might be a fast drain action in progress, don't restore deregistration delays
        stack.restore_stack_state_if_needed(action.action, deregistration_delays=False)
        log.exception(f"Stack action {action.action} with id {action.id} failed")
        log_to_ui(stack, ERROR, f"Stack action {action.action} with id {action.id} failed: {e}", write_to_changelog=False)
    except Exception as e:
        result = False
        stack.restore_stack_state_if_needed(action.action)
        log.exception(f"Stack action {action.action} with id {action.id} failed")
        log_to_ui(stack, ERROR, f"Stack action {action.action} with id {action.id} failed: {e}", write_to_changelog=False)
    else:
        stack.restore_stack_state_if_needed(action.action)
    if action.change_log_file:
        save_changelog(stack)
    if action.stack_name in current_app.config['LOCKS']:
        del current_app.config['LOCKS'][action.stack_name]
    action.status = "COMPLETE"
    action.result = "SUCCESS" if result else "FAILURE"
    db.session.commit()


def store_current_action(stack, action, id='123456', changelog=True):
    stack.logfile = create_action_log(stack, action, id)
    log_to_ui(stack, INFO, f"Action '{get_nice_action_name(action)}' with ID '{id}' has begun", write_to_changelog=False, send_sns_msg=False)
    if current_app.config['STACK_LOCKING']:
        action_already_in_progress = get_stack_action_in_progress(stack.stack_name)
        if action_already_in_progress:
            log_to_ui(stack, ERROR, f'Cannot begin action: {action}. Another action is in progress: {action_already_in_progress}', write_to_changelog=False)
            return False
    current_app.config['LOCKS'][stack.stack_name] = action
    if changelog:
        stack.changelogfile = create_changelog(stack.stack_name, action, id)
        try:
            actor = get_username()
        except Exception:
            log.exception('Failed to retrieve username from SAML or the OS')
            actor = False
        if actor:
            log_to_changelog(stack.changelogfile, f'Action triggered by {actor}')
    return True


def clear_current_action(stack):
    if stack.changelogfile is not None:
        save_changelog(stack)
    if 'LOCKS' in current_app.config and stack.stack_name in current_app.config['LOCKS']:
        del current_app.config['LOCKS'][stack.stack_name]
    return True


def wait_index_health(stack, node, personal_access_token):
    acceptable_index_health = current_app.config['ACCEPTABLE_INDEX_HEALTH']
    log_to_ui(stack, INFO, f'Waiting for index health to report > {acceptable_index_health}%', write_to_changelog=False)
    index_health = stack.get_index_health(node, personal_access_token)
    action_start = time.time()
    action_timeout = current_app.config['ACTION_TIMEOUTS']['check_index_health']
    try:
        float(index_health)
    except ValueError:
        log_to_ui(stack, WARN, 'Index health checking failed. Sleeping 30s and retying once.', write_to_changelog=False)
        time.sleep(30)
        index_health = stack.get_index_health(node, personal_access_token)
        try:
            float(index_health)
        except ValueError:
            log_to_ui(stack, ERROR, 'Index health checking failed a second time', write_to_changelog=False)
            return False
    while index_health == 503 or float(index_health) < float(acceptable_index_health):
        if (time.time() - action_start) > action_timeout:
            if index_health == 503:
                error_msg = f'Jira was returning 503 Maintenance for longer than {format_timespan(action_timeout)}'
            else:
                error_msg = f"Node {node} did not have index health over {acceptable_index_health}% after {format_timespan(action_timeout)}. Index health was {index_health}%"
            log_to_ui(
                stack,
                ERROR,
                error_msg,
                write_to_changelog=True,
            )
            return False
        if 'TESTING' not in current_app.config or current_app.config['TESTING'] is False:
            time.sleep(30)
        index_health = stack.get_index_health(node, personal_access_token)
    log_to_ui(stack, INFO, f'Index health on node {node} is {index_health}%', write_to_changelog=False)
    return True


##
# Git functions
#


def get_git_repo_base(repo_name):
    if repo_name == 'Weaver (requires restart)':
        repo = git.Repo(Path(dirname(current_app.root_path)))
    else:
        if repo_name != 'atlassian-aws-deployment':
            repo_name = f'custom-templates/{repo_name}'
        repo = git.Repo(Path(repo_name))
    return repo


def get_git_commit_difference(repo):
    try:
        behind = sum(1 for c in repo.iter_commits(f'HEAD..origin/{repo.active_branch.name}'))
        ahead = sum(1 for d in repo.iter_commits(f'origin/{repo.active_branch.name}..HEAD'))
    except (TypeError, git.exc.GitCommandError):
        behind = -1
        ahead = -1
    return [behind, ahead]


def get_git_revision(repo):
    return repo.head.object.hexsha


def get_weaver_revision(repo):
    git_hash = get_git_revision(repo)[:7]
    diff = get_git_commit_difference(repo)
    if int(diff[0]) > 0:
        update_available = '(update available)'
    else:
        update_available = '(up to date)'
    return f'{git_hash} {update_available}'


##
# Overrides
#


def general_constructor(loader, tag_suffix, node):
    return node.value


##
# Front-end helpers
#


def create_toast_alert(
    type: Literal['info', 'success', 'error', 'warning'], title: str, body: Optional[Union[list[str], str]] = None, close: Optional[Literal['auto', 'manual', 'never']] = None
) -> dict[str, str]:
    toast_alert: dict[str, str] = {'type': type, 'title': title}
    if body:
        if isinstance(body, list):
            toast_alert['body'] = ''.join([f'<p>{line}</p>' for line in body])
        else:
            toast_alert['body'] = f'<p>{body}</p>'
    if close:
        toast_alert['close'] = close
    return toast_alert


##
# Other
#


def chunk(list_to_chunk: list, size_of_chunks: int) -> Iterator[list]:
    """
    Yield successive n-sized chunks from l.
    """
    for i in range(0, len(list_to_chunk), size_of_chunks):
        yield list_to_chunk[i : i + size_of_chunks]


def force_cache_update(*args, **kwargs) -> bool:
    return kwargs.get('force', False)


def tag_list_to_dict(tags: Sequence[TagTypeDef]) -> dict[str, str]:
    return {tag["Key"]: tag["Value"] for tag in tags}


def tag_dict_to_list(labels: dict[str, str]) -> Sequence[TagTypeDef]:
    return [{"Key": key, "Value": value} for key, value in labels.items() if value]


# Adapted from Python 3.12's version of pathlib; follows symlinks
# TODO: remove and use native Path.walk() after upgrade to Python 3.12
def pathlib_walk(root_path, top_down=True, on_error=None):
    root_dir = str(root_path)
    results = walk(root_dir, top_down, on_error, followlinks=True)
    for path_str, dirnames, filenames in results:
        if root_dir == '.':
            path_str = path_str[2:]
        yield Path(path_str), dirnames, filenames
