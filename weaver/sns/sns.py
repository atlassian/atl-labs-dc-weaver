import json
import time

import boto3
from flask import current_app
from logging import ERROR, getLogger

import weaver.weaver_logging.weaver_logging as weaver_logging
from weaver import utils

log = getLogger('app_log')


def get_sns_topic_arn(stack):
    sns = boto3.client('sns', region_name=current_app.config['SNS_REGION'])
    try:
        topics = sns.list_topics()
        if 'Topics' in topics:
            for topic in topics['Topics']:
                if 'weaver-action-msgs' in topic['TopicArn']:
                    return topic['TopicArn']
    except Exception as e:
        log.exception('Error occurred getting sns topic')
        weaver_logging.log_to_ui(stack, ERROR, f'Error occurred getting sns topic: {e}', write_to_changelog=True)
        return False


def send_sns_msg(stack, action_msg):
    sns = boto3.client('sns', region_name=current_app.config['SNS_REGION'])
    try:
        topic_arn = get_sns_topic_arn(stack)
        if not topic_arn:
            sns.create_topic(Name='weaver-action-msgs')
            topic_arn = get_sns_topic_arn(stack)
        published_msg = sns.publish(
            TopicArn=topic_arn,
            Message=json.dumps(
                {
                    'default': json.dumps(
                        {
                            "user": utils.get_username(),
                            "msg": action_msg,
                            "stack": getattr(stack, 'stack_name', stack),
                            "timestamp": str(int(time.time())),
                            "tags": json.dumps({"name": "Action performed by Weaver"}),
                        }
                    )
                }
            ),
            MessageStructure='json',
        )
        return published_msg
    except Exception as e:
        log.exception('Error occurred sending sns message')
        weaver_logging.log_to_ui(stack, ERROR, f'Error occurred sending sns message: {e}', write_to_changelog=True)
