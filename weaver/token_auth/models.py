from weaver.extensions import db


class ApiToken(db.Model):  # type: ignore[name-defined]
    id = db.Column(db.Integer, primary_key=True)
    token_type = db.Column(db.Enum("access", "refresh", name="api_token_types"), nullable=False)
    revoked = db.Column(db.Boolean, nullable=False, default=False)
    user_name = db.Column(db.String, nullable=False)
    jti = db.Column(db.String(36), index=True, nullable=False, unique=True)
    created_at = db.Column(db.DateTime, nullable=False)
    expires_at = db.Column(db.DateTime, nullable=False)
    refresh_jti = db.Column(db.String(36))
    revoked_at = db.Column(db.DateTime)
