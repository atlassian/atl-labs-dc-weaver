#!/usr/bin/env python3

from __future__ import annotations

import logging

from datetime import datetime, timezone
from os import path

from flask import Blueprint, current_app, abort
from flask.views import MethodView
from flask_jwt_extended import create_access_token, create_refresh_token, decode_token, get_jwt, get_jwt_identity, jwt_required
from webargs import fields

from weaver.api.helpers import auth_required, use_kwargs
from weaver.extensions import db, jwt
from weaver.saml_auth.saml_auth import get_saml_user_and_groups, load_permissions_file, PERMISSIONS_FILE_NAME

# from .models import ApiToken, ApiTokenSchema
from .models import ApiToken

token_blueprint = Blueprint('token_auth', __name__)

log = logging.getLogger('app_log')


def build_access_token_authorization_details(user_groups):
    permissions_file_path = path.join(current_app.instance_path, PERMISSIONS_FILE_NAME)
    permissions = load_permissions_file(permissions_file_path)
    authorization_details: list[dict] = []
    if "groups" not in permissions:
        return authorization_details
    api_routes = [rule for rule in current_app.url_map.iter_rules() if rule.rule.startswith('/api')]
    for group in permissions["groups"]:
        if group["name"] in user_groups:
            for group_role_name, group_perms in group['roles'].items():
                auth_layer = {}
                weaver_role = next(role for role in permissions['roles'] if group_role_name == role['name'])
                auth_layer["stacks"] = group_perms["stacks"]
                auth_layer["regions"] = group_perms["regions"]
                auth_layer["methods"] = weaver_role["http_methods"]
                auth_layer["routes"] = []
                if "*" in weaver_role["weaver_methods"]:
                    auth_layer["routes"].append("*")
                else:
                    for method in weaver_role["weaver_methods"]:
                        for rule in api_routes:
                            if rule.endpoint == f"api.{method.lower()}":
                                auth_layer["routes"].append(rule.rule)
                authorization_details.append(auth_layer)
    return authorization_details


# Callback function to check if a JWT exists in the database blocklist
@jwt.token_in_blocklist_loader
def check_if_token_revoked(jwt_header, jwt_payload: dict) -> bool:
    token = db.session.execute(db.select(ApiToken).filter_by(jti=jwt_payload["jti"])).scalar()  # type: ignore[attr-defined]
    return token.revoked


# Callback function to check if a JWT is valid
@jwt.invalid_token_loader
def my_invalid_token_callback(errormessage: str):
    if errormessage == "Not enough segments":
        return {
            "error_msg": f"422 Error: '{errormessage}'. The JWT refresh token may be malformed. Check that it was copied correctly, or create a new one in Weaver - Token Admin."
        }, 422
    else:
        return {"error_msg": errormessage}, 422


# /api/v1/token/refresh
class RefreshTokenMethods(MethodView):
    decorators = [auth_required(auth_type="saml")]

    def post(self):
        # get the user and their saml groups
        user_name, groups = get_saml_user_and_groups()

        # create refresh token
        refresh_token = create_refresh_token(identity=user_name, additional_claims={"groups": groups})

        # decode the token to retrieve encoded data
        refresh_token_decoded = decode_token(refresh_token)
        refresh_token_created_at = datetime.fromtimestamp(refresh_token_decoded["iat"], tz=timezone.utc)
        refresh_token_expiry = datetime.fromtimestamp(refresh_token_decoded["exp"], tz=timezone.utc)

        # save selected token details to the database
        db.session.add(
            ApiToken(
                created_at=refresh_token_created_at,
                expires_at=refresh_token_expiry,
                jti=refresh_token_decoded["jti"],
                token_type=refresh_token_decoded["type"],
                user_name=user_name,
            )
        )
        db.session.commit()

        log.info(f"Refresh token {refresh_token_decoded['jti']} created by {user_name}; expires: {refresh_token_expiry}")
        return {
            "jti": refresh_token_decoded["jti"],
            "created_at": refresh_token_created_at.strftime("%Y-%m-%d %H:%M:%S"),
            "expires_at": refresh_token_expiry.strftime("%Y-%m-%d %H:%M:%S"),
            "refresh_token": refresh_token,
        }, 200

    @use_kwargs({"jti": fields.Str(required=True)})
    def delete(self, jti):
        user_name = get_saml_user_and_groups()[0]
        token = db.session.execute(db.select(ApiToken).filter_by(jti=jti)).scalar()  # type: ignore[attr-defined]
        if token and token.revoked is False:
            token.revoked = True
            token.revoked_at = datetime.now(tz=timezone.utc)
            db.session.commit()
        else:
            abort(404, "Token not found or already revoked")
        for access_token in db.session.execute(db.select(ApiToken).filter_by(refresh_jti=jti)).scalars():  # type: ignore[attr-defined]
            access_token.revoked = True
            access_token.revoked_at = datetime.now(tz=timezone.utc)
        db.session.commit()
        log.info(f"Refresh token {token.jti} and all associated access tokens revoked by {user_name}")
        return {"message": "Token revoked"}, 200


# /api/v1/token/access
class AccessTokenMethods(MethodView):
    @jwt_required(refresh=True)
    def post(self):
        user_name = get_jwt_identity()
        refresh_token = get_jwt()
        access_token = create_access_token(
            identity=user_name,
            additional_claims={
                "authorization_details": build_access_token_authorization_details(refresh_token["groups"]),
            },
        )
        access_token_decoded = decode_token(access_token)
        access_token_expiry = datetime.fromtimestamp(access_token_decoded["exp"], tz=timezone.utc)
        db.session.add(
            ApiToken(
                created_at=datetime.fromtimestamp(access_token_decoded["iat"], tz=timezone.utc),
                expires_at=access_token_expiry,
                jti=access_token_decoded["jti"],
                refresh_jti=refresh_token["jti"],
                token_type=access_token_decoded["type"],
                user_name=user_name,
            )
        )
        db.session.commit()
        log.info(f"Access token {access_token_decoded['jti']} created by {user_name}; expires: {access_token_expiry}")
        return {
            "access_token": access_token,
            "expires_at": access_token_expiry.strftime("%Y-%m-%d %H:%M:%S"),
        }, 200

    @jwt_required(verify_type=False)
    def delete(self):
        jti = get_jwt()["jti"]
        token = db.session.execute(db.select(ApiToken).filter_by(jti=jti)).scalar()  # type: ignore[attr-defined]
        if token and token.revoked is False:
            token.revoked = True
            token.revoked_at = datetime.now(tz=timezone.utc)
            db.session.commit()
        else:
            abort(404, "Token not found or already revoked")
        return {"message": "Token revoked"}, 200
