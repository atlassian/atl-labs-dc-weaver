import os
import weaver

env = os.environ.get('ATL_ENVIRONMENT')
if not env:
    # assume that any environment that's not defined is a local environment
    app = weaver.create_app('weaver.config.DebugConfig')
else:
    app = weaver.create_app('weaver.config.BaseConfig')

if __name__ == '__main__':
    run_params = {
        'port': 8000,
        'threaded': True,
    }

    # If we're running locally (i.e., debug config) and SESSION_COOKIE_SECURE=True, we need ssl_context set due to the change in SameSite cookie behaviour
    # https://developers.google.com/search/blog/2020/01/get-ready-for-new-samesitenone-secure
    if app.debug and app.config.get('SESSION_COOKIE_SECURE'):
        run_params['ssl_context'] = 'adhoc'  # type: ignore[assignment]
        run_params['host'] = '0.0.0.0'  # type: ignore[assignment]  # nosec B104

    app.run(**run_params)
