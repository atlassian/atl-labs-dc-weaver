# weaver gunicorn config

import multiprocessing

from gunicorn.glogging import CONFIG_DEFAULTS

accesslog = 'logs/weaver_access.log'
access_log_format = '%({x-forwarded-for}i)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
bind = ['0.0.0.0:8000']
capture_output = True
disable_redirect_access_to_syslog = True
errorlog = 'logs/weaver.log'
forwarded_allow_ips = '*'
logconfig_dict = CONFIG_DEFAULTS | {
    'formatters': {
        'generic': {
            'format': '%(asctime)s [%(process)d] [%(levelname)s] [%(thread)d] [%(threadName)s] %(message)s',
            'datefmt': '[%Y-%m-%d %H:%M:%S %z]',
            'class': 'logging.Formatter',
        }
    },
    'loggers': {
        'gunicorn.error': {'level': 'INFO', 'handlers': ['error_console'], 'propagate': False, 'qualname': 'gunicorn.error'},
    },
    'root': {'level': 'INFO', 'handlers': []},
}
timeout = 600
worker_class = 'gevent'
workers = multiprocessing.cpu_count() * 2
wsgi_app = 'run:app'
