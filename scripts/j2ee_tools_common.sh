#!/bin/bash

umask 0022

unset SERVICE_NAME
unset SIDECAR_PID

errexit() {
    echo "ERROR: $1"
    exit 1
}

fancywait() {
    FOO=$1
    echo -n "Sleeping $1 seconds "
    while [ $FOO -ne 0 ] ; do
        sleep 1
        echo -n "."
        FOO=$((FOO-1))
    done
    echo -ne "\r\033[K"
}

update_permission() {
    ITEM=$1
    if [ ! -e "$ITEM" ] ; then
        echo "ERROR: $ITEM does not exist!"
        exit 1
    fi
    if [ -d "$ITEM" ] ; then
        if [[ "$UID" -eq 0 ]] ; then
            /bin/chgrp atlassian-staff $ITEM
        fi
        chmod 2775 $ITEM
    elif [ -f "$ITEM" ] ; then
        chmod 644 $ITEM
    fi
}

echo_process_and_user_info() {
    echo "SERVICE_PID = $SERVICE_PID"
    if [[ ! -z "$SIDECAR_PID" ]] ; then
        echo "SIDECAR_PID = $SIDECAR_PID"
    fi
    echo "PROCUSER = $PROCUSER"
    echo "LOGNAME = $LOGNAME"
    printf "\n"
}

upload_s3() {
    # upload a file to s3
    local FILE=${1}
    echo "Uploading to S3"
    local FILENAME=`basename ${FILE}`
    local IMDSV2_TOKEN=$(curl --silent -H "X-aws-ec2-metadata-token-ttl-seconds: 15" -X PUT "http://169.254.169.254/latest/api/token")
    local IP=$(curl --fail --silent -H "X-aws-ec2-metadata-token: $IMDSV2_TOKEN" http://169.254.169.254/latest/meta-data/local-ipv4 || echo "127.0.0.1")
    source /etc/atl
    local ACCOUNT_ID=`aws sts get-caller-identity | grep Account | awk {'print $2'} | sed 's/\"\|,//g'`
    local BUCKET=atl-labs-dc-weaver-${ACCOUNT_ID}
    local DEST="${BUCKET}/diagnostics/${ATL_AWS_STACK_NAME}/$(date +%Y-%m-%d)"
    aws s3 cp --content-type application/x-gzip "${FILE}" "s3://${DEST}/${ATL_AWS_STACK_NAME}_${IP}_${FILENAME}" || echo "Upload of ${ATL_AWS_STACK_NAME}_${IP}_${FILENAME} to ${DEST} Failed"
}

if [ -z $SERVICE_DIR ]; then
    echo "Attempting to auto-detect your service..."
    VALID_UPSTART_TYPES="confluence jira"  # bitbucket and mesh do not use upstart
    UPSTART_FOUND=
    for type in $VALID_UPSTART_TYPES ; do
        if [ -f "/etc/init/$type.conf" ] ; then
            UPSTART_FOUND=$type
            break
        fi
    done

    # Try upstart if found
    UPSTART_IS_VALID=0
    if [ ! -z "$UPSTART_FOUND" ] ; then
        STATUS_OUT=$(status $UPSTART_FOUND 2> /dev/null)
        if [ $? == 0 ] ; then
            UPSTART_IS_VALID=1
        fi
    fi

    if [ $UPSTART_IS_VALID == 0 ] ; then
        # try to find pid
        # confluence and jira use tomcat, bitbucket and mesh do not; try tomcat first
        SERVICE_PID=$(pgrep -f 'tomcat')
        if [ -z "$SERVICE_PID" ] ; then
            # try bitbucket
            SERVICE_PID=$(systemctl show --property MainPID bitbucket | cut -d= -f2)
            if [ -z "$SERVICE_PID" ] || [ "$SERVICE_PID" == 0 ] ; then
                # try mesh
                SERVICE_PID=$(systemctl show --property MainPID mesh | cut -d= -f2)
                if [ -z "$SERVICE_PID" ] ; then
                    echo "Cannot auto detect service; please supply the service name (eg bitbucket|confluence|jira|mesh)"
                    exit 3
                else
                    SERVICE_NAME="mesh"
                fi
            else
                SERVICE_NAME="bitbucket"
            fi
        fi
    else
        if [[ ! "$STATUS_OUT" =~ ([0-9]+)$ ]] ; then
            echo "Service does not appear to be running"
        else
            echo "Detected upstart service - ${UPSTART_FOUND}"
            SERVICE_PID=${BASH_REMATCH[1]}
        fi
    fi
else
    if [[ "$SERVICE_DIR" =~ ^/ ]] ; then
        echo "Please supply the service name (eg bitbucket|confluence|jira|mesh)"
    else
        UPSTART_FOUND=$SERVICE_DIR
        STATUS_OUT=$(status $UPSTART_FOUND 2> /dev/null)
        if [ $? != 0 ] || [[ ! "$STATUS_OUT" =~ ([0-9]+)$ ]] ; then
            # if service is not upstart, try to find pid
            # confluence and jira use tomcat, bitbucket does not; try tomcat first
            SERVICE_PID=$(pgrep -f 'tomcat')
            if [ -z "$SERVICE_PID" ] ; then
                # try bitbucket
                SERVICE_PID=$(systemctl show --property MainPID bitbucket | cut -d= -f2)
                if [ -z "$SERVICE_PID" ] || [ "$SERVICE_PID" == 0 ] ; then
                    # try mesh
                    SERVICE_PID=$(systemctl show --property MainPID mesh | cut -d= -f2)
                    if [[ ! -z "$SERVICE_PID" && "$SERVICE_PID" != 0 ]] ; then
                        SERVICE_NAME="mesh"
                    fi
                else
                    SERVICE_NAME="bitbucket"
                fi
            fi
            if [ -z "$SERVICE_PID" ] ; then
                echo "Supplied service ${UPSTART_FOUND} does not seem to be up, aborting"
                exit 3
            else
                echo "PID for service ${UPSTART_FOUND} detected"
            fi
        else
            echo "Upstart service - ${UPSTART_FOUND} detected"
            SERVICE_PID=${BASH_REMATCH[1]}
        fi
    fi
fi

# Set service name
if [[ ! -z "$SERVICE_NAME" ]]; then
    # bitbucket/mesh service name is handled above
    :
elif [ ! -z "$UPSTART_FOUND" ] ; then
    SERVICE_NAME=${UPSTART_FOUND}
else
    SERVICE_NAME="$(grep ATL_DB_NAME /etc/atl | cut -d= -f2)"
fi

# Make sure this PID exists and is sane - otherwise, bail out.
if [ -z "$SERVICE_PID" ] ; then
    echo "Unable to determine service pid of running app!"
    exit 4
fi

if [ ! -e /proc/${SERVICE_PID}/status ]; then
    echo "Couldn't find details about SERVICE_PID \"$SERVICE_PID\" - bailing out"
    exit 3
fi

# OK - SERVICE_PID seems valid...let's get some details about it...
JAVA=$(ps --no-headers -o cmd ${SERVICE_PID} | awk '{print $1}')
SERVICE_JAVA_HOME=${JAVA%/bin/java}
PROCESS=`awk '/^Name/ {print $2}' /proc/${SERVICE_PID}/status`
PROCUID=`awk '/^Uid/ {print $2}' /proc/${SERVICE_PID}/status`
PROCUSER=$(getent passwd ${PROCUID})
PROCUSER=${PROCUSER%%:*}

# If we're a bitbucket service, grab the sidecar process id if it is running
if [ "$SERVICE_NAME" == "bitbucket" ] ; then
    # Ref: https://confluence.atlassian.com/bitbucketserverkb/generate-a-thread-dump-externally-779171716.html
    SIDECAR_PID="$(ps aux | grep -v search | grep -i java | grep 'mesh-app.jar' | awk  -F '[ ]*' '{print $2}' 2> /dev/null)"
fi

TMPDIR=/tmp
SCRATCHDIR=/scratch

if [ -d $SCRATCHDIR ]; then
    STORE_DIR=$SCRATCHDIR
else
    STORE_DIR=$TMPDIR
fi

if [[ $0 =~ (thread|heap)_dump ]] ; then
    DUMP_DIR_EXT=${BASH_REMATCH[0]}
    STORE_DIR=$STORE_DIR/$DUMP_DIR_EXT
fi

if [ ! -d "$STORE_DIR" ] ; then
    mkdir $STORE_DIR
    /bin/chown $PROCUSER $STORE_DIR
fi
