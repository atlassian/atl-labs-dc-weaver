import os

from moto import mock_aws

import weaver
import pytest


# citation: https://www.patricksoftwareblog.com/testing-a-flask-application-using-pytest/


@pytest.fixture(scope='module')
def test_client():
    os.environ["NO_SAML"] = "1"
    app = weaver.create_app('weaver.config.BaseConfig')
    testing_client = app.test_client()
    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    yield testing_client  # this is where the testing happens!

    ctx.pop()
    # app.run(threaded=True, debug=False, host='0.0.0.0', port=8000)


@mock_aws
def test_api_root(test_client):
    """
    GIVEN the Weaver Flask application
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('http://127.0.0.1:8000/')
    assert response.status_code == 200
    return


def test_api_status(test_client):
    """
    GIVEN the Weaver Flask application
    WHEN the '/status' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('http://127.0.0.1:8000/status')
    assert response.status_code == 200
    return


class TestApiStackInfo:
    def test_api_get_logs(self, test_client):
        return


class TestApiHelpers:
    def test_api_get_ebs_snapshots(self, test_client):
        return
