#!/usr/bin/env python3

import pytest
import shutil

from pathlib import Path

import weaver.utils as utils


def find_project_root_dir(start_path: Path) -> Path:
    marker = ".git"
    current_path = start_path
    while current_path != current_path.parent:
        if (current_path / marker).exists() and (current_path / marker).is_dir():
            return current_path
        current_path = current_path.parent
    raise FileNotFoundError("Could not find the project root")


@pytest.fixture
def custom_templates_path():
    root_dir = find_project_root_dir(Path(__file__).resolve())
    temp_repo = root_dir / "custom-templates" / "unit-test-templates"
    if temp_repo.exists():
        shutil.rmtree(temp_repo)
    temp_repo.mkdir(parents=True, exist_ok=True)
    return temp_repo


@pytest.fixture
def custom_templates(custom_templates_path):
    fake_templates = [
        custom_templates_path / "FakeJira.template.yaml",
        custom_templates_path / "FakeConfluence.template.yaml",
    ]
    for file in fake_templates:
        file.touch()
    return fake_templates


class TestListTemplates:
    def test_list_no_templates(self, custom_templates_path):
        templates = utils.list_templates(product=None, repo_name=custom_templates_path.name)
        assert isinstance(templates, list)
        assert len(templates) == 0

    def test_list_templates(self, custom_templates_path, custom_templates):
        templates = utils.list_templates(product=None, repo_name=custom_templates_path.name)
        assert isinstance(templates, list)
        assert len(templates) > 0
        assert isinstance(templates[0], dict)
        assert list(templates[0].keys()) == ["repo", "name"]
        assert isinstance(templates[0]["repo"], str)
        assert isinstance(templates[0]["name"], str)
        assert sorted(templates, key=lambda x: (x["repo"], x["name"])) == templates
        for template in templates:
            corresponding_file = next(f for f in custom_templates if f.name == template["name"])
            assert corresponding_file
            assert template["repo"] in corresponding_file.parts
