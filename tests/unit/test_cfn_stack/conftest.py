from unittest.mock import MagicMock

import boto3
from moto import mock_aws
import pytest

import weaver
from weaver.aws_cfn.cfn_stack import CfnStack

TEST_AWS_REGION = 'ap-southeast-2'


# Workaround for disabling cache
@pytest.fixture
def test_app():
    app = weaver.create_app('weaver.config.TestingConfig')
    return app


@pytest.fixture
def test_cfn_stack():
    test_cfn_stack = CfnStack('test-stack', TEST_AWS_REGION)
    return test_cfn_stack


@pytest.fixture
def mock_boto3_cfn():
    boto3_cfn = MagicMock()
    return boto3_cfn


@mock_aws
@pytest.fixture
def mock_bitbucket_primary_stack_node():
    ec2 = boto3.resource('ec2', region_name=TEST_AWS_REGION)
    mock_node = ec2.Instance('bitbucket-primary-node')
    return mock_node
