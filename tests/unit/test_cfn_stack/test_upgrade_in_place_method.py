from unittest.mock import patch, call, MagicMock, ANY

import pytest

import weaver.enums as weaver_enums


def get_tag_side_effect_product_only(product: str):
    def side_effect(tag_key):
        if tag_key == 'product':
            return product
        else:
            return False

    return side_effect


class TestBitbucketMesh:
    MOCK_MESH_VERSION = '2.5.1'
    MOCK_ALT_DOWNLOAD_URL = 'https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-mesh-2.5.1.tar.gz'
    MOCK_NUM_NODES = 5

    @pytest.fixture
    def test_cfn_stack_with_mocks(self, test_cfn_stack):
        test_cfn_stack.get_tag = MagicMock(side_effect=get_tag_side_effect_product_only('bitbucket_mesh'))
        test_cfn_stack.has_alb = MagicMock(return_value=False)
        test_cfn_stack.has_nlb = MagicMock(return_value=False)
        test_cfn_stack.get_pre_upgrade_information = MagicMock()
        test_cfn_stack.check_nfs_server_replacement = MagicMock(return_value=False)
        test_cfn_stack.get_primary_stack_nodes = MagicMock()
        test_cfn_stack.shutdown_app = MagicMock(return_value=True)
        test_cfn_stack.get_stacknodes = MagicMock()
        test_cfn_stack.toggle_cfn_hup = MagicMock(return_value=True)
        test_cfn_stack.update_version_and_product_download_url = MagicMock(return_value=True)
        test_cfn_stack.execute_cfn_init_parallel = MagicMock(return_value=True)
        test_cfn_stack.validate_nodes_responding = MagicMock(return_value=True)
        test_cfn_stack.startup_app = MagicMock(return_value=True)
        return test_cfn_stack

    def test_standard_upgrade(
        self,
        test_cfn_stack_with_mocks,
        mock_bitbucket_primary_stack_node,
    ):
        test_cfn_stack_with_mocks.get_primary_stack_nodes.return_value = [mock_bitbucket_primary_stack_node]
        test_cfn_stack_with_mocks.preupgrade_app_node_count = self.MOCK_NUM_NODES
        manager = MagicMock()
        manager.attach_mock(test_cfn_stack_with_mocks.toggle_cfn_hup, 'mock_toggle_cfn_hup')
        manager.attach_mock(test_cfn_stack_with_mocks.update_version_and_product_download_url, 'mock_update_version_and_product_download_url')
        manager.attach_mock(test_cfn_stack_with_mocks.execute_cfn_init_parallel, 'mock_execute_cfn_init_parallel')
        manager.attach_mock(test_cfn_stack_with_mocks.validate_nodes_responding, 'mock_validate_nodes_responding')
        assert test_cfn_stack_with_mocks.upgrade_in_place(self.MOCK_MESH_VERSION, self.MOCK_ALT_DOWNLOAD_URL)
        # Ensure we stop cfn-hup before stack updates, reinit all nodes at the same time.
        manager.assert_has_calls(
            [
                call.mock_toggle_cfn_hup('stop'),
                call.mock_update_version_and_product_download_url(self.MOCK_MESH_VERSION, self.MOCK_ALT_DOWNLOAD_URL),
                call.mock_execute_cfn_init_parallel(ANY),
                call.mock_validate_nodes_responding(ANY),
            ]
        )

    def test_skip_primary_startup_upgrade(
        self,
        test_cfn_stack_with_mocks,
        mock_bitbucket_primary_stack_node,
    ):
        test_cfn_stack_with_mocks.get_primary_stack_nodes.return_value = [mock_bitbucket_primary_stack_node]
        test_cfn_stack_with_mocks.preupgrade_app_node_count = self.MOCK_NUM_NODES
        manager = MagicMock()
        manager.attach_mock(test_cfn_stack_with_mocks.toggle_cfn_hup, 'mock_toggle_cfn_hup')
        manager.attach_mock(test_cfn_stack_with_mocks.update_version_and_product_download_url, 'mock_update_version_and_product_download_url')
        manager.attach_mock(test_cfn_stack_with_mocks.execute_cfn_init_parallel, 'mock_execute_cfn_init_parallel')
        manager.attach_mock(test_cfn_stack_with_mocks.validate_nodes_responding, 'mock_validate_nodes_responding')
        assert test_cfn_stack_with_mocks.upgrade_in_place(self.MOCK_MESH_VERSION, self.MOCK_ALT_DOWNLOAD_URL, startup_primary=False)
        # Ensure we stop cfn-hup before stack updates, reinit all nodes at the same time.
        manager.assert_has_calls(
            [
                call.mock_toggle_cfn_hup('stop'),
                call.mock_update_version_and_product_download_url(self.MOCK_MESH_VERSION, self.MOCK_ALT_DOWNLOAD_URL),
                call.mock_execute_cfn_init_parallel(ANY),
                call.mock_validate_nodes_responding(ANY),
            ]
        )
        # Ensure we haven't started up the primary cluster
        test_cfn_stack_with_mocks.startup_app.assert_not_called()


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_tag', side_effect=get_tag_side_effect_product_only('bitbucket_mirror'))
@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_pre_upgrade_information')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.check_nfs_server_replacement', return_value=False)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.shutdown_app', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_stacknodes')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.toggle_cfn_hup', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.update_version_and_product_download_url', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.begin_deregistering_nodes_from_all_target_groups', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.wait_nodes_registration_states', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.restore_original_deregistration_delays_if_needed', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.reinit_one_node_and_wait_healthy', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.reinit_remaining_nodes_and_wait_healthy', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.startup_app', return_value=True)
class TestBitbucketMirror:
    MOCK_VERSION = '8.19.1'
    MOCK_ALT_DOWNLOAD_URL = 'https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-8.19.1.tar.gz'
    MOCK_NUM_NODES = 21

    @pytest.mark.parametrize('drain_type', [weaver_enums.DrainType.DEFAULT, weaver_enums.DrainType.SKIP])
    def test_upgrade(
        self,
        mock_startup_app,
        mock_reinit_remaining_nodes_and_wait_healthy,
        mock_reinit_one_node_and_wait_healthy,
        mock_restore_original_deregistration_delays_if_needed,
        mock_wait_nodes_registration_states,
        mock_begin_deregistering_nodes_from_all_target_groups,
        mock_update_version_and_product_download_url,
        mock_toggle_cfn_hup,
        mock_get_stacknodes,
        mock_shutdown_app,
        mock_check_nfs_server_replacement,
        mock_get_pre_upgrade_information,
        mock_get_tag,
        drain_type,
        test_cfn_stack,
        mock_bitbucket_primary_stack_node,
    ):
        test_cfn_stack.preupgrade_app_node_count = self.MOCK_NUM_NODES
        manager = MagicMock()
        manager.attach_mock(mock_toggle_cfn_hup, 'mock_toggle_cfn_hup')
        manager.attach_mock(mock_update_version_and_product_download_url, 'mock_update_version_and_product_download_url')
        manager.attach_mock(mock_begin_deregistering_nodes_from_all_target_groups, 'mock_begin_deregistering_nodes_from_all_target_groups')
        manager.attach_mock(mock_reinit_one_node_and_wait_healthy, 'mock_reinit_one_node_and_wait_healthy')
        manager.attach_mock(mock_reinit_remaining_nodes_and_wait_healthy, 'mock_reinit_remaining_nodes_and_wait_healthy')
        assert test_cfn_stack.upgrade_in_place(self.MOCK_VERSION, self.MOCK_ALT_DOWNLOAD_URL, drain_type=drain_type)
        # Ensure the cfn-hup is stopped before stack updates...
        expected_calls = [call.mock_toggle_cfn_hup('stop'), call.mock_update_version_and_product_download_url(self.MOCK_VERSION, self.MOCK_ALT_DOWNLOAD_URL)]
        # and deregistration is skipped if drain type is set to SKIP...
        if drain_type != weaver_enums.DrainType.SKIP:
            expected_calls.append(call.mock_begin_deregistering_nodes_from_all_target_groups(ANY, fast_drain=(drain_type == weaver_enums.DrainType.FAST)))
        # and we reinit one node first before the rest.
        expected_calls.extend(
            [
                call.mock_reinit_one_node_and_wait_healthy(ANY, ANY, skip_node_drain=(drain_type == weaver_enums.DrainType.SKIP)),
                call.mock_reinit_remaining_nodes_and_wait_healthy(ANY, ANY, skip_node_drain=(drain_type == weaver_enums.DrainType.SKIP)),
            ]
        )
        manager.assert_has_calls(expected_calls)


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_tag', side_effect=get_tag_side_effect_product_only('bitbucket_mirror'))
@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_pre_upgrade_information')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.check_nfs_server_replacement', return_value=False)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.shutdown_app', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_stacknodes')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.toggle_cfn_hup', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.update_version_and_product_download_url', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.begin_deregistering_nodes_from_all_target_groups', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.wait_nodes_registration_states', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.restore_original_deregistration_delays_if_needed', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.reinit_one_node_and_wait_healthy', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.reinit_remaining_nodes_and_wait_healthy', return_value=True)
@patch('weaver.aws_cfn.cfn_stack.CfnStack.startup_app', return_value=True)
class TestBitbucketPrimary:
    MOCK_VERSION = '8.19.1'
    MOCK_ALT_DOWNLOAD_URL = 'https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-8.19.1.tar.gz'
    MOCK_NUM_NODES = 4

    @pytest.mark.parametrize('drain_type', [weaver_enums.DrainType.DEFAULT, weaver_enums.DrainType.SKIP])
    def test_upgrade(
        self,
        mock_startup_app,
        mock_reinit_remaining_nodes_and_wait_healthy,
        mock_reinit_one_node_and_wait_healthy,
        mock_restore_original_deregistration_delays_if_needed,
        mock_wait_nodes_registration_states,
        mock_begin_deregistering_nodes_from_all_target_groups,
        mock_update_version_and_product_download_url,
        mock_toggle_cfn_hup,
        mock_get_stacknodes,
        mock_shutdown_app,
        mock_check_nfs_server_replacement,
        mock_get_pre_upgrade_information,
        mock_get_tag,
        drain_type,
        test_cfn_stack,
        mock_bitbucket_primary_stack_node,
    ):
        test_cfn_stack.preupgrade_app_node_count = self.MOCK_NUM_NODES
        manager = MagicMock()
        manager.attach_mock(mock_toggle_cfn_hup, 'mock_toggle_cfn_hup')
        manager.attach_mock(mock_update_version_and_product_download_url, 'mock_update_version_and_product_download_url')
        manager.attach_mock(mock_begin_deregistering_nodes_from_all_target_groups, 'mock_begin_deregistering_nodes_from_all_target_groups')
        assert test_cfn_stack.upgrade_in_place(self.MOCK_VERSION, self.MOCK_ALT_DOWNLOAD_URL, drain_type=drain_type)
        # Ensure the cfn-hup is stopped before stack updates...
        expected_calls = [call.mock_toggle_cfn_hup('stop'), call.mock_update_version_and_product_download_url(self.MOCK_VERSION, self.MOCK_ALT_DOWNLOAD_URL)]
        # and deregistration is skipped if drain type is set to SKIP...
        if drain_type != weaver_enums.DrainType.SKIP:
            expected_calls.append(call.mock_begin_deregistering_nodes_from_all_target_groups(ANY, fast_drain=(drain_type == weaver_enums.DrainType.FAST)))
        # TODO: Add in calls for expected reinit logic - this will soon be changed to all at once.
        manager.assert_has_calls(expected_calls)


# TODO: Add tests for other products (Jira, Confluence, Crowd)
