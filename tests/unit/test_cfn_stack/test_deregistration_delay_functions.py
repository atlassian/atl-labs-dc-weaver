from typing import Callable
from unittest.mock import patch

import pytest
import weaver.enums as weaver_enums
import weaver.exceptions as weaver_exceptions


def lb_deregistration_delay_is_full_value_side_effect(alb_full_value: bool, nlb_full_value: bool) -> Callable[[weaver_enums.LoadBalancer], bool]:
    def side_effect(load_balancer):
        if load_balancer == weaver_enums.LoadBalancer.ALB:
            return alb_full_value
        elif load_balancer == weaver_enums.LoadBalancer.NLB:
            return nlb_full_value
        else:
            return False

    return side_effect


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_load_balancer_tg_arn')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_deregistration_delay')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_cfn_param_deregistration_delay')
class TestLbDeregistrationDelayIsFullValue:
    @pytest.mark.parametrize('load_balancer', [weaver_enums.LoadBalancer.ALB, weaver_enums.LoadBalancer.NLB])
    @pytest.mark.parametrize('deregistration_delay,cfn_param_deregistration_delay', [[10, 90], [10, 1200], [123, 456]])
    def test_mismatching_values(
        self,
        mock_get_cfn_param_deregistration_delay,
        mock_get_deregistration_delay,
        mock_get_load_balancer_tg_arn,
        load_balancer,
        deregistration_delay,
        cfn_param_deregistration_delay,
        test_cfn_stack,
    ):
        mock_get_deregistration_delay.return_value = deregistration_delay
        mock_get_cfn_param_deregistration_delay.return_value = cfn_param_deregistration_delay
        assert not test_cfn_stack.lb_deregistration_delay_is_full_value(load_balancer)

    @pytest.mark.parametrize('load_balancer', [weaver_enums.LoadBalancer.ALB, weaver_enums.LoadBalancer.NLB])
    @pytest.mark.parametrize('deregistration_delay,cfn_param_deregistration_delay', [[90, 90], [1200, 1200], [456, 456]])
    def test_matching_values(
        self,
        mock_get_cfn_param_deregistration_delay,
        mock_get_deregistration_delay,
        mock_get_load_balancer_tg_arn,
        load_balancer,
        deregistration_delay,
        cfn_param_deregistration_delay,
        test_cfn_stack,
    ):
        mock_get_deregistration_delay.return_value = deregistration_delay
        mock_get_cfn_param_deregistration_delay.return_value = cfn_param_deregistration_delay
        assert test_cfn_stack.lb_deregistration_delay_is_full_value(load_balancer)

    @pytest.mark.parametrize('load_balancer', [weaver_enums.LoadBalancer.ALB, weaver_enums.LoadBalancer.NLB])
    def test_no_cfn_value(
        self,
        mock_get_cfn_param_deregistration_delay,
        mock_get_deregistration_delay,
        mock_get_load_balancer_tg_arn,
        load_balancer,
        test_cfn_stack,
    ):
        mock_get_cfn_param_deregistration_delay.side_effect = weaver_exceptions.StackParameterNotFound
        assert test_cfn_stack.lb_deregistration_delay_is_full_value(load_balancer)


@patch('weaver.aws_cfn.cfn_stack.CfnStack.lb_deregistration_delay_is_full_value')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.has_nlb')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.has_alb')
class TestStackDeregistrationValuesAreFullValue:
    @pytest.mark.parametrize('alb_full_value', [True, False])
    @pytest.mark.parametrize('nlb_full_value', [True, False])
    def test_alb_only(self, mock_has_alb, mock_has_nlb, mock_lb_deregistration_delay_is_full_value, alb_full_value, nlb_full_value, test_cfn_stack):
        mock_has_alb.return_value = True
        mock_has_nlb.return_value = False
        mock_lb_deregistration_delay_is_full_value.side_effect = lb_deregistration_delay_is_full_value_side_effect(alb_full_value, nlb_full_value)
        assert test_cfn_stack.stack_deregistration_delays_are_full_value() == alb_full_value

    @pytest.mark.parametrize('alb_full_value', [True, False])
    @pytest.mark.parametrize('nlb_full_value', [True, False])
    def test_nlb_only(self, mock_has_alb, mock_has_nlb, mock_lb_deregistration_delay_is_full_value, alb_full_value, nlb_full_value, test_cfn_stack):
        mock_has_alb.return_value = False
        mock_has_nlb.return_value = True
        mock_lb_deregistration_delay_is_full_value.side_effect = lb_deregistration_delay_is_full_value_side_effect(alb_full_value, nlb_full_value)
        assert test_cfn_stack.stack_deregistration_delays_are_full_value() == nlb_full_value

    @pytest.mark.parametrize('alb_full_value', [True, False])
    @pytest.mark.parametrize('nlb_full_value', [True, False])
    def test_alb_and_nlb(self, mock_has_alb, mock_has_nlb, mock_lb_deregistration_delay_is_full_value, alb_full_value, nlb_full_value, test_cfn_stack):
        mock_has_alb.return_value = True
        mock_has_nlb.return_value = True
        mock_lb_deregistration_delay_is_full_value.side_effect = lb_deregistration_delay_is_full_value_side_effect(alb_full_value, nlb_full_value)
        assert test_cfn_stack.stack_deregistration_delays_are_full_value() == (alb_full_value and nlb_full_value)
