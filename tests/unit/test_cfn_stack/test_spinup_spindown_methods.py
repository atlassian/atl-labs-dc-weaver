from unittest.mock import ANY, MagicMock, patch

import pytest

MOCK_OLD_VERSION = '7.12.2'
MOCK_NEW_VERSION = '7.13.12'

MOCK_PARAMS = {}
MOCK_PARAMS['ClusterNodeCountOnly'] = {}
MOCK_PARAMS['ClusterNodeCountOnly']['Full'] = [
    {
        'ParameterKey': 'ClusterNodeCount',
        'ParameterValue': '3',
    },
]
MOCK_PARAMS['ClusterNodeCountOnly']['Spundown'] = [{'ParameterKey': param['ParameterKey'], 'ParameterValue': '0'} for param in MOCK_PARAMS['ClusterNodeCountOnly']['Full']]

MOCK_PARAMS['AZNodeCountOnly'] = {}
MOCK_PARAMS['AZNodeCountOnly']['Full'] = [
    {
        'ParameterKey': 'Az1NodeCount',
        'ParameterValue': '2',
    },
    {
        'ParameterKey': 'Az2NodeCount',
        'ParameterValue': '1',
    },
    {
        'ParameterKey': 'Az3NodeCount',
        'ParameterValue': '4',
    },
]
MOCK_PARAMS['AZNodeCountOnly']['Spundown'] = [{'ParameterKey': param['ParameterKey'], 'ParameterValue': '0'} for param in MOCK_PARAMS['AZNodeCountOnly']['Full']]


MOCK_PARAMS['AZNodesAndBackup'] = {}
MOCK_PARAMS['AZNodesAndBackup']['Full'] = [
    {
        'ParameterKey': 'Az1NodeCount',
        'ParameterValue': '6',
    },
    {
        'ParameterKey': 'Az2NodeCount',
        'ParameterValue': '3',
    },
    {
        'ParameterKey': 'Az3NodeCount',
        'ParameterValue': '0',
    },
    {
        'ParameterKey': 'BackupNodeCount',
        'ParameterValue': '1',
    },
]
MOCK_PARAMS['AZNodesAndBackup']['Spundown'] = [{'ParameterKey': param['ParameterKey'], 'ParameterValue': '0'} for param in MOCK_PARAMS['AZNodesAndBackup']['Full']]

MOCK_PARAMS['ClusterAndSynchronyAndBot'] = {}
MOCK_PARAMS['ClusterAndSynchronyAndBot']['Full'] = [
    {
        'ParameterKey': 'ClusterNodeCount',
        'ParameterValue': '3',
    }
]
MOCK_PARAMS['ClusterAndSynchronyAndBot']['Spundown'] = [
    {'ParameterKey': param['ParameterKey'], 'ParameterValue': '0'} for param in MOCK_PARAMS['ClusterAndSynchronyAndBot']['Full']
]

for stack_type in MOCK_PARAMS:
    for stack_state in MOCK_PARAMS[stack_type]:
        MOCK_PARAMS[stack_type][stack_state].extend(
            [{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_OLD_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}]
        )


class TestSpindownToZeroAppnodes:
    @pytest.fixture
    def test_cfn_stack_with_mocks(self, test_cfn_stack):
        test_cfn_stack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
        test_cfn_stack.get_params = MagicMock()
        test_cfn_stack.wait_stack_action_complete = MagicMock(return_value=True)
        return test_cfn_stack

    @patch('boto3.client')
    def test_cluster_node_count_only_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['ClusterNodeCountOnly']['Full']
        test_cfn_stack_with_mocks.spindown_to_zero_appnodes()
        expected_spundown_paramlist = [
            {
                'ParameterKey': 'ClusterNodeCount',
                'ParameterValue': '0',
            }
        ]
        expected_spundown_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_OLD_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spundown_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )

    @patch('boto3.client')
    def test_az_node_count_only_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['AZNodeCountOnly']['Full']
        test_cfn_stack_with_mocks.spindown_to_zero_appnodes()
        expected_spundown_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '0',
            },
        ]
        expected_spundown_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_OLD_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spundown_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )

    @patch('boto3.client')
    def test_az_nodes_and_backup_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['AZNodesAndBackup']['Full']
        test_cfn_stack_with_mocks.spindown_to_zero_appnodes()
        expected_spundown_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'BackupNodeCount',
                'ParameterValue': '0',
            },
        ]
        expected_spundown_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_OLD_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spundown_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )


class TestSpinupAllNodes:
    @pytest.fixture
    def test_cfn_stack_with_mocks(self, test_cfn_stack):
        test_cfn_stack.get_params = MagicMock()
        test_cfn_stack.wait_stack_action_complete = MagicMock(return_value=True)
        return test_cfn_stack

    @patch('boto3.client')
    def test_cluster_node_count_only_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['ClusterNodeCountOnly']['Spundown']
        test_cfn_stack_with_mocks.preupgrade_app_node_count = 2
        test_cfn_stack_with_mocks.spinup_all_nodes(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'ClusterNodeCount',
                'ParameterValue': '2',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )

    @patch('boto3.client')
    def test_az_node_count_only_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['AZNodeCountOnly']['Spundown']
        test_cfn_stack_with_mocks.preupgrade_az_node_counts = {'Az1NodeCount': 3, 'Az2NodeCount': 5, 'Az3NodeCount': 1}
        test_cfn_stack_with_mocks.spinup_all_nodes(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '3',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '5',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '1',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )

    @patch('boto3.client')
    def test_az_nodes_and_backup_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['AZNodesAndBackup']['Spundown']
        test_cfn_stack_with_mocks.preupgrade_az_node_counts = {'Az1NodeCount': 1, 'Az2NodeCount': 2, 'Az3NodeCount': 0}
        test_cfn_stack_with_mocks.preupgrade_backup_node_count = 1
        test_cfn_stack_with_mocks.spinup_all_nodes(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '1',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '2',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'BackupNodeCount',
                'ParameterValue': '1',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )


class TestSpinupOneThenRemainingNodes:
    @pytest.fixture
    def test_cfn_stack_with_mocks(self, test_cfn_stack):
        test_cfn_stack.get_params = MagicMock()
        test_cfn_stack.validate_service_responding = MagicMock(return_value=True)
        test_cfn_stack.validate_all_stack_nodes_product_versions = MagicMock(return_value=True)
        test_cfn_stack.wait_stack_action_complete = MagicMock(return_value=True)
        return test_cfn_stack

    @patch('boto3.client')
    def test_cluster_node_count_only_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['ClusterNodeCountOnly']['Spundown']
        test_cfn_stack_with_mocks.preupgrade_app_node_count = 5
        test_cfn_stack_with_mocks.spinup_to_one_appnode(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'ClusterNodeCount',
                'ParameterValue': '1',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )
        test_cfn_stack_with_mocks.get_params.return_value = expected_spinup_paramlist
        test_cfn_stack_with_mocks.spinup_remaining_nodes(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'ClusterNodeCount',
                'ParameterValue': '5',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )

    @patch('boto3.client')
    def test_az_node_count_only_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['AZNodeCountOnly']['Spundown']
        test_cfn_stack_with_mocks.preupgrade_az_node_counts = {'Az1NodeCount': 3, 'Az2NodeCount': 5, 'Az3NodeCount': 1}
        test_cfn_stack_with_mocks.spinup_to_one_appnode(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '1',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '0',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )
        test_cfn_stack_with_mocks.get_params.return_value = expected_spinup_paramlist
        test_cfn_stack_with_mocks.spinup_remaining_nodes(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '3',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '5',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '1',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )

    @patch('boto3.client')
    def test_az_nodes_and_backup_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.get_params.return_value = MOCK_PARAMS['AZNodesAndBackup']['Spundown']
        test_cfn_stack_with_mocks.preupgrade_az_node_counts = {'Az1NodeCount': 1, 'Az2NodeCount': 2, 'Az3NodeCount': 0}
        test_cfn_stack_with_mocks.preupgrade_backup_node_count = 1
        test_cfn_stack_with_mocks.spinup_to_one_appnode(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '1',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'BackupNodeCount',
                'ParameterValue': '0',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )
        test_cfn_stack_with_mocks.get_params.return_value = expected_spinup_paramlist
        test_cfn_stack_with_mocks.spinup_remaining_nodes(MOCK_NEW_VERSION)
        expected_spinup_paramlist = [
            {
                'ParameterKey': 'Az1NodeCount',
                'ParameterValue': '1',
            },
            {
                'ParameterKey': 'Az2NodeCount',
                'ParameterValue': '2',
            },
            {
                'ParameterKey': 'Az3NodeCount',
                'ParameterValue': '0',
            },
            {
                'ParameterKey': 'BackupNodeCount',
                'ParameterValue': '1',
            },
        ]
        expected_spinup_paramlist.extend([{'ParameterKey': 'ProductVersion', 'ParameterValue': MOCK_NEW_VERSION}, {'ParameterKey': 'ProductDownloadUrl', 'ParameterValue': ''}])
        mock_boto3_cfn.update_stack.assert_called_with(
            StackName=test_cfn_stack_with_mocks.stack_name,
            Parameters=expected_spinup_paramlist,
            UsePreviousTemplate=True,
            Capabilities=ANY,
            ClientRequestToken=ANY,
        )
