from typing import Callable
from unittest.mock import MagicMock, patch

import pytest
from weaver.aws_cfn.cfn_stack import DRAIN_TIMEOUT_PARAM_KEY
import weaver.enums as weaver_enums

MOCK_ALB_DRAIN_TIMEOUT = 90
MOCK_NLB_DRAIN_TIMEOUT = 1200


def drain_type_allowed_for_lb_side_effect(alb_allowed: bool, nlb_allowed: bool) -> Callable[[weaver_enums.LoadBalancer, weaver_enums.DrainType], bool]:
    def side_effect(load_balancer, drain_type):
        if load_balancer == weaver_enums.LoadBalancer.ALB:
            return alb_allowed
        elif load_balancer == weaver_enums.LoadBalancer.NLB:
            return nlb_allowed
        else:
            return False

    return side_effect


def get_param_value_side_effect(param_key: str) -> str:
    if param_key == DRAIN_TIMEOUT_PARAM_KEY['ALB']:
        return MOCK_ALB_DRAIN_TIMEOUT
    elif param_key == DRAIN_TIMEOUT_PARAM_KEY['NLB']:
        return MOCK_NLB_DRAIN_TIMEOUT
    else:
        return ''


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_param_value')
class TestDrainTypeAllowedForLb:
    @pytest.mark.parametrize('load_balancer', [weaver_enums.LoadBalancer.ALB, weaver_enums.LoadBalancer.NLB])
    def test_fast_drain_with_timeout_param(self, mock_get_param_value, load_balancer, test_cfn_stack):
        mock_get_param_value.side_effect = get_param_value_side_effect
        assert test_cfn_stack.drain_type_allowed_for_lb(load_balancer, weaver_enums.DrainType.FAST)

    @pytest.mark.parametrize('load_balancer', [weaver_enums.LoadBalancer.ALB, weaver_enums.LoadBalancer.NLB])
    def test_fast_drain_without_timeout_param(self, mock_get_param_value, load_balancer, test_cfn_stack):
        mock_get_param_value.return_value = ''
        assert not test_cfn_stack.drain_type_allowed_for_lb(load_balancer, weaver_enums.DrainType.FAST)

    @pytest.mark.parametrize('load_balancer', [weaver_enums.LoadBalancer.ALB, weaver_enums.LoadBalancer.NLB])
    def test_default_drain(self, mock_get_param_value, load_balancer, test_cfn_stack):
        assert test_cfn_stack.drain_type_allowed_for_lb(load_balancer, weaver_enums.DrainType.DEFAULT)


@patch('weaver.aws_cfn.cfn_stack.CfnStack.has_nlb')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.has_alb')
@patch('weaver.aws_cfn.cfn_stack.CfnStack.drain_type_allowed_for_lb')
class TestDrainTypeAllowed:
    @pytest.mark.parametrize('drain_type', [weaver_enums.DrainType.DEFAULT, weaver_enums.DrainType.FAST, weaver_enums.DrainType.SKIP])
    @pytest.mark.parametrize('alb_allowed', [True, False])
    @pytest.mark.parametrize('nlb_allowed', [True, False])
    def test_alb_only(self, mock_drain_type_allowed_for_lb, mock_has_alb, mock_has_nlb, drain_type, alb_allowed, nlb_allowed, test_cfn_stack):
        mock_drain_type_allowed_for_lb.side_effect = drain_type_allowed_for_lb_side_effect(alb_allowed, nlb_allowed)
        mock_has_alb.return_value = True
        mock_has_nlb.return_value = False
        assert test_cfn_stack.drain_type_allowed(drain_type) == alb_allowed

    @pytest.mark.parametrize('drain_type', [weaver_enums.DrainType.DEFAULT, weaver_enums.DrainType.FAST, weaver_enums.DrainType.SKIP])
    @pytest.mark.parametrize('alb_allowed', [True, False])
    @pytest.mark.parametrize('nlb_allowed', [True, False])
    def test_nlb_only(self, mock_drain_type_allowed_for_lb, mock_has_alb, mock_has_nlb, drain_type, alb_allowed, nlb_allowed, test_cfn_stack):
        mock_drain_type_allowed_for_lb.side_effect = drain_type_allowed_for_lb_side_effect(alb_allowed, nlb_allowed)
        mock_has_alb.return_value = False
        mock_has_nlb.return_value = True
        assert test_cfn_stack.drain_type_allowed(drain_type) == nlb_allowed

    @pytest.mark.parametrize('drain_type', [weaver_enums.DrainType.DEFAULT, weaver_enums.DrainType.FAST, weaver_enums.DrainType.SKIP])
    @pytest.mark.parametrize('alb_allowed', [True, False])
    @pytest.mark.parametrize('nlb_allowed', [True, False])
    def test_alb_and_nlb(self, mock_drain_type_allowed_for_lb, mock_has_alb, mock_has_nlb, drain_type, alb_allowed, nlb_allowed, test_cfn_stack):
        mock_drain_type_allowed_for_lb.side_effect = drain_type_allowed_for_lb_side_effect(alb_allowed, nlb_allowed)
        mock_has_alb.return_value = True
        mock_has_nlb.return_value = True
        assert test_cfn_stack.drain_type_allowed(drain_type) == (alb_allowed and nlb_allowed)
