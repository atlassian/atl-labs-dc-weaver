from typing import Callable
from unittest.mock import MagicMock, patch

import botocore
import pytest
from weaver.aws_cfn.cfn_stack import CfnStack
import weaver.enums as weaver_enums
import weaver.exceptions as weaver_exceptions

MOCK_LOAD_BALANCER_TG_ARN = {weaver_enums.LoadBalancer.ALB: 'alb-tg-arn', weaver_enums.LoadBalancer.NLB: 'nlb-tg-arn'}

MOCK_RESOURCE_LIST = [
    {
        "LogicalResourceId": "ALB",
        "PhysicalResourceId": "ALB-asdaofoso",
        "ResourceType": "AWS::ElasticLoadBalancingV2::LoadBalancer",
    },
    {
        "LogicalResourceId": "NLB",
        "PhysicalResourceId": "NLB-oipwre",
        "ResourceType": "AWS::ElasticLoadBalancingV2::LoadBalancer",
    },
    {
        "LogicalResourceId": "LoadBalancerV2",
        "PhysicalResourceId": "LoadBalancerV2-54werqw",
        "ResourceType": "AWS::ElasticLoadBalancingV2::LoadBalancer",
    },
    {
        "LogicalResourceId": "ClusterNodeRole",
        "PhysicalResourceId": "ClusterNodeRole-sdlafsa",
        "ResourceType": "AWS::IAM::Role",
    },
    {
        "LogicalResourceId": "ALBMainTargetGroup",
        "PhysicalResourceId": MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.ALB],
        "ResourceType": "AWS::ElasticLoadBalancingV2::TargetGroup",
    },
    {
        "LogicalResourceId": "MainTargetGroup",
        "PhysicalResourceId": MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.ALB],
        "ResourceType": "AWS::ElasticLoadBalancingV2::TargetGroup",
    },
    {
        "LogicalResourceId": "NlbSshTargetGroup",
        "PhysicalResourceId": MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.NLB],
        "ResourceType": "AWS::ElasticLoadBalancingV2::TargetGroup",
    },
    {
        "LogicalResourceId": "SshTargetGroup",
        "PhysicalResourceId": MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.NLB],
        "ResourceType": "AWS::ElasticLoadBalancingV2::TargetGroup",
    },
]


def describe_stack_resource_side_effect(existing_resources: tuple[str]) -> Callable[[str, str], None]:
    def side_effect(StackName, LogicalResourceId):
        if LogicalResourceId not in existing_resources:
            raise botocore.exceptions.ClientError(
                error_response={'Error': {'Code': 'ValidationError', 'Message': f'{LogicalResourceId} does not exist in stack'}}, operation_name='describe_stack_resource'
            )

    return side_effect


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_param_value')
class TestGetCfnParamDeregistrationDelay:
    def test_alb(self, mock_get_param_value, test_cfn_stack):
        mock_get_param_value.return_value = 23
        assert test_cfn_stack.get_cfn_param_deregistration_delay(weaver_enums.LoadBalancer.ALB) == 23

    def test_nlb(self, mock_get_param_value, test_cfn_stack):
        mock_get_param_value.return_value = 26
        assert test_cfn_stack.get_cfn_param_deregistration_delay(weaver_enums.LoadBalancer.ALB) == 26


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_stack_resource_list')
class TestHasAlb:
    def test_alb_attr_set_true(self, mock_get_stack_resource_list, test_cfn_stack):
        test_cfn_stack.load_balancers['ALB'] = True
        assert test_cfn_stack.has_alb()

    def test_alb_attr_set_false(self, mock_get_stack_resource_list, test_cfn_stack):
        test_cfn_stack.load_balancers['ALB'] = False
        assert not test_cfn_stack.has_alb()

    def test_alb_resource_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = MOCK_RESOURCE_LIST
        assert test_cfn_stack.has_alb()

    def test_loadbalancerv2_resource_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] != 'ALB']
        assert test_cfn_stack.has_alb()

    def test_no_valid_resource_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] not in ('ALB', 'LoadBalancerV2')]
        assert not test_cfn_stack.has_alb()

    def test_attr_caching_alb_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = MOCK_RESOURCE_LIST
        assert test_cfn_stack.has_alb()
        # subsequent runs should pull in the value from the object attribute
        assert test_cfn_stack.has_alb()
        mock_get_stack_resource_list.assert_called_once()
        assert test_cfn_stack.has_alb()
        mock_get_stack_resource_list.assert_called_once()

    def test_attr_caching_alb_does_not_exist(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] not in ('ALB', 'LoadBalancerV2')]
        assert not test_cfn_stack.has_alb()
        # subsequent runs should pull in the value from the object attribute
        assert not test_cfn_stack.has_alb()
        # call_count == 2 since code tries two parameter keys in the first run
        assert mock_get_stack_resource_list.call_count == 2
        assert not test_cfn_stack.has_alb()
        assert mock_get_stack_resource_list.call_count == 2


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_stack_resource_list')
class TestHasNlb:
    def test_nlb_attr_set_true(self, mock_get_stack_resource_list, test_cfn_stack):
        test_cfn_stack.load_balancers['NLB'] = True
        assert test_cfn_stack.has_nlb()

    def test_nlb_attr_set_false(self, mock_get_stack_resource_list, test_cfn_stack):
        test_cfn_stack.load_balancers['NLB'] = False
        assert not test_cfn_stack.has_nlb()

    def test_nlb_resource_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = MOCK_RESOURCE_LIST
        assert test_cfn_stack.has_nlb()

    def test_no_valid_resource_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] != 'NLB']
        assert not test_cfn_stack.has_nlb()

    def test_attr_caching_nlb_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = MOCK_RESOURCE_LIST
        assert test_cfn_stack.has_nlb()
        # subsequent runs should pull in the value from the object attribute
        assert test_cfn_stack.has_nlb()
        mock_get_stack_resource_list.assert_called_once()
        assert test_cfn_stack.has_nlb()
        mock_get_stack_resource_list.assert_called_once()

    def test_attr_caching_nlb_does_not_exist(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] != 'NLB']
        assert not test_cfn_stack.has_nlb()
        # subsequent runs should pull in the value from the object attribute
        assert not test_cfn_stack.has_nlb()
        mock_get_stack_resource_list.assert_called_once()
        assert not test_cfn_stack.has_nlb()
        mock_get_stack_resource_list.assert_called_once()


@patch('weaver.aws_cfn.cfn_stack.CfnStack.get_stack_resource_list')
class TestGetLoadBalancerTgArn:
    @pytest.mark.parametrize('load_balancer', [weaver_enums.LoadBalancer.ALB, weaver_enums.LoadBalancer.NLB])
    def test_attr_exists(self, mock_get_stack_resource_list, load_balancer, test_cfn_stack):
        test_cfn_stack.load_balancer_tg_arns[load_balancer] = MOCK_LOAD_BALANCER_TG_ARN[load_balancer]
        assert test_cfn_stack.get_load_balancer_tg_arn(load_balancer) == MOCK_LOAD_BALANCER_TG_ARN[load_balancer]
        mock_get_stack_resource_list.assert_not_called()

    def test_alb_main_target_group_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = MOCK_RESOURCE_LIST
        assert test_cfn_stack.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.ALB) == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.ALB]
        # ensure attr cache is set
        assert test_cfn_stack.load_balancer_tg_arns[weaver_enums.LoadBalancer.ALB] == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.ALB]

    def test_main_target_group_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] != 'ALBMainTargetGroup']
        assert test_cfn_stack.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.ALB) == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.ALB]
        # ensure attr cache is set
        assert test_cfn_stack.load_balancer_tg_arns[weaver_enums.LoadBalancer.ALB] == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.ALB]

    def test_no_alb_tg_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] not in ('ALBMainTargetGroup', 'MainTargetGroup')]
        with pytest.raises(weaver_exceptions.StackParameterNotFound):
            test_cfn_stack.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.ALB)
        # ensure nothing set in attr cache
        assert weaver_enums.LoadBalancer.ALB not in test_cfn_stack.load_balancer_tg_arns

    def test_nlb_ssh_target_group_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = MOCK_RESOURCE_LIST
        assert test_cfn_stack.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.NLB) == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.NLB]
        # ensure attr cache is set
        assert test_cfn_stack.load_balancer_tg_arns[weaver_enums.LoadBalancer.NLB] == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.NLB]

    def test_ssh_target_group_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] != 'NlbSshTargetGroup']
        assert test_cfn_stack.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.NLB) == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.NLB]
        # ensure attr cache is set
        assert test_cfn_stack.load_balancer_tg_arns[weaver_enums.LoadBalancer.NLB] == MOCK_LOAD_BALANCER_TG_ARN[weaver_enums.LoadBalancer.NLB]

    def test_no_nlb_tg_exists(self, mock_get_stack_resource_list, test_cfn_stack):
        mock_get_stack_resource_list.return_value = [resource for resource in MOCK_RESOURCE_LIST if resource['LogicalResourceId'] not in ('NlbSshTargetGroup', 'SshTargetGroup')]
        with pytest.raises(weaver_exceptions.StackParameterNotFound):
            test_cfn_stack.get_load_balancer_tg_arn(weaver_enums.LoadBalancer.NLB)
        # ensure nothing set in attr cache
        assert weaver_enums.LoadBalancer.NLB not in test_cfn_stack.load_balancer_tg_arns
