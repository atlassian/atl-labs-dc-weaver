from unittest.mock import patch, ANY, MagicMock
import pytest

import weaver.enums as weaver_enums

MOCK_BB_MESH_VERSION = '3.0.0'
NO_ALT_DOWNLOAD_URL = ''


class TestUpgradeMesh:
    @pytest.fixture
    def test_cfn_stack_with_mocks(self, test_cfn_stack):
        test_cfn_stack.get_app_type = MagicMock(return_value='bitbucket_mesh')
        test_cfn_stack.has_alb = MagicMock(return_value=False)
        test_cfn_stack.has_nlb = MagicMock(return_value=False)
        test_cfn_stack.get_pre_upgrade_information = MagicMock()
        test_cfn_stack.get_primary_stack_nodes = MagicMock()
        test_cfn_stack.shutdown_app = MagicMock()
        test_cfn_stack.get_stacknodes = MagicMock()
        test_cfn_stack.spindown_to_zero_appnodes = MagicMock()
        test_cfn_stack.spinup_all_nodes = MagicMock()
        test_cfn_stack.startup_app = MagicMock()
        test_cfn_stack.wait_all_serving_mesh_nodes = MagicMock()
        return test_cfn_stack

    def test_skip_startup_primary(
        self,
        test_cfn_stack_with_mocks,
    ):
        assert test_cfn_stack_with_mocks.upgrade(MOCK_BB_MESH_VERSION, NO_ALT_DOWNLOAD_URL, startup_primary=False)
        test_cfn_stack_with_mocks.startup_app.assert_not_called()

    def test_startup_primary(
        self,
        test_cfn_stack_with_mocks,
    ):
        test_cfn_stack_with_mocks.preupgrade_app_node_count = 1
        assert test_cfn_stack_with_mocks.upgrade(MOCK_BB_MESH_VERSION, NO_ALT_DOWNLOAD_URL, startup_primary=True)
        test_cfn_stack_with_mocks.startup_app.assert_called_with(ANY, primary_stack=True)


class TestUpgradeBBPrimaryCluster:
    MOCK_NEW_VERSION = '8.19.2'
    MOCK_ALT_DOWNLOAD_URL = 'https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-8.19.2.tar.gz'

    @pytest.fixture
    def test_cfn_stack_with_mocks(self, test_cfn_stack):
        test_cfn_stack.get_app_type = MagicMock(return_value='bitbucket')
        test_cfn_stack.drain_type_allowed = MagicMock(return_value=True)
        test_cfn_stack.get_pre_upgrade_information = MagicMock()
        test_cfn_stack.check_nfs_server_replacement = MagicMock(return_value=False)
        test_cfn_stack.shutdown_app = MagicMock()
        test_cfn_stack.get_stacknodes = MagicMock()
        test_cfn_stack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
        test_cfn_stack.get_spindown_to_zero_appnodes_paramlist = MagicMock()
        test_cfn_stack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
        test_cfn_stack.wait_stack_action_complete = MagicMock()
        test_cfn_stack.restore_original_deregistration_delays_if_needed = MagicMock()
        test_cfn_stack.spinup_all_nodes = MagicMock()
        test_cfn_stack.wait_all_healthy_targets = MagicMock()
        return test_cfn_stack

    @patch('boto3.client')
    def test_upgrade_default_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.preupgrade_app_node_count = 1
        assert test_cfn_stack_with_mocks.upgrade(self.MOCK_NEW_VERSION, self.MOCK_ALT_DOWNLOAD_URL)
        test_cfn_stack_with_mocks.spinup_all_nodes.assert_called_once()

    @patch('boto3.client')
    def test_upgrade_fast_drain(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.preupgrade_app_node_count = 1
        assert test_cfn_stack_with_mocks.upgrade(self.MOCK_NEW_VERSION, self.MOCK_ALT_DOWNLOAD_URL, drain_type=weaver_enums.DrainType.FAST)
        test_cfn_stack_with_mocks.modify_deregistration_delays_to_fast_drain_value.assert_called_once()
        test_cfn_stack_with_mocks.spinup_all_nodes.assert_called_once()


class TestUpgradeBBMirror:
    MOCK_NEW_VERSION = '8.19.2'
    MOCK_ALT_DOWNLOAD_URL = 'https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-8.19.2.tar.gz'

    @pytest.fixture
    def test_cfn_stack_with_mocks(self, test_cfn_stack):
        test_cfn_stack.get_app_type = MagicMock(return_value='bitbucket_mirror')
        test_cfn_stack.drain_type_allowed = MagicMock(return_value=True)
        test_cfn_stack.get_pre_upgrade_information = MagicMock()
        test_cfn_stack.check_nfs_server_replacement = MagicMock(return_value=False)
        test_cfn_stack.shutdown_app = MagicMock()
        test_cfn_stack.get_stacknodes = MagicMock()
        test_cfn_stack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
        test_cfn_stack.get_spindown_to_zero_appnodes_paramlist = MagicMock()
        test_cfn_stack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
        test_cfn_stack.wait_stack_action_complete = MagicMock()
        test_cfn_stack.restore_original_deregistration_delays_if_needed = MagicMock()
        test_cfn_stack.spinup_all_nodes = MagicMock()
        test_cfn_stack.spinup_to_one_appnode = MagicMock()
        test_cfn_stack.spinup_remaining_nodes = MagicMock()
        test_cfn_stack.wait_all_healthy_targets = MagicMock()
        return test_cfn_stack

    @patch('boto3.client')
    def test_upgrade_default_drain_one_node(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.preupgrade_app_node_count = 1
        assert test_cfn_stack_with_mocks.upgrade(self.MOCK_NEW_VERSION, self.MOCK_ALT_DOWNLOAD_URL)
        test_cfn_stack_with_mocks.spinup_to_one_appnode.assert_called_once()

    @pytest.mark.parametrize('num_nodes', [2, 5, 24])
    @patch('boto3.client')
    def test_upgrade_default_drain_many_nodes(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn, num_nodes):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.preupgrade_app_node_count = num_nodes
        assert test_cfn_stack_with_mocks.upgrade(self.MOCK_NEW_VERSION, self.MOCK_ALT_DOWNLOAD_URL)
        test_cfn_stack_with_mocks.spinup_to_one_appnode.assert_called_once()
        test_cfn_stack_with_mocks.spinup_remaining_nodes.assert_called_once()

    @patch('boto3.client')
    def test_upgrade_fast_drain_one_node(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.preupgrade_app_node_count = 1
        assert test_cfn_stack_with_mocks.upgrade(self.MOCK_NEW_VERSION, self.MOCK_ALT_DOWNLOAD_URL, drain_type=weaver_enums.DrainType.FAST)
        test_cfn_stack_with_mocks.modify_deregistration_delays_to_fast_drain_value.assert_called_once()
        test_cfn_stack_with_mocks.spinup_to_one_appnode.assert_called_once()

    @pytest.mark.parametrize('num_nodes', [2, 5, 24])
    @patch('boto3.client')
    def test_upgrade_fast_drain_many_node(self, mock_client, test_cfn_stack_with_mocks, mock_boto3_cfn, num_nodes):
        mock_client.return_value = mock_boto3_cfn
        test_cfn_stack_with_mocks.preupgrade_app_node_count = num_nodes
        assert test_cfn_stack_with_mocks.upgrade(self.MOCK_NEW_VERSION, self.MOCK_ALT_DOWNLOAD_URL, drain_type=weaver_enums.DrainType.FAST)
        test_cfn_stack_with_mocks.modify_deregistration_delays_to_fast_drain_value.assert_called_once()
        test_cfn_stack_with_mocks.spinup_to_one_appnode.assert_called_once()
        test_cfn_stack_with_mocks.spinup_remaining_nodes.assert_called_once()
