from unittest.mock import MagicMock, patch

import boto3
from moto import mock_aws
import pytest
import weaver
from weaver.aws_cfn.cfn_stack import CfnStack

TEST_AWS_REGION = 'ap-southeast-2'


# Workaround for disabling cache
@pytest.fixture
def test_app():
    app = weaver.create_app('weaver.config.TestingConfig')
    return app


@pytest.fixture
def test_cfn_stack():
    test_cfn_stack = CfnStack('test-stack', TEST_AWS_REGION)
    return test_cfn_stack


@mock_aws
@pytest.fixture
def mock_backup_node():
    ec2 = boto3.resource('ec2')
    mock_node = ec2.Instance('backup-node')
    mock_node.is_backup_node = MagicMock(return_value=True)
    return mock_node


@mock_aws
@pytest.fixture
def mock_non_backup_node():
    ec2 = boto3.resource('ec2', region_name=TEST_AWS_REGION)
    mock_node = ec2.Instance('non-backup-node')
    mock_node.is_backup_node = MagicMock(return_value=False)
    return mock_node


class TestGetNodeRegistrationAlb:
    @patch('weaver.aws_cfn.cfn_stack.CfnStack.get_tag')
    def test_backup_node(mock_get_tag, mock_backup_node, test_cfn_stack):
        assert not test_cfn_stack.get_node_registration_state_alb(mock_backup_node)


class TestGetNodeRegistrationNlb:
    @patch('weaver.aws_cfn.cfn_stack.CfnStack.get_tag')
    def test_backup_node(mock_get_tag, mock_backup_node, test_cfn_stack):
        assert not test_cfn_stack.get_node_registration_state_nlb(mock_backup_node)


class TestBeginRegisteringNodesToAllTargetGroups:
    def test_only_backup_node(mock_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([mock_backup_node], []))
        test_cfn_stack.begin_registering_node_to_all_target_groups = MagicMock()
        assert test_cfn_stack.begin_registering_nodes_to_all_target_groups([mock_backup_node])
        test_cfn_stack.begin_registering_node_to_all_target_groups.assert_not_called()

    def test_backup_node_and_non_backup_node(mock_backup_node, mock_non_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([mock_backup_node], [mock_non_backup_node]))
        test_cfn_stack.begin_registering_node_to_all_target_groups = MagicMock(return_value=True)
        assert test_cfn_stack.begin_registering_nodes_to_all_target_groups([mock_backup_node])
        test_cfn_stack.begin_registering_node_to_all_target_groups.assert_called_once()

    def test_only_non_backup_node(mock_non_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([], [mock_non_backup_node]))
        test_cfn_stack.begin_registering_node_to_all_target_groups = MagicMock()
        assert test_cfn_stack.begin_registering_nodes_to_all_target_groups([mock_non_backup_node])
        test_cfn_stack.begin_registering_node_to_all_target_groups.assert_called_once()


class TestBeginDeregisteringNodesFromAllTargetGroups:
    def test_only_backup_node(mock_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([mock_backup_node], []))
        test_cfn_stack.begin_deregistering_node_from_all_target_groups = MagicMock()
        assert test_cfn_stack.begin_deregistering_nodes_from_all_target_groups([mock_backup_node])
        test_cfn_stack.begin_deregistering_node_from_all_target_groups.assert_not_called()

    def test_backup_node_and_non_backup_node(mock_backup_node, mock_non_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([mock_backup_node], [mock_non_backup_node]))
        test_cfn_stack.begin_deregistering_node_from_all_target_groups = MagicMock(return_value=True)
        assert test_cfn_stack.begin_deregistering_nodes_from_all_target_groups([mock_backup_node])
        test_cfn_stack.begin_deregistering_node_from_all_target_groups.assert_called_once()

    def test_only_non_backup_node(mock_non_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([], [mock_non_backup_node]))
        test_cfn_stack.begin_deregistering_node_from_all_target_groups = MagicMock()
        assert test_cfn_stack.begin_deregistering_nodes_from_all_target_groups([mock_non_backup_node])
        test_cfn_stack.begin_deregistering_node_from_all_target_groups.assert_called_once()


class WaitNodesRegistrationStates:
    def test_only_backup_node(mock_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([mock_backup_node], []))
        test_cfn_stack.get_node_registration_state_alb = MagicMock()
        test_cfn_stack.get_node_registration_state_nlb = MagicMock()
        assert test_cfn_stack.wait_nodes_registration_states([mock_backup_node], 'deregistered')
        test_cfn_stack.get_node_registration_state_alb.assert_not_called()
        test_cfn_stack.get_node_registration_state_nlb.assert_not_called()

    def test_backup_node_and_non_backup_node(mock_backup_node, mock_non_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([mock_backup_node], [mock_non_backup_node]))
        test_cfn_stack.get_node_registration_state_alb = MagicMock(return_value='deregistered')
        test_cfn_stack.get_node_registration_state_nlb = MagicMock(return_value='deregistered')
        assert test_cfn_stack.wait_nodes_registration_states([mock_backup_node], 'deregistered')
        test_cfn_stack.get_node_registration_state_alb.assert_called_once()
        test_cfn_stack.get_node_registration_state_nlb.assert_called_once()

    def test_only_backup_node(mock_non_backup_node, test_cfn_stack):
        test_cfn_stack.split_backup_from_non_backup_nodes = MagicMock(return_value=([], [mock_non_backup_node]))
        test_cfn_stack.get_node_registration_state_alb = MagicMock(return_value='deregistered')
        test_cfn_stack.get_node_registration_state_nlb = MagicMock(return_value='deregistered')
        assert test_cfn_stack.wait_nodes_registration_states([mock_non_backup_node], 'deregistered')
        test_cfn_stack.get_node_registration_state_alb.assert_called_once()
        test_cfn_stack.get_node_registration_state_nlb.assert_called_once()
