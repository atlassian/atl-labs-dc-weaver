import inspect
import json
import re
import os
import shutil
import string
from pathlib import Path
from unittest.mock import MagicMock, Mock

import pytest
from requests import Session

import boto3
import mock
import moto
from moto.moto_api._internal import mock_random as random

import weaver.utils as utils

from tests.func.aws_functionality import moto_overrides
import weaver.exceptions as weaver_exceptions
import weaver.enums as weaver_enums
from weaver.models import ProductEnum
from weaver.sns.sns import send_sns_msg, get_sns_topic_arn
import weaver

from flask import g


# Environment setup

CONF_STACKNAME_PREFIX = 'my-confluence'
CONF_CLONE_STACKNAME_PREFIX = 'my-cloned-confluence'
JIRA_STACKNAME_PREFIX = 'my-jira'
REGION = 'us-east-1'
SERVICE_URL = 'http://my-stack.itplat.atlassian.com'

_CURRENT_FRAME = inspect.currentframe()
if _CURRENT_FRAME:
    TEST_ROOT = Path(inspect.getfile(_CURRENT_FRAME)).parent
    DUMMY_FILE = Path(f'{TEST_ROOT}/dummy.file')
    CONF_TEMPLATE_FILE = Path(f'{TEST_ROOT}/func-test-confluence.template.yaml')
    CONF_CLONE_TEMPLATE_FILE = Path(f'{TEST_ROOT}/func-test-confluence-clone.template.yaml')
    JIRA_TEMPLATE_FILE = Path(f'{TEST_ROOT}/func-test-jira.template.yaml')
    # copy templates to custom-templates directory
    testing_template_repo_dir = Path("custom-templates/testing")
    testing_template_repo_dir.mkdir(parents=True, exist_ok=True)
    shutil.copy(CONF_TEMPLATE_FILE, testing_template_repo_dir / CONF_TEMPLATE_FILE.name)
    shutil.copy(CONF_CLONE_TEMPLATE_FILE, testing_template_repo_dir / CONF_CLONE_TEMPLATE_FILE.name)
    shutil.copy(JIRA_TEMPLATE_FILE, testing_template_repo_dir / JIRA_TEMPLATE_FILE.name)
else:
    print(f"Call to inspect.currentframe returned {_CURRENT_FRAME}; unable to determine test directory")
    exit(1)

# Configure app
app = weaver.create_app('weaver.config.TestingConfig')
app.config['ACCEPTABLE_INDEX_HEALTH'] = 98
app.config['PLATFORM'] = 'cfn'
app.config['SNS_REGION'] = 'us-east-1'
app.config['STACK_LOCKING'] = False
app.config['ACTION_TIMEOUTS'] = {
    'check_index_health': 3600,
    'node_initialisation': 3600,
    'node_registration_deregistration': 3600,
    'validate_node_responding': 3600,
    'validate_service_responding': 3600,
    'generate_support_zip': 300,
}
app.config['ACCESSIBLE_ENVIRONMENTS'] = {'local': ['dev', 'dr', 'stg', 'prod'], 'stg': ['dev', 'stg']}

# override buggy moto functions
moto.cloudformation.models.FakeChangeSet.apply = moto_overrides.apply
moto.cloudformation.parsing.ResourceMap.parse_ssm_parameter = moto_overrides.parse_ssm_parameter
moto.cloudformation.parsing.parse_and_update_resource = moto_overrides.parse_and_update_resource
moto.cloudformation.responses.CloudFormationResponse.create_change_set = moto_overrides.create_change_set
moto.cloudformation.responses.DESCRIBE_STACKS_TEMPLATE = moto_overrides.DESCRIBE_STACKS_TEMPLATE
moto.ec2.models.instances.Instance.create_from_cloudformation_json = moto_overrides.ec2_instance_create_from_cloudformation_json
moto.elbv2.models.ELBv2Backend.convert_and_validate_certificates = moto_overrides.convert_and_validate_certificates
moto.efs.models.EFSBackend.delete_mount_target = moto_overrides.delete_mount_target
moto.efs.models.EFSBackend.delete_file_system = moto_overrides.delete_file_system
moto.efs.models.FileSystem.add_mount_target = moto_overrides.add_mount_target
moto.iam.models.InlinePolicy.update = moto_overrides.update
moto.iam.models.InlinePolicy.delete_from_cloudformation_json = moto_overrides.iam_inline_policy_delete_from_cloudformation_json
moto.autoscaling.models.FakeAutoScalingGroup.image_id = moto_overrides.image_id
moto.autoscaling.models.FakeAutoScalingGroup.instance_type = moto_overrides.instance_type
moto.autoscaling.models.FakeAutoScalingGroup.security_groups = moto_overrides.security_groups
moto.autoscaling.models.FakeAutoScalingGroup.user_data = moto_overrides.user_data
moto.ssm.models.Parameter.delete_from_cloudformation_json = moto_overrides.ssm_delete_from_cloudformation_json


def get_stack_params(product: ProductEnum) -> list[dict[str, str]]:
    base_params = {
        "CatalinaOpts": "",
        "CidrBlock": "0.0.0.0/0",
        "ClusterBotNodeCount": "0",
        "ClusterBotNodeInstanceType": "t3.medium",
        "ClusterNodeVolumeIops": "4000",
        "ClusterNodeVolumeThroughput": "300",
        "ClusterNodeVolumeType": "gp3",
        "CustomDnsName": "",
        "DBAutoMinorVersionUpgrade": "true",
        "DBEngineVersion": "14",
        "DBInstanceClass": "db.t4g.medium",
        "DBIops": "1000",
        "DBMaintenanceWindow": "sat:23:00-sat:23:30",
        "DBMasterUserPassword": "changeme",
        "DBMultiAZ": "true",
        "DBPassword": "changeme",
        "DBReadReplicaInstanceClass": "none",
        "DBSnapshotName": "",
        "DBStorage": "10",
        "DBStorageType": "General Purpose (SSD)",
        "DBUseSecretsManager": "false",
        "DeployEnvironment": "prod",
        "EfsTransitionToIaStorageDays": "none",
        "EfsTransitionToPrimaryStorage": "false",
        "ExternalSubnets": f"{app.config['RESOURCES']['subnet_1_id']},{app.config['RESOURCES']['subnet_2_id']},{app.config['RESOURCES']['subnet_3_id']}",
        "InternalSubnets": f"{app.config['RESOURCES']['subnet_1_id']},{app.config['RESOURCES']['subnet_2_id']},{app.config['RESOURCES']['subnet_3_id']}",
        "JvmHeap": "2g",
        "JvmHeapBots": "2g",
        "KeyPairName": "a-key",
        "KmsKeyArn": "",
        "LatestAmiId": "/aws/service/ami-amazon-linux-latest/al2023-ami-minimal-kernel-default-x86_64",
        "LoadBalancerScheme": "internal",
        "LoadBalancerStickinessSeconds": "18000",
        "LocalAnsibleGitRepo": "git@bitbucket.org:atlassian/some-ansible.git;main;pb.setup.yml",
        "LocalAnsibleGitSshKeyName": "",
        "MailEnabled": "false",
        "ProductDownloadUrl": "",
        "SubDomainName": "",
        "TomcatContextPath": "",
        "TomcatDefaultConnectorPort": "8080",
        "VPC": f"{app.config['RESOURCES']['vpc_id']}",
        "WebTargetGroupDrainingTimeout": "90",
    }

    product_specific_params = {
        "confluence": {
            "AutologinCookieAge": "18000",
            "ClusterNodeCount": "2",
            "ClusterNodeInstanceType": "t3.medium",
            "ClusterNodeVolumeSize": "100",
            "CollaborativeEditingMode": "synchrony-local",
            "DeployFromAnsible": "https://bitbucket.org/atlassian/dc-deployments-automation.git;master;aws_confluence_dc_node.yml",
            "DeploymentAutomationCustomParams": "-e atl_db_acquireincrement=1 -e atl_db_idletestperiod=100 -e atl_db_maxstatements=0 -e atl_db_poolmaxsize=60 -e atl_db_poolminsize=20 -e atl_db_timeout=30 -e atl_db_validate=false -e atl_tomcat_acceptcount=10 -e atl_tomcat_connectiontimeout=20000 -e atl_tomcat_enablelookups=false -e atl_tomcat_maxthreads=48 -e atl_tomcat_minsparethreads=10 -e atl_tomcat_protocol=HTTP/1.1 -e atl_tomcat_redirectport=8443",
            "HostedZone": "wpt.atlassian.com.",
            "JvmHeapSynchrony": "2g",
            "OpenSearchBucketName": "",
            "OpenSearchInstanceType": "m6g.large.search",
            "OpenSearchNodeCount": "0",
            "OpenSearchNodeVolumeSize": "100",
            "OpenSearchPassword": "",
            "OpenSearchSnapshotId": "",
            "OpenSearchVersion": "",
            "ProductVersion": "6.11.0",
            "SSLCertificateARN": "",
            "SynchronyClusterNodeCount": "0",
            "SynchronyNodeInstanceType": "t3.medium",
            "TomcatScheme": "http",
        },
        "jira": {
            "ClusterNodeCount": "1",
            "ClusterNodeInstanceType": "t3.large",
            "ClusterNodeVolumeSize": "200",
            "DeployFromAnsible": "https://bitbucket.org/atlassian/dc-deployments-automation.git;master;aws_jira_dc_node.yml",
            "DeploymentAutomationCustomParams": "-e atl_db_maxidle=20 -e atl_db_maxwaitmillis=10000 -e atl_db_minevictableidletimemillis=180000 -e atl_db_minidle=10 -e atl_db_poolmaxsize=20 -e atl_db_poolminsize=20 -e atl_db_removeabandoned=true -e atl_db_removeabandonedtimeout=300 -e atl_db_testonborrow=false -e atl_db_testwhileidle=true -e atl_db_timebetweenevictionrunsmillis=60000 -e atl_tomcat_acceptcount=10 -e atl_tomcat_connectiontimeout=20000 -e atl_tomcat_enablelookups=false -e atl_tomcat_maxthreads=200 -e atl_tomcat_minsparethreads=10 -e atl_tomcat_protocol=HTTP/1.1 -e atl_tomcat_redirectport=8443",
            "JiraProduct": "All",
            "ProductVersion": "8.22.5",
            "SSLCertificateARN": "a-cert",
            "TomcatScheme": "https",
        },
    }

    return [{"ParameterKey": k, "ParameterValue": v} for k, v in (base_params | product_specific_params[product.name]).items()]


def random_suffix() -> str:
    size = 12
    chars = list(range(10)) + list(string.ascii_uppercase)
    return "".join(str(random.choice(chars)) for x in range(size))


@moto.mock_aws
def setup_confluence_stack(custom_tags={}):
    # not using pytest.setup_class or a fixture here as the moto environment does not persist - it tears itself down
    # each test must call this at the start
    with app.app_context():
        setup_env_resources()
        mystack = utils.init_stack(f"{CONF_STACKNAME_PREFIX}-{random_suffix()}", REGION)
        # setup mocks
        mystack.has_alb = MagicMock(return_value=True)
        mystack.has_nlb = MagicMock(return_value=False)
        mystack.validate_service_responding = MagicMock(return_value=True)
        mystack.wait_stack_action_complete = MagicMock(return_value=True)
        utils.get_username = MagicMock(return_value='TestUser')
        # setup S3 bucket
        boto3.resource('s3', region_name=REGION).create_bucket(Bucket=mystack.get_s3_bucket_name())
        # create stack
        try:
            g.action_id = utils.create_action(mystack, 'create', 'prod')
        except weaver_exceptions.StackEnvironmentInaccessible as e:
            # this test suite includes testing this path
            raise e
        except Exception:
            pytest.fail('Could not store action')
        outcome = mystack.create(
            product=ProductEnum('Confluence'),
            template={"repo": "testing", "name": CONF_TEMPLATE_FILE.name},
            stack_params=get_stack_params(ProductEnum('Confluence')),
            tags=custom_tags if custom_tags else {},
            cloned_from=None,
        )
        assert outcome
        return mystack


@moto.mock_aws
def setup_jira_stack():
    # not using pytest.setup_class or a fixture here as the moto environment does not persist - it tears itself down
    # each test must call this at the start
    with app.app_context():
        setup_env_resources()
        mystack = utils.init_stack(f"{JIRA_STACKNAME_PREFIX}-{random_suffix()}", REGION)
        # setup mocks
        mystack.has_alb = MagicMock(return_value=True)
        mystack.has_nlb = MagicMock(return_value=False)
        mystack.validate_service_responding = MagicMock(return_value=True)
        mystack.wait_stack_action_complete = MagicMock(return_value=True)
        utils.get_username = MagicMock(return_value='test-user')
        # setup S3 bucket
        boto3.resource('s3', region_name=REGION).create_bucket(Bucket=mystack.get_s3_bucket_name())
        # create stack
        try:
            g.action_id = utils.create_action(mystack, 'create', 'prod')
        except weaver_exceptions.StackEnvironmentInaccessible as e:
            # this test suite includes testing this path
            raise e
        except Exception:
            pytest.fail('Could not store action')
        outcome = mystack.create(
            product=ProductEnum('Jira'),
            template={"repo": "testing", "name": JIRA_TEMPLATE_FILE.name},
            stack_params=get_stack_params(ProductEnum('Jira')),
            tags={},
            cloned_from=None,
        )
        assert outcome
        return mystack


def setup_env_resources():
    # create VPC and subnets
    ec2 = boto3.resource('ec2', region_name=REGION)
    vpc = ec2.create_vpc(CidrBlock='10.0.0.0/22')
    subnet_1 = vpc.create_subnet(CidrBlock='10.0.0.0/24')
    subnet_2 = vpc.create_subnet(CidrBlock='10.0.1.0/24')
    subnet_3 = vpc.create_subnet(CidrBlock='10.0.2.0/24')
    # create hosted zone
    r53 = boto3.client('route53')
    try:
        r53.create_hosted_zone(
            Name='wpt.atlassian.com.',
            VPC={'VPCRegion': REGION, 'VPCId': vpc.vpc_id},
            CallerReference='caller_ref',
            HostedZoneConfig={'Comment': 'string', 'PrivateZone': True},
            DelegationSetId='string',
        )
    except r53.exceptions.ConflictingDomainExists:
        pass
    resources = {}
    resources['vpc_id'] = vpc.vpc_id
    resources['subnet_1_id'] = subnet_1.subnet_id
    resources['subnet_2_id'] = subnet_2.subnet_id
    resources['subnet_3_id'] = subnet_3.subnet_id
    app.config['RESOURCES'] = resources


# Tests
@moto.mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestAwsStacks:
    def test_create(self):
        mystack = setup_confluence_stack()
        stacks = boto3.client('cloudformation', REGION).describe_stacks(StackName=mystack.stack_name)
        assert stacks['Stacks'][0]['StackName'] == mystack.stack_name
        assert ('product', 'confluence') in mystack.get_tags().items()  # check that a default tag is present

    def test_create_with_custom_tags(self):
        custom_tags = {"these": "are", "custom": "tags"}
        mystack = setup_confluence_stack(custom_tags=custom_tags)
        mystack_tags = mystack.get_tags()
        assert ('product', 'confluence') in mystack_tags.items()  # check that a default tag is still present
        assert all(key in mystack_tags and mystack_tags[key] == value for key, value in custom_tags.items())  # all custom tags are present

    def test_create_inaccessible_env(self):
        os.environ['ATL_ENVIRONMENT'] = 'stg'
        with pytest.raises(weaver_exceptions.StackEnvironmentInaccessible):
            setup_confluence_stack()

    def test_create_and_execute_changeset_with_template(self):
        mystack = setup_confluence_stack()
        params_for_update = [{"ParameterKey": "WebTargetGroupDrainingTimeout", "ParameterValue": "2024"}]
        with app.app_context():
            change_set_outcome = mystack.create_change_set(template={"repo": "testing", "name": CONF_TEMPLATE_FILE.name}, params=params_for_update, tags={})
            assert 'ResponseMetadata' in change_set_outcome
            assert change_set_outcome['ResponseMetadata']['HTTPStatusCode'] == 200
            change_set_name = change_set_outcome['Id']
            mystack.validate_service_responding = MagicMock(return_value=True)
            result = mystack.execute_change_set(change_set_name)
            assert result is True
            cfn = boto3.client('cloudformation', REGION)
            stacks = cfn.describe_stacks(StackName=mystack.stack_name)
            assert [param for param in stacks['Stacks'][0]['Parameters'] if param['ParameterKey'] == 'WebTargetGroupDrainingTimeout'][0]['ParameterValue'] == '2024'

    def test_create_and_execute_changeset_previous_template(self):
        mystack = setup_confluence_stack()
        params_for_update = [{"ParameterKey": "WebTargetGroupDrainingTimeout", "ParameterValue": "37"}]
        with app.app_context():
            change_set_outcome = mystack.create_change_set(template=None, params=params_for_update, tags={})
            assert 'ResponseMetadata' in change_set_outcome
            assert change_set_outcome['ResponseMetadata']['HTTPStatusCode'] == 200
            change_set_name = change_set_outcome['Id']
            mystack.validate_service_responding = MagicMock(return_value=True)
            result = mystack.execute_change_set(change_set_name)
            assert result is True
            cfn = boto3.client('cloudformation', REGION)
            stacks = cfn.describe_stacks(StackName=mystack.stack_name)
            assert [param for param in stacks['Stacks'][0]['Parameters'] if param['ParameterKey'] == 'WebTargetGroupDrainingTimeout'][0]['ParameterValue'] == '37'

    def test_clone(self):
        # set up stack to clone from (req for stack param retrieval)
        setup_confluence_stack()
        with app.app_context():
            deploy_env = 'stg'
            source_stack_name = f"{CONF_STACKNAME_PREFIX}-{random_suffix()}"
            clone_stack = utils.init_stack(f"{CONF_CLONE_STACKNAME_PREFIX}-{random_suffix()}", REGION)
            clone_params = get_stack_params(ProductEnum('Confluence'))
            for param in clone_params:
                for key, value in param.items():
                    if value == 'DeployEnvironment':
                        param['ParameterValue'] = deploy_env
            # setup mocks
            clone_stack.full_restart = MagicMock(return_value=True)
            clone_stack.get_sql = MagicMock(return_value='Select * from cwd_user limit 1;')
            clone_stack.validate_service_responding = MagicMock(return_value=True)
            clone_stack.wait_stack_action_complete = MagicMock(return_value=True)
            utils.get_username = MagicMock(return_value='test-user')
            try:
                g.action_id = utils.create_action(clone_stack, 'clone', deploy_env)
            except Exception:
                pytest.fail('Could not store action')
            outcome = clone_stack.clone(
                product=ProductEnum('Confluence'),
                template={"repo": "testing", "name": CONF_CLONE_TEMPLATE_FILE.name},
                stack_params=clone_params,
                tags={},
                cloned_from=source_stack_name,
            )
            assert outcome
            stacks = boto3.client('cloudformation', REGION).describe_stacks(StackName=clone_stack.stack_name)
            assert stacks['Stacks'][0]['StackName'] == clone_stack.stack_name
            clone_stack_tags = clone_stack.get_tags()
            assert ('environment', deploy_env) in clone_stack_tags.items()  # check that a default tag is present
            assert ('cloned_from', source_stack_name) in clone_stack_tags.items()  # check that cloned_from tag is preset

    def test_clone_inaccessible_env(self):
        setup_confluence_stack()
        with app.app_context():
            clone_stack = utils.init_stack(f"{CONF_CLONE_STACKNAME_PREFIX}-{random_suffix()}", REGION)
            os.environ['ATL_ENVIRONMENT'] = 'stg'
            with pytest.raises(weaver_exceptions.StackEnvironmentInaccessible):
                g.action_id = utils.create_action(clone_stack, 'clone', 'prod')

    def test_clone_destroy_failed(self):
        setup_confluence_stack()
        with app.app_context():
            clone_stack = utils.init_stack(f"{CONF_CLONE_STACKNAME_PREFIX}-{random_suffix()}", REGION)
            clone_params = get_stack_params(ProductEnum('Confluence'))
            # setup mocks
            clone_stack.wait_stack_action_complete = MagicMock(return_value=False)
            clone_stack.create = MagicMock(return_value=True)
            clone_stack.check_exists = MagicMock(return_value=True)
            clone_stack.destroy = MagicMock(return_value=False)
            utils.get_username = MagicMock(return_value='test-user')
            deploy_env: str = next((param['ParameterValue'] for param in clone_params if param['ParameterKey'] == 'DeployEnvironment'), '')
            try:
                g.action_id = utils.create_action(clone_stack, 'clone', deploy_env)
            except Exception:
                pytest.fail('Could not store action')
            outcome = clone_stack.clone(
                product=ProductEnum('Confluence'),
                template={"repo": "testing", "name": CONF_CLONE_TEMPLATE_FILE.name},
                stack_params=clone_params,
                tags={},
                cloned_from=f"{CONF_STACKNAME_PREFIX}-{random_suffix()}",
            )
            assert not outcome
            clone_stack.create.assert_not_called()

    def test_destroy(self):
        mystack = setup_confluence_stack()
        s3_bucket = mystack.get_s3_bucket_name()
        with app.app_context():
            try:
                g.action_id = utils.create_action(mystack, 'destroy')
            except Exception:
                pytest.fail('Could not store action')
            # upload a changelog and thread dump
            s3 = boto3.client('s3')
            s3.upload_file(os.path.relpath(DUMMY_FILE), s3_bucket, f'changelogs/{mystack.stack_name}')
            s3.upload_file(os.path.relpath(DUMMY_FILE), s3_bucket, f'changelogs/{mystack.stack_name}/changelog.log')
            s3.upload_file(os.path.relpath(DUMMY_FILE), s3_bucket, f'diagnostics/{mystack.stack_name}')
            s3.upload_file(os.path.relpath(DUMMY_FILE), s3_bucket, f'diagnostics/{mystack.stack_name}/threaddump.zip')
            # confirm files exist
            changelogs = s3.list_objects_v2(Bucket=s3_bucket, Prefix=f'changelogs/{mystack.stack_name}/')
            assert len(changelogs['Contents']) == 1
            diagnostics = s3.list_objects_v2(Bucket=s3_bucket, Prefix=f'diagnostics/{mystack.stack_name}/')
            assert len(diagnostics['Contents']) == 1
            # confirm stack exists
            cfn = boto3.client('cloudformation', REGION)
            stacks = cfn.describe_stacks()
            assert len(stacks['Stacks']) == 1
            # confirm stack has been deleted
            outcome = mystack.destroy(delete_changelogs=True, delete_threaddumps=True)
            assert outcome
            stacks = cfn.describe_stacks()
            assert len(stacks['Stacks']) == 0
            # confirm changelogs have been deleted
            changelogs = s3.list_objects_v2(Bucket=s3_bucket, Prefix=f'changelogs/{mystack.stack_name}/')
            assert 'Contents' not in changelogs
            # confirm threaddumps have been deleted
            diagnostics = s3.list_objects_v2(Bucket=s3_bucket, Prefix=f'diagnostics/{mystack.stack_name}/')
            assert 'Contents' not in diagnostics

    def test_dr_clone(self):
        setup_confluence_stack()
        with app.app_context():
            dr_stack = utils.init_stack(f"{CONF_CLONE_STACKNAME_PREFIX}-{random_suffix()}", REGION)
            clone_params = get_stack_params(ProductEnum('Confluence'))
            next(param for param in clone_params if param['ParameterKey'] == 'DeployEnvironment')['ParameterValue'] = 'dr'
            # setup mocks
            dr_stack.run_sql = MagicMock(return_value=False)
            dr_stack.validate_service_responding = MagicMock(return_value=True)
            dr_stack.wait_stack_action_complete = MagicMock(return_value=True)
            utils.get_username = MagicMock(return_value='test-user')
            deploy_env: str = next((param['ParameterValue'] for param in clone_params if param['ParameterKey'] == 'DeployEnvironment'), '')
            try:
                g.action_id = utils.create_action(dr_stack, 'clone', deploy_env)
            except Exception:
                pytest.fail('Could not store action')
            outcome = dr_stack.clone(
                product=ProductEnum('Confluence'),
                template={"repo": "testing", "name": CONF_CLONE_TEMPLATE_FILE.name},
                stack_params=clone_params,
                tags={},
                cloned_from=f"{CONF_STACKNAME_PREFIX}-{random_suffix()}",
            )
            assert outcome
            dr_stack.run_sql.assert_not_called()

    def test_ec2rollingreboot(self):
        mystack = setup_confluence_stack()
        # setup mocks
        mystack.check_node_status = MagicMock(return_value='RUNNING')
        mystack.get_tag = MagicMock(return_value='Confluence')
        stack_asg_names = [resource['PhysicalResourceId'] for resource in mystack.get_stack_resource_list() if resource['ResourceType'] == 'AWS::AutoScaling::AutoScalingGroup']
        drain_target_states = ['healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy', 'healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy']
        no_drain_target_states = ['initial', 'initial', 'healthy', 'initial', 'initial', 'healthy', 'initial', 'initial', 'healthy']
        with app.app_context():
            if not utils.store_current_action(mystack, 'ec2rollingreboot', id='dummyactionid'):
                pytest.fail('Could not store action')
            # mock node app_type
            nodes = mystack.get_stacknodes()
            for node in nodes:
                node.get_app_type = MagicMock(return_value='confluence')
            mystack.validate_nodes_product_version = MagicMock(return_value=True)
            # test rolling EC2 node reboot if deregistration delays are not expected values
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=False)
            with pytest.raises(weaver_exceptions.StackDeregistrationDelaysNotFullValueException):
                rolling_result = mystack.ec2_rolling_reboot(drain_type=weaver_enums.DrainType.DEFAULT)
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
            # test rolling EC2 node reboot with and without draining
            mystack.get_node_registration_state_alb = MagicMock(side_effect=no_drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=no_drain_target_states)
            rolling_result = mystack.ec2_rolling_reboot(drain_type=weaver_enums.DrainType.SKIP)
            assert rolling_result is True
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            rolling_drain_result = mystack.ec2_rolling_reboot(drain_type=weaver_enums.DrainType.DEFAULT)
            assert rolling_drain_result is True
            # test rolling EC2 node reboot with fast drain
            mystack.drain_type_allowed = MagicMock(return_value=True)
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            mystack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
            rolling_drain_result = mystack.ec2_rolling_reboot(drain_type=weaver_enums.DrainType.FAST)
            assert rolling_drain_result is True
            mystack.modify_deregistration_delays_to_fast_drain_value.assert_called()
            # test rolling EC2 node reboot with specific asg
            mystack.get_node_registration_state_alb = MagicMock(side_effect=no_drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=no_drain_target_states)
            rolling_result_with_asg = mystack.ec2_rolling_reboot(asg_name=stack_asg_names[0], drain_type=weaver_enums.DrainType.SKIP)
            assert rolling_result_with_asg is True
            # test rolling EC2 node reboot if a node is started on wrong version
            mystack.validate_nodes_product_version = MagicMock(return_value=False)
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            rolling_result = mystack.ec2_rolling_reboot(drain_type=weaver_enums.DrainType.DEFAULT)
            assert rolling_result is False

    @mock.patch.object(Session, 'get')
    def test_index_health_checking(self, mock_get):
        mystack = setup_confluence_stack()
        mockresponse = Mock()
        mockresponse.text = json.dumps({'indexHealth': '100.0'})
        mock_get.return_value = mockresponse
        all_nodes_result = mystack.get_index_health_all_nodes('personal_access_token')
        for node_result in all_nodes_result:
            assert float(node_result['index_health']) == 100.0

    def test_node_cpu(self):
        mystack = setup_confluence_stack()
        nodes = mystack.get_stacknodes()
        result = mystack.get_node_cpu(nodes[0].private_ip_address)
        # moto returns no metrics, but this proves that the function completed successfully
        assert result == {}

    def test_rebuild(self):
        mystack = setup_confluence_stack()
        # setup mocks
        mystack.check_service_status = MagicMock(return_value='RUNNING')
        mystack.check_node_status = MagicMock(return_value='RUNNING')
        mystack.get_tag = MagicMock(return_value='confluence')
        mystack.is_app_clustered = MagicMock(return_value=True)
        mystack.validate_nodes_product_version = MagicMock(return_value=True)
        with app.app_context():
            if not utils.store_current_action(mystack, 'rebuild', id='dummyactionid'):
                pytest.fail('Could not store action')
            # test rolling rebuild if deregistration delays are not expected values
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=False)
            rebuild_result = mystack.rolling_rebuild()
            assert not rebuild_result
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
            # simple rolling rebuild of Confluence
            drain_target_states = ['healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy', 'healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy']
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            mystack.validate_node_responding = MagicMock(return_value=True)
            rebuild_result = mystack.rolling_rebuild()
            assert rebuild_result is True
            # rolling rebuild with index health checking in Jira
            mystack.get_tag = MagicMock(return_value='jira')
            mystack.get_index_health = MagicMock(return_value=100)
            mystack.resume_AddToLoadBalancer = MagicMock(return_value=True)  # not implemented in moto
            drain_target_states = [
                'healthy',
                'draining',
                'notregistered',
                'draining',
                'notregistered',
                'initial',
                'healthy',
                'healthy',
                'draining',
                'notregistered',
                'draining',
                'notregistered',
                'initial',
                'initial',
                'healthy',
            ]
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            # override buggy moto function that throws an exception when deregistering a target that hasn't been registered yet
            moto.elbv2.models.ELBv2Backend.deregister_targets = moto_overrides.deregister_targets
            rebuild_result = mystack.rolling_rebuild(personal_access_token='test')  # nosec B106
            assert rebuild_result is True
            # test rolling rebuild with fast drain
            mystack.drain_type_allowed = MagicMock(return_value=True)
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            mystack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
            rebuild_result = mystack.rolling_rebuild(drain_type=weaver_enums.DrainType.FAST)
            assert rebuild_result is True
            mystack.modify_deregistration_delays_to_fast_drain_value.assert_called()
            # test rolling rebuild with node started on wrong version
            mystack.validate_nodes_product_version = MagicMock(return_value=False)
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            rebuild_result = mystack.rolling_rebuild()
            assert rebuild_result is False

    def test_restarts(self):
        mystack = setup_confluence_stack()
        # setup mocks
        mystack.check_service_status = MagicMock(return_value='RUNNING')
        mystack.check_node_status = MagicMock(return_value='RUNNING')
        mystack.get_tag = MagicMock(return_value='Confluence')
        mystack.is_app_clustered = MagicMock(return_value=True)
        stack_asg_names = [resource['PhysicalResourceId'] for resource in mystack.get_stack_resource_list() if resource['ResourceType'] == 'AWS::AutoScaling::AutoScalingGroup']
        drain_target_states = ['healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy', 'healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy']
        no_drain_target_states = ['initial', 'initial', 'healthy', 'initial', 'initial', 'healthy', 'initial', 'initial', 'healthy']
        with app.app_context():
            if not utils.store_current_action(mystack, 'restarts', id='dummyactionid'):
                pytest.fail('Could not store action')
            # mock nodes and target states
            nodes = mystack.get_stacknodes()
            mystack.get_node_registration_state_alb = MagicMock(side_effect=no_drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=no_drain_target_states)
            mystack.validate_node_responding = MagicMock(return_value=True)
            # mock node app_type
            for node in nodes:
                node.get_app_type = MagicMock(return_value='confluence')
            # test rolling restart if deregistration delays are not expected values
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=False)
            with pytest.raises(weaver_exceptions.StackDeregistrationDelaysNotFullValueException):
                rolling_drain_result = mystack.rolling_restart(drain_type=weaver_enums.DrainType.DEFAULT)
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
            # test rolling restart with and without draining
            rolling_result = mystack.rolling_restart(drain_type=weaver_enums.DrainType.SKIP)
            assert rolling_result is True
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            rolling_drain_result = mystack.rolling_restart(drain_type=weaver_enums.DrainType.DEFAULT)
            assert rolling_drain_result is True
            # test rolling restart with fast drain
            mystack.drain_type_allowed = MagicMock(return_value=True)
            mystack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            rolling_drain_result = mystack.rolling_restart(drain_type=weaver_enums.DrainType.FAST)
            assert rolling_drain_result is True
            mystack.modify_deregistration_delays_to_fast_drain_value.assert_called()
            # test rolling restart with specific asg
            mystack.get_node_registration_state_alb = MagicMock(side_effect=no_drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=no_drain_target_states)
            rolling_result_with_asg = mystack.rolling_restart(asg_name=stack_asg_names[0], drain_type=weaver_enums.DrainType.SKIP)
            assert rolling_result_with_asg is True
            # test full restart
            full_result = mystack.full_restart()
            assert full_result is True
            # test node restart if deregistration delays are not expected values
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=False)
            with pytest.raises(weaver_exceptions.StackDeregistrationDelaysNotFullValueException):
                restart_node_result = mystack.restart_node(nodes[1].private_ip_address, drain_type=weaver_enums.DrainType.DEFAULT)
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
            # test node restart with and without draining
            mystack.get_node_registration_state_alb = MagicMock(side_effect=no_drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=no_drain_target_states)
            restart_node_result = mystack.restart_node(nodes[0].private_ip_address, drain_type=weaver_enums.DrainType.SKIP)
            assert restart_node_result is True
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            restart_node_drain_result = mystack.restart_node(nodes[1].private_ip_address, drain_type=weaver_enums.DrainType.DEFAULT)
            assert restart_node_drain_result is True
            # test node restart with fast drain
            mystack.drain_type_allowed = MagicMock(return_value=True)
            mystack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            restart_node_drain_result = mystack.restart_node(nodes[1].private_ip_address, drain_type=weaver_enums.DrainType.FAST)
            assert restart_node_drain_result is True
            mystack.modify_deregistration_delays_to_fast_drain_value.assert_called()
            # perform restarts
            # expect failures when node count is 0
            mystack.get_stacknodes = MagicMock(return_value=[])
            rolling_result = mystack.rolling_restart(drain_type=weaver_enums.DrainType.SKIP)
            assert rolling_result is False
            full_result = mystack.full_restart()
            assert full_result is False

    def test_reinit(self):
        mystack = setup_confluence_stack()
        # setup mocks
        mystack.check_service_status = MagicMock(return_value='RUNNING')
        mystack.check_node_status = MagicMock(return_value='RUNNING')
        mystack.ssm_get_command_output = MagicMock(return_value=f'/opt/aws/bin/cfn-init -v --stack {mystack.stack_name} --resource ClusterNodeLaunchTemplate --region {REGION}')
        mystack.validate_nodes_product_version = MagicMock(return_value=True)
        drain_target_states = ['healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy', 'healthy', 'draining', 'notregistered', 'initial', 'initial', 'healthy']
        # mock node app_type
        for node in mystack.get_stacknodes():
            node.get_app_type = MagicMock(return_value='confluence')
        with app.app_context():
            if not utils.store_current_action(mystack, 'reinit', id='dummyactionid'):
                pytest.fail('Could not store action')
            # test rolling reinit if deregistration delays are not expected values
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=False)
            with pytest.raises(weaver_exceptions.StackDeregistrationDelaysNotFullValueException):
                rolling_result = mystack.rolling_reinit()
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
            # test rolling reinit
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            mystack.validate_node_responding = MagicMock(return_value=True)
            rolling_result = mystack.rolling_reinit()
            assert rolling_result is True
            # test rolling reinit with fast drain
            mystack.drain_type_allowed = MagicMock(return_value=True)
            mystack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            mystack.validate_node_responding = MagicMock(return_value=True)
            rolling_result = mystack.rolling_reinit(drain_type=weaver_enums.DrainType.FAST)
            assert rolling_result is True
            mystack.modify_deregistration_delays_to_fast_drain_value.assert_called()
            # test rolling reinit with node started up on wrong version
            mystack.get_node_registration_state_alb = MagicMock(side_effect=drain_target_states)
            mystack.get_node_registration_state_nlb = MagicMock(side_effect=drain_target_states)
            mystack.validate_nodes_product_version = MagicMock(return_value=False)
            rolling_result = mystack.rolling_reinit()
            assert rolling_result is False

    def test_sns(self):
        mystack = setup_confluence_stack()
        action_msg = 'test msg'
        with app.test_request_context(''):
            session = {'saml': {'subject': 'UserA'}}
            # stack creation sends an sns msg now which creates the sns topic
            # confirm a topic has been created for weaver msgs
            topic_arn = get_sns_topic_arn(mystack)
            assert topic_arn is not None
            # send a msg
            published_msg = send_sns_msg(mystack, action_msg)
            # confirm the message was sent successfully
            assert published_msg is not None
            assert published_msg['MessageId'] is not None

    def test_adding_tags(self):
        mystack = setup_confluence_stack()
        tags_to_add = {"Tag1": "Value1", "Tag2": "Value2"}
        with app.app_context():
            change_set_outcome = mystack.create_change_set(template=None, params=[], tags=tags_to_add)
            assert 'ResponseMetadata' in change_set_outcome
            assert change_set_outcome['ResponseMetadata']['HTTPStatusCode'] == 200
            change_set_name = change_set_outcome['Id']
            mystack.validate_service_responding = MagicMock(return_value=True)
            result = mystack.execute_change_set(change_set_name)
            assert result is True
            tags = mystack.get_tags()
            assert all(key in tags and tags[key] == value for key, value in tags_to_add.items())  # all tags_to_add are present with correct values

    def test_updating_tags(self):
        mystack = setup_confluence_stack()
        tags_to_update = {"created_by": "Trogdor"}
        with app.app_context():
            change_set_outcome = mystack.create_change_set(template=None, params=[], tags=tags_to_update)
            assert 'ResponseMetadata' in change_set_outcome
            assert change_set_outcome['ResponseMetadata']['HTTPStatusCode'] == 200
            change_set_name = change_set_outcome['Id']
            mystack.validate_service_responding = MagicMock(return_value=True)
            result = mystack.execute_change_set(change_set_name)
            assert result is True
            tags = mystack.get_tags()
            assert all(key in tags and tags[key] == value for key, value in tags_to_update.items())  # all tags_to_update are present with correct values

    def test_removing_tags(self):
        mystack = setup_confluence_stack()
        tags_to_remove = {"created_by": ""}
        with app.app_context():
            change_set_outcome = mystack.create_change_set(template=None, params=[], tags=tags_to_remove)
            assert 'ResponseMetadata' in change_set_outcome
            assert change_set_outcome['ResponseMetadata']['HTTPStatusCode'] == 200
            change_set_name = change_set_outcome['Id']
            mystack.validate_service_responding = MagicMock(return_value=True)
            result = mystack.execute_change_set(change_set_name)
            assert result is True
            tags = mystack.get_tags()
            assert "created_by" not in tags

    def test_thread_and_heap_dumps(self):
        mystack = setup_confluence_stack()
        # setup mocks
        nodes = mystack.get_stacknodes()
        with app.app_context():
            if not utils.store_current_action(mystack, 'thread_and_heap_dumps', id='dummyactionid'):
                pytest.fail('Could not store action')
            # test thread dumps
            thread_result = mystack.thread_dump(alsoHeaps=False)
            assert thread_result is True
            single_node_thread_result = mystack.thread_dump(node_ip=nodes[0].private_ip_address, alsoHeaps=False)
            assert single_node_thread_result is True
            # test heap dumps
            heap_result = mystack.heap_dump()
            assert heap_result is True
            single_node_heap_result = mystack.heap_dump(node_ip=nodes[1].private_ip_address)
            assert single_node_heap_result is True

    def test_thread_dump_links(self):
        mystack = setup_confluence_stack()
        # upload a dummy thread dump
        s3 = boto3.client('s3')
        s3.upload_file(os.path.relpath(DUMMY_FILE), mystack.get_s3_bucket_name(), f'diagnostics/{mystack.stack_name}/stack_1.12.123.255_thread_dumps_19700101_060100.tar.gz')
        with app.app_context():
            thread_dump_links = mystack.get_s3_file_links(file_type=weaver_enums.DiagnosticFileType.THREAD_DUMP)
            assert len(thread_dump_links) > 0

    @mock.patch.object(Session, 'get')
    @mock.patch.object(Session, 'post')
    def test_support_zip(self, mock_post, mock_get):
        mystack = setup_confluence_stack()
        nodes = mystack.get_stacknodes()
        node1_ip = nodes[0].private_ip_address
        node2_ip = nodes[1].private_ip_address
        # simulate JSON data returned by status job check
        ZIP_JSON_RESPONSE_FROM_POST = json.dumps(
            {
                "clusterTaskId": "3e58f26a-fdde-432e-b725-96b9d4991fb2",
                "tasks": [
                    {
                        "taskId": "f30073ba-1b01-4704-ba9a-4f9fee39c25d",
                        "progressPercentage": 70,
                        "progressMessage": "Processing 'Application properties' ",
                        "nodeId": f"i-0188f4e47c950af08-ipp-{node1_ip}",
                        "fileName": f"Jira_i-0188f4e47c950af08-ipp-{node1_ip.replace('.', '-')}_ip-{node1_ip.replace('.', '-')}-us-west-2-compute-internal_support_2022-02-05-03-06-59.zip",
                        "warnings": [],
                        "status": "IN_PROGRESS",
                        "truncatedFiles": [],
                    },
                    {
                        "taskId": "e5dda050-d561-461b-bd88-d9d36d3a0a33",
                        "progressPercentage": 70,
                        "progressMessage": "Processing 'Application properties'",
                        "nodeId": f"i-028790d63e841f047-ipp-{node2_ip}",
                        "fileName": f"Jira_i-028790d63e841f047-ipp-{node2_ip.replace('.', '-')}_ip-{node2_ip.replace('.', '-')}-us-west-2-compute-internal_support_2022-02-05-03-07-00.zip",
                        "warnings": [],
                        "status": "IN_PROGRESS",
                        "truncatedFiles": [],
                    },
                ],
            }
        )
        ZIP_JSON_RESPONSE_FROM_GET = json.dumps(
            {
                "clusterTaskId": "3e58f26a-fdde-432e-b725-96b9d4991fb2",
                "tasks": [
                    {
                        "taskId": "f30073ba-1b01-4704-ba9a-4f9fee39c25d",
                        "progressPercentage": 100,
                        "progressMessage": f"It was saved to /media/atl/jira/shared/export/Jira_i-0188f4e47c950af08-ipp-{node1_ip.replace('.', '-')}_ip-{node1_ip.replace('.', '-')}-us-west-2-compute-internal_support_2022-02-05-03-06-59.zip.",
                        "nodeId": f"i-0188f4e47c950af08-ipp-{node1_ip}",
                        "fileName": f"Jira_i-0188f4e47c950af08-ipp-{node1_ip.replace('.', '-')}_ip-{node1_ip.replace('.', '-')}-us-west-2-compute-internal_support_2022-02-05-03-06-59.zip",
                        "warnings": [],
                        "status": "SUCCESSFUL",
                        "truncatedFiles": [],
                    },
                    {
                        "taskId": "e5dda050-d561-461b-bd88-d9d36d3a0a33",
                        "progressPercentage": 100,
                        "progressMessage": f"It was saved to /media/atl/jira/shared/export/Jira_i-028790d63e841f047-ipp-{node2_ip.replace('.', '-')}_ip-{node2_ip.replace('.', '-')}-us-west-2-compute-internal_support_2022-02-05-03-07-00.zip.",
                        "nodeId": f"i-028790d63e841f047-ipp-{node2_ip}",
                        "fileName": f"Jira_i-028790d63e841f047-ipp-{node2_ip.replace('.', '-')}_ip-{node2_ip.replace('.', '-')}-us-west-2-compute-internal_support_2022-02-05-03-07-00.zip",
                        "warnings": [],
                        "status": "SUCCESSFUL",
                        "truncatedFiles": [],
                    },
                ],
            }
        )
        # setup mocks
        mystack.get_service_url = mystack.service_url = MagicMock(return_value=SERVICE_URL)
        mockresponse_post = MagicMock(status_code=200)
        mockresponse_post.text = ZIP_JSON_RESPONSE_FROM_POST
        mock_post.return_value = mockresponse_post
        mockresponse_get = MagicMock(status_code=200)
        mockresponse_get.text = ZIP_JSON_RESPONSE_FROM_GET
        mock_get.return_value = mockresponse_get
        with app.app_context():
            if not utils.store_current_action(mystack, 'support_zip', id='dummyactionid'):
                pytest.fail('Could not store action')
            # test generate support zip
            support_zip_result = mystack.generate_support_zip('personal_access_token')
            assert support_zip_result is True

    def test_support_zip_links(self):
        mystack = setup_confluence_stack()
        FILENAME = "Jira_i-0188f4e47c950af08-ipp-10-161-30-131_ip-10-161-30-131-us-west-2-compute-internal_support_2022-02-05-03-06-59.zip"
        search_pattern = rf"^https://atl-labs-dc-weaver-[0-9]{{12}}.s3.amazonaws.com/diagnostics/\w.+?/{FILENAME}\?AWSAccessKeyId.+$"
        # upload dummy support zip files
        s3 = boto3.client('s3')
        s3.upload_file(
            os.path.relpath(DUMMY_FILE),
            mystack.get_s3_bucket_name(),
            f'diagnostics/{mystack.stack_name}/{FILENAME}',
        )
        with app.app_context():
            support_zip_links = mystack.get_s3_file_links(file_type=weaver_enums.DiagnosticFileType.SUPPORT_ZIP)
            assert len(support_zip_links) > 0
            assert re.match(search_pattern, support_zip_links[0])

    def test_toggle_node(self):
        mystack = setup_confluence_stack()
        # setup mocks
        mystack.check_service_status = MagicMock(return_value='RUNNING')
        mystack.check_node_status = MagicMock(return_value='RUNNING')
        mystack.get_tag = MagicMock(return_value='Confluence')
        mystack.is_app_clustered = MagicMock(return_value=True)
        deregister_target_states = ['healthy', 'healthy', 'draining', 'draining', 'notregistered']
        register_target_states = ['notregistered', 'notregistered', 'initial', 'initial', 'healthy']
        with app.app_context():
            with app.test_request_context(''):
                if not utils.store_current_action(mystack, 'toggle_node', id='dummyactionid'):
                    pytest.fail('Could not store action')
                session = {'saml': {'subject': 'UserA'}}
                # mock nodes
                nodes = mystack.get_stacknodes()
                # register node initially
                mystack.get_node_registration_state_alb = MagicMock(side_effect=register_target_states)
                mystack.get_node_registration_state_nlb = MagicMock(side_effect=register_target_states)
                register_result = mystack.toggle_node_registration(node_ip=nodes[0].private_ip_address)
                assert register_result is True
                # deregister node
                # fail if deregistration delays are not expected values
                mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=False)
                mystack.get_node_registration_state_alb = MagicMock(side_effect=deregister_target_states)
                mystack.get_node_registration_state_nlb = MagicMock(side_effect=deregister_target_states)
                with pytest.raises(weaver_exceptions.StackDeregistrationDelaysNotFullValueException):
                    deregister_result = mystack.toggle_node_registration(node_ip=nodes[0].private_ip_address)
                # otherwise ensure success
                mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
                deregister_result = mystack.toggle_node_registration(node_ip=nodes[0].private_ip_address)
                assert deregister_result is True
                # re-register node
                mystack.get_node_registration_state_alb = MagicMock(side_effect=register_target_states)
                mystack.get_node_registration_state_nlb = MagicMock(side_effect=register_target_states)
                register_result = mystack.toggle_node_registration(node_ip=nodes[0].private_ip_address)
                assert register_result is True
                # confirm a draining node re-registers
                mystack.get_node_registration_state_alb = MagicMock(side_effect=['draining'])
                mystack.get_node_registration_state_nlb = MagicMock(side_effect=['draining'])
                mystack.begin_registering_node_to_all_target_groups = MagicMock(return_value=True)
                mystack.wait_nodes_registration_states = MagicMock(return_value=True)
                mystack.toggle_node_registration(node_ip=nodes[0].private_ip_address)
                assert mystack.begin_registering_node_to_all_target_groups.called
                # test fast drain success
                mystack.drain_type_allowed = MagicMock(return_value=True)
                mystack.modify_deregistration_delays_to_fast_drain_value = MagicMock()
                mystack.get_node_registration_state_alb = MagicMock(side_effect=deregister_target_states)
                mystack.get_node_registration_state_nlb = MagicMock(side_effect=deregister_target_states)
                deregister_result = mystack.toggle_node_registration(node_ip=nodes[0].private_ip_address, drain_type=weaver_enums.DrainType.FAST)
                assert deregister_result is True
                mystack.modify_deregistration_delays_to_fast_drain_value.assert_called()

    def test_upgrade(self):
        mystack = setup_confluence_stack()
        assert mystack.get_param_value('ProductVersion') == '6.11.0'
        # mock stack methods
        mystack.check_service_status = MagicMock(return_value='RUNNING')
        mystack.get_service_url = mystack.service_url = MagicMock(return_value=SERVICE_URL)
        mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
        mystack.validate_nodes_product_version = MagicMock(return_value=True)
        # mock node app_type
        for node in mystack.get_stacknodes():
            node.get_app_type = MagicMock(return_value='confluence')
        # upgrade
        with app.app_context():
            if not utils.store_current_action(mystack, 'upgrade', id='dummyactionid'):
                pytest.fail('Could not store action')
            upgrade_result: bool = mystack.upgrade('8.5.7', '')
        assert upgrade_result is True
        assert mystack.get_param_value('ProductVersion') == '8.5.7'

    def test_upgrade_inaccessible_env(self):
        mystack = setup_confluence_stack()
        os.environ['ATL_ENVIRONMENT'] = 'stg'
        with app.app_context():
            with pytest.raises(weaver_exceptions.StackEnvironmentInaccessible):
                g.action_id = utils.create_action(mystack, 'upgrade')

    ### JIRA TESTS

    def test_create_jira(self):
        mystack = setup_jira_stack()
        stacks = boto3.client('cloudformation', REGION).describe_stacks(StackName=mystack.stack_name)
        assert stacks['Stacks'][0]['StackName'] == mystack.stack_name

    def test_upgrade_jira(self):
        with app.app_context():
            mystack = setup_jira_stack()
            assert mystack.get_param_value('ProductVersion') == '8.22.5'
            # mock stack methods
            mystack.check_service_status = MagicMock(return_value='RUNNING')
            mystack.get_service_url = mystack.service_url = MagicMock(return_value=SERVICE_URL)
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
            mystack.validate_nodes_product_version = MagicMock(return_value=True)
            # mock node app_type
            for node in mystack.get_stacknodes():
                node.get_app_type = MagicMock(return_value='jira')
            # upgrade
            if not utils.store_current_action(mystack, 'upgrade', id='dummyactionid'):
                pytest.fail('Could not store action')
            upgrade_result: bool = mystack.upgrade('8.22.6', '')
        assert upgrade_result is True
        assert mystack.get_param_value('ProductVersion') == '8.22.6'

    def test_upgrade_jira_with_custom_url(self):
        with app.app_context():
            mystack = setup_jira_stack()
            assert mystack.get_param_value('ProductVersion') == '8.22.5'
            # mock stack methods
            mystack.check_service_status = MagicMock(return_value='RUNNING')
            mystack.get_service_url = mystack.service_url = MagicMock(return_value=SERVICE_URL)
            mystack.stack_deregistration_delays_are_full_value = MagicMock(return_value=True)
            mystack.validate_nodes_product_version = MagicMock(return_value=True)
            # mock node app_type
            for node in mystack.get_stacknodes():
                node.get_app_type = MagicMock(return_value='jira')
            # upgrade
            if not utils.store_current_action(mystack, 'upgrade', id='dummyactionid'):
                pytest.fail('Could not store action')
            upgrade_result: bool = mystack.upgrade(
                '8.22.6',
                'https://downloads-internal-us-east-1.s3.amazonaws.com/public/jira/8.22.6-RELEASE-BDOG-10491/atlassian-jira-software-8.22.6-RELEASE-BDOG-10491-standalone.tar.gz',
            )
        assert upgrade_result is True
        assert mystack.get_param_value('ProductVersion') == '8.22.6'
        assert (
            mystack.get_param_value('ProductDownloadUrl')
            == 'https://downloads-internal-us-east-1.s3.amazonaws.com/public/jira/8.22.6-RELEASE-BDOG-10491/atlassian-jira-software-8.22.6-RELEASE-BDOG-10491-standalone.tar.gz'
        )
