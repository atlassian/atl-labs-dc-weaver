import json
import mock
import os

from copy import deepcopy
from unittest.mock import MagicMock
from requests.models import Response

import boto3
from moto import mock_aws
import pytest

from weaver.models import ProductEnum, get_incompatible_field_error_msg, get_missing_required_field_error_msg
import weaver
import weaver.aws_cfn
import weaver.enums as weaver_enums

from tests.func.aws_functionality import test_stacks

test_stacks.app.config['CLONE_DEFAULTS'] = {
    'all': {'ClusterNodeCount': '1', 'CustomDnsName': '', 'DBMultiAZ': 'false', 'DeployEnvironment': 'stg', 'MailEnabled': 'false', 'Monitoring': 'false', 'TomcatScheme': 'http'},
}
test_stacks.app.config['NO_SAML'] = True
test_stacks.app.config['AVAILABLE_REGIONS'] = ['us-east-1', 'us-east-2']  # tests below only use us-east-1/2
test_stacks.app.config['ACCESSIBLE_ENVIRONMENTS'] = {'local': ['dev', 'dr', 'stg', 'prod'], 'no-access': []}
weaver_regions = [region_tuple[0] for region_tuple in test_stacks.app.config["REGIONS"]]

NO_REGION_ERROR = ['Missing data for required field.']
INVALID_REGION_ERROR = ['Region not a valid AWS region']
UNCONFIGURED_REGION_ERROR = [f'Region not configured for use with this Weaver deployment (expected one of: {", ".join(weaver_regions)})']


def mocked_alternate_download_url_head_request(*args, **kwargs):
    response = Response()
    response.status_code = kwargs.get("status_code", 418)
    response.reason = kwargs.get("reason", "I'm a teapot")
    return response


def mock_get_tag_side_effect(tag_name: str, log_msgs: bool):
    if tag_name == 'environment':
        return 'dev'
    else:
        return False


# Common test assertions


def assert_no_region_error_res(response):
    assert response.get_json()['error_msg']['query']['region'] == NO_REGION_ERROR
    assert response.status_code == 400


def assert_bad_region_error_res(response):
    assert response.get_json()['error_msg']['query']['region'] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
    assert response.status_code == 400


def assert_no_req_body_error_res(response):
    assert response.get_json()['error_msg']
    assert response.status_code == 400


def assert_req_body_invalid_ipv4(response, field: str):
    assert response.get_json()['error_msg']['json'][field] == ['Not a valid IPv4 address.']
    assert response.status_code == 400


def assert_req_body_missing_required_field(response, required_field: str):
    assert response.get_json()['error_msg']['json'][required_field] == ['Missing data for required field.']
    assert response.status_code == 400


def assert_req_body_unrecognised_field_value(response, field: str):
    assert 'Must be one of' in response.get_json()['error_msg']['json'][field][0]
    assert response.status_code == 400


# For errors thrown in @validates_schema decorated functions
def assert_req_body_schema_error(response, error_msg: str):
    assert response.get_json()['error_msg']['json']['_schema'] == [error_msg]
    assert response.status_code == 400


def assert_req_body_field_validation_error(response, field_name: str, error_msg: str):
    assert response.get_json()['error_msg']['json'][field_name] == [error_msg]
    assert response.status_code == 400


# Fixtures - Mocks


@pytest.fixture
def mock_client():
    with test_stacks.app.test_client() as client:
        # Not sure why but putting test_stacks.setup_confluence_stack() here doesn't work
        yield client


@pytest.fixture(scope="session")
def mock_valid_asg_name():
    return 'ClusterNodeGroup-asdfg'


@pytest.fixture(scope="session")
def mock_invalid_asg_name():
    return 'ClusterBotNodeGroup-qwerty'


@pytest.fixture(scope="session")
def mock_asgs(mock_valid_asg_name):
    return [{'AutoScalingGroupName': mock_valid_asg_name}] + [{'AutoScalingGroupName': f'ClusterNodeGroup-{i}' for i in range(3)}]


@pytest.fixture
def mock_cpu_res():
    return {
        "1727246700": 11.359842414529915,
        "1727246760": 11.20123921516373,
        "1727246580": 11.384092272818187,
        "1727247600": 14.72965092060318,
        "1727246640": 10.649813482547295,
        "1727247660": 11.135578799859866,
        "1727245920": 12.137581486853733,
        "1727246940": 10.904880121294923,
        "1727247060": 11.401682351961357,
        "1727245980": 10.936118387026811,
        "1727247000": 11.205976520811099,
        "1727246820": 11.02107509670535,
        "1727246880": 11.355683673809445,
        "1727247240": 11.065006915629322,
        "1727246160": 11.628294961628294,
        "1727247300": 11.718684838273807,
        "1727246220": 10.985807569296377,
        "1727247120": 10.808693623120769,
        "1727246040": 11.38105712190895,
        "1727247180": 11.414954423503142,
        "1727246100": 10.95936042638241,
        "1727246460": 11.159449278261159,
        "1727247480": 11.087953313880783,
        "1727246400": 14.402332361516034,
        "1727246520": 11.038906939931683,
        "1727247540": 11.062794688525683,
        "1727246280": 11.577894473618404,
        "1727247360": 11.839541738131318,
        "1727247420": 11.234082272151477,
        "1727246340": 10.899636678777375,
    }


@pytest.fixture(scope="session")
def mock_index_health_res():
    return 99.995


@pytest.fixture
def mock_heap_dump_links():
    return [f"https://my-bucket.s3.amazonaws.com/diagnostics/my-confluence/2000-01-0{i}/my-confluence_10.10.10.10_heap_dump_2000010{i}_000000.hprof.gz" for i in range(4)]


@pytest.fixture
def mock_thread_dump_links():
    return [f"https://my-bucket.s3.amazonaws.com/diagnostics/my-confluence/2000-01-0{i}/my-confluence_10.10.10.10_thread_dumps_2000010{i}_000000.tar.gz" for i in range(4)]


@pytest.fixture
def mock_support_zip_links():
    return [
        f"https://my-bucket.s3.amazonaws.com/diagnostics/my-confluence/2000-01-0{i}/Confluence_i-0000000000000000-ipp-10-10-10-10_521d9204_-10-10-10-10-5801_support_2000-00-0{i}-00-00-00.zip"
        for i in range(4)
    ]


@pytest.fixture(scope="session")
def mock_valid_node_ip():
    return '10.10.10.10'


@pytest.fixture(scope="session")
def mock_invalid_node_ip():
    return '13.13.13.13'


@pytest.fixture(scope="session")
def create_mock_node():
    def _create_mock_node(node_ip):
        mock_node = MagicMock()
        mock_node.private_ip_address = node_ip
        return mock_node

    return _create_mock_node


@pytest.fixture
def mock_stack_nodes(create_mock_node, mock_valid_node_ip):
    return [create_mock_node(mock_valid_node_ip)]


# Fixtures - Side Effects


@pytest.fixture
def get_s3_file_links_side_effect(mock_heap_dump_links, mock_support_zip_links, mock_thread_dump_links):
    def _get_s3_file_links_side_effect(*args, **kwargs):
        if kwargs['file_type'] == weaver_enums.DiagnosticFileType.HEAP_DUMP:
            return mock_heap_dump_links
        elif kwargs['file_type'] == weaver_enums.DiagnosticFileType.SUPPORT_ZIP:
            return mock_support_zip_links
        elif kwargs['file_type'] == weaver_enums.DiagnosticFileType.THREAD_DUMP:
            return mock_thread_dump_links

    return _get_s3_file_links_side_effect


# Fixtures - Other Functions


# Use this after successfully sumbitting an action to ensure graceful completion
@pytest.fixture
def wait_for_action_to_complete(mock_client):
    def _wait_for_action_to_complete(action_id: str, stack_name: str):
        action_complete = False
        while not action_complete:
            action_status = mock_client.get(f'/api/v1/stack/{stack_name}/action?region=us-east-1&action_id={action_id}')
            action_complete = action_status.json['status'] == 'COMPLETE'

    return _wait_for_action_to_complete


@mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestStackMethods:
    def test_stack_put(self):
        with test_stacks.app.app_context():
            test_stacks.setup_env_resources()
        with test_stacks.app.test_client() as client:
            # set up mocks – we're testing the route here, not the ability to actually create stacks
            _orig_cfn_stack_create = weaver.aws_cfn.CfnStack.create
            weaver.aws_cfn.CfnStack.create = MagicMock(return_value=True)
            # prevent futures from background threads from creating new futures threads (used by s3.meta.client.upload_file)
            weaver.utils.save_changelog = MagicMock(return_value=True)
            # prepare request and body data
            test_stack_name = f"{test_stacks.CONF_STACKNAME_PREFIX}-{test_stacks.random_suffix()}"
            req_body = {
                "product": "confluence",
                "template": {"repo": "testing", "name": test_stacks.CONF_TEMPLATE_FILE.name},
                "params": [
                    {"parameter_key": param["ParameterKey"], "parameter_value": param["ParameterValue"]} for param in test_stacks.get_stack_params(ProductEnum('Confluence'))
                ],
            }
            # test no region provided
            response = client.put(f"/api/v1/stack/{test_stack_name}", json=req_body)
            assert response.get_json()["error_msg"]["query"]["region"] == NO_REGION_ERROR
            assert response.status_code == 400
            # test bad region
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=does-not-exist", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["query"]["region"] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            # test bad product
            bad_req_body = deepcopy(req_body)
            bad_req_body["product"] = "somevalhere"
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=us-east-1", json=bad_req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["product"] == ["Must be one of: bitbucket, bitbucket_mesh, bitbucket_mirror, confluence, crowd, jira."]
            # test non-existent template
            bad_req_body = deepcopy(req_body)
            bad_req_body["template"] = {"repo": "testing", "name": "non-existent.template.yaml"}
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=us-east-1", json=bad_req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["template"]["_schema"] == ["Template not found"]
            # test bad parameter
            bad_req_body = deepcopy(req_body)
            bad_req_body["params"].append({"parameter_key": "SomeBadParamHere", "parameter_value": "bad value"})
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=us-east-1", json=bad_req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["params"] == ["Unsupported parameters for template func-test-confluence.template.yaml: SomeBadParamHere"]
            # test no request body
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=us-east-1")
            assert response.status_code == 400
            # test inaccessible stack environ
            os.environ['ATL_ENVIRONMENT'] = 'no-access'
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=us-east-1", json=req_body)
            assert response.status_code == 500
            del os.environ['ATL_ENVIRONMENT']
            # test action in-progress
            test_stacks.app.config["STACK_LOCKING"] = True
            test_stacks.app.config["LOCKS"][test_stack_name] = "delete"
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=us-east-1", json=req_body)
            assert response.status_code == 409
            test_stacks.app.config["STACK_LOCKING"] = False
            del test_stacks.app.config["LOCKS"][test_stack_name]
            # test normal request body
            response = client.put(f"/api/v1/stack/{test_stack_name}?region=us-east-1", json=req_body)
            assert response.status_code == 202
            # test jira normal request body
            jira_req_body = {
                "product": "jira",
                "template": {"repo": "testing", "name": test_stacks.JIRA_TEMPLATE_FILE.name},
                "params": [{"parameter_key": param["ParameterKey"], "parameter_value": param["ParameterValue"]} for param in test_stacks.get_stack_params(ProductEnum('Jira'))],
            }
            response = client.put(f"/api/v1/stack/{test_stacks.JIRA_STACKNAME_PREFIX}-{test_stacks.random_suffix()}?region=us-east-1", json=jira_req_body)
            assert response.status_code == 202
            # test stack exists – reset mocked CfnStack.create first
            weaver.aws_cfn.CfnStack.create = _orig_cfn_stack_create
            mystack = test_stacks.setup_confluence_stack()
            response = client.put(f"/api/v1/stack/{mystack.stack_name}?region=us-east-1", json=req_body)
            assert response.status_code == 409

    def test_stack_list_get(self):
        with test_stacks.app.test_client() as client:
            mystack = test_stacks.setup_confluence_stack()
            # test no region provided
            response = client.get('/api/v1/stack')
            assert response.get_json()['error_msg']['query']['region'] == NO_REGION_ERROR
            assert response.status_code == 400
            # test non-configured region
            response = client.get('/api/v1/stack?region=us-east-2')
            assert response.get_json()['error_msg']['query']['region'] == UNCONFIGURED_REGION_ERROR
            assert response.status_code == 400
            # test invalid region
            response = client.get('/api/v1/stack?region=does-not-exist')
            assert response.get_json()['error_msg']['query']['region'] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            assert response.status_code == 400
            # test can't load regions at app start, but can during request
            original_regions = test_stacks.app.config['AVAILABLE_REGIONS']
            test_stacks.app.config['AVAILABLE_REGIONS'] = []
            response = client.get('/api/v1/stack?region=us-east-1')
            assert response.get_json()[0] == mystack.stack_name
            assert response.status_code == 200
            # test can't load regions at all
            test_stacks.app.config['AVAILABLE_REGIONS'] = []
            with mock.patch('weaver.api.helpers.get_available_regions', return_value=[]):
                response = client.get('/api/v1/stack?region=us-east-1')
            assert response.status_code == 500
            assert response.get_json()['error_msg'] == 'Unable to retrieve valid regions from AWS; check logs for details'
            test_stacks.app.config['AVAILABLE_REGIONS'] = original_regions
            # test us-east-1
            response = client.get('/api/v1/stack?region=us-east-1')
            assert len(response.get_json()) == 1
            assert response.get_json()[0] == mystack.stack_name
            assert response.status_code == 200

    def test_change_set_put(self):
        with test_stacks.app.test_client() as client:
            mystack = test_stacks.setup_confluence_stack()
            # request body data
            req_body = {"params": [{"parameter_key": "JvmHeap", "parameter_value": "2048m"}]}
            # test no region provided
            response = client.put(f"/api/v1/stack/{mystack.stack_name}/changeset", json=req_body)
            assert response.get_json()["error_msg"]["query"]["region"] == NO_REGION_ERROR
            assert response.status_code == 400
            # test bad region
            response = client.put(f"/api/v1/stack/{mystack.stack_name}/changeset?region=does-not-exist", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["query"]["region"] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            # test non-existent template
            bad_req_body = deepcopy(req_body)
            bad_req_body["template"] = {"repo": "testing", "name": "non-existent.template.yaml"}
            response = client.put(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1", json=bad_req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["template"]["_schema"] == ["Template not found"]
            # test bad parameter
            bad_req_body = deepcopy(req_body)
            bad_req_body["params"].append({"parameter_key": "SomeBadParamHere", "parameter_value": "bad value"})
            bad_req_body["template"] = {"repo": "testing", "name": test_stacks.CONF_TEMPLATE_FILE.name}
            response = client.put(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1", json=bad_req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["params"] == ["Unsupported parameters for template func-test-confluence.template.yaml: SomeBadParamHere"]
            # test no request body
            response = client.put(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1")
            assert response.status_code == 202
            # test normal request body
            response = client.put(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1", json=req_body)
            assert response.status_code == 202

    def test_change_set_get(self):
        with test_stacks.app.test_client() as client:
            mystack = test_stacks.setup_confluence_stack()
            # test no region provided
            response = client.get(f"/api/v1/stack/{mystack.stack_name}/changeset")
            assert response.get_json()["error_msg"]["query"]["region"] == NO_REGION_ERROR
            assert response.status_code == 400
            # test bad region
            response = client.get(f"/api/v1/stack/{mystack.stack_name}/changeset?region=does-not-exist")
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["query"]["region"] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            # create a change set
            req_body = {"params": [{"parameter_key": "JvmHeap", "parameter_value": "2048m"}]}
            client.put(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1", json=req_body)
            # test get list of change sets
            response = client.get(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1")
            assert response.status_code == 200
            change_sets = response.get_json()["change_sets"]
            assert len(change_sets) == 1
            # test change set details
            change_set_name = change_sets[0]['change_set_name']
            response = client.get(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1&change_set_name={change_set_name}")
            assert response.status_code == 200
            change_set_details = response.get_json()
            assert change_set_details["change_set_name"] == change_set_name

    def test_stack_patch(self):
        with test_stacks.app.test_client() as client:
            mystack = test_stacks.setup_confluence_stack()
            # set up mocks
            weaver.utils.save_changelog = MagicMock(return_value=True)
            req_body = {"change_set_name": ""}
            # test no region provided
            response = client.patch(f"/api/v1/stack/{mystack.stack_name}", json=req_body)
            assert response.get_json()["error_msg"]["query"]["region"] == NO_REGION_ERROR
            assert response.status_code == 400
            # test bad region
            response = client.patch(f"/api/v1/stack/{mystack.stack_name}?region=does-not-exist", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["query"]["region"] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            # test no body/change_set_name provided
            response = client.patch(f"/api/v1/stack/{mystack.stack_name}?region=us-east-1")
            assert response.get_json()["error_msg"]["json"]["change_set_name"] == ["Missing data for required field."]
            assert response.status_code == 400
            # test empty change_set_name provided
            response = client.patch(f"/api/v1/stack/{mystack.stack_name}?region=us-east-1", json=req_body)
            assert response.get_json()["error_msg"]["json"]["change_set_name"] == ["Field cannot be blank."]
            assert response.status_code == 400
            # create a change set
            change_set_put_req_body = {"params": [{"parameter_key": "JvmHeap", "parameter_value": "2048m"}]}
            change_set_put_response = client.put(f"/api/v1/stack/{mystack.stack_name}/changeset?region=us-east-1", json=change_set_put_req_body)
            req_body["change_set_name"] = change_set_put_response.get_json()["change_set_name"]
            # test inaccessible stack environ
            os.environ['ATL_ENVIRONMENT'] = 'no-access'
            response = client.patch(f"/api/v1/stack/{mystack.stack_name}?region=us-east-1", json=req_body)
            assert response.status_code == 500
            del os.environ['ATL_ENVIRONMENT']
            # patch the stack with the changeset
            response = client.patch(f"/api/v1/stack/{mystack.stack_name}?region=us-east-1", json=req_body)
            assert response.status_code == 202
            assert response.get_json()["message"] == "Stack update started"

    @mock.patch(
        'requests.head',
        side_effect=[
            mocked_alternate_download_url_head_request(status_code=403, reason="Forbidden"),
            mocked_alternate_download_url_head_request(status_code=401, reason="Not Authorized"),
            mocked_alternate_download_url_head_request(status_code=401, reason="Not Authorized"),
            mocked_alternate_download_url_head_request(status_code=200, reason="OK"),
        ],
    )
    def test_stack_upgrade(self, mock_head):
        with test_stacks.app.test_client() as client:
            mystack = test_stacks.setup_confluence_stack()
            # set up mocks
            weaver.utils.save_changelog = MagicMock(return_value=True)
            req_body = {"new_version": ""}
            # test no region provided
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade", json=req_body)
            assert response.get_json()["error_msg"]["query"]["region"] == NO_REGION_ERROR
            assert response.status_code == 400
            # test bad region
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=does-not-exist", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["query"]["region"] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            # test no body/new_version provided
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1")
            assert response.get_json()["error_msg"]["json"]["new_version"] == ["Missing data for required field."]
            assert response.status_code == 400
            # test valid new_version
            req_body["new_version"] = "6.12.0"
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 202
            assert response.get_json()["message"] == "Stack upgrade started"
            # test inaccessible stack environ
            os.environ['ATL_ENVIRONMENT'] = 'no-access'
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 500
            del os.environ['ATL_ENVIRONMENT']
            # test unallowed alternate download URL
            req_body["alternate_download_url"] = "https://bogus.domain.goes.here/non/existent/path.tar.gz"
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 400
            assert "not in the list of allowed domains" in response.get_json()["error_msg"]["json"]["alternate_download_url"][0]
            del req_body["alternate_download_url"]
            # test allowed but invalid alternate download URL
            test_stacks.app.config["ALTERNATE_DOWNLOAD_URL_ALLOWED_DOMAINS"] = ["bogus.domain.goes.here"]
            req_body["alternate_download_url"] = "https://bogus.domain.goes.here/non/existent/path.tar.gz"
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["alternate_download_url"] == [
                f"Invalid URL ({req_body['alternate_download_url']})? Response from server: 403 Forbidden"
            ]
            del req_body["alternate_download_url"]
            # test alternate download URL that requires auth; secret missing
            test_stacks.app.config["ALTERNATE_DOWNLOAD_URL_ALLOWED_DOMAINS"].append("some.url.that.requires.auth")
            req_body["alternate_download_url"] = "https://some.url.that.requires.auth/some/path.tar.gz"
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 500
            assert "could not be retrieved" in response.get_json()["error_msg"]
            del req_body["alternate_download_url"]
            # test alternate download URL that requires auth
            sm = boto3.client("secretsmanager", region_name="us-east-1")
            sm.create_secret(Name=weaver.models.DOWNLOAD_ATLASSIAN_SECRET_NAME, SecretString=json.dumps({"username": "someuser", "password": "somepassword"}))
            req_body["alternate_download_url"] = "https://some.url.that.requires.auth/some/path.tar.gz"
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 202
            assert response.get_json()["message"] == "Stack upgrade started"
            del req_body["alternate_download_url"]
            # test zdu missing auth field
            req_body["zdu"] = True
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["auth"] == ["Missing data for required field."]
            # test zdu missing personal access token field
            req_body["auth"] = {}
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["auth"]["personal_access_token"] == ["Missing data for required field."]
            # test zdu wrong zdu type
            req_body["auth"]["personal_access_token"] = "fakepat"
            req_body["zdu_type"] = "faketype"
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"]["json"]["zdu_type"] == ["Invalid value; expected one of: inplace, rebuild"]
            # test zdu incompatible version
            req_body["zdu_type"] = "inplace"
            response = client.post(f"/api/v1/stack/{mystack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"] == "version 6.11.0 does not support ZDU"
            # test zdu too few nodes
            jira_stack = test_stacks.setup_jira_stack()
            req_body["new_version"] = "8.22.6"
            response = client.post(f"/api/v1/stack/{jira_stack.stack_name}/upgrade?region=us-east-1", json=req_body)
            assert response.status_code == 400
            assert response.get_json()["error_msg"] == "too few nodes"

    def test_stack_get(self):
        with test_stacks.app.test_client() as client:
            # create test stack
            mystack = test_stacks.setup_confluence_stack()
            # set up mocks
            test_stacks.app.config['CLONE_DEFAULTS'][mystack.stack_name] = {'ClusterNodeCount': '4', 'CustomDnsName': 'mystack.mycompany.com'}
            weaver.aws_cfn.CfnStack.check_service_status = MagicMock(return_value='RUNNING')
            # test no region provided
            response = client.get(f'/api/v1/stack/{mystack.stack_name}')
            assert response.get_json()['error_msg']['query']['region'] == NO_REGION_ERROR
            assert response.status_code == 400
            # test bad region
            response = client.get(f'/api/v1/stack/{mystack.stack_name}?region=does-not-exist')
            assert response.get_json()['error_msg']['query']['region'] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            assert response.status_code == 400
            # test no expands
            response = client.get(f'/api/v1/stack/{mystack.stack_name}?region=us-east-1')
            assert response.get_json()['product'] == 'confluence'
            assert response.get_json()['stack_status'] == 'CREATE_COMPLETE'
            # test clone_defaults where stack does not exist
            response = client.get('/api/v1/stack/new-stack?region=us-east-1&expand=clone_defaults')
            assert response.status_code == 200
            response_body = response.get_json()
            assert 'clone_defaults' in response_body
            assert len(response_body['clone_defaults']) == 7
            assert response_body['clone_defaults']['ClusterNodeCount'] == '1'
            # test all expands
            response = client.get(f'/api/v1/stack/{mystack.stack_name}?region=us-east-1&expand=clone_defaults,params,tags,template,termination_protection,zdu_compatibility')
            response_body = response.get_json()
            # test clone_defaults expand
            assert 'clone_defaults' in response_body
            assert response_body['clone_defaults']['ClusterNodeCount'] == '4'
            assert response_body['clone_defaults']['CustomDnsName'] == 'mystack.mycompany.com'
            assert response_body['clone_defaults']['DeployEnvironment'] == 'stg'
            # test params expand
            assert 'params' in response_body
            assert len(response_body['params']) == 63
            # test tags expand
            assert 'tags' in response_body
            assert response_body['tags']['product'] == 'confluence'
            # test template expand
            assert 'template' in response_body
            assert 'repo' in response_body['template']
            assert 'name' in response_body['template']
            assert response_body['template']['repo'] == 'testing'
            assert response_body['template']['name'] == 'func-test-confluence.template.yaml'
            # test termination_protection expand
            assert 'termination_protection' in response_body
            assert response_body['termination_protection'] is False
            # test zdu_compatibility expand
            assert 'zdu_compatibility' in response_body
            assert response_body['zdu_compatibility']['compatible'] is False
            assert response_body['zdu_compatibility']['reason'] == 'version 6.11.0 does not support ZDU'
            assert response.status_code == 200

    def test_stack_delete(self):
        with test_stacks.app.test_client() as client:
            # create test stack
            mystack = test_stacks.setup_confluence_stack()
            # set up mocks
            weaver.aws_cfn.CfnStack.check_service_status = MagicMock(return_value='RUNNING')
            weaver.aws_cfn.CfnStack.has_alb = MagicMock(return_value=True)
            weaver.aws_cfn.CfnStack.has_nlb = MagicMock(return_value=False)
            weaver.aws_cfn.cfn_resources.get_cfn_stacknames = MagicMock(return_value=[])
            # prevent futures from background threads from creating new futures threads (used by s3.meta.client.upload_file)
            weaver.utils.save_changelog = MagicMock(return_value=True)
            # request body data
            req_body = {
                'delete_changelogs': True,
                'delete_threaddumps': True,
            }
            # test no region provided
            response = client.delete(f'/api/v1/stack/{mystack.stack_name}', json=req_body)
            assert response.get_json()['error_msg']['query']['region'] == NO_REGION_ERROR
            assert response.status_code == 400
            # test bad region
            response = client.delete(f'/api/v1/stack/{mystack.stack_name}?region=does-not-exist', json=req_body)
            assert response.get_json()['error_msg']['query']['region'] == [*INVALID_REGION_ERROR, *UNCONFIGURED_REGION_ERROR]
            assert response.status_code == 400
            # test inaccessible stack environ
            os.environ['ATL_ENVIRONMENT'] = 'no-access'
            response = client.delete(f'/api/v1/stack/{mystack.stack_name}?region=us-east-1')
            assert response.status_code == 500
            del os.environ['ATL_ENVIRONMENT']
            # test action in-progress
            test_stacks.app.config['STACK_LOCKING'] = True
            test_stacks.app.config['LOCKS'][mystack.stack_name] = 'update'
            response = client.delete(f'/api/v1/stack/{mystack.stack_name}?region=us-east-1')
            assert response.status_code == 409
            test_stacks.app.config['STACK_LOCKING'] = False
            del test_stacks.app.config['LOCKS'][mystack.stack_name]
            # test bad request body
            bad_req_body = req_body.copy()
            bad_req_body['delete_threaddumps'] = 'somevalhere'
            response = client.delete(f'/api/v1/stack/{mystack.stack_name}?region=us-east-1', json=bad_req_body)
            assert response.get_json()['error_msg']['json']['delete_threaddumps'] == ['Not a valid boolean.']
            assert response.status_code == 400
            # test no request body
            response = client.delete(f'/api/v1/stack/{mystack.stack_name}?region=us-east-1')
            assert response.status_code == 202
            # wait for delete to complete before re-creating test stack to run further deletion tests
            delete_complete = False
            while not delete_complete:
                action_status = client.get(f'/api/v1/stack/{mystack.stack_name}/action?region=us-east-1&action_id={response.json["action_id"]}')
                delete_complete = action_status.json['status'] == 'COMPLETE'
            # test normal request body
            mystack2 = test_stacks.setup_confluence_stack()
            response = client.delete(f'/api/v1/stack/{mystack2.stack_name}?region=us-east-1', json=req_body)
            assert response.status_code == 202
            # wait for delete to complete before re-creating test stack to run further deletion tests
            delete_complete = False
            while not delete_complete:
                action_status = client.get(f'/api/v1/stack/{mystack2.stack_name}/action?region=us-east-1&action_id={response.json["action_id"]}')
                delete_complete = action_status.json['status'] == 'COMPLETE'
            # test strings request body
            mystack3 = test_stacks.setup_confluence_stack()
            strings_req_body = {
                'delete_changelogs': 'true',
                'delete_threaddumps': 'true',
            }
            response = client.delete(f'/api/v1/stack/{mystack3.stack_name}?region=us-east-1', json=strings_req_body)
            assert response.status_code == 202


@mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestGetDiagnosticsApi:
    def test_query_param_errors(self, mock_client):
        # test no region
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics')
        assert_no_region_error_res(response)
        # test bad region
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics?region=does-not-exist')
        assert_bad_region_error_res(response)
        # test missing required query param
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics?region=us-east-1&node_ip=10.10.10.10')
        assert response.get_json()['error_msg']['query']['diagnostic_types'] == ['Missing data for required field.']
        assert response.status_code == 400
        # test missing stack-personal-access-token for index health check
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics?region=us-east-1&diagnostic_types=index_health')
        assert response.get_json()['error_msg']['query']['_schema'] == ['index_health in diagnostic_types requires a stack-personal-access-token in the HTTP header']
        assert response.status_code == 400
        # test duplicate diagnostic types
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics?region=us-east-1&diagnostic_types=index_health,thread_dump,thread_dump')
        assert response.get_json()['error_msg']['query']['diagnostic_types'] == ['Cannot contain duplicates']
        assert response.status_code == 400
        # test unknown diagnostic type
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics?region=us-east-1&diagnostic_types=cpu,heap_dump,gibberish')
        assert 'Must be one of' in str(response.get_json()['error_msg']['query']['diagnostic_types'])
        assert response.status_code == 400
        # test no diagnostic types
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics?region=us-east-1&diagnostic_types=')
        assert 'Must be one of' in str(response.get_json()['error_msg']['query']['diagnostic_types'])
        assert response.status_code == 400

    def test_stack_validation_errors(self, mock_client):
        # test stack not found
        response = mock_client.get('/api/v1/stack/my-confluence/diagnostics?region=us-east-1&diagnostic_types=heap_dump,thread_dump')
        assert response.get_json()['error_msg'] == 'Stack my-confluence not found'
        assert response.status_code == 404
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # test node ip not found
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=cpu,heap_dump&node_ip=10.10.10.10')
        assert response.get_json()['error_msg'] == f'Node 10.10.10.10 not found for stack {mystack.stack_name}'
        assert response.status_code == 404

    def test_cpu(self, mock_client, mock_cpu_res, mock_stack_nodes, mock_valid_node_ip):
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # setup mocks
        weaver.aws_cfn.CfnStack.get_node_cpu = MagicMock(return_value=mock_cpu_res)
        # test get cpu for all nodes
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=cpu')
        assert len(response.get_json()['cpu']) > 0
        for cpu_res_item in response.get_json()['cpu']:
            assert cpu_res_item['cpu']
        assert response.status_code == 200
        # test get cpu for single node
        weaver.aws_cfn.CfnStack.get_stacknodes = MagicMock(return_value=mock_stack_nodes)
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=cpu&node_ip={mock_valid_node_ip}')
        assert len(response.get_json()['cpu']) == 1
        assert response.get_json()['cpu'][0]['cpu']
        assert response.get_json()['cpu'][0]['node_ip'] == mock_valid_node_ip
        assert response.status_code == 200

    def test_heap_dump(self, mock_client, mock_heap_dump_links):
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # setup mocks
        weaver.aws_cfn.CfnStack.get_s3_file_links = MagicMock(return_value=mock_heap_dump_links)
        # test heap dumps found
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=heap_dump')
        assert response.get_json()['heap_dumps'] == mock_heap_dump_links
        assert response.status_code == 200
        # test heap dumps found for specific node
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=heap_dump&node_ip=10.10.10.10')
        assert response.get_json()['heap_dumps'] == mock_heap_dump_links
        assert response.status_code == 200
        # test no heap dumps found
        weaver.aws_cfn.CfnStack.get_s3_file_links.return_value = []
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=heap_dump')
        assert response.get_json()['heap_dumps'] == []
        assert response.status_code == 200  # don't want a response error, other diagnostic types may have been retrieved fine

    def test_index_health(self, mock_client, mock_index_health_res, mock_stack_nodes, mock_valid_node_ip):
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # setup mocks
        weaver.aws_cfn.CfnStack.get_index_health = MagicMock(return_value=mock_index_health_res)
        # test get index health for all nodes
        headers = {'stack-personal-access-token': 'fake-pat'}
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=index_health', headers=headers)
        assert len(response.get_json()['index_health']) > 0
        for index_health_res_item in response.get_json()['index_health']:
            assert index_health_res_item['index_health']
        assert response.status_code == 200
        # test get index health for single node
        weaver.aws_cfn.CfnStack.get_stacknodes = MagicMock(return_value=mock_stack_nodes)
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=index_health&node_ip={mock_valid_node_ip}', headers=headers)
        assert len(response.get_json()['index_health']) == 1
        assert response.get_json()['index_health'][0]['index_health']
        assert response.get_json()['index_health'][0]['node_ip'] == mock_valid_node_ip
        assert response.status_code == 200

    def test_thread_dump(self, mock_client, mock_thread_dump_links):
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # setup mocks
        weaver.aws_cfn.CfnStack.get_s3_file_links = MagicMock(return_value=mock_thread_dump_links)
        # test thread dumps found
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=thread_dump')
        assert response.status_code == 200
        assert response.get_json()['thread_dumps'] == mock_thread_dump_links
        # test thread dumps found for specific node
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=thread_dump&node_ip=10.10.10.10')
        assert response.status_code == 200
        assert response.get_json()['thread_dumps'] == mock_thread_dump_links
        # test no thread dumps found
        weaver.aws_cfn.CfnStack.get_s3_file_links.return_value = []
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=thread_dump')
        assert response.status_code == 200  # don't want a response error, other diagnostic types may have been retrieved fine
        assert response.get_json()['thread_dumps'] == []

    def test_support_zip(self, mock_client, mock_support_zip_links):
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # setup mocks
        weaver.aws_cfn.CfnStack.get_s3_file_links = MagicMock(return_value=mock_support_zip_links)
        # test support zips found
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=support_zip')
        assert response.get_json()['support_zips'] == mock_support_zip_links
        assert response.status_code == 200
        # test support zips found for specific node
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=support_zip&node_ip=10.10.10.10')
        assert response.get_json()['support_zips'] == mock_support_zip_links
        assert response.status_code == 200
        # test no support zips found
        weaver.aws_cfn.CfnStack.get_s3_file_links.return_value = []
        response = mock_client.get(f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=support_zip')
        assert response.get_json()['support_zips'] == []
        assert response.status_code == 200  # don't want a response error, other diagnostic types may have been retrieved fine

    def test_multi(
        self,
        mock_client,
        mock_cpu_res,
        mock_heap_dump_links,
        mock_index_health_res,
        mock_support_zip_links,
        mock_thread_dump_links,
        mock_stack_nodes,
        mock_valid_node_ip,
        get_s3_file_links_side_effect,
    ):
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # setup mocks
        weaver.aws_cfn.CfnStack.get_index_health = MagicMock(return_value=mock_index_health_res)
        weaver.aws_cfn.CfnStack.get_node_cpu = MagicMock(return_value=mock_cpu_res)
        weaver.aws_cfn.CfnStack.get_s3_file_links = MagicMock(side_effect=get_s3_file_links_side_effect)
        weaver.aws_cfn.CfnStack.get_stacknodes = MagicMock(return_value=mock_stack_nodes)
        # test all diagnostic types, all nodes
        headers = {'stack-personal-access-token': 'fake-pat'}
        response = mock_client.get(
            f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=cpu,heap_dump,index_health,support_zip,thread_dump', headers=headers
        )
        assert len(response.get_json()['cpu']) > 0
        assert response.get_json()['heap_dumps'] == mock_heap_dump_links
        assert len(response.get_json()['index_health']) > 0
        assert response.get_json()['support_zips'] == mock_support_zip_links
        assert response.get_json()['thread_dumps'] == mock_thread_dump_links
        assert response.status_code == 200
        # test all diagnostic types, one node
        response = mock_client.get(
            f'/api/v1/stack/{mystack.stack_name}/diagnostics?region=us-east-1&diagnostic_types=cpu,heap_dump,index_health,support_zip,thread_dump&node_ip={mock_valid_node_ip}',
            headers=headers,
        )
        assert len(response.get_json()['cpu']) == 1
        assert response.get_json()['heap_dumps'] == mock_heap_dump_links
        assert len(response.get_json()['index_health']) == 1
        assert response.get_json()['support_zips'] == mock_support_zip_links
        assert response.get_json()['thread_dumps'] == mock_thread_dump_links
        assert response.status_code == 200


@mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestPostDiagnosticsApi:
    @pytest.fixture(autouse=True)
    def setup_attr(self):
        self.default_api_path: str = '/api/v1/stack/my-confluence/diagnostics'
        self.default_api_path_valid_region: str = f'{self.default_api_path}?region=us-east-1'

    def get_api_path_valid_region(self, stack_name: str):
        return f'/api/v1/stack/{stack_name}/diagnostics?region=us-east-1'

    def test_query_param_errors(self, mock_client):
        # test no region
        response = mock_client.post(self.default_api_path)
        assert_no_region_error_res(response)
        # test bad region
        response = mock_client.post(f'{self.default_api_path}?region=does-not-exist')
        assert_bad_region_error_res(response)

    def test_req_body_errors(self, mock_client):
        # test no req body
        response = mock_client.post(self.default_api_path_valid_region)
        assert_no_req_body_error_res(response)
        # test no diagnostic type
        req_body = {}
        response = mock_client.post(self.default_api_path_valid_region, json=req_body)
        assert_req_body_missing_required_field(response, 'diagnostic_type')
        # test unknown diagnostic type
        req_body = {'diagnostic_type': 'gibberish'}
        response = mock_client.post(self.default_api_path_valid_region, json=req_body)
        assert_req_body_unrecognised_field_value(response, 'diagnostic_type')
        # test missing personal_access_token for support_zip
        req_body = {'diagnostic_type': weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP.value}
        response = mock_client.post(self.default_api_path_valid_region, json=req_body)
        assert response.get_json()['error_msg']['json']['_schema'] == [
            f'{weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP.value} diagnostic type requires a personal_access_token'
        ]
        assert response.status_code == 400
        # test node_ip set for incompatible diagnostic_type
        req_body = {'diagnostic_type': weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP.value, 'node_ip': '13.13.13.13', 'personal_access_token': 'fake-pat'}
        response = mock_client.post(self.default_api_path_valid_region, json=req_body)
        assert response.get_json()['error_msg']['json']['_schema'] == [f'Cannot select node_ip for {weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP.value} diagnostic type']
        assert response.status_code == 400

    def test_heap_dump(self, mock_client, mock_stack_nodes, mock_valid_node_ip, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        weaver.aws_cfn.CfnStack.get_stacknodes = MagicMock(return_value=mock_stack_nodes)
        weaver.aws_cfn.CfnStack.has_alb = MagicMock(return_value=True)
        weaver.aws_cfn.CfnStack.has_nlb = MagicMock(return_value=False)
        weaver.aws_cfn.CfnStack.ssm_send_and_wait_response = MagicMock(return_value='Success')
        # test heap dump for all nodes
        req_body = {'diagnostic_type': weaver_enums.PostMethodDiagnosticType.HEAP_DUMP.value}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.status_code == 202
        assert response.get_json()['message'] == "Stack diagnostics started"
        wait_for_action_to_complete(response.json["action_id"], mystack.stack_name)
        # test heap dump for one node
        req_body = {'diagnostic_type': weaver_enums.PostMethodDiagnosticType.HEAP_DUMP.value, 'node_ip': mock_valid_node_ip}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.status_code == 202
        assert response.get_json()['message'] == "Stack diagnostics started"
        wait_for_action_to_complete(response.json["action_id"], mystack.stack_name)

    def test_support_zip(self, mock_client, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        weaver.utils.save_changelog = MagicMock(return_value=True)
        req_body = {'diagnostic_type': weaver_enums.PostMethodDiagnosticType.SUPPORT_ZIP.value, 'personal_access_token': 'fake-pat'}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.status_code == 202
        assert response.get_json()['message'] == "Stack diagnostics started"
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)

    def test_thread_dump(self, mock_client, mock_stack_nodes, mock_valid_node_ip, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        weaver.aws_cfn.CfnStack.get_stacknodes = MagicMock(return_value=mock_stack_nodes)
        weaver.aws_cfn.CfnStack.has_alb = MagicMock(return_value=True)
        weaver.aws_cfn.CfnStack.has_nlb = MagicMock(return_value=False)
        weaver.aws_cfn.CfnStack.run_command = MagicMock(return_value=[{'node_ip': node.private_ip_address, 'result': 'Success'} for node in mock_stack_nodes])
        # test thread dump for all nodes
        req_body = {'diagnostic_type': weaver_enums.PostMethodDiagnosticType.THREAD_DUMP.value}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.status_code == 202
        assert response.get_json()['message'] == "Stack diagnostics started"
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test thread dump for one node
        req_body = {'diagnostic_type': weaver_enums.PostMethodDiagnosticType.THREAD_DUMP.value, 'node_ip': mock_valid_node_ip}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.status_code == 202
        assert response.get_json()['message'] == "Stack diagnostics started"
        wait_for_action_to_complete(response.json["action_id"], mystack.stack_name)


@mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestRestartApi:
    @pytest.fixture(autouse=True)
    def setup_attr(self):
        self.default_stack_name = f'{test_stacks.CONF_STACKNAME_PREFIX}-{test_stacks.random_suffix()}'

    def get_api_path(self, stack_name: str) -> str:
        return f'/api/v1/stack/{stack_name}/restart'

    def get_api_path_valid_region(self, stack_name: str) -> str:
        return f'/api/v1/stack/{stack_name}/restart?region=us-east-1'

    def test_query_param_errors(self, mock_client):
        # test no region
        response = mock_client.post(self.get_api_path(self.default_stack_name))
        assert_no_region_error_res(response)
        # test bad region
        response = mock_client.post(f'{self.get_api_path(self.default_stack_name)}?region=does-not-exist')
        assert_bad_region_error_res(response)

    def test_req_body_errors(self, mock_client):
        # test no req body
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name))
        assert_no_req_body_error_res(response)
        # test no restart type
        req_body = {}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_missing_required_field(response, 'restart_type')
        # test unknown restart type
        req_body = {'restart_type': 'gibberish'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_unrecognised_field_value(response, 'restart_type')
        # test missing node_ip for single node restart
        req_body = {'restart_type': weaver_enums.RestartType.SINGLE.value, 'drain_type': 'default'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_missing_required_field_error_msg('node_ip', 'restart_type', weaver_enums.RestartType.SINGLE.value))
        # test invalid node_ip value for single node restart
        req_body = {'restart_type': weaver_enums.RestartType.SINGLE.value, 'node_ip': 'asdf', 'drain_type': 'default'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_invalid_ipv4(response, 'node_ip')
        # test missing drain_type for single node restart
        req_body = {'restart_type': weaver_enums.RestartType.SINGLE.value, 'node_ip': '10.10.10.10'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_missing_required_field_error_msg('drain_type', 'restart_type', weaver_enums.RestartType.SINGLE.value))
        # test invalid drain_type for single node restart
        req_body = {'restart_type': weaver_enums.RestartType.SINGLE.value, 'node_ip': '10.10.10.10', 'drain_type': 'gibberish'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_unrecognised_field_value(response, "drain_type")
        # test asg_name set for single node restart
        req_body = {'restart_type': weaver_enums.RestartType.SINGLE.value, 'node_ip': '10.10.10.10', 'asg_name': 'ClusterNodeAutoScalingGroup-ASDFG'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_incompatible_field_error_msg('asg_name', 'restart_type', weaver_enums.RestartType.SINGLE.value))
        # test node_ip set for full restart
        req_body = {'restart_type': weaver_enums.RestartType.FULL.value, 'node_ip': '10.10.10.10'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_incompatible_field_error_msg('node_ip', 'restart_type', weaver_enums.RestartType.FULL.value))
        # test asg_name set for full restart
        req_body = {'restart_type': weaver_enums.RestartType.FULL.value, 'asg_name': 'ClusterNodeAutoScalingGroup-ASDFG'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_incompatible_field_error_msg('asg_name', 'restart_type', weaver_enums.RestartType.FULL.value))
        # test drain_type set for full restart
        req_body = {'restart_type': weaver_enums.RestartType.FULL.value, 'drain_type': 'fast'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_incompatible_field_error_msg('drain_type', 'restart_type', weaver_enums.RestartType.FULL.value))
        # test node_ip set for rolling restart
        req_body = {'restart_type': weaver_enums.RestartType.ROLLING.value, 'node_ip': '10.10.10.10'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_incompatible_field_error_msg('node_ip', 'restart_type', weaver_enums.RestartType.ROLLING.value))
        # test missing drain_type for single node restart
        req_body = {'restart_type': weaver_enums.RestartType.ROLLING.value}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_field_validation_error(response, 'restart_type', get_missing_required_field_error_msg('drain_type', 'restart_type', weaver_enums.RestartType.ROLLING.value))
        # test invalid drain_type for single node restart
        req_body = {'restart_type': weaver_enums.RestartType.ROLLING.value, 'drain_type': 'gibberish'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_unrecognised_field_value(response, "drain_type")

    def test_stack_validation_errors(self, mock_client, mock_invalid_node_ip, mock_asgs, mock_invalid_asg_name):
        # test stack not found
        req_body = {'restart_type': weaver_enums.RestartType.FULL.value}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert response.get_json()['error_msg'] == f'Stack {self.default_stack_name} not found'
        assert response.status_code == 404
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # test node ip not found
        req_body = {'restart_type': weaver_enums.RestartType.SINGLE.value, 'node_ip': mock_invalid_node_ip, 'drain_type': 'default'}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()['error_msg'] == f"Node '{mock_invalid_node_ip}' not found for stack {mystack.stack_name}"
        assert response.status_code == 404
        # test autoscaling group not found
        weaver.aws_cfn.CfnStack.get_stack_asgs = MagicMock(return_value=mock_asgs)
        req_body = {'restart_type': weaver_enums.RestartType.ROLLING.value, 'asg_name': mock_invalid_asg_name, 'drain_type': 'default'}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()['error_msg'] == f"Autoscaling group '{mock_invalid_asg_name}' not found for stack {mystack.stack_name}"
        assert response.status_code == 404

    def test_full_restart(self, mock_client, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        success_msg = "Stack fullrestart started"
        req_body = {'restart_type': weaver_enums.RestartType.FULL.value}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with thread dumps
        req_body['thread_dumps'] = True
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with thread and heap dumps
        req_body['heap_dumps'] = True
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)

    def test_rolling_restart(self, mock_client, mock_asgs, mock_valid_asg_name, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        success_msg = "Stack rollingrestart started"
        weaver.aws_cfn.CfnStack.check_node_status = MagicMock(return_value=True)
        weaver.aws_cfn.CfnStack.get_stack_asgs = MagicMock(return_value=mock_asgs)
        # all nodes
        req_body = {'restart_type': weaver_enums.RestartType.ROLLING.value, 'drain_type': 'default'}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # specific autoscaling group
        req_body['asg_name'] = mock_valid_asg_name
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with thread dumps
        req_body['thread_dumps'] = True
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with thread and heap dumps
        req_body['heap_dumps'] = True
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with fast node drain
        req_body['drain_type'] = 'fast'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with skip node drain
        req_body['drain_type'] = 'skip'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)

    def test_single_node_restart(self, mock_client, mock_stack_nodes, mock_valid_node_ip, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        success_msg = "Stack restartnode started"
        weaver.aws_cfn.CfnStack.get_stacknodes = MagicMock(return_value=mock_stack_nodes)
        req_body = {'restart_type': weaver_enums.RestartType.SINGLE.value, 'node_ip': mock_valid_node_ip, 'drain_type': 'default'}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with thread dumps
        req_body['thread_dumps'] = True
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with thread and heap dumps
        req_body['heap_dumps'] = True
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with fast node drain
        req_body['drain_type'] = 'fast'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # with skip node drain
        req_body['drain_type'] = 'skip'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)


@mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestRebuildApi:
    @pytest.fixture(autouse=True)
    def setup_attr(self):
        self.default_stack_name = f'{test_stacks.CONF_STACKNAME_PREFIX}-{test_stacks.random_suffix()}'
        self.api_res_success_msg = "Stack rollingrebuild started"

    def get_api_path(self, stack_name: str) -> str:
        return f'/api/v1/stack/{stack_name}/rebuild'

    def get_api_path_valid_region(self, stack_name: str) -> str:
        return f'/api/v1/stack/{stack_name}/rebuild?region=us-east-1'

    def test_query_param_errors(self, mock_client):
        # test no region
        response = mock_client.post(self.get_api_path(self.default_stack_name))
        assert_no_region_error_res(response)
        # test bad region
        response = mock_client.post(f'{self.get_api_path(self.default_stack_name)}?region=does-not-exist')
        assert_bad_region_error_res(response)

    def test_req_body_errors(self, mock_client):
        # test unknown drain type
        req_body = {'drain_type': 'gibberish'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_unrecognised_field_value(response, 'drain_type')

    def test_stack_validation_errors(self, mock_client, mock_asgs, mock_invalid_asg_name):
        # test stack not found
        req_body = {}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert response.get_json()['error_msg'] == f'Stack {self.default_stack_name} not found'
        assert response.status_code == 404
        # create test stack
        mystack = test_stacks.setup_confluence_stack()
        # test autoscaling group not found
        weaver.aws_cfn.CfnStack.get_stack_asgs = MagicMock(return_value=mock_asgs)
        req_body = {'drain_type': 'default', 'asg_name': mock_invalid_asg_name}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()['error_msg'] == f"Autoscaling group '{mock_invalid_asg_name}' not found for stack {mystack.stack_name}"
        assert response.status_code == 404

    def test_rebuild_all_nodes(self, mock_client, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        weaver.aws_cfn.CfnStack.rolling_rebuild = MagicMock(return_value=True)
        req_body = {}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test fast drain
        req_body['drain_type'] = 'fast'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test skip drain
        req_body['drain_type'] = 'skip'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test with personal access token
        headers = {'stack-personal-access-token': 'fake-pat'}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body, headers=headers)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)

    def test_rebuild_asg(self, mock_client, mock_valid_asg_name, mock_asgs, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        weaver.aws_cfn.CfnStack.get_stack_asgs = MagicMock(return_value=mock_asgs)
        weaver.aws_cfn.CfnStack.rolling_rebuild = MagicMock(return_value=True)
        req_body = {'asg_name': mock_valid_asg_name}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test fast drain
        req_body['drain_type'] = 'fast'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test skip drain
        req_body['drain_type'] = 'skip'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test with personal access token
        headers = {'stack-personal-access-token': 'fake-pat'}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body, headers=headers)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)


@mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestReinitApi:
    @pytest.fixture(autouse=True)
    def setup_attr(self):
        self.default_stack_name = f'{test_stacks.CONF_STACKNAME_PREFIX}-{test_stacks.random_suffix()}'
        self.api_res_success_msg = "Stack rollingreinit started"

    def get_api_path(self, stack_name: str) -> str:
        return f'/api/v1/stack/{stack_name}/reinit'

    def get_api_path_valid_region(self, stack_name: str) -> str:
        return f'/api/v1/stack/{stack_name}/reinit?region=us-east-1'

    def test_query_param_errors(self, mock_client):
        # test no region
        response = mock_client.post(self.get_api_path(self.default_stack_name))
        assert_no_region_error_res(response)
        # test bad region
        response = mock_client.post(f'{self.get_api_path(self.default_stack_name)}?region=does-not-exist')
        assert_bad_region_error_res(response)

    def test_req_body_errors(self, mock_client):
        # test unknown drain type
        req_body = {'drain_type': 'gibberish'}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert_req_body_unrecognised_field_value(response, 'drain_type')

    def test_stack_validation_errors(self, mock_client, mock_asgs, mock_invalid_asg_name):
        # test stack not found
        req_body = {}
        response = mock_client.post(self.get_api_path_valid_region(self.default_stack_name), json=req_body)
        assert response.get_json()['error_msg'] == f'Stack {self.default_stack_name} not found'
        assert response.status_code == 404

    def test_reinit_all_nodes(self, mock_client, wait_for_action_to_complete):
        mystack = test_stacks.setup_confluence_stack()
        req_body = {}
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test fast drain
        req_body['drain_type'] = 'fast'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)
        # test skip drain
        req_body['drain_type'] = 'skip'
        response = mock_client.post(self.get_api_path_valid_region(mystack.stack_name), json=req_body)
        assert response.get_json()["message"] == self.api_res_success_msg
        assert response.status_code == 202
        wait_for_action_to_complete(response.json['action_id'], mystack.stack_name)


class TestUtilsMethods:
    def test_api_get_cfn_templates(self):
        with test_stacks.app.test_client() as client:
            response = client.get('http://127.0.0.1:8000/api/v1/util/cfn_templates')
            assert response.status_code == 200
            assert hasattr(response, 'json')
            assert isinstance(response.json, list)
            assert not {'repo', 'name'}.difference(response.json[0].keys())
            return

    def test_api_get_cfn_templates_invalid_product(self):
        with test_stacks.app.test_client() as client:
            response = client.get('http://127.0.0.1:8000/api/v1/util/cfn_templates?product=bamboo')
            assert response.status_code == 400
            assert hasattr(response, 'json')
            assert isinstance(response.json, dict)
            assert response.json["error_msg"]["query"]["product"][0].startswith("Must be one of")
            return

    def test_api_get_cfn_templates_valid_product(self):
        with test_stacks.app.test_client() as client:
            response = client.get('http://127.0.0.1:8000/api/v1/util/cfn_templates?product=jira')
            assert response.status_code == 200
            assert hasattr(response, 'json')
            assert isinstance(response.json, list)
            assert all(template for template in response.json if "jira" in template["name"])
            assert any(template for template in response.json if template["name"] == test_stacks.JIRA_TEMPLATE_FILE.name)
            return

    def test_api_get_cfn_templates_no_results(self):
        with test_stacks.app.test_client() as client:
            response = client.get('http://127.0.0.1:8000/api/v1/util/cfn_templates?repo_name=non-existent-repo')
            assert response.status_code == 404
            assert hasattr(response, 'json')
            assert isinstance(response.json, dict)
            assert response.json["error_msg"] == "Template(s) not found"
