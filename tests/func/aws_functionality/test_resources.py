import os

import mock
from moto import mock_aws
from tests.func.aws_functionality import moto_overrides
import weaver
from moto.rds.responses import RDSResponse

# create a basic testing app
app = weaver.create_app('weaver.config.TestingConfig')

# patch moto to add support for missing/busted functionality
RDSResponse.describe_db_engine_versions = moto_overrides.describe_db_engine_versions


@mock_aws
@mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'AWS_ACCESS_KEY_ID'})
@mock.patch.dict(os.environ, {'AWS_SECRET_ACCESS_KEY': 'AWS_SECRET_ACCESS_KEY'})
class TestAwsResources:
    # set base query params for the next few tests
    _db_snapshot_query_string_params = {'region': 'us-east-1', 'dbEngine': 'postgres'}

    # 12.5 -> 14 should fail; version doesn't exist
    def test_snapshot_db_engine_upgrade_nonexistent(self):
        with app.test_client() as client:
            response = client.get(
                '/getDbVersionCompatibilityForSnapshot', query_string=self._db_snapshot_query_string_params | {'dbEngineVersion': '14', 'snapshotEngineVersion': '12.5'}
            ).get_json()
            print(response)
            assert response['type'] == 'warning'
            assert response['title'] == 'Invalid DB Engine Major Version'

    # 12.5 -> 12.4 should fail; downgrades not possible
    def test_snapshot_db_engine_downgrade(self):
        with app.test_client() as client:
            response = client.get(
                '/getDbVersionCompatibilityForSnapshot', query_string=self._db_snapshot_query_string_params | {'dbEngineVersion': '12.4', 'snapshotEngineVersion': '12.5'}
            ).get_json()
            assert response['type'] == 'error'
            assert response['title'] == 'Database cannot be downgraded'

    # 12.5 -> 12.5 should succeed; same version
    def test_snapshot_db_engine_same_version(self):
        with app.test_client() as client:
            response = client.get(
                '/getDbVersionCompatibilityForSnapshot', query_string=self._db_snapshot_query_string_params | {'dbEngineVersion': '12.5', 'snapshotEngineVersion': '12.5'}
            ).get_json()
            assert response['type'] == 'success'
            assert response['title'] == 'Compatible snapshot version'

    # upgrade to specific version (11.11 -> 12.6) should work; version is a valid upgrade target
    def test_snapshot_db_engine_specific_upgrade_success(self):
        with app.test_client() as client:
            response = client.get(
                '/getDbVersionCompatibilityForSnapshot', query_string=self._db_snapshot_query_string_params | {'dbEngineVersion': '12.6', 'snapshotEngineVersion': '11.11'}
            ).get_json()
            assert response['type'] == 'info'
            assert response['title'] == 'Database will be upgraded'

    # upgrade to specific version (11.11 -> 12.5) should fail; version is not a valid upgrade target
    def test_snapshot_db_engine_specific_upgrade_fail(self):
        with app.test_client() as client:
            response = client.get(
                '/getDbVersionCompatibilityForSnapshot', query_string=self._db_snapshot_query_string_params | {'dbEngineVersion': '12.5', 'snapshotEngineVersion': '11.11'}
            ).get_json()
            assert response['type'] == 'error'
            assert response['title'] == 'Database cannot be upgraded'

    # upgrade to generic version (11.11 -> 12) should work; default minor version 12.6 is a valid upgrade target
    def test_snapshot_db_engine_generic_upgrade_success(self):
        with app.test_client() as client:
            response = client.get(
                '/getDbVersionCompatibilityForSnapshot', query_string=self._db_snapshot_query_string_params | {'dbEngineVersion': '12', 'snapshotEngineVersion': '11.11'}
            ).get_json()
            assert response['type'] == 'info'
            assert response['title'] == 'Database will be upgraded'

    # upgrade to generic version (11.11 -> 13) should fail; default minor version 13.3 is not a valid upgrade target for 11.11
    def test_snapshot_db_engine_generic_upgrade_fail(self):
        with app.test_client() as client:
            response = client.get(
                '/getDbVersionCompatibilityForSnapshot', query_string=self._db_snapshot_query_string_params | {'dbEngineVersion': '13', 'snapshotEngineVersion': '11.11'}
            ).get_json()
            assert response['type'] == 'error'
            assert response['title'] == 'Database cannot be upgraded'
