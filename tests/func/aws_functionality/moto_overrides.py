from __future__ import annotations

import json

from typing import Any, Dict, Optional, Tuple, Type, TYPE_CHECKING

from moto.cloudformation.parsing import clean_json, ResourceMap, parse_resource_and_generate_name, CF_MODEL
from moto.cloudformation.responses import CREATE_CHANGE_SET_RESPONSE_TEMPLATE
from moto.core.exceptions import RESTError
from moto.core.common_models import CloudFormationModel
from moto.efs.exceptions import FileSystemNotFound, MountTargetConflict
from moto.elbv2.exceptions import DuplicateTargetGroupName, InvalidConditionValueError, InvalidTargetGroupNameError, TargetGroupNotFoundError
from moto.elbv2.models import FakeTargetGroup
from moto.elbv2.utils import make_arn_for_target_group
from moto.iam.models import iam_backends
from moto.iam.exceptions import IAMNotFoundException
from moto.rds.exceptions import RDSClientError
from moto.ssm import ssm_backends
from moto.utilities.utils import get_partition

if TYPE_CHECKING:
    from moto.ec2.models.instances import Instance


###
## These override functions are used to fix bugs in moto
## If you are trying to return something other than what moto returns, use a MagicMock instead
##


# autoscaling models.py
# launch templates are not very well implented, mocking some aspects here
@property
def image_id(self):
    return '11'


@property
def instance_type(self):
    return 'instance_type'


@property
def user_data(self):
    return 'user_data'


@property
def security_groups(self):
    return []


# cloudformation models.py
# adds basic support for updating stack tags; does not update resource tags
def apply(self) -> None:
    self.stack.resource_map.update(self.template_dict, self.parameters)
    # only overwrite tags if passed
    if self.tags is not None:
        self.stack.tags = self.tags
        # TODO: update tags in the resource map


# cloudformation parsing.py
# Fails looking up SSM param for AMI
def parse_ssm_parameter(self, value, value_type):
    # The Value in SSM parameters is the SSM parameter path
    # we need to use ssm_backend to retrieve the
    # actual value from parameter store
    parameter = ssm_backends[self._account_id][self._region_name].get_parameter(value)
    if not parameter:
        return '12345'
    actual_value = parameter.value
    if value_type.find("List") > 0:
        return actual_value.split(",")
    return actual_value


# skip trying to update resources that conditionally shouldn't exist in the first place
def parse_and_update_resource(
    logical_id: str,
    resource_json: Dict[str, Any],
    resources_map: "ResourceMap",
    account_id: str,
    region_name: str,
) -> Optional[CF_MODEL]:
    condition = resource_json.get("Condition")
    if condition and not resources_map.lazy_condition_map[condition]:
        # If this has a False condition, don't try to update the resource
        return None
    resource_tuple: Optional[Tuple[Type[CloudFormationModel], Dict[str, Any], str]] = parse_resource_and_generate_name(logical_id, resource_json, resources_map)
    if not resource_tuple:
        return None
    resource_class, resource_json, new_resource_name = resource_tuple
    original_resource = resources_map[logical_id]
    if not hasattr(resource_class.update_from_cloudformation_json, "__isabstractmethod__"):
        new_resource = resource_class.update_from_cloudformation_json(
            original_resource=original_resource,
            new_resource_name=new_resource_name,
            cloudformation_json=resource_json,
            account_id=account_id,
            region_name=region_name,
        )
        new_resource.type = resource_json["Type"]
        new_resource.logical_resource_id = logical_id
        return new_resource
    else:
        return None


# cloudformation responses.py
# adds support for "use_previous_value" and "UsePreviousTemplate"
def create_change_set(self):
    stack_name = self._get_param("StackName")
    change_set_name = self._get_param("ChangeSetName")
    stack_body = self._get_param("TemplateBody")
    stack = self.cloudformation_backend.get_stack(stack_name)
    template_url = self._get_param("TemplateURL")
    description = self._get_param("Description")
    role_arn = self._get_param("RoleARN")
    update_or_create = self._get_param("ChangeSetType", "CREATE")
    parameters_list = self._get_list_prefix("Parameters.member")
    tags = dict((item["key"], item["value"]) for item in self._get_list_prefix("Tags.member"))
    parameters = {param["parameter_key"]: param["parameter_value"] for param in parameters_list if "parameter_value" in param}
    previous = dict([(param["parameter_key"], stack.parameters[param["parameter_key"]]) for param in parameters_list if "use_previous_value" in param])
    parameters.update(previous)
    if self._get_param("UsePreviousTemplate") == "true":
        stack_body = stack.template
    elif not stack_body and template_url:
        stack_body = self._get_stack_from_s3_url(template_url)
    stack_notification_arns = self._get_multi_param("NotificationARNs.member")
    change_set_id, stack_id = self.cloudformation_backend.create_change_set(
        stack_name=stack_name,
        change_set_name=change_set_name,
        template=stack_body,
        parameters=parameters,
        description=description,
        notification_arns=stack_notification_arns,
        tags=tags,
        role_arn=role_arn,
        change_set_type=update_or_create,
    )
    if self.request_json:
        return json.dumps(
            {
                "CreateChangeSetResponse": {
                    "CreateChangeSetResult": {
                        "Id": change_set_id,
                        "StackId": stack_id,
                    }
                }
            }
        )
    else:
        template = self.response_template(CREATE_CHANGE_SET_RESPONSE_TEMPLATE)
        return template.render(stack_id=stack_id, change_set_id=change_set_id)


DESCRIBE_STACKS_TEMPLATE = """<DescribeStacksResponse>
  <DescribeStacksResult>
    <Stacks>
      {% for stack in stacks %}
      <member>
        <StackName>{{ stack.name }}</StackName>
        <StackId>{{ stack.stack_id }}</StackId>
        {% if stack.change_set_id %}
        <ChangeSetId>{{ stack.change_set_id }}</stack.timeout_in_minsChangeSetId>
        {% endif %}
        <Description><![CDATA[{{ stack.description }}]]></Description>
        <CreationTime>{{ stack.creation_time_iso_8601 }}</CreationTime>
        <StackStatus>{{ stack.status }}</StackStatus>
        {% if stack.notification_arns %}
        <NotificationARNs>
          {% for notification_arn in stack.notification_arns %}
          <member>{{ notification_arn }}</member>
          {% endfor %}
        </NotificationARNs>
        {% else %}
        <NotificationARNs/>
        {% endif %}
        <DisableRollback>false</DisableRollback>
        {%if stack.stack_outputs %}
        <Outputs>
        {% for output in stack.stack_outputs %}
          <member>
            <OutputKey>{{ output.key }}</OutputKey>
            <OutputValue>{{ output.value }}</OutputValue>
            {% for export in stack.exports if export.value == output.value %}
                <ExportName>{{ export.name }}</ExportName>
            {% endfor %}
            {% if output.description %}<Description>{{ output.description }}</Description>{% endif %}
          </member>
        {% endfor %}
        </Outputs>
        {% endif %}
        <Parameters>
        {% for param_name, param_value in stack.stack_parameters.items() %}
          <member>
            <ParameterKey>{{ param_name }}</ParameterKey>
            {% if param_name in stack.resource_map.no_echo_parameter_keys %}
                <ParameterValue>****</ParameterValue>
            {% elif param_value.__class__.__name__ == "list" %}
                <ParameterValue>{{ param_value | join(',') }}</ParameterValue>
            {% else %}
                <ParameterValue>{{ param_value }}</ParameterValue>
            {% endif %}
          </member>
        {% endfor %}
        </Parameters>
        {% if stack.role_arn %}
        <RoleARN>{{ stack.role_arn }}</RoleARN>
        {% endif %}
        <Tags>
          {% for tag_key, tag_value in stack.tags.items() %}
            <member>
              <Key>{{ tag_key }}</Key>
              <Value>{{ tag_value }}</Value>
            </member>
          {% endfor %}
        </Tags>
        <EnableTerminationProtection>{{ stack.enable_termination_protection }}</EnableTerminationProtection>
        {% if stack.timeout_in_mins %}
        <TimeoutInMinutes>{{ stack.timeout_in_mins }}</TimeoutInMinutes>
        {% endif %}
      </member>
      {% endfor %}
    </Stacks>
    {% if next_token %}
    <NextToken>{{ next_token }}</NextToken>
    {% endif %}
  </DescribeStacksResult>
</DescribeStacksResponse>"""


# ec2 instances.py
# adds ability to launch an instance from a launchtemplate
@classmethod
def ec2_instance_create_from_cloudformation_json(  # type: ignore[misc]
    cls,
    resource_name: str,
    cloudformation_json: Any,
    account_id: str,
    region_name: str,
    **kwargs: Any,
) -> "Instance":
    from moto.ec2.models import ec2_backends

    properties = cloudformation_json["Properties"]

    ec2_backend = ec2_backends[account_id][region_name]
    security_group_ids = properties.get("SecurityGroups", [])
    group_names = [ec2_backend.get_security_group_from_id(group_id).name for group_id in security_group_ids]  # type: ignore[union-attr]

    if "LaunchTemplate" in properties:
        reservation = ec2_backend.run_instances(
            count=1,
            image_id=properties.get("ImageId", ""),
            is_instance_type_default=not properties.get("InstanceType"),
            launch_template=properties.get("LaunchTemplate"),
            security_group_names=group_names,
            user_data=properties.get("UserData"),
        )
    else:
        reservation = ec2_backend.run_instances(
            image_id=properties["ImageId"],
            user_data=properties.get("UserData"),
            count=1,
            security_group_names=group_names,
            instance_type=properties.get("InstanceType", "m1.small"),
            is_instance_type_default=not properties.get("InstanceType"),
            subnet_id=properties.get("SubnetId"),
            key_name=properties.get("KeyName"),
            private_ip=properties.get("PrivateIpAddress"),
            block_device_mappings=properties.get("BlockDeviceMappings", {}),
        )
    instance = reservation.instances[0]
    for tag in properties.get("Tags", []):
        instance.add_tag(tag["Key"], tag["Value"])

    # Associating iam instance profile.
    # TODO: Don't forget to implement replace_iam_instance_profile_association once update_from_cloudformation_json
    #  for ec2 instance will be implemented.
    if properties.get("IamInstanceProfile"):
        ec2_backend.associate_iam_instance_profile(
            instance_id=instance.id,
            iam_instance_profile_name=properties.get("IamInstanceProfile"),
        )

    return instance


# elbv2 models.py
# Works around an issue attempting to deregister a target before it has been registered
def deregister_targets(self, target_group_arn, instances):
    target_group = self.target_groups.get(target_group_arn)
    if target_group is None:
        raise TargetGroupNotFoundError()
    try:
        for target in instances:
            if target["id"] in target_group.targets:
                target_group.targets.pop(target["id"], None)
    except StopIteration:
        pass


# elbv2 models.py
# cert["CertificateArn"] throws key error, commenting out transform
def convert_and_validate_certificates(self, certificates):
    # transform default certificate to conform with the rest of the code and XML templates
    # for cert in certificates or []:
    #     cert["certificate_arn"] = cert["CertificateArn"]
    return certificates


# efs models.py
# If mount target not found, return
def delete_mount_target(self, mount_target_id):
    """Delete a mount target specified by the given mount_target_id.

    Note that this will also delete a network interface.

    https://docs.aws.amazon.com/efs/latest/ug/API_DeleteMountTarget.html
    """
    if mount_target_id not in self.mount_targets_by_id:
        return

    mount_target = self.mount_targets_by_id[mount_target_id]
    self.ec2_backend.delete_network_interface(mount_target.network_interface_id)
    del self.mount_targets_by_id[mount_target_id]
    mount_target.clean_up()
    return


# efs models.py
# ignore remaining mount targets and delete anyway
def delete_file_system(self, file_system_id):
    """Delete the file system specified by the given file_system_id.

    Note that mount targets must be deleted first.

    https://docs.aws.amazon.com/efs/latest/ug/API_DeleteFileSystem.html
    """
    if file_system_id not in self.file_systems_by_id:
        raise FileSystemNotFound(file_system_id)

    file_system = self.file_systems_by_id[file_system_id]
    if file_system.number_of_mount_targets > 0:
        pass

    del self.file_systems_by_id[file_system_id]
    self.creation_tokens.remove(file_system.creation_token)
    return


# efs models.py
# If mount target already exists, ignore - this is because resources are persisting across tests in newer versions of moto
# State still does not persist, but some resources do, causing intermittent failures.
def add_mount_target(self, subnet, mount_target):
    # Check that the mount target doesn't violate constraints.
    for other_mount_target in self._mount_targets.values():
        if other_mount_target.subnet_vpc_id != subnet.vpc_id:
            raise MountTargetConflict("requested subnet for new mount target is not in the same VPC as existing mount targets")

    if subnet.availability_zone in self._mount_targets:
        pass  # ignore existing mount targets

    self._mount_targets[subnet.availability_zone] = mount_target


# iam models.py
# Add default=str to prevent `Object of type date is not JSON serializable` error
def update(
    self,
    policy_name,
    policy_document,
    group_names,
    role_names,
    user_names,
):
    self.policy_name = policy_name
    self.policy_document = json.dumps(policy_document, default=str) if isinstance(policy_document, dict) else policy_document
    self.group_names = group_names
    self.role_names = role_names
    self.user_names = user_names


# adds try/except wrapper around call to delete_inline_policy; moto sometimes deletes the role before deleting the
# attached inline policies, which throws IAMNotFoundException, and in turn, a NoSuchEntity error on the stack deletion
@classmethod
def iam_inline_policy_delete_from_cloudformation_json(  # type: ignore[misc]
    cls,
    resource_name: str,
    cloudformation_json: Dict[str, Any],
    account_id: str,
    region_name: str,
) -> None:
    try:
        iam_backends[account_id][get_partition(region_name)].delete_inline_policy(resource_name)
    except IAMNotFoundException:
        pass


# rds responses.py
# adds basic support (i.e., hard-coded responses) for describe_db_engine_versions (which is not yet implemented by moto)
def describe_db_engine_versions(self):
    engine = self._get_param('Engine')
    engine_version = self._get_param('EngineVersion')
    response_obj = {}

    if engine == 'postgres':
        if engine_version == '11.11':
            response_obj = {
                'Engine': 'postgres',
                'EngineVersion': '11.11',
                'DBParameterGroupFamily': 'postgres11',
                'DBEngineDescription': 'PostgreSQL',
                'DBEngineVersionDescription': 'PostgreSQL 11.11-R1',
                'ValidUpgradeTarget': [
                    {'Engine': 'postgres', 'EngineVersion': '11.12', 'Description': 'PostgreSQL 11.12-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': False},
                    {'Engine': 'postgres', 'EngineVersion': '12.6', 'Description': 'PostgreSQL 12.6-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                    {'Engine': 'postgres', 'EngineVersion': '12.7', 'Description': 'PostgreSQL 12.7-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                    {'Engine': 'postgres', 'EngineVersion': '13.2', 'Description': 'PostgreSQL 13.2-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                ],
            }
        elif engine_version == '12':
            response_obj = {
                'Engine': 'postgres',
                'EngineVersion': '12.6',
                'DBParameterGroupFamily': 'postgres12',
                'DBEngineDescription': 'PostgreSQL',
                'DBEngineVersionDescription': 'PostgreSQL 12.6-R1',
                'ValidUpgradeTarget': [
                    {'Engine': 'postgres', 'EngineVersion': '12.7', 'Description': 'PostgreSQL 12.7-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': False},
                    {'Engine': 'postgres', 'EngineVersion': '13.2', 'Description': 'PostgreSQL 13.2-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                    {'Engine': 'postgres', 'EngineVersion': '13.3', 'Description': 'PostgreSQL 13.3-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                ],
            }
        elif engine_version == '12.5':
            response_obj = {
                'Engine': 'postgres',
                'EngineVersion': '12.5',
                'DBParameterGroupFamily': 'postgres12',
                'DBEngineDescription': 'PostgreSQL',
                'DBEngineVersionDescription': 'PostgreSQL 12.5-R1',
                'ValidUpgradeTarget': [
                    {'Engine': 'postgres', 'EngineVersion': '12.6', 'Description': 'PostgreSQL 12.6-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': False},
                    {'Engine': 'postgres', 'EngineVersion': '12.7', 'Description': 'PostgreSQL 12.7-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': False},
                    {'Engine': 'postgres', 'EngineVersion': '13.1', 'Description': 'PostgreSQL 13.1-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                    {'Engine': 'postgres', 'EngineVersion': '13.2', 'Description': 'PostgreSQL 13.2-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                    {'Engine': 'postgres', 'EngineVersion': '13.3', 'Description': 'PostgreSQL 13.3-R1', 'AutoUpgrade': False, 'IsMajorVersionUpgrade': True},
                ],
            }
        elif engine_version == '13':
            response_obj = {
                'Engine': 'postgres',
                'EngineVersion': '13.3',
                'DBParameterGroupFamily': 'postgres13',
                'DBEngineDescription': 'PostgreSQL',
                'DBEngineVersionDescription': 'PostgreSQL 13.3-R1',
                'ValidUpgradeTarget': [],
            }
        else:
            raise RDSClientError('InvalidParameterCombination', f'Cannot find major version {engine_version} for {engine}')

    if self.request_json:
        return json.dumps({'DBEngineVersions': [response_obj]})
    else:
        template = self.response_template(DESCRIBE_DB_ENGINE_VERSIONS_RESPONSE_TEMPLATE)
        return template.render(engine_data=response_obj)


DESCRIBE_DB_ENGINE_VERSIONS_RESPONSE_TEMPLATE = """<DescribeDbEngineVersionsResponse>
  <DescribeDBEngineVersionsResult>
    <DBEngineVersions>
        <DBEngineVersion>
            <Engine>{{ engine_data.Engine }}</Engine>
            <EngineVersion>{{ engine_data.EngineVersion }}</EngineVersion>
            <DBParameterGroupFamily>{{ engine_data.DBParameterGroupFamily }}</DBParameterGroupFamily>
            <DBEngineDescription>{{ engine_data.DBEngineDescription }}</DBEngineDescription>
            <DBEngineVersionDescription>{{ engine_data.DBEngineVersionDescription }}</DBEngineVersionDescription>
            <ValidUpgradeTarget>
                {% for target in engine_data.ValidUpgradeTarget %}
                <UpgradeTarget>
                    <Engine>{{ target.Engine }}</Engine>
                    <EngineVersion>{{ target.EngineVersion }}</EngineVersion>
                    <Description>{{ target.Description }}</Description>
                    <AutoUpgrade>{{ target.AutoUpgrade }}</AutoUpgrade>
                    <IsMajorVersionUpgrade>{{ target.IsMajorVersionUpgrade }}</IsMajorVersionUpgrade>
                </UpgradeTarget>
                {% endfor %}
            </ValidUpgradeTarget>
        </DBEngineVersion>
    </DBEngineVersions>
  </DescribeDBEngineVersionsResult>
  <ResponseMetadata>
    <RequestId>3d3200a1-810e-3023-6cc3-example</RequestId>
  </ResponseMetadata>
</DescribeDbEngineVersionsResponse>"""


# ssm models.py
# passes resource_name instead of pulling it from template properties, which has
# un-templated Fn::Sub replacement with stackname (??)
@classmethod
def ssm_delete_from_cloudformation_json(  # type: ignore[misc]
    cls,
    resource_name: str,
    cloudformation_json: Any,
    account_id: str,
    region_name: str,
) -> None:
    ssm_backend = ssm_backends[account_id][region_name]
    properties = cloudformation_json["Properties"]

    # ssm_backend.delete_parameter(properties.get("Name"))
    ssm_backend.delete_parameter(resource_name)
