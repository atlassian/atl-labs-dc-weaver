#!/usr/bin/env python3

from __future__ import annotations

from datetime import datetime, timezone
from unittest import mock
from unittest.mock import MagicMock

from flask_jwt_extended import decode_token, create_refresh_token
from ruamel import yaml

import weaver
from weaver.token_auth.models import ApiToken
from weaver.sns import sns

# create a testing app; override the instance path with our examples
app = weaver.create_app('weaver.config.TestingConfig')
app.instance_path = f"{app.instance_path}-examples"
app.config['AVAILABLE_REGIONS'] = ['us-east-1']  # tests below only use this region

# load the contents of the example YAML permissions file; use it as magicmock return and mock_open response
# this allows us to use the data during the test and also mock the loading such that we can know if it was loaded
YAML_PERMISSIONS_FILE = f'{app.instance_path}/permissions.yaml'
MOCK_YAML_PERMISSIONS = yaml.YAML(typ="safe", pure=True).load(open(YAML_PERMISSIONS_FILE, 'r'))

# provide some consistent example responses
MOCK_STACK_LIST_DATA = ['a', 'list', 'of', 'stacks']
MOCK_STACK_DATA = {
    "nodes": [
        {
            "arch": "x86_64",
            "autoscaling_group_name": "my-jira-stg-ClusterNodeGroup-JDH82HSFE89W",
            "az": "us-east-1a",
            "id": "i-026f90cb0addf4395",
            "ip": "10.28.68.113",
            "registration_status_alb": "healthy",
            "service": "jira",
            "state": "running",
            "status": "RUNNING",
            "type": "c6i.4xlarge",
            "uptime": "21d 21h 57m",
        }
    ],
    "action_in_progress": "none",
    "stack_status": "CREATE_COMPLETE",
    "service_status": "RUNNING",
    "product": "jira",
    "version": "8.20.12",
}


def create_testing_refresh_token(user_name: str, groups: list[str]):
    """
    A stripped-down version of the post method provided by RefreshTokenMethods
    """
    with app.app_context():
        refresh_token = create_refresh_token(identity=user_name, additional_claims={"groups": groups})
        refresh_token_decoded = decode_token(refresh_token)
        refresh_token_created_at = datetime.fromtimestamp(refresh_token_decoded["iat"], tz=timezone.utc)
        refresh_token_expiry = datetime.fromtimestamp(refresh_token_decoded["exp"], tz=timezone.utc)
        weaver.extensions.db.session.add(
            ApiToken(
                created_at=refresh_token_created_at,
                expires_at=refresh_token_expiry,
                jti=refresh_token_decoded["jti"],
                token_type=refresh_token_decoded["type"],
                user_name=user_name,
            )
        )
        weaver.extensions.db.session.commit()
        return refresh_token


def revoke_testing_token(token: str):
    with app.app_context():
        jti = decode_token(token)['jti']
        token_obj = weaver.extensions.db.session.execute(weaver.extensions.db.select(ApiToken).filter_by(jti=jti)).scalar()
        token_obj.revoked = True
        token_obj.revoked_at = datetime.now(tz=timezone.utc)
        weaver.extensions.db.session.commit()


@weaver.api.helpers.auth_required
def mock_git_pull(*args, **kwargs):
    return 'Weaver updated successfully'


@mock.patch('weaver.saml_auth.saml_auth.check_user_permissions_legacy', wraps=weaver.saml_auth.saml_auth.check_user_permissions_legacy)
@mock.patch('weaver.saml_auth.saml_auth.check_user_permissions', wraps=weaver.saml_auth.saml_auth.check_user_permissions)
class TestPermissions:
    def test_no_saml(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = True
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        # call get stack list
        with app.test_client() as client:
            response = client.get('/api/v1/stack?region=us-east-1')
        mocked_check_user_permissions.assert_not_called()  # permissions.yaml was not loaded
        assert response.status_code == 200  # request was successful

    def test_saml_get_stacklist_everyone(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            with client.session_transaction() as session:
                # set user details in session
                session['saml'] = {'subject': 'UserA', 'attributes': {'memberOf': ['everyone']}}
            # call get stack list
            response = client.get('/api/v1/stack?region=us-east-1')
        mocked_check_user_permissions.assert_called()  # yaml permissions handler was invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_called()  # permissions.yaml was loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert response.status_code == 200  # request was successful

    def test_saml_get_stack_allowed(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.StackMethods.get = MagicMock(return_value=MOCK_STACK_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            with client.session_transaction() as session:
                # set user details in session  (second group has access)
                session['saml'] = {'subject': 'UserA', 'attributes': {'memberOf': ['fake-group', 'jira-dev']}}
            # call get stack list
            response = client.get('/api/v1/stack/my-jira-stg?region=us-east-1')
        mocked_check_user_permissions.assert_called()  # yaml permissions handler was invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_called()  # permissions.yaml was loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert response.status_code == 200  # request was successful

    def test_saml_get_stack_not_in_allowed_groups_no_json(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.StackMethods.get = MagicMock(return_value=MOCK_STACK_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME_LEGACY = "permissions.json.does.not.exist"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value={})
        sns.send_sns_msg = MagicMock(return_value=True)
        with app.test_client() as client:
            with client.session_transaction() as session:
                # set user details in session
                session['saml'] = {'subject': 'UserA', 'attributes': {'memberOf': ['fake-group']}}
            # call get stack
            response = client.get('/api/v1/stack/my-jira-stg?region=us-east-1')
        mocked_check_user_permissions.assert_called()  # yaml permissions handler was invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_called()  # permissions.yaml was loaded
        mocked_check_user_permissions_legacy.assert_called()  # json permissions handler was invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json load was NOT loaded
        assert response.status_code == 403  # the request was NOT dispatched
        weaver.weaver.StackMethods.get.assert_not_called()  # request was NOT dispatched
        assert sns.send_sns_msg.called  # an sns msg was sent about the failure

    def test_saml_get_stack_session_expired(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        with app.test_client() as client:
            # do not set user details in session
            # call get stack
            response = client.get('/api/v1/stack/my-jira-stg?region=us-east-1')
        assert response.status_code == 401  # the request was NOT dispatched
        assert response.json['error_msg'] == 'Session expired'
        weaver.weaver.StackMethods.get.assert_not_called()  # request was NOT dispatched

    def test_create_jwt_refresh_token(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            with client.session_transaction() as session:
                # set user details in session
                session['saml'] = {'subject': 'UserA', 'attributes': {'memberOf': ['everyone']}}
            # create a refresh token
            refresh_token_response = client.post('/api/v1/token/refresh')
            decoded_token = decode_token(refresh_token_response.json['refresh_token'])
        mocked_check_user_permissions.assert_called()  # yaml permissions handler was invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_called()  # permissions.yaml was loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert refresh_token_response.status_code == 200  # request was successful
        assert 'everyone' in decoded_token['groups']

    def test_jwt_create_access_token(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            # create a refresh token
            refresh_token = create_testing_refresh_token('UserA', ['everyone'])
            # use refresh token to create an access token
            access_token_response = client.post('/api/v1/token/access', headers={'Authorization': f'Bearer {refresh_token}'})
            decoded_token = decode_token(access_token_response.json['access_token'])
        mocked_check_user_permissions.assert_not_called()  # yaml permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_not_called()  # permissions.yaml was NOT loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert access_token_response.status_code == 200  # request was successful
        assert decoded_token['authorization_details'][0]['stacks'] == ['*']
        assert decoded_token['authorization_details'][0]['regions'] == ['*']
        assert decoded_token['authorization_details'][0]['methods'] == ['get']
        assert decoded_token['authorization_details'][0]['routes'] == ['/api/v1/stack', '/api/v1/stack/<stack_name>', '/api/v1/util/cfn_templates']

    def test_jwt_create_access_token_refresh_revoked(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            # create a refresh token
            refresh_token = create_testing_refresh_token('UserA', ['everyone'])
            # revoke the token
            revoke_testing_token(refresh_token)
            # attempt to use refresh token to create an access token
            access_token_response = client.post('/api/v1/token/access', headers={'Authorization': f'Bearer {refresh_token}'})
        mocked_check_user_permissions.assert_not_called()  # yaml permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_not_called()  # permissions.yaml was NOT loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert access_token_response.status_code == 401  # request was NOT successful
        assert access_token_response.json['error_msg'] == 'Token has been revoked'

    def test_jwt_get_stacklist_with_refresh_token(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            # create a refresh token
            refresh_token = create_testing_refresh_token('UserA', ['everyone'])
            # attempt to use refresh token to get stacklist
            response = client.get('/api/v1/stack?region=us-east-1', headers={'Authorization': f'Bearer {refresh_token}'})
        mocked_check_user_permissions.assert_not_called()  # yaml permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_not_called()  # permissions.yaml was NOT loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert response.status_code == 422  # request was NOT successful
        assert response.json['error_msg'] == 'Only non-refresh tokens are allowed'

    def test_jwt_get_stacklist_everyone(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            # create a refresh token
            refresh_token = create_testing_refresh_token('UserA', ['everyone'])
            # use refresh token to create an access token
            access_token_response = client.post('/api/v1/token/access', headers={'Authorization': f'Bearer {refresh_token}'})
            # call get stack list
            response = client.get('/api/v1/stack?region=us-east-1', headers={'Authorization': f'Bearer {access_token_response.json["access_token"]}'})
        mocked_check_user_permissions.assert_not_called()  # yaml permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_not_called()  # permissions.yaml was NOT loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert response.status_code == 200  # request was successful
        assert response.json == MOCK_STACK_LIST_DATA  # list of stacks was retrieved successfully

    def test_jwt_put_stack_everyone(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            # create a refresh token
            refresh_token = create_testing_refresh_token('UserA', ['everyone'])
            # use refresh token to create an access token
            access_token_response = client.post('/api/v1/token/access', headers={'Authorization': f'Bearer {refresh_token}'})
            # call get stack list
            response = client.put('/api/v1/stack/somestackname?region=us-east-1', headers={'Authorization': f'Bearer {access_token_response.json["access_token"]}'})
        mocked_check_user_permissions.assert_not_called()  # yaml permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_not_called()  # permissions.yaml was NOT loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert response.status_code == 403  # request was forbidden

    def test_jwt_put_stack_jiradev_wrong_stack(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            # create a refresh token
            refresh_token = create_testing_refresh_token('UserB', ['jira-dev'])
            # use refresh token to create an access token
            access_token_response = client.post('/api/v1/token/access', headers={'Authorization': f'Bearer {refresh_token}'})
            # call get stack list
            response = client.put('/api/v1/stack/somestackname?region=us-east-1', headers={'Authorization': f'Bearer {access_token_response.json["access_token"]}'})
        mocked_check_user_permissions.assert_not_called()  # yaml permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_not_called()  # permissions.yaml was NOT loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert response.status_code == 403  # request was forbidden

    def test_jwt_put_stack_jiradev_right_stack(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        weaver.weaver.utils.get_stacks = MagicMock(return_value=MOCK_STACK_LIST_DATA)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value=True)  # value doesn't matter; won't be called
        with app.test_client() as client:
            # create a refresh token
            refresh_token = create_testing_refresh_token('UserB', ['everyone', 'jira-dev'])
            # use refresh token to create an access token
            access_token_response = client.post('/api/v1/token/access', headers={'Authorization': f'Bearer {refresh_token}'})
            # call get stack list
            response = client.put('/api/v1/stack/my-jira-stg?region=us-east-1', headers={'Authorization': f'Bearer {access_token_response.json["access_token"]}'})
        mocked_check_user_permissions.assert_not_called()  # yaml permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_not_called()  # permissions.yaml was NOT loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        assert response.status_code == 400  # request was allowed, but malformed (our PUT request had no data)

    # temporary test prior to refactor
    def test_saml_DoGitPull(self, mocked_check_user_permissions: MagicMock, mocked_check_user_permissions_legacy: MagicMock):
        app.config['NO_SAML'] = False
        # set up mocks
        # somewhat convoluted because mocking the function removes its decorator
        weaver.weaver.DoGitPull.get = MagicMock(wraps=mock_git_pull)
        weaver.saml_auth.saml_auth.PERMISSIONS_FILE_NAME = "permissions.yaml"
        weaver.saml_auth.saml_auth.load_permissions_file = MagicMock(return_value=MOCK_YAML_PERMISSIONS)
        weaver.saml_auth.saml_auth.load_legacy_permissions_file = MagicMock(return_value={})
        with app.test_client() as client:
            with client.session_transaction() as session:
                # set user details in session  (second group has access)
                session['saml'] = {'subject': 'UserA', 'attributes': {'memberOf': ['admin']}}
            # call DoGitPull on Weaver
            response = client.get(path='/doGitPull/Weaver (requires restart)/__weaver__')
        mocked_check_user_permissions.assert_called()  # yaml permissions handler was invoked
        weaver.saml_auth.saml_auth.load_permissions_file.assert_called()  # permissions.yaml was loaded
        mocked_check_user_permissions_legacy.assert_not_called()  # json permissions handler was NOT invoked
        weaver.saml_auth.saml_auth.load_legacy_permissions_file.assert_not_called()  # permissions.json was NOT loaded
        weaver.weaver.DoGitPull.get.assert_called()  # request was dispatched
        assert response.status_code == 200
