Feature: As a service actor,
            I want to be be able to create or upgrade a DR or Staging service by UI

  Scenario: Weaver service index page returns expected status_code
   Given weaver is set up
   When i browse to the weaver index
    Then i should receive a 200

  Scenario: Weaver service index page returns expected title
     Given weaver is set up
     When i browse to the weaver index
      Then i see the "Atlassian Labs Data Center Weaver" title

  Scenario: Weaver index.html presents an environment of "Staging"
   Given weaver is set up
   When i browse to the weaver index
    Then i see an option of staging

  Scenario: Weaver index.html presents an environment of "Production"
   Given weaver is set up
   When i browse to the weaver index
    Then i see an option of production

  Scenario: Weaver index.html presents an "upgrade" option
     Given weaver is set up
     When i browse to the weaver index
      Then i see an option to upgrade

  Scenario: Weaver index.html presents a "fullrestart" option
     Given weaver is set up
     When i browse to the weaver index
      Then i see an option to fullrestart

  Scenario: Weaver index.html presents a "rollingrestart" option
   Given weaver is set up
   When i browse to the weaver index
    Then i see an option to rollingrestart

  Scenario: Weaver index.html for staging environment presents a "clone" option
     Given weaver is set up
     When i select staging on weaver index
      Then i see an option to clone

  Scenario: Weaver index.html for staging environment presents a "destroy" option
     Given weaver is set up
     When i select staging on weaver index
      Then i see an option to destroy

  Scenario: after environment is set and i chose an action, the "stack selection" form is presented
     Given environment has a value
     When i select an action on weaver index
      Then i see the stack_selection form

  Scenario: after action is set and i chose an environment, the "stack selection" form is presented
     Given action has a value
     When i select an environment on weaver index
      Then i see the stack_selection form

#  Scenario: Upgrade dash presented
#     Given weaver is setup
#      When i login with "admin" and "default"
#      Then i should see the alert "You were logged in"
#
#  Scenario: "Upgrade or Clone or Restart" option shown
#     Given weaver is setup
#      When i login with "monty" and "default"
#      Then i should see the alert "Invalid username"
#
#  Scenario: on "upgrade", List of existing stacks presented
#     Given weaver is setup
#      When i login with "admin" and "python"
#      Then  i should see the alert "Invalid password"
#
#  Scenario: on "rebuild", List of restore points presented
#     Given weaver is setup
#      When i login with "admin" and "python"
#      Then  i should see the alert "Invalid password"
#
#  Scenario: Aws Tags derived for charging
#     Given weaver is setup
#     and i login with "admin" and "default"
#      When i logout
#      Then  i should see the alert "You were logged out"
#
#  Scenario: Duo requested on upgrade/build
#     Given weaver is setup
#     and i login with "admin" and "default"
#      When i logout
#      Then  i should see the alert "You were logged out"
#
#  Scenario: progress bar presented based upon cfn events list
#     Given weaver is setup
#     and i login with "admin" and "default"
#      When i logout
#      Then  i should see the alert "You were logged out"
#
#  Scenario: bring up 1 cluster node then bring up required others in sequence
#     Given weaver is setup
#     and i login with "admin" and "default"
#      When i logout
#      Then  i should see the alert "You were logged out"steps here
