# Tools

- [pipenv](https://pipenv.pypa.io/en/latest/)
- [pre-commit](https://pypi.org/project/pre-commit/)
- [Python](https://www.python.org/downloads/)

# Getting started

Install the above tools. Run `pipenv sync --dev` to setup your virtual environment with Python dependencies installed. Run `pre-commit install` within your dev environment to have our pre-commit checks automatically added to your local git repo.

See [Installation & Configuration](#installation--configuration) to run a local instance of Weaver.

# IDE

**Preferred/Recommended IDE:** IntelliJ / VSCode

# Installation & Configuration

#### AWS

Your local environment must be configured with AWS environment variables, and the access keys or IAM role you're using must have enough permissions to deploy the CloudFormation stack. Generally, you'll need administrator level of access; either the AWS managed "AdministratorAccess" policy or the effective "Allow *" for at least the following services:

* EC2
* ECS
* ElasticLoadBalancing
* IAM
* CloudWatch (and Logs)
* Route53
* S3
* SSM

#### Git submodule

Weaver relies on grabbing CloudFormation templates from a git repository included as a submodule of `atl-labs-dc-weaver`. If you're using the templates found in [atlassian-aws-deployment](https://bitbucket.org/atlassian/atlassian-aws-deployment), no extra config is necessary; just run the following to initialize the repo:

```
git submodule update --init --recursive
```

#### SAML

To test SAML auth locally you will need to install libxml2 and xmlsec1 on your OS; when SAML auth is enabled, the app will not load if these packages/libraries are missing.

Debian:
`apt-get install libxml2 libxmlsec1`

Mac OS:
`brew install libxml2 libxmlsec1`

You'll also need to have already deployed the CloudFormation template or configured the metadata protocol and URL for your SAML provider as keys in AWS Systems Manager Parameter Store (`atl_weaver_saml_metadata_protocol` and `atl_weaver_saml_metadata_url`, respectively).

For information about enabling SAML auth, see [Enabling SAML in Atlassian Labs Data Center Weaver](https://community.atlassian.com/t5/Data-Center-articles/Enabling-SAML-in-Atlassian-Labs-Data-Center-Weaver/ba-p/1781426).

### Run

#### From an IDE (recommended)

Running Weaver from within an IDE is preferred, to allow for debugging on breakpoints.

##### IntelliJ

A sample run configuration in IntelliJ has:
* Script path: `/<path-to-repo>/atl-labs-dc-weaver/run.py`
* Environment variables: `PYTHONUNBUFFERED=1`
* Use SDK of module: `<the-name-of-your-virtual-env>`
* Working directory: `/<path-to-repo>/atl-labs-dc-weaver`

This can then be started in debug mode and breakpoints added. Weaver will be available on https://0.0.0.0:8000

Note, testing SSL locally now requires an adhoc certificate due to the SameSite cookie behaviour in most browsers. To enable this for local debugging, set SESSION_COOKIE_SECURE=True in your local _instance/weaver.cfg_ file and the "adhoc" `ssl_context` will be automatically enabled (note that generating the adhoc certificate can take a few seconds when starting Weaver). More info: https://developers.google.com/search/blog/2020/01/get-ready-for-new-samesitenone-secure

##### VS Code

In _.vscode/launch.json_, two methods are available for running Weaver via the built-in debugger:

1. The "Python: Flask" launcher utilizes the debugpy module embedded with VS Code and runs the Flask application directly with the debugger attached automatically
    * You should configure VS Code to point to the Python interpreter in your pipenv virtual environment (VS Code Command Palette &rarr; Python: Select Interpreter &rarr; Select the path matching `pipenv run which python3`)
2. The "Python: Remote Attach" launcher relies on a local debugpy module installed in your virtual environment and manual invocation of the Flask application via _run.py_ (e.g.: `python -m debugpy --listen 5678 run.py`), after which this launcher can be executed to attach/detach the debugger if/when desired

#### Using gunicorn locally (not recommended)

To start the app locally using gunicorn do the following in a python3 environment:

```shell
pip3 install pipenv
pipenv --python 3.11 sync
pipenv run weaver-gunicorn
```

If you're using Mac OS and run into the following errors running gunicorn locally:

```
objc[1031]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called.
objc[1031]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called. We cannot safely call it or ignore it in the fork() child process. Crashing instead. Set a breakpoint on objc_initializeAfterForkError to debug.
```

... you may need to allow multithreading via the following environment variable ([ref][4]):

```shell
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
```

A couple things to note about the use of gunicorn for the deployed application:

1. Weaver's provided systemd unit file includes an `ExecStartPre` directive to install dependencies via `pipenv`
2. Weaver's built-in restart function utilizes [Gunicorn's built-in signal handler][2]

This means that a restart of Weaver via the UI (e.g., after an update) does not include updating dependencies; in the case where an update includes dependency changes, we recommend issuing a restart via systemd after the update.

#### Configuration

Weaver uses 2 config files (_config/config.py_ and the optional [instance-relative][3] _weaver.cfg_), two permissions files (_permissions.yaml_ and/or _permissions.json_), and 3 values in AWS Systems Manager Parameter Store.

##### _config/config.py_

Includes 3 config classes with defaults for configuring which AWS regions are available for stack creation/management, analytics collection, S3 bucket definitions, and more.

##### _weaver.cfg_

Provides a method for overriding any of the config options defined in _config/config.py_; should be placed in the _instance_ directory of your deployed Weaver application. An example is provided in the _instance-examples_ directory.

##### _permissions.yaml_

Used for configuring SAML permissions by AD group, AWS region, CloudFormation stack name, Weaver actions and http request methods; should be placed in the _instance_ directory of your deployed Weaver application. A sample permissions.yaml is provided in the _instance-examples_ directory.

##### _permissions.json_ (deprecated)

Used for configuring SAML permissions by AD group, AWS region, CloudFormation stack name, and Weaver actions; should be placed in the _instance_ directory of your deployed Weaver application. A sample permissions.json is provided in the _instance-examples_ directory.

##### AWS Systems Manager Parameter Store

* **atl_weaver_secret_key**
  The secret key passed to Flask to enable sessions ([more info][1])

* **atl_weaver_saml_metadata_protocol**
  The protocol of the metadata URL for your SAML auth provider

* **atl_weaver_saml_metadata_url**
  The metadata URL for your SAML auth provider

#### Database

Weaver uses a database for storage of server-side session information, action history, and API tokens, and we use Flask-SQLAlchemy and Flask-Migrate to keep database management easy. By default, local development environments will use a local SQLite database; environments deployed using the included CloudFormation template will use a postgres database provisioned via RDS. To get started, prior to running Weaver locally, all that should be required is to create the local database from the migration data:

```shell
$ flask --app "run:app" db upgrade
INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.runtime.migration] Running upgrade  -> a2c3f9f90783, initial
```

This will create the local SQLite database at _instance/weaver.db_. To use a local postgres database instead (e.g. with Docker, which is recommended as the container will use UTC, which is required currently as Flask-Session doesn't currently/natively support databases with timezone awareness), ensure first that you've created a "weaver" role and database, e.g.:

```shell
# start the container
$ docker run -d --name weaverdb -p 5432:5432 -e POSTGRES_HOST_AUTH_METHOD=trust postgres:14 -c log_statement=all
dbae41b637ea07f312e7b1d4b0856d065dd21cfe221e8a07c53efd844d3f5df2

# create the weaver user; password below shown for clarity
$ createuser -P -h localhost -U postgres weaver
Enter password for new role: weaver
Enter it again: weaver

# create the database
$ createdb -h localhost -U postgres -O weaver weaver

# attach to the container, which will print logs from postgres – ctrl+c to kill the DB
$ docker attach weaverdb

# resume the previously stopped container; use attach as above to resume console output
$ docker start weaverdb
```

Then create an instance config file at _instance/weaver-deployment.cfg_ which overrides the value of `SQLALCHEMY_DATABASE_URI` with the connection string for your local database; something like:

```
SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg://weaver:weaver@localhost/weaver'
```

... and then run the `db upgrade` commands above to init the DB with the necessary tables, schemas, etc.

Note: regardless of whether you use a local postgres database or not, you'll need postgres installed locally for requirements purposes; e.g. on Mac OS:

`brew install postgresql@14`

To create new migrations when adding/removing/modifying DB models, use the following command to create a migration script to be commited with your changes:

```shell
$ flask --app "run:app" db migrate -m 'some message here'
```

For example, when adding a field named "somefield" as a string to the `StackAction` model:

```diff
diff --git a/weaver/models.py b/weaver/models.py
index 2b22a56..79052b6 100644
--- a/weaver/models.py
+++ b/weaver/models.py
@@ -39,6 +39,7 @@ class StackAction(db.Model):
     region = db.Column(db.String())
     action = db.Column(db.String())
     user = db.Column(db.String())
+    somefield = db.Column(db.String())
     status = db.Column(db.Enum("IN_PROGRESS", "COMPLETE"))
     result = db.Column(db.Enum("SUCCESS", "FAILURE"))
     log_file = db.Column(db.String())
```

... you'd create the corresponding database migration like so:

```shell
$ flask --app "run:app" db migrate -m 'added somefield to StackAction model'
INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.autogenerate.compare] Detected added column 'stack_action.somefield'
  Generating ~/git/atl-labs-dc-weaver/migrations/versions/fdca4dfca45a_added_somefield_to_stackaction_model.py ...  done
```

... and then apply that migration to your local database:

```shell
$ flask --app "run:app" db upgrade
INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.runtime.migration] Running upgrade a2c3f9f90783 -> fdca4dfca45a, added somefield to StackAction model
```

For deployments of Weaver, the `db upgrade` task is handled automatically by systemd via an `ExecStartPre` directive and the `weaver-db-upgrade` script in _Pipfile_. Note that this does not cover Weaver's built-in functionality to do application restarts via Gunicorn's HUP signal for graceful restarts – in the case where a schema change is included in an application update, we recommend issuing a restart via systemd after the update.

#### Caching

Weaver uses a node-local Redis cache to limit common AWS API calls for a given set of resources within short timespans when Weaver is being heavily used by multiple users. By default, when running locally this cache is disabled; to enable it locally for testing purposes, you'll need a local Redis instance, e.g.:

```shell
$ brew install redis
$ brew services start redis
```

Alternatively, use a local Docker container:

```shell
# start the container; ctrl+c to stop the container
$ docker run --name weavercache -p 6379:6379 redis

# resume the previously stopped container
$ docker start weaverdb

# attach to the container, which will print logs from redis – ctrl+c to stop the container
$ docker attach weaverdb
```

Either way, you'll also need to adjust your local instance config file at _instance/weaver-deployment.cfg_ to enable the use of a Redis-based cache:

```python
CACHE_TYPE = "RedisCache"
CACHE_REDIS_URL = "redis://localhost:6379/0"
```

##### Potential errors
* If you have previously run Weaver with SAML locally, you may encounter the error `sqlalchemy.exc.OperationalError: (sqlite3.OperationalError) table sessions already exists`. If you receive this error, you will need to delete the file `weaver/weaver.db` and/or `instance/weaver.db` and run the command above to recreate it
* If you receive a `AttributeError: 'SQLAlchemy' object has no attribute 'get_or_404'` error when performing stack operations, your local version of Flask-SQLAlchemy is out of date; you will need to upgrade it and/or delete and recreate your python/pip virtualenv. You may also need to delete and recreate `weaver/weaver.db` and/or `instance/weaver.db` as above

[1]: https://bit.ly/2PRfJRk
[2]: https://docs.gunicorn.org/en/stable/signals.html
[3]: https://flask.palletsprojects.com/en/2.2.x/config/#instance-folders
[4]: https://stackoverflow.com/questions/50168647/multiprocessing-causes-python-to-crash-and-gives-an-error-may-have-been-in-progr


# Infrastructure setup

# Code

You're done, go code, check the [CONTRIBUTING](CONTRIBUTING.md) guide for guidelines.
